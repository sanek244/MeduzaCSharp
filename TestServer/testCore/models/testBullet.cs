﻿using MeduzaServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestBullet 
    {
        
        //*** Свойства ***//
        Bullet bullet;
        Gamer gamer;


        //*** Контруктор ***//
        public TestBullet()
        {
            bullet = new Bullet() { DamageArmor = 4 };
            gamer = new Gamer() { Health = 1000, Armor = 5 };
        }
        
        
        //*** Методы ***//
        
        [TestMethod]
        public void Init__Processors_Length()
        {
            Assert.AreEqual(bullet.Processors.Count, 1);
        }

        [TestMethod]
        public void Init__Processors()
        {
            Assert.AreEqual(bullet.Processors[0].ClassName, "BulletProcessor");
        }

        [TestMethod]
        public void GetDamageArmorBeforeClashModel__Ok_0()
        {
            bullet.DamageArmor = 4;
            Assert.AreEqual(bullet.GetDamageArmorBeforeClashModel(gamer), 0);
        }

        [TestMethod]
        public void GetDamageArmorBeforeClashModel__Ok()
        {
            bullet.DamageArmor = 20;
            Assert.AreEqual(bullet.GetDamageArmorBeforeClashModel(gamer), 5);
        }
    }
}
