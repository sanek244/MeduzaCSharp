﻿using System;
using MeduzaServer;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestRay 
    {
        
        //*** Свойства ***//
        Ray rayHorizontal;
        Ray rayVertical;
        Ray rayDiagonal;
        Ray rayVertical2;
        
        
        
        //*** Контруктор ***//
        public TestRay()
        {
            rayHorizontal = new Ray(new Point(0, 0), new Point(100, 0));
                rayVertical = new Ray(new Point(50, 0), new Point(50, 50));
                rayDiagonal = new Ray(new Point(35, 0), new Point(70, 35)); //45%
                rayVertical2 = new Ray(new Point(110, 10), new Point(110, 20)); //45%
        }
        
        
        //*** Методы ***//
        
        [TestMethod]
        public void Init__RayDiagonal()
        {
            Assert.AreEqual(rayDiagonal.StartPoint.X, 35);
            Assert.AreEqual(rayDiagonal.StartPoint.Y, 0);
            Assert.AreEqual(rayDiagonal.SecondPoint.X, 70);
            Assert.AreEqual(rayDiagonal.SecondPoint.Y, 35);
        }

        [TestMethod]
        public void Fx__RayHorizontal_INFINITY_нет_Пересечений()
        {
            Assert.AreEqual(double.IsPositiveInfinity(rayHorizontal.GetX(50)), true);
        }

        [TestMethod]
        public void Fx__RayHorizontal_NaN_точка_На_Всём_Луче()
        {
            Assert.AreEqual(rayHorizontal.GetX(0), double.NaN);
        }

        [TestMethod]
        public void Fx__RayVertical_Ok()
        {
            Assert.AreEqual(rayVertical.GetX(756), 50);
        }

        [TestMethod]
        public void Fx__RayDiagonal_Ok()
        {
            Assert.AreEqual(rayDiagonal.GetX(0), 35);
        }

        [TestMethod]
        public void Fx__RayDiagonal_2_Ok()
        {
            Assert.AreEqual(rayDiagonal.GetX(35), 70);
        }

        [TestMethod]
        public void Fy__RayVertical_INFINITY_нет_Пересечений()
        {
            Assert.AreEqual(rayVertical.GetY(150), double.NegativeInfinity);
        }

        [TestMethod]
        public void Fy__RayVertical_NaN_точка_На_Всём_Луче()
        {
            Assert.AreEqual(rayVertical.GetY(50), double.NaN);
        }

        [TestMethod]
        public void Fy__RayHorizontal_Ok()
        {
            Assert.AreEqual(rayHorizontal.GetY(5), 0);
        }

        [TestMethod]
        public void Fy__RayDiagonal_Ok()
        {
            Assert.AreEqual(rayDiagonal.GetY(35), 0);
        }

        [TestMethod]
        public void Fy__RayDiagonal_2_Ok()
        {
            Assert.AreEqual(rayDiagonal.GetY(70), 35);
        }

        [TestMethod]
        public void Fy__RayDiagonal_3_Ok()
        {
            Assert.AreEqual(rayDiagonal.GetY(0), -35);
        }

        [TestMethod]
        public void GetDistanceToPoint__RayHorizontal_1()
        {
            Assert.AreEqual(rayHorizontal.GetDistanceToPoint(new Point(50, 50)), 50);
        }

        [TestMethod]
        public void GetDistanceToPoint__RayHorizontal_2()
        {
            Assert.AreEqual(rayHorizontal.GetDistanceToPoint(new Point(50, 0)), 0);
        }

        [TestMethod]
        public void GetDistanceToPoint__RayVertical()
        {
            Assert.AreEqual(rayVertical.GetDistanceToPoint(new Point(50, 50)), 0);
        }

        [TestMethod]
        public void GetDistanceToPoint__RayDiagonal()
        {
            Assert.AreEqual(rayDiagonal.GetDistanceToPoint(new Point(0, 0)), Math.Sqrt(612.5));
        }

        [TestMethod]
        public void GetDistanceToPoint__RayDiagonal_2()
        {
            Assert.AreEqual(rayDiagonal.GetDistanceToPoint(new Point(70, -35)), Math.Sqrt(Math.Pow(35, 2) + Math.Pow(35, 2)));
        }

        [TestMethod]
        public void GetPointIntersectionRay__Horizontal_Vertical()
        {
            var point = rayHorizontal.GetPointIntersectionRay(rayVertical);
            Assert.AreEqual(point.X, 50);
            Assert.AreEqual(point.Y, 0);
        }

        [TestMethod]
        public void GetPointIntersectionRay__Horizontal_Diagonal()
        {
            var point = rayHorizontal.GetPointIntersectionRay(rayDiagonal);
            Assert.AreEqual(point.X, 35);
            Assert.AreEqual(point.Y, 0);
        }

        [TestMethod]
        public void GetPointIntersectionRay__Horizontal_Vertical_First_Line_Segment()
        {
            Assert.AreEqual(rayHorizontal.GetPointIntersectionRay(rayVertical2, true), null);
        }

        [TestMethod]
        public void GetPointIntersectionRay__Horizontal_Vertical_Second_Line_Segment()
        {
            Assert.AreEqual(rayHorizontal.GetPointIntersectionRay(rayVertical2, false, true), null);
        }

        [TestMethod]
        public void GetPointIntersectionRay__Horizontal_Vertical_2()
        {
            Assert.AreEqual(rayHorizontal.GetPointIntersectionRay(rayVertical2).X, 110);
        }
    }
}
