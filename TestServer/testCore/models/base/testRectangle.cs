﻿using MeduzaServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestRectangle 
    {

        //*** Свойства ***//
        int x, y, width, height;
        RectangleRay rectangle;


        //*** Контруктор ***//
        public TestRectangle()
        {
            x = 234;
            y = 8546;
            width = 355;
            height = 957;
            rectangle = new RectangleRay(new Point(x, y), new Size(width, height), 0);
        }


        //*** Методы ***//

        [TestMethod]
        public void Set_Get__Set_Location()
        {
            rectangle.Location = new Point(x, y);

            Assert.AreEqual(rectangle.TopRay.StartPoint.X, x);
            Assert.AreEqual(rectangle.TopRay.StartPoint.Y, y);
            Assert.AreEqual(rectangle.TopRay.SecondPoint.X, x + width);
            Assert.AreEqual(rectangle.TopRay.SecondPoint.Y, y);

            Assert.AreEqual(rectangle.RightRay.StartPoint.X, x + width);
            Assert.AreEqual(rectangle.RightRay.StartPoint.Y, y);
            Assert.AreEqual(rectangle.RightRay.SecondPoint.X, x + width);
            Assert.AreEqual(rectangle.RightRay.SecondPoint.Y, y + height);

            Assert.AreEqual(rectangle.BottomRay.StartPoint.X, x + width);
            Assert.AreEqual(rectangle.BottomRay.StartPoint.Y, y + height);
            Assert.AreEqual(rectangle.BottomRay.SecondPoint.X, x);
            Assert.AreEqual(rectangle.BottomRay.SecondPoint.Y, y + height);

            Assert.AreEqual(rectangle.LeftRay.StartPoint.X, x);
            Assert.AreEqual(rectangle.LeftRay.StartPoint.Y, y + height);
            Assert.AreEqual(rectangle.LeftRay.SecondPoint.X, x);
            Assert.AreEqual(rectangle.LeftRay.SecondPoint.Y, y);
        }

        [TestMethod]
        public void Set_Get__Set_Size()
        {
            rectangle.Size = new Size(width, height);

            Assert.AreEqual(rectangle.TopRay.StartPoint.X, x);
            Assert.AreEqual(rectangle.TopRay.StartPoint.Y, y);
            Assert.AreEqual(rectangle.TopRay.SecondPoint.X, x + width);
            Assert.AreEqual(rectangle.TopRay.SecondPoint.Y, y);

            Assert.AreEqual(rectangle.RightRay.StartPoint.X, x + width);
            Assert.AreEqual(rectangle.RightRay.StartPoint.Y, y);
            Assert.AreEqual(rectangle.RightRay.SecondPoint.X, x + width);
            Assert.AreEqual(rectangle.RightRay.SecondPoint.Y, y + height);

            Assert.AreEqual(rectangle.BottomRay.StartPoint.X, x + width);
            Assert.AreEqual(rectangle.BottomRay.StartPoint.Y, y + height);
            Assert.AreEqual(rectangle.BottomRay.SecondPoint.X, x);
            Assert.AreEqual(rectangle.BottomRay.SecondPoint.Y, y + height);

            Assert.AreEqual(rectangle.LeftRay.StartPoint.X, x);
            Assert.AreEqual(rectangle.LeftRay.StartPoint.Y, y + height);
            Assert.AreEqual(rectangle.LeftRay.SecondPoint.X, x);
            Assert.AreEqual(rectangle.LeftRay.SecondPoint.Y, y);
        }

        [TestMethod]
        public void Set_Get__Set_Location_X()
        {
            rectangle.Location.X = x;

            Assert.AreEqual(rectangle.TopRay.StartPoint.X, x);
            Assert.AreEqual(rectangle.TopRay.StartPoint.Y, y);
            Assert.AreEqual(rectangle.TopRay.SecondPoint.X, x + width);
            Assert.AreEqual(rectangle.TopRay.SecondPoint.Y, y);

            Assert.AreEqual(rectangle.RightRay.StartPoint.X, x + width);
            Assert.AreEqual(rectangle.RightRay.StartPoint.Y, y);
            Assert.AreEqual(rectangle.RightRay.SecondPoint.X, x + width);
            Assert.AreEqual(rectangle.RightRay.SecondPoint.Y, y + height);

            Assert.AreEqual(rectangle.BottomRay.StartPoint.X, x + width);
            Assert.AreEqual(rectangle.BottomRay.StartPoint.Y, y + height);
            Assert.AreEqual(rectangle.BottomRay.SecondPoint.X, x);
            Assert.AreEqual(rectangle.BottomRay.SecondPoint.Y, y + height);

            Assert.AreEqual(rectangle.LeftRay.StartPoint.X, x);
            Assert.AreEqual(rectangle.LeftRay.StartPoint.Y, y + height);
            Assert.AreEqual(rectangle.LeftRay.SecondPoint.X, x);
            Assert.AreEqual(rectangle.LeftRay.SecondPoint.Y, y);
        }


        [TestMethod]
        public void Set_Get__Set_RotateAngle_0()
        {
            rectangle.RotateAngle = 0;

            Assert.AreEqual(rectangle.TopRay.StartPoint.X, x);
            Assert.AreEqual(rectangle.TopRay.StartPoint.Y, y);
            Assert.AreEqual(rectangle.TopRay.SecondPoint.X, x + width);
            Assert.AreEqual(rectangle.TopRay.SecondPoint.Y, y);

            Assert.AreEqual(rectangle.RightRay.StartPoint.X, x + width);
            Assert.AreEqual(rectangle.RightRay.StartPoint.Y, y);
            Assert.AreEqual(rectangle.RightRay.SecondPoint.X, x + width);
            Assert.AreEqual(rectangle.RightRay.SecondPoint.Y, y + height);

            Assert.AreEqual(rectangle.BottomRay.StartPoint.X, x + width);
            Assert.AreEqual(rectangle.BottomRay.StartPoint.Y, y + height);
            Assert.AreEqual(rectangle.BottomRay.SecondPoint.X, x);
            Assert.AreEqual(rectangle.BottomRay.SecondPoint.Y, y + height);

            Assert.AreEqual(rectangle.LeftRay.StartPoint.X, x);
            Assert.AreEqual(rectangle.LeftRay.StartPoint.Y, y + height);
            Assert.AreEqual(rectangle.LeftRay.SecondPoint.X, x);
            Assert.AreEqual(rectangle.LeftRay.SecondPoint.Y, y);
        }

        [TestMethod]
        public void Set_Get__Set_RotateAngle_90()
        {
            rectangle.RotateAngle = 90;

            Assert.AreEqual(rectangle.TopRay.StartPoint.X, x);
            Assert.AreEqual(rectangle.TopRay.StartPoint.Y, y);
            Assert.AreEqual(rectangle.TopRay.SecondPoint.X, x);
            Assert.AreEqual(rectangle.TopRay.SecondPoint.Y, y + width);

            Assert.AreEqual(rectangle.RightRay.StartPoint.X, x);
            Assert.AreEqual(rectangle.RightRay.StartPoint.Y, y + width);
            Assert.AreEqual(rectangle.RightRay.SecondPoint.X, x - height);
            Assert.AreEqual(rectangle.RightRay.SecondPoint.Y, y + width);

            Assert.AreEqual(rectangle.BottomRay.StartPoint.X, x - height);
            Assert.AreEqual(rectangle.BottomRay.StartPoint.Y, y + width);
            Assert.AreEqual(rectangle.BottomRay.SecondPoint.X, x - height);
            Assert.AreEqual(rectangle.BottomRay.SecondPoint.Y, y);

            Assert.AreEqual(rectangle.LeftRay.StartPoint.X, x - height);
            Assert.AreEqual(rectangle.LeftRay.StartPoint.Y, y);
            Assert.AreEqual(rectangle.LeftRay.SecondPoint.X, x);
            Assert.AreEqual(rectangle.LeftRay.SecondPoint.Y, y);
        }
        [TestMethod]
        public void Init__Init_Default()
        {
            Rectangle rectangle = new Rectangle() {
                Location = new Point(x, y),
                Size = new Size(width, height),
                RotateAngle = 45.324f
            };
            Assert.AreEqual(rectangle.Location.X, 234);
            Assert.AreEqual(rectangle.Location.Y, 8546);
            Assert.AreEqual(rectangle.Size.Width, 355);
            Assert.AreEqual(rectangle.Size.Height, 957);
            Assert.AreEqual(rectangle.RotateAngle, 45.324);
        }

        [TestMethod]
        public void Init__Point__Size__Number()
        {
            Rectangle rectangle2 = new Rectangle(new Point(234, 8546), new Size(355, 957), 45.324f);
            Assert.AreEqual(rectangle2.Location.X, 234);
            Assert.AreEqual(rectangle2.Location.Y, 8546);
            Assert.AreEqual(rectangle2.Size.Width, 355);
            Assert.AreEqual(rectangle2.Size.Height, 957);
            Assert.AreEqual(rectangle2.RotateAngle, 45.324);
        }

        [TestMethod]
        public void Init__SimpleGameObject()
        {
            SimpleGameObject simpleGameObject = new SimpleGameObject() { Rectangle = rectangle };
            Rectangle rectangle3 = new Rectangle(simpleGameObject);
            Assert.AreEqual(rectangle3.Location.X, rectangle.Location.X);
            Assert.AreEqual(rectangle3.Location.Y, rectangle.Location.Y);
            Assert.AreEqual(rectangle3.Size.Width, rectangle.Size.Width);
            Assert.AreEqual(rectangle3.Size.Height, rectangle.Size.Height);
            Assert.AreEqual(rectangle3.RotateAngle, rectangle.RotateAngle);
        }

        [TestMethod]
        public void IsRightAngle__0()
        {
            rectangle.RotateAngle = 0;
            Assert.AreEqual(rectangle.IsRightAngle(), true);
        }

        [TestMethod]
        public void IsRightAngle__0_23()
        {
            rectangle.RotateAngle = 0.23f;
            Assert.AreEqual(rectangle.IsRightAngle(), false);
        }

        [TestMethod]
        public void IsRightAngle__90()
        {
            rectangle.RotateAngle = 90;
            Assert.AreEqual(rectangle.IsRightAngle(), true);
        }

        [TestMethod]
        public void IsRightAngle__1()
        {
            rectangle.RotateAngle = 1;
            Assert.AreEqual(rectangle.IsRightAngle(), false);
        }

        [TestMethod]
        public void IsRightAngle__180()
        {
            rectangle.RotateAngle = 180;
            Assert.AreEqual(rectangle.IsRightAngle(), true);
        }

        [TestMethod]
        public void IsRightAngle__53()
        {
            rectangle.RotateAngle = 53;
            Assert.AreEqual(rectangle.IsRightAngle(), false);
        }

        [TestMethod]
        public void IsRightAngle__270()
        {
            rectangle.RotateAngle = 270;
            Assert.AreEqual(rectangle.IsRightAngle(), true);
        }

        [TestMethod]
        public void IsRightAngle__123()
        {
            rectangle.RotateAngle = 123;
            Assert.AreEqual(rectangle.IsRightAngle(), false);
        }

        [TestMethod]
        public void IsRightAngle__360()
        {
            rectangle.RotateAngle = 360;
            Assert.AreEqual(rectangle.IsRightAngle(), true);
        }

        [TestMethod]
        public void IsRightAngle__280()
        {
            rectangle.RotateAngle = 280;
            Assert.AreEqual(rectangle.IsRightAngle(), false);
        }

        [TestMethod]
        public void IsContainsPoint__RectangleSimple_Inside()
        {
            Assert.AreEqual(rectangle.IsContainsPoint(new Point(300, 8600)), true);
        }

        [TestMethod]
        public void IsContainsPoint__RectangleSimple_Outside()
        {
            Assert.AreEqual(rectangle.IsContainsPoint(new Point(1000, 10000)), false);
        }

        [TestMethod]
        public void IsContainsPoint__RectangleSimple_Outside_2()
        {
            Assert.AreEqual(rectangle.IsContainsPoint(new Point(100, 100)), false);
        }

        [TestMethod]
        public void IsContainsModel__Inside()
        {
            SimpleGameObject model = new SimpleGameObject() { Rectangle = new Rectangle(new Point(300, 9000), new Size(10, 10)) };
            Assert.AreEqual(rectangle.IsContainsModel(model), true);
        }

        [TestMethod]
        public void IsContainsModel__Outside()
        {
            SimpleGameObject model = new SimpleGameObject() { Rectangle = new Rectangle(new Point(200, 9000), new Size(10, 10)) };
            Assert.AreEqual(rectangle.IsContainsModel(model), false);
        }

        [TestMethod]
        public void IsContainsModel__Outside_2()
        {
            SimpleGameObject model = new SimpleGameObject() { Rectangle = new Rectangle(new Point(300, 8546 + 957 - 5), new Size(10, 10)) };
            Assert.AreEqual(rectangle.IsContainsModel(model), false);
        }

        [TestMethod]
        public void IsContainsModel__Outside_3()
        {
            SimpleGameObject model = new SimpleGameObject() { Rectangle = new Rectangle(new Point(300, 8546 + 957 - 10), new Size(10, 10)) };
            Assert.AreEqual(rectangle.IsContainsModel(model), false);
        }

        [TestMethod]
        public void IsContainsModel__Inside_2()
        {
            SimpleGameObject model = new SimpleGameObject() { Rectangle = new Rectangle(new Point(300, 8546 + 957 - 11), new Size(10, 10)) };
            Assert.AreEqual(rectangle.IsContainsModel(model), true);
        }

        [TestMethod]
        public void IsIntersectionOrContainsModel__Inside()
        {
            SimpleGameObject model = new SimpleGameObject() { Rectangle = new Rectangle(new Point(300, 9000), new Size(10, 10)) };
            Assert.AreEqual(rectangle.IsIntersectionOrContainsModel(model), true);
        }

        [TestMethod]
        public void IsIntersectionOrContainsModel__Ok()
        {
            //rectangle = new Rectangle(new Point(x, y), new Size(width, height), 45.324);
            SimpleGameObject model = new SimpleGameObject() { Rectangle = new Rectangle(new Point(300, 8546 + 957 - 5), new Size(10, 10)) };
            Assert.AreEqual(rectangle.IsIntersectionOrContainsModel(model), true);
        }

        [TestMethod]
        public void IsIntersectionOrContainsModel__Ok_2()
        {
            SimpleGameObject model = new SimpleGameObject() { Rectangle = new Rectangle(new Point(300, 8546 + 957 - 10), new Size(10, 10)) };
            Assert.AreEqual(rectangle.IsIntersectionOrContainsModel(model), true);
        }

        [TestMethod]
        public void IsIntersectionOrContainsModel__Outside()
        {
            SimpleGameObject model = new SimpleGameObject() { Rectangle = new Rectangle(new Point(300, 8546 + 957 + 11), new Size(10, 10)) };
            Assert.AreEqual(rectangle.IsIntersectionOrContainsModel(model), false);
        }
    }
}
