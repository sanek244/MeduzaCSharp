﻿using MeduzaServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestPoint 
    {
        [TestMethod]
        public void Addition()
        {
            Point pointA = new Point(7457, 548),
                pointB = new Point(234, 85);

            Point pointC = pointA + pointB;

            Assert.AreEqual(pointC.X, 7457 + 234);
            Assert.AreEqual(pointC.Y, 548 + 85);
        }

        [TestMethod]
        public void CopyPoint()
        {
            Point pointA = new Point(7457, 548);
            Point pointB = new Point(pointA);
            pointA.X = 32;

            Assert.AreEqual(pointB.X, 7457);
        }

        [TestMethod]
        public void Callback()
        {
            float x = 3;
            Point pointA = new Point(() => { x = 25; });
            pointA.X = 32;

            Assert.AreEqual(x, 25);
        }
    }
}
