﻿using MeduzaServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace TestServer
{
    [TestClass]
    public class TestMap 
    {
        
        //*** Свойства ***//
        Map map;
        
        
        //*** Контруктор ***//
        public TestMap()
        {
            map = new Map() { Name = "Test" };
        }
        
        
        //*** Методы ***//
        
        [TestMethod]
        public void GetDefaultObjects__Length()
        {
            Assert.AreEqual(map.GetDefaultObjects().Count, 2);
        }

        [TestMethod]
        public void GetDefaultObjects__Objects()
        {
            var first = JsonConvert.DeserializeObject<SimpleGameObject>(map.GetDefaultObjects()[0].ToString());
            var second = JsonConvert.DeserializeObject<SimpleGameObject>(map.GetDefaultObjects()[1].ToString());
            Assert.AreEqual(first.Rectangle.Location.X, 300);
            Assert.AreEqual(first.Rectangle.Location.Y, 233);
            Assert.AreEqual(first.Name, "block");
        }
    }
}
