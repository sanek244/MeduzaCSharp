﻿using MeduzaServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestGameObject 
    {
        
        //*** Свойства ***//
        Building gameObject;
        TestEffect testEffect;
        PhysicsMoveAbility testAbility;

        int x, y, width, height;

        //*** Контруктор ***//
        public TestGameObject()
        {
            gameObject = new Building();
            testEffect = new TestEffect(gameObject);
            testAbility = new PhysicsMoveAbility();


            x = 70;
            y = 50;
            width = 15;
            height = 20;
        }
        
        
        //*** Методы ***//
        
        [TestMethod]
        public void Set__HealthStart_Null()
        {
            gameObject.HealthStart = -1;
            gameObject.Health = 1000;

            Assert.AreEqual(gameObject.HealthStart, 1000);
        }

        [TestMethod]
        public void Set__HealthStart_Non_Set()
        {
            gameObject.HealthStart = -1;
            gameObject.Health = 1000;
            gameObject.Health = 568568;

            Assert.AreEqual(gameObject.HealthStart, 1000);
        }

        [TestMethod]
        public void AddEffect__Fields()
        {
            gameObject.AddEffect(testEffect);
            Assert.AreEqual(gameObject.Effects[testEffect.ClassName].TimeLife, 3000);
            Assert.AreEqual(gameObject.Effects[testEffect.ClassName].ModifiableAttributes["damage"], 15);
            Assert.AreEqual(gameObject.Effects[testEffect.ClassName].ModifiableAttributes["health"], -50);
        }

        [TestMethod]
        public void AddEffect__Merge()
        {
            gameObject.Effects = new System.Collections.Generic.Dictionary<string, BaseEffect>();
            gameObject.AddEffect(testEffect);
            gameObject.AddEffect(testEffect);
            Assert.AreEqual(testEffect.TimeLife, 3000);
            Assert.AreEqual(testEffect.ModifiableAttributes["damage"], 30);
            Assert.AreEqual(testEffect.ModifiableAttributes["health"], -100);
        }
        [TestMethod]
        public void Get__Effects()
        {
            gameObject.Effects = new System.Collections.Generic.Dictionary<string, BaseEffect>();
            gameObject.AddEffect(testEffect);
            gameObject.Damage = 10;
            gameObject.Health = 20;
            Assert.AreEqual(gameObject.Damage, 10 + (int)testEffect.ModifiableAttributes["damage"]);
            Assert.AreEqual(gameObject.Health, 20 + (int)testEffect.ModifiableAttributes["health"]);
        }

        [TestMethod]
        public void GetPointsIntersectionRays__Null_Left()
        {
            x = 70;
            y = 50;
            width = 15;
            height = 20;
            gameObject.Rectangle = new RectangleRay(new Point(x, y), new Size(width, height));

            Assert.AreEqual(gameObject.GetPointsIntersectionRays(new Ray(new Point(50, 0), new Point(50, 200))).Count, 0);
        }

        [TestMethod]
        public void GetPointsIntersectionRays__Null_Top()
        {
            Assert.AreEqual(gameObject.GetPointsIntersectionRays(new Ray(new Point(80, 0), new Point(80, 40)), true).Count, 0);
        }

        [TestMethod]
        public void GetPointsIntersectionRays__Top_Length()
        {
            Assert.AreEqual(gameObject.GetPointsIntersectionRays(new Ray(new Point(80, 0), new Point(80, 40))).Count, 2);
        }

        [TestMethod]
        public void GetPointsIntersectionRays__Top()
        {
            var contacts = gameObject.GetPointsIntersectionRays(new Ray(new Point(80, 0), new Point(80, 40)));
            Assert.AreEqual(contacts[0].X, 80);
            Assert.AreEqual(contacts[0].Y, 50);
            Assert.AreEqual(contacts[1].X, 80);
            Assert.AreEqual(contacts[1].Y, 70);
        }

        [TestMethod]
        public void GetPointsIntersectionRays__Top_Length_2()
        {
            Assert.AreEqual(gameObject.GetPointsIntersectionRays(new Ray(new Point(80, 0), new Point(80, 55)), true).Count, 1);
        }

        [TestMethod]
        public void GetBesidePointIntersectionRays__Null_Left()
        {
            gameObject.Rectangle = new RectangleRay(new Point(x, y), new Size(width, height));
            Assert.AreEqual(gameObject.GetBesidePointIntersectionRays(new Ray(new Point(50, 0), new Point(50, 200))), null);
        }

        [TestMethod]
        public void GetBesidePointIntersectionRays__Null_Top()
        {
            gameObject.Rectangle = new RectangleRay(new Point(x, y), new Size(width, height));
            Assert.AreEqual(gameObject.GetBesidePointIntersectionRays(new Ray(new Point(80, 0), new Point(80, 40)), true), null);
        }

        [TestMethod]
        public void GetBesidePointIntersectionRays__Top()
        {
            gameObject.Rectangle = new RectangleRay(new Point(x, y), new Size(width, height));
            var contact = gameObject.GetBesidePointIntersectionRays(new Ray(new Point(80, 0), new Point(80, 40)));
            Assert.AreEqual(contact.X, 80);
            Assert.AreEqual(contact.Y, 50);
        }

        [TestMethod]
        public void AddAbility__Ok()
        {
            gameObject.Abilities.Add(13, testAbility);
            Assert.AreEqual(gameObject.Abilities.ContainsKey(13), true);
            Assert.AreEqual(gameObject.Abilities[13].ClassName, testAbility.ClassName);
        }
    }
}