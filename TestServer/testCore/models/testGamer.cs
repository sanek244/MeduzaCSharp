﻿using MeduzaServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestGamer 
    {
        
        //*** Свойства ***//
        Gamer gamer;
        
        
        //*** Контруктор ***//
        public TestGamer()
        {
            gamer = new Gamer();
        }
        
        
        //*** Методы ***//
        
        [TestMethod]
        public void Set__AimPoint()
        {
            gamer.Rectangle.Location = new Point(100, 200);
            gamer.AimPoint = new Point(100, 300);
            Assert.AreEqual(gamer.Rectangle.RotateAngle, 90);
        }

        [TestMethod]
        public void Set__AimPoint_2()
        {
            gamer.Rectangle.Location = new Point(100, 200);
            gamer.AimPoint = new Point(50, 200);
            Assert.AreEqual(gamer.Rectangle.RotateAngle, 180);
        }

        [TestMethod]
        public void Set__AimPoint_3()
        {
            gamer.Rectangle.Location = new Point(100, 200);
            gamer.AimPoint = new Point(100, 100);
            Assert.AreEqual(gamer.Rectangle.RotateAngle, 270);
        }

        [TestMethod]
        public void Set__AimPoint_4()
        {
            gamer.Rectangle.Location = new Point(100, 200);
            gamer.AimPoint = new Point(200, 200);
            Assert.AreEqual(gamer.Rectangle.RotateAngle, 0);
        }

        [TestMethod]
        public void Set__AimPoint_5()
        {
            gamer.Rectangle.Location = new Point(100, 200);
            gamer.AimPoint = new Point(150, 250);
            Assert.AreEqual(gamer.Rectangle.RotateAngle, 45);
        }

        [TestMethod]
        public void Set__Bottom_Path_Body()
        {
            gamer.LoadPersonage(0);
            gamer.Rectangle.Location = new Point(100, 200);
            gamer.Rectangle.RotateAngle = 0;
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.Location.X, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.Location.Y, 200);
        }
        //TODO = написать тест на части тела с поворотом

        [TestMethod]
        public void ToggleKey__On()
        {
            gamer.ToggleKey(32, true);
            Assert.AreEqual(gamer.KeysMap.Contains(32), true);
        }

        [TestMethod]
        public void ToggleKey__Off()
        {
            gamer.ToggleKey(32, true);
            Assert.AreEqual(gamer.KeysMap.Contains(32), true);
            gamer.ToggleKey(32, false);
            Assert.AreEqual(gamer.KeysMap.Contains(32), false);
        }

        [TestMethod]
        public void LoadPersonage__Ok()
        {
            var personage = Application.DBController.FindOne<Personage>(0);
            gamer.LoadPersonage(personage.Id);
            Assert.AreEqual(gamer.Race, personage.Race);
            Assert.AreEqual(gamer.ViewType, personage.ViewType);
        }
    }
}
