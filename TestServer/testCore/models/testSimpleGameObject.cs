﻿using System;
using MeduzaServer;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestSimpleGameObject 
    {
        
        //*** Свойства ***//
        SimpleGameObject simpleGameObject;
        
        
        //*** Контруктор ***//
        public TestSimpleGameObject()
        {
            simpleGameObject = new SimpleGameObject() {
                Rectangle = new Rectangle(new Point(300, 70), new Size(20, 30)),
            };
        }
        
        
        //*** Методы ***//

        [TestMethod]
        public void ToJSONinStringForClient()
        {
            //Todo
        }

        [TestMethod]
        public void IsChangedSendAttributes__Ok()
        {
            Assert.AreEqual(simpleGameObject.IsChangedSendAttributes(), true);
        }

        [TestMethod]
        public void IsChangedSendAttributes__No()
        {
            simpleGameObject.СhangesAttributes = new List<string>();
            Assert.AreEqual(simpleGameObject.IsChangedSendAttributes(), false);
        }
    }
}
