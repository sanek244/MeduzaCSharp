﻿using System;
using MeduzaServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestHelper
    {
        int rotateAngle = 180,
            max = 20,
            forceGamer = 5,
            lengthToPoint = 125;
        Point anglePoint = new Point(0, 0);
        Point center = new Point() { X = 100, Y = 100 };
        uint timeLast = 0;
        Gamer model = new Gamer() { Rectangle = new RectangleRay(), AimPoint = new Point(90, 90) };

        bool maxGet = false;

        Weapon weapon = new Weapon() { ScatterMax = 0, Rectangle = new Rectangle() { RotateAngle = 180 }, ScatterType = TypeScatter.Equable };
        Gamer gamer = new Gamer() { Force = 5 };

        [TestMethod]
        public void GetRandom__Рандом_В_0()
        {
            Assert.AreEqual(Helper.GetRandom(0, 0), 0);
        }

        [TestMethod]
        public void GetRandom__Рандом_В_5()
        {
            Assert.AreEqual(Helper.GetRandom(5, 5), 5);
        }

        [TestMethod]
        public void GetRandom__Рандом_От_4_До_5_Включительно()
        {
            var random = Helper.GetRandom(4, 5);
            if(random < 4 || random > 5){
                Assert.IsTrue(false);
            }
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void GetRandomInt__Рандом_В_0()
        {
            Assert.AreEqual(Helper.rnd.Next(0, 0), 0);
        }

        [TestMethod]
        public void GetRandomInt__Рандом_В_5()
        {
            Assert.AreEqual(Helper.rnd.Next(5, 5), 5);
        }

        [TestMethod]
        public void GetRandomInt__Рандом_От_4_До_5_Включительно()
        {
            var random = Helper.rnd.Next(4, 5);
            if(random < 4 || random > 5){
                Assert.IsTrue(false);
            }
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void GetScatterIncreasing__Получение_Угла_В_180_Градусов_сила_Игрока_1()
        {
            Assert.AreEqual(Helper.GetScatterIncreasing(rotateAngle, max, timeLast, 1, maxGet), 180);
        }

        [TestMethod]
        public void GetScatterIncreasing__Получение_Угла_В_180_Градусов()
        {
            Assert.AreEqual(Helper.GetScatterIncreasing(rotateAngle, max, timeLast, forceGamer, maxGet), 180);
        }

        [TestMethod]
        public void GetScatterIncreasing__Получение_Угла_В_200_Градусов_сила_Игрока_1_MaxGet_True()
        {
            Assert.AreEqual(Helper.GetScatterIncreasing(rotateAngle, max, Helper.GetUnixTime(), 1, true), 200);
        }

        [TestMethod]
        public void GetScatterIncreasing__Получение_Угла_В_190_Градусов_сила_Игрока_2_MaxGet_True()
        {
            Assert.AreEqual(Helper.GetScatterIncreasing(rotateAngle, max, Helper.GetUnixTime(), 2, true), 190);
        }

        [TestMethod]
        public void GetScatterIncreasing__Получение_Угла_В_182_Градусов_maxGet_True()
        {
            Assert.AreEqual(Helper.GetScatterIncreasing(rotateAngle, max, Helper.GetUnixTime(), forceGamer, true), 184);
        }

        [TestMethod]
        public void GetScatterIncreasing__Получение_Угла_В_180_Градусов_max_0()
        {
            Assert.AreEqual(Helper.GetScatterIncreasing(rotateAngle, 0, Helper.GetUnixTime(), forceGamer, maxGet), 180);
        }

        [TestMethod]
        public void GetScatterUniform__Получение_Угла_В_180_Градусов()
        {
            rotateAngle = 180;
            max = 20;
            forceGamer = 5;
            Assert.AreEqual(Helper.GetScatterUniform(rotateAngle, 0, forceGamer), 180);
        }

        [TestMethod]
        public void GetScatterUniform__Получение_Угла_Около_180_Градусов__20_сила_Игрока_1()
        {
            rotateAngle = 180;
            max = 20;
            forceGamer = 5;

            for(var i = 0; i < 10000; i++){
                var angle = Helper.GetScatterUniform(rotateAngle, max, 1);
                if(angle < rotateAngle - max || angle > rotateAngle + max){
                    Assert.Fail();
                }
            }
        }

        [TestMethod]
        public void GetScatterUniform__Получение_Угла_Около_180_Градусов__2()
        {
            rotateAngle = 180;
            max = 20;
            forceGamer = 5;

            for (var i = 0; i < 10000; i++){
                var angle = Helper.GetScatterUniform(rotateAngle, max, forceGamer);
                if(angle < rotateAngle - max / forceGamer || angle > rotateAngle + max / forceGamer) {
                    Assert.Fail();
                }
            }
        }

        [TestMethod]
        public void GetScatterWeapon__Получение_Угла_В_180_Градусов_равномерный_Разброс_И_Max_0()
        {
            Assert.AreEqual(Helper.GetScatterWeapon(weapon, gamer), 180);
        }

        [TestMethod]
        public void GetScatterWeapon__Получение_Угла_В_180_Градусов_нарастающий_Разброс_И_Max_0()
        {
            weapon.ScatterType = TypeScatter.Increasing;
            Assert.AreEqual(Helper.GetScatterWeapon(weapon, gamer), 180);
        }

        [TestMethod]
        public void GetScatterWeapon__Получение_Угла_В_180_Градусов_равномерный_Разброс_И_Max_20()
        {
            weapon.ScatterMax = 20;
            weapon.ScatterType = TypeScatter.Equable;

            for (var i = 0; i < 1000; i++) 
            {
                var angle = Helper.GetScatterWeapon(weapon, gamer);
                if (angle < weapon.Rectangle.RotateAngle - weapon.ScatterMax / gamer.Force
                    || angle > weapon.Rectangle.RotateAngle + weapon.ScatterMax / gamer.Force)
                {
                    Assert.Fail();
                }
            }
        }


        [TestMethod]
        public void Distance__Расстояние_По_Вертикали_В_100()
        {
            Assert.AreEqual(Helper.Distance(new Point(0, 0), new Point(0, 100)), 100);
        }

        [TestMethod]
        public void Distance__Расстояние_По_Горизонтали_В_100()
        {
            Assert.AreEqual(Helper.Distance(new Point(0, 0), new Point(100, 0)), 100);
        }

        [TestMethod]
        public void Distance__Расстояние_По_Диагонали_Квадрата_Со_Стороной_В_100()
        {
            Assert.AreEqual(Helper.Distance(new Point(0, 0), new Point(100, 100)), Math.Sqrt(Math.Pow(100, 2) + Math.Pow(100, 2)));
        }

        [TestMethod]
        public void Distance__Расстояние_По_Диагонали_прямоугольника_Со_сторонами_В_100_и_20()
        {
            Assert.AreEqual(Helper.Distance(new Point(0, 0), new Point(100, 20)), Math.Sqrt(Math.Pow(100, 2) + Math.Pow(20, 2)));
        }


        [TestMethod]
        public void GetAngleRightTriangle__Расстояние_По_горизонтали_В_125()
        {
            rotateAngle = 0;
            var pointRes = Helper.GetAngleRightTriangle(anglePoint, rotateAngle, lengthToPoint);
            Assert.AreEqual(pointRes.Y, 0);
            Assert.AreEqual(pointRes.X, 125);
        }

        [TestMethod]
        public void GetAngleRightTriangle__Расстояние_По_Вертикали_В_125()
        {
            rotateAngle = 0;
            var pointRes = Helper.GetAngleRightTriangle(anglePoint, 90, lengthToPoint);
            Assert.AreEqual(pointRes.Y, 125);
            Assert.AreEqual(Math.Floor(pointRes.X), 0);
        }

        [TestMethod]
        public void GetAngleRightTriangle__Расстояние_По_Вертикали_В_125__90градусов()
        {
            rotateAngle = 0;
            var pointRes = Helper.GetAngleRightTriangle(anglePoint, 180, lengthToPoint);
            Assert.AreEqual(Math.Floor(pointRes.Y), 0);
            Assert.AreEqual(pointRes.X, -125);
        }

        [TestMethod]
        public void GetAngleRightTriangle__Расстояние_По_Диагонали_В_125()
        {
            rotateAngle = 0;
            var pointRes = Helper.GetAngleRightTriangle(anglePoint, 45, lengthToPoint);
            float answerGood = (float)(lengthToPoint * Math.Sin(45 * Math.PI / 180));

            Assert.AreEqual(pointRes.Y, answerGood);
            Assert.AreEqual(pointRes.X, answerGood);
        }

        [TestMethod]
        public void GetAngleRightTriangle__Расстояние_По_Горизонтали_В_125__90градусов()
        {
            rotateAngle = 0;
            var pointRes = Helper.GetAngleRightTriangle(anglePoint, -90, lengthToPoint);
            Assert.AreEqual(pointRes.Y, -125);
            Assert.AreEqual(Math.Floor(pointRes.X), 0);
        }

        [TestMethod]
        public void GetRotateAngle__Получение_Угла_В_90_Градусов()
        {
            Assert.AreEqual(Helper.GetRotateAngle(center, new Point() {X = 100, Y = 145}), 90);
        }

        [TestMethod]
        public void GetRotateAngle__Получение_Угла_В_270_Градусов()
        {
            Assert.AreEqual(Helper.GetRotateAngle(center, new Point() {X = 100, Y = 50}), 270);
        }


        [TestMethod]
        public void GetRotateAngle__Получение_Угла_В_0_Градусов()
        {
            Assert.AreEqual(Helper.GetRotateAngle(center, new Point() {X = 100, Y = 100}), 0);
        }

        [TestMethod]
        public void GetRotateAngle__Получение_Угла_В_180_Градусов()
        {
            Assert.AreEqual(Helper.GetRotateAngle(center, new Point() {X = 40, Y = 100}), 180);
        }

        [TestMethod]
        public void GetRotateAngle__Получение_Угла_В_0_Градусов_2()
        {
            Assert.AreEqual(Helper.GetRotateAngle(center, new Point() {X = 140, Y = 100}), 0);
        }

        [TestMethod]
        public void GetRotateAngle__Получение_Угла_В_45_Градусов()
        {
            Assert.AreEqual(Helper.GetRotateAngle(center, new Point() {X = 125, Y = 125}), 45);
        }

        [TestMethod]
        public void GetRotateAngle__Получение_Угла_В_135_Градусов()
        {
            Assert.AreEqual(Helper.GetRotateAngle(center, new Point() {X = 75, Y = 125}), 135);
        }

        [TestMethod]
        public void GetRotateAngle__Получение_Угла_В_225_Градусов()
        {
            Assert.AreEqual(Helper.GetRotateAngle(center, new Point() {X = 75, Y = 75}), 225);
        }

        [TestMethod]
        public void GetRotateAngle__Получение_Угла_В_315_Градусов()
        {
            Assert.AreEqual(Helper.GetRotateAngle(center, new Point() {X = 125, Y = 75}), 315);
        }

        [TestMethod]
        public void GetRotateAngle__Получение_Угла_В_Примерно_300_Градусов()
        {
            int angle = (int)Helper.GetRotateAngle(center, new Point() {X = 600, Y = -766});
            Assert.AreEqual(angle, 300);
        }

        [TestMethod]
        public void GetRotateAngleModel__Получение_Угла_В_45_Градусов()
        {
            Assert.AreEqual(Helper.GetRotateAngleModel(model), 45);
        }

        [TestMethod]
        public void GetRotateAngleModel__Получение_Угла_В_90_Градусов()
        {
            model.AimPoint.X = 0;
            Assert.AreEqual(Helper.GetRotateAngleModel(model), 90);
        }
        [TestMethod]
        public void GenerateUid__Проверка_Длины()
        {
            Assert.AreEqual(Helper.GenerateUid().Length, 36);
        }
    }
}
