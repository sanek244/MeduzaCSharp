﻿using System;
using MeduzaServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace TestServer
{
    [TestClass]
    public class TestFileController 
    {
        FileController fileController = new FileController();

        public void FindOne__First_In_Db()
        {
            Assert.AreEqual((JsonConvert.DeserializeObject<ActiveModelTest>(fileController.FindOne("ActiveModelTest", 0).ToString())).Name, "Test1");
        }

        [TestMethod]
        public void FindOne__Id()
        {
            Assert.AreEqual(fileController.FindOne<ActiveModelTest>(3).Name, "Test4");
        }

        [TestMethod]
        public void FindOne__Name()
        {
            Assert.AreEqual(fileController.FindOne<ActiveModelTest>(new Dictionary<string, string>() { { "Name", "Test5" } }).Name, "Test5");
        }

        [TestMethod]
        public void FindOne__TestField()
        {
            Assert.AreEqual(fileController.FindOne<ActiveModelTest>(new Dictionary<string, string>() { { "testField", "2352" } }).Name, "Test1");
        }

        [TestMethod]
        public void FindOne__TestField__Name()
        {
            Assert.AreEqual(fileController.FindOne<ActiveModelTest>(new Dictionary<string, string>() { { "testField", "2352" }, { "name", "Test2"} }).Name, "Test2");
        }

        [TestMethod]
        public void FindOne__None()
        {
            Assert.AreEqual(fileController.FindOne<ActiveModelTest>(new Dictionary<string, string>() { { "testField", "2352" }, { "name", "Test3" } }), null);
        }

        [TestMethod]
        public void FindOne__Setter()
        {
            Assert.AreEqual(fileController.FindOne<ActiveModelTest>(new Dictionary<string, string>() { { "name", "Test2" } }).testFieldObject.textTest, "Рыбная рыбалка на пароге рыбного дождя");
        }

        [TestMethod]
        public void Find__Db_Length()
        {
            Assert.AreEqual(fileController.Find<ActiveModelTest>(null).Count, 5);
        }

        [TestMethod]
        public void Find__First_In_Db()
        {
            Assert.AreEqual(fileController.Find<ActiveModelTest>(null)[0].Name, "Test1");
        }

        [TestMethod]
        public void Find__Name()
        {
            Assert.AreEqual(fileController.Find<ActiveModelTest>(new Dictionary<string, string>() { { "name", "Test5" } })[0].Name, "Test5");
        }

        [TestMethod]
        public void Find__TestField_Length()
        {
            Assert.AreEqual(fileController.Find<ActiveModelTest>(new Dictionary<string, string>() { { "testField", "2352" } }).Count, 2);
        }

        [TestMethod]
        public void Find__TestField__Name_Length()
        {
            Assert.AreEqual(fileController.Find<ActiveModelTest>(new Dictionary<string, string>() { { "testField", "2352" }, { "name", "Test2" } }).Count, 1);
        }

        [TestMethod]
        public void Find__None_Length()
        {
            Assert.AreEqual(fileController.Find<ActiveModelTest>(new Dictionary<string, string>() { { "testField", "2352" }, { "name", "Test3" } }).Count, 0);
        }

        [TestMethod]
        public void Find__Setter()
        {
            Assert.AreEqual(fileController.Find<ActiveModelTest>(new Dictionary<string, string>() { { "name", "Test2" } })[0].testFieldObject.textTest, "Рыбная рыбалка на пароге рыбного дождя");
        }

        [TestMethod]
        public void GetVersionData__Check_Db()
        {
            Console.WriteLine(fileController.VersionData);
        }

        //TODO: AddModel + LoadMap
    }
}
