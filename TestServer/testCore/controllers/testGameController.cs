﻿using MeduzaServer;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace TestServer
{
    [TestClass]
    public class TestGameController 
    {
        
        //*** Свойства ***//
        GameController gameController;
        Gamer gamer;
        Map map;
        
        
        //*** Контруктор ***//
        public TestGameController()
        {
            map = Application.DBController.FindOne<Map>(0); //0 - id тестового полигона
            gameController = new GameController(map.Id); 
            gamer = new Gamer() {
                Rectangle = new RectangleRay(new Point(100, 200), new Size(25, 25)),
            };
            gamer.LoadPersonage(0);
        }
        
        
        //*** Методы ***//
        
        [TestMethod]
        public void Init__Load_Map_Objects()
        {
            Assert.AreEqual(gameController.DataProviderGame.GetCountAllObjects(), map.GetDefaultObjects().Count);
        }
        [TestMethod]
        public void AddModel__Simple()
        {
            gameController.DataProviderGame.ClearAll();
            gameController.AddModel(new SimpleGameObject());
            Assert.AreEqual(gameController.DataProviderGame.GetCountAllObjects(), 1);
        }

        [TestMethod]
        public void AddModel__Gamer()
        {
            gameController.DataProviderGame.ClearAll();
            gameController.AddModel(gamer);
            Assert.AreEqual(gameController.DataProviderGame.GetCountAllObjects(), 2);
            Assert.AreEqual(gameController.DataProviderGame.VisualSpaceCollection.Models.Count, 1);
        }
        [TestMethod]
        public void AddGamer()
        {
            gameController.DataProviderGame.ClearAll();
            gameController.AddGamer(0, "Frenk");
            Assert.AreEqual(gameController.DataProviderGame.GetCountAllObjects(), 2);
            var startPoint = gameController.MapGame.StartPoints.Find(start => start.Race == gamer.Race && !start.IsFree);
            Assert.AreEqual(gamer.Rectangle.Location.X, startPoint.Rectangle.Location.X);
            Assert.AreEqual(gamer.Rectangle.Location.Y, startPoint.Rectangle.Location.Y);
            Assert.AreEqual(gamer.Race, "Human");
        }
        [TestMethod]
        public void DeleteGamer()
        {
            gamer.IsDelete = false;
            gameController.DeleteGamer(gamer);
            Assert.AreEqual(gamer.IsDelete, true);
        }
        [TestMethod]
        public void GetGamer()
        {
            var socketId = "Asdf sdfs dg 34";
            gamer.SocketId = socketId;
            gameController.DataProviderGame.ClearAll();
            gameController.AddModel(gamer);
            Assert.AreEqual(gameController.GetGamer(socketId).ObjectId, gamer.ObjectId);
        }
        [TestMethod]
        public void GetGamerById()
        {
            gameController.DataProviderGame.ClearAll();
            gameController.AddModel(gamer);
            Assert.AreEqual(gameController.GetGamerById(gamer.ObjectId).ObjectId, gamer.ObjectId);
        }
        [TestMethod]
        public void GetGamers()
        {
            gameController.DataProviderGame.ClearAll();
            gameController.AddModel(gamer);
            gameController.AddGamer(0, "Frenk");
            Assert.AreEqual(gameController.GetGamers().Count, 2);
        }
        [TestMethod]
        public void GetData()
        {
            var res = gameController.GetData();
            Assert.AreEqual(res.Count, 7);
        }
        [TestMethod]
        public void ClearChanges()
        {
            gameController.AddModel(gamer);
            gameController.ClearChanges();
            Assert.AreEqual(gameController.DataProviderGame.GamerSpaceCollection.ModelChangedCoords.Count, 0);
        }
        [TestMethod]
        public void GetViewGamer__Empty()
        {
            gameController.ClearChanges();
            var res = gameController.GetViewGamer(gamer);
            Assert.AreEqual(res, false);
        }

        [TestMethod]
        public void GetViewGamer__Get()
        {
            gameController.DataProviderGame.ClearAll();
            var personage = new Personage() { Rectangle = new RectangleRay(new Point(300, 300), new Size(10, 10)) };
            gameController.AddModel(personage);
            var res = gameController.GetViewGamer(gamer);
            Assert.AreEqual(res["objects"].Count, 1);
            Assert.AreEqual(res["objects"][0].Contains(personage.ObjectId), true);
        }

        [TestMethod]
        public void GetViewGamer__Get_Sound()
        {
            gameController.DataProviderGame.ClearAll();
            var sound = new SoundObject() {
                Rectangle = new Rectangle(new Point(300, 300), new Size(10, 10)),
                Sound = new Sound() { Radius = 500, Volume = 100 }
            };
            gameController.AddModel(sound);
            var res = gameController.GetViewGamer(gamer);
            Assert.AreEqual(res["sound"].Count, 1);
            Assert.AreEqual(res["sound"][0].Contains(sound.ObjectId), true);
        }

        [TestMethod]
        public void Process()
        {
            /*
            * Цели теста:
            * 1. TimeComplete
            * 2. Движение игрока
            *
            * 3. Столкновение игрока со стеной и его остановка
            *
            * 4. process пуль
            * 5. изменение списка changes аттрибутов
            * 6. Действие Эффекта на игрока
            * 7. Изменение квадратов местоположения объектов
            * 8. Список изменённых моделей
            * 9. Разрушение пули после столкновения с бронированным игроком
            *
            * 10. Окончание действия эффекта и его снятие с игрока
            * 11. Разрушение пулей стены и убийство игрока + их исчезновение из игры
            * */
            //модели участвующие в тесте
            var building = new Building() {
                Rectangle = new RectangleRay(new Point(10, 250), new Size(500, 10)),
                Health = 10,
                Armor = 0
            };
            Bullet bullet = new Bullet() {
                Rectangle = new RectangleRay(new Point(100, 100), new Size(5, 5), 90),
                Health = 100,
                Damage = 2000,
                DamageArmor = 0
            };
            Bullet bullet2 = new Bullet() {
                Rectangle = new RectangleRay(new Point(100, 100), new Size(5, 5), 90),
                Health = 100,
                Damage = 2000,
                DamageArmor = 30
            };
            BaseMergeEffect effectArmor = new BaseMergeEffect() {
                ModifiableAttributes = {
                    { "Armor", 100 }
                },
                TimeLife = 100
            };
            SoundObject sound = new SoundObject() {
                Rectangle = new Rectangle(new Point(200, 200)),
                Sound = new Sound() {
                    Radius = 500
                }
            };
            SimpleGameObject simpleGameObject = new SimpleGameObject() {
                Rectangle = new Rectangle(new Point(50, 100), new Size(400, 400)),
            };
            Gamer gamer = new Gamer() {
                KeysMap = new List<ushort>() { 40 },
                Health = 2000,
                HealthStart = 2000,
                Rectangle = new RectangleRay(new Point(95, 210), new Size(25, 25)),
                Speed = 0.1f,
                Armor = 0,
            };

            //подготовка dataProvider
            gameController.DataProviderGame.ClearAll();
            gameController.AddModel(gamer);
            gameController.AddModel(building);
            gameController.AddModel(sound);
            gameController.AddModel(simpleGameObject);

            //этап 1
            var lastTimeComplete = Helper.GetUnixTime() - 100;
            gameController.LastTimeComplete = lastTimeComplete;
            gameController.Process();


            Assert.AreEqual(gameController.LastTimeComplete != lastTimeComplete, true); //"Цель 1. TimeComplete"
            Assert.AreEqual(gamer.Rectangle.Location.Y != 210, true); //"Цель 2. Движение игрока"
            //gamer y примерно равен 220 + height (25) = 245

            //этап 2
            gameController.LastTimeComplete = Helper.GetUnixTime() - 100;
            gameController.Process();

            Assert.AreEqual(gamer.Rectangle.Location.Y, 250 - 25); //"Цель 3. Столкновение игрока со стеной и его остановка"

            //этап 3
            gameController.AddModel(bullet);
            var squaresBullet = gameController.DataProviderGame.BulletSpaceCollection.GetModelSquares(bullet);
            gamer.AddEffect(effectArmor);
            gameController.LastTimeComplete = Helper.GetUnixTime() - 100;
            gameController.Process();

            Assert.AreEqual(gamer.Rectangle.Location.Y, 250 - 25); //"Цель 3. Столкновение игрока со стеной и его остановка (2)"
            Assert.AreEqual(bullet.Rectangle.Location.Y != 100, true); //"Цель 4. process пули"
            Assert.AreEqual(gamer.IsChangedSendAttributes(), true); // "Цель 5. изменение списка changes аттрибутов"
            Assert.AreEqual(bullet.IsChangedSendAttributes(), true); //"Цель 5. изменение списка changes аттрибутов (2)"
            //100 armor protect from 1000 damage bullet without armorDamage
            Assert.AreEqual(gamer.Health > 100 && gamer.Health < 2000, true); //"Цель 6. Действие Эффекта на игрока + цель 4. process пули"
            var squaresBullet2 = gameController.DataProviderGame.BulletSpaceCollection.GetModelSquares(bullet);

            Assert.AreEqual(
                squaresBullet.Except(squaresBullet2).Count() != 0 || squaresBullet2.Except(squaresBullet).Count() != 0,
                true); //"Цель 7. Изменение квадратов местоположения объектов"

            Assert.AreEqual(gameController.GetViewGamer(gamer)["objects"].Count != 0, true); //"Цель 8. Список изменённых моделей"

            Assert.AreEqual(
                gameController.DataProviderGame.VisualSpaceCollection.GetModelsInArea(new Rectangle(new Size(1000, 1000)), false, false, true).Count == 1,
                true
            ); //"Цель 8. Список изменённых моделей (2) визуальность добавилась анимация поподания пули  gamer"

            Assert.AreEqual(
                gameController.DataProviderGame.SoundSpaceCollection.GetModelsInArea(new Rectangle(new Size(1000, 1000)), false, false, true).Count == 0,
                true
            ); //"Цель 8. Список изменённых моделей (3) sound не менялся"

            Assert.AreEqual(bullet.IsDelete, true); //"Цель 9. Разрушение пули после столкновения с бронированным игроком"


            gameController.AddModel(bullet2);
            gameController.LastTimeComplete = Helper.GetUnixTime() - 100;
            effectArmor.CreatedTime = Helper.GetUnixTime() - 200;
            gameController.Process();

            Assert.AreEqual(gamer.Effects.ContainsKey(effectArmor.ClassName), false); //"Цель 10. Окончание действия эффекта и его снятие с игрока"
            Assert.AreEqual(gamer.IsDelete, true); //"Цель 10. Окончание действия эффекта и его снятие с игрока + 11. Разрушение пулей стены и убийство игрока + их исчезновение из игры"
            Assert.AreEqual(building.IsDelete, true); //"Цель 11. Разрушение пулей стены и убийство игрока + их исчезновение из игры"


            gameController.LastTimeComplete = Helper.GetUnixTime() - 100;
            gameController.Process();
            Assert.AreEqual(gameController.DataProviderGame.GamerSpaceCollection.GetModel(gamer.ObjectId), null); //"Цель 11. Разрушение пулей стены и убийство игрока + их исчезновение из игры (2)"
            Assert.AreEqual(gameController.DataProviderGame.BuildingSpaceCollection.GetModel(building.ObjectId), null); //"Цель 11. Разрушение пулей стены и убийство игрока + их исчезновение из игры (3)"
        }
    }
}
