﻿using System;
using MeduzaServer;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestDataProvider 
    {
        
        //*** Свойства ***//
        DataProvider dataProvider;
        Gamer gamer;
        Gamer gamer2;
        Building building;
        Building gameObject;
        SoundObject sound;
        SimpleGameObject simpleGameObject;
        MessageKill messageKill;

        Gamer gamer0;
        Building building2, building3, building5, building6;
        Bullet bullet, bullet4, bullet2;

        //*** Контруктор ***//
        public TestDataProvider()
        {
            dataProvider = new DataProvider(new Size(1000, 2000));
            
            gamer = new Gamer() {
                Name = "gamer",
                Rectangle = new RectangleRay(new Point(100, 200), new Size(25, 30))
            };
            
            gamer2 = new Gamer() {
                Name = "gamer2",
                Rectangle = new RectangleRay(new Point(300, 300), new Size(25, 30))
            };
            
            building = new Building() {
                Rectangle = new RectangleRay(new Point(200, 250), new Size(500, 700))
            };
            
            sound = new SoundObject() {
                Rectangle = new Rectangle(new Point(500, 500))
            };

            gameObject = new Building() {
                Rectangle = new RectangleRay(new Point(600, 700), new Size(80, 80))
            };
            
            simpleGameObject = new SimpleGameObject() {
                Rectangle = new Rectangle(new Point(100, 100), new Size(20, 20)),
            };
            
            messageKill = new MessageKill() {
                KillerName = gamer.Name,
                VictimName = gamer2.Name,
                Rectangle = new Rectangle(new Point(300, 300), new Size(25, 30))
            };


            building2 = new Building() {
                Name = "Building2",
                Rectangle = new RectangleRay(new Point(200, 250), new Size(500, 100)),
                Health = 100
            };
            bullet2 = new Bullet() {
                Name = "bullet2",
                Rectangle = new RectangleRay(new Point(200, 250), new Size(500, 100)),
            };
            building3 = new Building() {
                Name = "building3",
                Rectangle = new RectangleRay(new Point(210, 250), new Size(200, 200)),
                Health = 150
            };
            building5 = new Building() {
                Name = "Building5",
                Rectangle = new RectangleRay(new Point(220, 250), new Size(100, 100)),
                Health = 150
            };
            building6 = new Building() {
                Name = "Building6",
                Rectangle = new RectangleRay(new Point(190, 350), new Size(50, 350)),
                Health = 150
            };

            bullet = new Bullet() {
                Rectangle = new RectangleRay(new Point(100, 100), new Size(5, 5)),
            };

            gamer0 = new Gamer() {
                Name = "gamer0",
                Rectangle = new RectangleRay(new Point(100, 200), new Size(25, 30)),
            };

            bullet4 = new Bullet() {
                Name = "bullet4",
                Rectangle = new RectangleRay(new Point(210, 250), new Size(200, 400))
            };
        }

        private void AddObjects()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(gamer);
            dataProvider.AddModel(gamer2);
            dataProvider.AddModel(building);
            dataProvider.AddModel(sound);
            dataProvider.AddModel(gameObject);
            dataProvider.AddModel(simpleGameObject);
            dataProvider.AddModel(messageKill);
        }


        //*** Методы ***//

        [TestMethod]
        public void AddModel__Gamer()
        {
            dataProvider.AddModel(gamer); 
            Assert.AreEqual(dataProvider.GamerSpaceCollection.Models[gamer.ObjectId].ClassName, gamer.ClassName);
        }

        [TestMethod]
        public void AddModel__Building()
        {
            dataProvider.AddModel(building);

            Assert.AreEqual(dataProvider.BuildingSpaceCollection.Models[building.ObjectId].ClassName, building.ClassName);
        }

        [TestMethod]
        public void AddModel__Sound()
        {
            dataProvider.AddModel(sound);

            Assert.AreEqual(dataProvider.SoundSpaceCollection.Models[sound.ObjectId].ClassName, sound.ClassName);
        }

        [TestMethod]
        public void AddModel__SimpleGameObject()
        {
            dataProvider.AddModel(simpleGameObject);

            Assert.AreEqual(dataProvider.VisualSpaceCollection.Models[simpleGameObject.ObjectId].ClassName, simpleGameObject.ClassName);
        }

        [TestMethod]
        public void AddModel__MessageKill()
        {
            dataProvider.AddModel(messageKill);

            Assert.AreEqual(dataProvider.KillMessageSpaceCollection.Models[messageKill.ObjectId].ClassName, messageKill.ClassName);
        }

        [TestMethod]
        public void GetCountAllObjects__Empty()
        {
            dataProvider.ClearAll();
            Assert.AreEqual(dataProvider.GetCountAllObjects(), 0);
        }

        [TestMethod]
        public void GetCountAllObjects__2_Count_In_One_SpaceCollection()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building);
            dataProvider.AddModel(gamer2);

            Assert.AreEqual(dataProvider.GetCountAllObjects(), 2);
        }

        [TestMethod]
        public void GetCountAllObjects__7_Count_In_Different_SpaceCollections()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(gamer);
            dataProvider.AddModel(gamer2);
            dataProvider.AddModel(building);
            dataProvider.AddModel(sound);
            dataProvider.AddModel(gameObject);
            dataProvider.AddModel(simpleGameObject);
            dataProvider.AddModel(messageKill);

            Assert.AreEqual(dataProvider.GetCountAllObjects(), 7);
        }

        [TestMethod]
        public void ClearChanges__Gamer()
        {
            dataProvider.AddModel(gamer);
            dataProvider.ClearChanges();
            Assert.AreEqual(dataProvider.GamerSpaceCollection.ModelChangedCoords.Count, 0);
        }

        [TestMethod]
        public void ClearChanges__Building()
        {
            dataProvider.AddModel(building);
            dataProvider.ClearChanges();
            Assert.AreEqual(dataProvider.BuildingSpaceCollection.ModelChangedCoords.Count, 0);
        }

        [TestMethod]
        public void ClearChanges__Sound()
        {
            dataProvider.AddModel(sound);
            dataProvider.ClearChanges();
            Assert.AreEqual(dataProvider.SoundSpaceCollection.ModelChangedCoords.Count, 0);
        }

        [TestMethod]
        public void ClearChanges__SimpleGameObject()
        {
            dataProvider.AddModel(simpleGameObject);
            dataProvider.ClearChanges();
            Assert.AreEqual(dataProvider.VisualSpaceCollection.ModelChangedCoords.Count, 0);
        }

        [TestMethod]
        public void ClearChanges__MessageKill()
        {
            dataProvider.AddModel(messageKill);
            dataProvider.ClearChanges();
            Assert.AreEqual(dataProvider.KillMessageSpaceCollection.ModelChangedCoords.Count, 0);
        }

        [TestMethod]
        public void ClearAll__Ok()
        {
            dataProvider.AddModel(gamer);
            dataProvider.ClearAll();
            Assert.AreEqual(dataProvider.GamerSpaceCollection.ModelChangedCoords.Count, 0);
            Assert.AreEqual(dataProvider.GamerSpaceCollection.ModelSquares.Count, 0);
        }

        [TestMethod]
        public void GetView__Gamer()
        {
            dataProvider.AddModel(gamer);
            dataProvider.AddModel(gamer2);
            dataProvider.AddModel(building);
            dataProvider.AddModel(sound);
            dataProvider.AddModel(gameObject);
            dataProvider.AddModel(simpleGameObject);
            dataProvider.AddModel(messageKill);
            var res = dataProvider.GetView(gamer);
            Assert.AreEqual(res["sound"].Count, 1);
            Assert.AreEqual(res["objects"].Count, 5);
            Assert.AreEqual(res["messagesKill"].Count, 1);
        }

        [TestMethod]
        public void GetView__Gamer_None_Changes()
        {
            dataProvider.AddModel(gamer);
            dataProvider.AddModel(gamer2);
            dataProvider.AddModel(building);
            dataProvider.AddModel(sound);
            dataProvider.AddModel(gameObject);
            dataProvider.AddModel(simpleGameObject);
            dataProvider.AddModel(messageKill);
            dataProvider.ClearChanges();
            var res = dataProvider.GetView(gamer);
            Assert.AreEqual(res["sound"].Count, 0);
            Assert.AreEqual(res["objects"].Count, 0);
            Assert.AreEqual(res["messagesKill"].Count, 0);
        }

        [TestMethod]
        public void GetView__Gamer_None_Changes__Force()
        {
            dataProvider.ClearChanges();
            var res = dataProvider.GetView(gamer, true);
            Assert.AreEqual(res["sound"].Count, 1);
            Assert.AreEqual(res["objects"].Count, 5);
            Assert.AreEqual(res["messagesKill"].Count, 1);
        }



        [TestMethod]
        public void GetModelContacts__One_Location()
        {
            var res = dataProvider.GetModelContacts(gamer, gamer.Rectangle.Location);
            Assert.AreEqual(res.Models.Count, 0);
            Assert.AreEqual(res.LastlocationModel.X, gamer.Rectangle.Location.X);
            Assert.AreEqual(res.LastlocationModel.Y, gamer.Rectangle.Location.Y);
        }

        [TestMethod]
        public void GetModelContacts__Object_Right_Direct_In_Empty_Space()
        {
            dataProvider.ClearAll();
            var newLocation = new Point(
                gamer.Rectangle.Location.X + 500,
                gamer.Rectangle.Location.Y + 300);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 0);
            Assert.AreEqual(res.LastlocationModel.X, newLocation.X);
            Assert.AreEqual(res.LastlocationModel.Y, newLocation.Y);
        }

        [TestMethod]
        public void GetModelContacts__Object_Right_Direct_In_Empty_Space_2()
        {
            dataProvider.ClearAll();
            var newLocation = new Point(
                gamer.Rectangle.Location.X + 1,
                gamer.Rectangle.Location.Y + 1);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 0);
            Assert.AreEqual(res.LastlocationModel.X, newLocation.X);
            Assert.AreEqual(res.LastlocationModel.Y, newLocation.Y);
        }

        [TestMethod]
        public void GetModelContacts__Object_Right_Direct_0_Degrees_With_1_Object()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            gamer.Rectangle.Location.X = 100;
            gamer.Rectangle.Location.Y = 300;
            var newLocation = new Point(
                gamer.Rectangle.Location.X + 500,
                gamer.Rectangle.Location.Y);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.LastlocationModel.X, 210 - gamer.Rectangle.Size.Width);
            Assert.AreEqual(res.LastlocationModel.Y, gamer.Rectangle.Location.Y);
        }

        [TestMethod]
        public void GetModelContacts__Object_Left_Direct_180_Degrees_With_1_Object()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            gamer.Rectangle.Location.X = 500;
            gamer.Rectangle.Location.Y = 300;
            var newLocation = new Point(100, 300);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.LastlocationModel.X, 410);
            Assert.AreEqual(res.LastlocationModel.Y, 300);
        }

        [TestMethod]
        public void GetModelContacts__Object_Top_Direct_270_Degrees_With_1_Object()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            gamer.Rectangle.Location.X = 300;
            gamer.Rectangle.Location.Y = 700;
            var newLocation = new Point(300, 0);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.LastlocationModel.X, 300);
            Assert.AreEqual(res.LastlocationModel.Y, 450);
        }

        [TestMethod]
        public void GetModelContacts__Object_Bottom_Direct_90_Degrees_With_1_Object()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            gamer.Rectangle.Location.X = 300;
            gamer.Rectangle.Location.Y = 0;
            var newLocation = new Point(300, 700);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.LastlocationModel.X, 300);
            Assert.AreEqual(res.LastlocationModel.Y, 250 - gamer.Rectangle.Size.Height);
        }

        [TestMethod]
        public void GetModelContacts__Object_Right_Direct_0_Degrees_With_3_Object()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);

            gamer.Rectangle.Size.Height = 400;
            gamer.Rectangle.Location.X = 100;
            gamer.Rectangle.Location.Y = 200;
            var newLocation = new Point(700, gamer.Rectangle.Location.Y);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.Models[0].Model.ObjectId, building6.ObjectId);
            Assert.AreEqual(res.LastlocationModel.X, 190 - gamer.Rectangle.Size.Width);
            Assert.AreEqual(res.LastlocationModel.Y, gamer.Rectangle.Location.Y);
        }

        [TestMethod]
        public void GetModelContacts__Object_Left_Direct_180_Degrees_With_3_Object()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);

            gamer.Rectangle.Size.Height = 400;
            gamer.Rectangle.Location.X = 700;
            gamer.Rectangle.Location.Y = 200;
            var newLocation = new Point(0, gamer.Rectangle.Location.Y);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.Models[0].Model.ObjectId, building3.ObjectId);
            Assert.AreEqual(res.LastlocationModel.X, 210 + building3.Rectangle.Size.Width);
            Assert.AreEqual(res.LastlocationModel.Y, gamer.Rectangle.Location.Y);
        }

        [TestMethod]
        public void GetModelContacts__Object_Bottom_Direct_90_Degrees_With_3_Object_2_Contact()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);

            gamer.Rectangle.Size.Width = 100;
            gamer.Rectangle.Location.X = 250;
            gamer.Rectangle.Location.Y = 0;
            var newLocation = new Point(gamer.Rectangle.Location.X, 800);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 2);
            Assert.AreEqual(res.Models[0].Model.ObjectId, building3.ObjectId);
            Assert.AreEqual(res.Models[1].Model.ObjectId, building5.ObjectId);
            Assert.AreEqual(res.LastlocationModel.X, gamer.Rectangle.Location.X);
            Assert.AreEqual(res.LastlocationModel.Y, 250 - gamer.Rectangle.Size.Height);
        }

        [TestMethod]
        public void GetModelContacts__Object_Top_Direct_270_Degrees_With_3_Object()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);

            gamer.Rectangle.Size.Width = 300;
            gamer.Rectangle.Location.X = 300;
            gamer.Rectangle.Location.Y = 900;
            var newLocation = new Point(gamer.Rectangle.Location.X, 100);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.Models[0].Model.ObjectId, building3.ObjectId);
            Assert.AreEqual(res.LastlocationModel.X, gamer.Rectangle.Location.X);
            Assert.AreEqual(res.LastlocationModel.Y, 450);
        }


        [TestMethod]
        public void GetModelContacts__Object_Diagonal_Right_Top_With_3_Object()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);

            gamer.Rectangle.Size.Width = 30;
            gamer.Rectangle.Size.Height = 30;
            gamer.Rectangle.Location.X = 50;
            gamer.Rectangle.Location.Y = 550;
            var newLocation = new Point(250, 275);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.Models[0].Model.ObjectId, building6.ObjectId);
            var x = building6.Rectangle.Location.X - gamer.Rectangle.Size.Width;
            Assert.AreEqual(res.LastlocationModel.X < x && res.LastlocationModel.X > x - 1, true);
            Assert.AreEqual(res.LastlocationModel.Y > 350 && res.LastlocationModel.Y < 400, true);
        }

        [TestMethod]
        public void GetModelContacts__Object_Diagonal_Right_Bottom_With_3_Object()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);

            gamer.Rectangle.Size.Width = 30;
            gamer.Rectangle.Size.Height = 30;
            gamer.Rectangle.Location.X = 25;
            gamer.Rectangle.Location.Y = 100;
            var newLocation = new Point(300, 375);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.Models[0].Model.ObjectId, building3.ObjectId);
            var x = building3.Rectangle.Location.X - gamer.Rectangle.Size.Width;
            Assert.AreEqual(res.LastlocationModel.X < x && res.LastlocationModel.X > x - 1, true);
            Assert.AreEqual(res.LastlocationModel.Y > 250 && res.LastlocationModel.Y < 300, true);
        }

        [TestMethod]
        public void GetModelContacts__Object_Diagonal_Left_Bottom_With_3_Object()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);

            gamer.Rectangle.Size.Width = 30;
            gamer.Rectangle.Size.Height = 30;
            gamer.Rectangle.Location.X = 300;
            gamer.Rectangle.Location.Y = 150;
            var newLocation = new Point(250, 275);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 2);
            Assert.AreEqual(res.Models[0].Model.ObjectId == building3.ObjectId || res.Models[0].Model.ObjectId == building5.ObjectId, true);
            Assert.AreEqual(res.Models[1].Model.ObjectId == building3.ObjectId || res.Models[1].Model.ObjectId == building5.ObjectId, true);
            var y = building3.Rectangle.Location.Y - gamer.Rectangle.Size.Height;
            Assert.AreEqual(res.LastlocationModel.X > 200 && res.LastlocationModel.X < 300, true);
            Assert.AreEqual(res.LastlocationModel.Y < y && res.LastlocationModel.Y > y - 1, true);
        }

        [TestMethod]
        public void GetModelContacts__Object_Diagonal_Left_Top_With_3_Object()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);

            gamer.Rectangle.Size.Width = 30;
            gamer.Rectangle.Size.Height = 30;
            gamer.Rectangle.Location.X = 350;
            gamer.Rectangle.Location.Y = 625;
            var newLocation = new Point(150, 450);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.Models[0].Model.ObjectId, building6.ObjectId);
            var x = building6.Rectangle.Location.X + building6.Rectangle.Size.Width;
            Assert.AreEqual(res.LastlocationModel.X > x && res.LastlocationModel.X < x + 1, true);
            Assert.AreEqual(res.LastlocationModel.Y > 450 && res.LastlocationModel.Y < 650, true);
        }

        [TestMethod]
        public void GetModelContacts__Real_Dx_Dy()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);

            gamer.Rectangle.Size.Width = 30;
            gamer.Rectangle.Size.Height = 30;
            gamer.Rectangle.Location.X = 240.1f;
            gamer.Rectangle.Location.Y = 450.1f;
            var newLocation = new Point(239.8f, 449.9f);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 2);
            Assert.AreEqual(res.Models[0].Model.ObjectId, building3.ObjectId);
            Assert.AreEqual(res.Models[1].Model.ObjectId, building6.ObjectId);
            var x = building6.Rectangle.Location.X + building6.Rectangle.Size.Width;
            Assert.AreEqual(res.LastlocationModel.X > x && res.LastlocationModel.X < x + 1, true);
            Assert.AreEqual(res.LastlocationModel.Y > 450 && res.LastlocationModel.Y < 451, true);
        }

        [TestMethod]
        public void GetModelContacts__Object_Diagonal_Left_Top_With_3_Object_not_Contact()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);

            gamer.Rectangle.Size.Width = 30;
            gamer.Rectangle.Size.Height = 30;
            gamer.Rectangle.Location.X = 450;
            gamer.Rectangle.Location.Y = 625;
            var newLocation = new Point(250, 460);

            var res = dataProvider.GetModelContacts(gamer, newLocation);
            Assert.AreEqual(res.Models.Count, 0);
            Assert.AreEqual(res.LastlocationModel.X, newLocation.X);
            Assert.AreEqual(res.LastlocationModel.Y, newLocation.Y);
        }

        [TestMethod]
        public void GetModelContacts___getBulletContacts__One_Location()
        {
            var res = dataProvider.GetModelContacts(bullet, bullet.Rectangle.Location);
            Assert.AreEqual(res.Models.Count, 0);
            Assert.AreEqual(res.LastlocationModel.X, bullet.Rectangle.Location.X);
            Assert.AreEqual(res.LastlocationModel.Y, bullet.Rectangle.Location.Y);
        }

        [TestMethod]
        public void GetModelContacts___getBulletContacts__Bullet_Right_Direct_In_Empty_Space()
        {
            dataProvider.ClearAll();
            var newLocation = new Point(
                bullet.Rectangle.Location.X + 500,
                bullet.Rectangle.Location.Y + 300);

            var res = dataProvider.GetModelContacts(bullet, newLocation);
            Assert.AreEqual(res.Models.Count, 0);
            Assert.AreEqual(res.LastlocationModel.X, newLocation.X);
            Assert.AreEqual(res.LastlocationModel.Y, newLocation.Y);
        }

        [TestMethod]
        public void GetModelContacts___getBulletContacts__Bullet_Right_Direct_0_Degrees_With_1_Object()
        {

            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);


            bullet.ContactsModelObjectId = new HashSet<string>();
            bullet.Rectangle.Location.X = 100;
            bullet.Rectangle.Location.Y = 300;

            var newLocation = new Point(
                bullet.Rectangle.Location.X + 500,
                bullet.Rectangle.Location.Y);

            bullet.Rectangle.RotateAngle = Helper.GetRotateAngle(bullet.Rectangle.Location, newLocation);

            var res = dataProvider.GetModelContacts(bullet, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.LastlocationModel.X, 210); //возвращается не положение пули при столкновении, а точка соприкосновения
            Assert.AreEqual(res.LastlocationModel.Y, bullet.Rectangle.Location.Y);
        }

        [TestMethod]
        public void GetModelContacts___getBulletContacts__Bullet_Left_Direct_180_Degrees_With_1_Object()
        {

            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);


            bullet.ContactsModelObjectId = new HashSet<string>();
            bullet.Rectangle.Location.X = 500;
            bullet.Rectangle.Location.Y = 300;

            var newLocation = new Point(100, 300);

            bullet.Rectangle.RotateAngle = Helper.GetRotateAngle(bullet.Rectangle.Location, newLocation);

            var res = dataProvider.GetModelContacts(bullet, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.LastlocationModel.X, 410);
            Assert.AreEqual(res.LastlocationModel.Y, 300);
        }

        [TestMethod]
        public void GetModelContacts___getBulletContacts__Bullet_Top_Direct_270_Degrees_With_1_Object()
        {

            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);


            bullet.ContactsModelObjectId = new HashSet<string>();
            bullet.Rectangle.Location.X = 300;
            bullet.Rectangle.Location.Y = 700;

            var newLocation = new Point(300, 0);

            bullet.Rectangle.RotateAngle = Helper.GetRotateAngle(bullet.Rectangle.Location, newLocation);

            var res = dataProvider.GetModelContacts(bullet, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.LastlocationModel.X, 300);
            Assert.AreEqual(res.LastlocationModel.Y, 450);
        }

        [TestMethod]
        public void GetModelContacts___getBulletContacts__Bullet_Bottom_Direct_90_Degrees_With_1_Object()
        {

            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);


            bullet.ContactsModelObjectId = new HashSet<string>();
            bullet.Rectangle.Location.X = 300;
            bullet.Rectangle.Location.Y = 0;

            var newLocation = new Point(300, 700);

            bullet.Rectangle.RotateAngle = Helper.GetRotateAngle(bullet.Rectangle.Location, newLocation);

            var res = dataProvider.GetModelContacts(bullet, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.LastlocationModel.X, 300);
            Assert.AreEqual(res.LastlocationModel.Y, 250);
        }

        [TestMethod]
        public void GetModelContacts___getBulletContacts__Bullet_Right_Direct_0_Degrees_With_3_Object()
        {

            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);


            bullet.ContactsModelObjectId = new HashSet<string>();
            bullet.Rectangle.Location.X = 100;
            bullet.Rectangle.Location.Y = 355;

            var newLocation = new Point(700, bullet.Rectangle.Location.Y);

            bullet.Rectangle.RotateAngle = Helper.GetRotateAngle(bullet.Rectangle.Location, newLocation);

            var res = dataProvider.GetModelContacts(bullet, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.Models[0].Model.ObjectId, building6.ObjectId);
            Assert.AreEqual(res.LastlocationModel.X, 190);
            Assert.AreEqual(res.LastlocationModel.Y, bullet.Rectangle.Location.Y);
        }

        [TestMethod]
        public void GetModelContacts___getBulletContacts__Bullet_Left_Direct_180_Degrees_With_3_Object()
        {

            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);


            bullet.ContactsModelObjectId = new HashSet<string>();
            bullet.Rectangle.Location.X = 700;
            bullet.Rectangle.Location.Y = 300;

            var newLocation = new Point(0, bullet.Rectangle.Location.Y);

            bullet.Rectangle.RotateAngle = Helper.GetRotateAngle(bullet.Rectangle.Location, newLocation);

            var res = dataProvider.GetModelContacts(bullet, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.Models[0].Model.ObjectId, building3.ObjectId);
            Assert.AreEqual(res.LastlocationModel.X, 410);
            Assert.AreEqual(res.LastlocationModel.Y, bullet.Rectangle.Location.Y);
        }

        [TestMethod]
        public void GetModelContacts___getBulletContacts__Bullet_Bottom_Direct_90_Degrees_With_3_Object_2_Contact()
        {

            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);


            bullet.ContactsModelObjectId = new HashSet<string>();
            bullet.Rectangle.Location.X = 250;
            bullet.Rectangle.Location.Y = 0;

            var newLocation = new Point(bullet.Rectangle.Location.X, 800);

            bullet.Rectangle.RotateAngle = Helper.GetRotateAngle(bullet.Rectangle.Location, newLocation);

            var res = dataProvider.GetModelContacts(bullet, newLocation);
            Assert.AreEqual(res.Models.Count, 1); //1, а не 2, т.К. в реальной ситуации объекты не находятся друг в друге, они либо друз за другом, либо все-равно существует расстояние между гранями
            Assert.AreEqual(res.Models[0].Model.ObjectId == building3.ObjectId || res.Models[0].Model.ObjectId == building5.ObjectId, true);
            Assert.AreEqual(res.LastlocationModel.X, bullet.Rectangle.Location.X);
            Assert.AreEqual(res.LastlocationModel.Y, 250);
        }

        [TestMethod]
        public void GetModelContacts___getBulletContacts__Bullet_Top_Direct_270_Degrees_With_3_Object()
        {

            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);


            bullet.ContactsModelObjectId = new HashSet<string>();
            bullet.Rectangle.Location.X = 300;
            bullet.Rectangle.Location.Y = 900;

            var newLocation = new Point(bullet.Rectangle.Location.X, 100);

            bullet.Rectangle.RotateAngle = Helper.GetRotateAngle(bullet.Rectangle.Location, newLocation);

            var res = dataProvider.GetModelContacts(bullet, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.Models[0].Model.ObjectId, building3.ObjectId);
            Assert.AreEqual(res.LastlocationModel.X, bullet.Rectangle.Location.X);
            Assert.AreEqual(res.LastlocationModel.Y, 450);
        }


        [TestMethod]
        public void GetModelContacts___getBulletContacts__Bullet_Diagonal_Right_Top_With_3_Object()
        {

            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);


            bullet.ContactsModelObjectId = new HashSet<string>();
            bullet.Rectangle.Location.X = 50;
            bullet.Rectangle.Location.Y = 550;

            var newLocation = new Point(250, 275);

            bullet.Rectangle.RotateAngle = Helper.GetRotateAngle(bullet.Rectangle.Location, newLocation);

            var res = dataProvider.GetModelContacts(bullet, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.Models[0].Model.ObjectId, building6.ObjectId);
            Assert.AreEqual(res.LastlocationModel.X, 190);
            Assert.AreEqual(res.LastlocationModel.Y > 350 && res.LastlocationModel.Y < 375, true);
        }

        [TestMethod]
        public void GetModelContacts___getBulletContacts__Bullet_Diagonal_Right_Bottom_With_3_Object()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);


            bullet.ContactsModelObjectId = new HashSet<string>();
            bullet.Rectangle.Location.X = 25;
            bullet.Rectangle.Location.Y = 100;
            var newLocation = new Point(300, 375);
            bullet.Rectangle.RotateAngle = Helper.GetRotateAngle(bullet.Rectangle.Location, newLocation);

            var res = dataProvider.GetModelContacts(bullet, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.Models[0].Model.ObjectId, building3.ObjectId);
            Assert.AreEqual(res.LastlocationModel.X, 210);
            Assert.AreEqual(res.LastlocationModel.Y > 250 && res.LastlocationModel.Y < 350, true);
        }

        [TestMethod]
        public void GetModelContacts___getBulletContacts__Bullet_Diagonal_Left_Bottom_With_3_Object()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);


            bullet.ContactsModelObjectId = new HashSet<string>();
            bullet.Rectangle.Location.X = 300;
            bullet.Rectangle.Location.Y = 150;
            var newLocation = new Point(250, 275);
            bullet.Rectangle.RotateAngle = Helper.GetRotateAngle(bullet.Rectangle.Location, newLocation);

            var res = dataProvider.GetModelContacts(bullet, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.Models[0].Model.ObjectId == building3.ObjectId || res.Models[0].Model.ObjectId == building5.ObjectId, true);
            var y = building3.Rectangle.Location.Y - bullet.Rectangle.Size.Height;
            Assert.AreEqual(res.LastlocationModel.X > 250 && res.LastlocationModel.X < 280, true);
            Assert.AreEqual(res.LastlocationModel.Y, 250);
        }

        [TestMethod]
        public void GetModelContacts___getBulletContacts__Bullet_Diagonal_Left_Top_With_3_Object()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);


            bullet.ContactsModelObjectId = new HashSet<string>();
            bullet.Rectangle.Location.X = 350;
            bullet.Rectangle.Location.Y = 625;
            var newLocation = new Point(150, 450);
            bullet.Rectangle.RotateAngle = Helper.GetRotateAngle(bullet.Rectangle.Location, newLocation);

            var res = dataProvider.GetModelContacts(bullet, newLocation);
            Assert.AreEqual(res.Models.Count, 1);
            Assert.AreEqual(res.Models[0].Model.ObjectId, building6.ObjectId);
            Assert.AreEqual(res.LastlocationModel.X, 240);
            Assert.AreEqual(res.LastlocationModel.Y > 525 && res.LastlocationModel.Y < 575, true);
        }


        [TestMethod]
        public void GetModelContacts___getBulletContacts__Bullet_Top_Direct_270_Degrees_With_3_Object_none_Contact()
        {

            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);


            bullet.ContactsModelObjectId = new HashSet<string>();
            bullet.Rectangle.Location.X = 300;
            bullet.Rectangle.Location.Y = 900;

            var newLocation = new Point(bullet.Rectangle.Location.X, 451);

            bullet.Rectangle.RotateAngle = Helper.GetRotateAngle(bullet.Rectangle.Location, newLocation);

            var res = dataProvider.GetModelContacts(bullet, newLocation);
            Assert.AreEqual(res.Models.Count, 0);
            Assert.AreEqual(res.LastlocationModel.X, bullet.Rectangle.Location.X);
            Assert.AreEqual(res.LastlocationModel.Y, 451);
        }

        [TestMethod]
        public void GetModelContacts___getBulletContacts__Bullet_Top_Direct_270_Degrees_With_3_Object_damageArmor()
        {

            dataProvider.ClearAll();
            dataProvider.AddModel(building3); //Point(210, 250), Size(200, 200);

            dataProvider.AddModel(building5); //Point(220, 250), Size(100, 100);

            dataProvider.AddModel(building6); //Point(190, 350), Size(50, 350);


            bullet.ContactsModelObjectId = new HashSet<string>();
            bullet.DamageArmor = 2;
            bullet.Rectangle.Location.X = 275;
            bullet.Rectangle.Location.Y = 900;

            var newLocation = new Point(bullet.Rectangle.Location.X, 100);

            bullet.Rectangle.RotateAngle = Helper.GetRotateAngle(bullet.Rectangle.Location, newLocation);

            var res = dataProvider.GetModelContacts(bullet, newLocation);
            Assert.AreEqual(res.Models.Count, 2);
            Assert.AreEqual(res.Models[0].Model.Name, building3.Name);
            Assert.AreEqual(res.Models[1].Model.Name, building5.Name);
            Assert.AreEqual(res.LastlocationModel.X, bullet.Rectangle.Location.X);
            Assert.AreEqual(res.LastlocationModel.Y, 350);
        }

        [TestMethod]
        public void GetPhysicalModelsInArea__Area_1()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3);
            dataProvider.AddModel(bullet2);
            dataProvider.AddModel(building);
            dataProvider.AddModel(bullet4);
            dataProvider.AddModel(gamer0);
            var res = dataProvider.GetPhysicalModelsInArea(new Rectangle(new Point(700, 900), new Size(200, 400)), true);
            Assert.AreEqual(res.Count, 1);
            Assert.AreEqual(res[0].ObjectId, building.ObjectId);
        }

        [TestMethod]
        public void GetPhysicalModelsInArea__Area_2()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building6);
            var res = dataProvider.GetPhysicalModelsInArea(new Rectangle(new Point(150, 450), new Size(230, 205)), false);
            Assert.AreEqual(res.Count, 1);
        }

        [TestMethod]
        public void GetPhysicalModelsInArea__Area_3()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(gamer0);
            var res = dataProvider.GetPhysicalModelsInArea(new Rectangle(new Point(100, 300), new Size(30, 30)), true);
            Assert.AreEqual(res.Count, 1);
        }

        [TestMethod]
        public void GetPhysicalModelsInArea__Exactly_1()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(gamer0);
            var res = dataProvider.GetPhysicalModelsInArea(new Rectangle(new Point(100, 300), new Size(30, 30)), true, true);
            Assert.AreEqual(res.Count, 0);
        }

        [TestMethod]
        public void GetPhysicalModelsInArea__Exactly_2()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building6);
            var res = dataProvider.GetPhysicalModelsInArea(new Rectangle(new Point(150, 450), new Size(230, 205)), false, true);
            Assert.AreEqual(res.Count, 1);
        }

        [TestMethod]
        public void GetPhysicalModelsInArea__Exactly_3()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3);
            dataProvider.AddModel(bullet2);
            dataProvider.AddModel(building);
            dataProvider.AddModel(bullet4);
            dataProvider.AddModel(gamer0);
            var res = dataProvider.GetPhysicalModelsInArea(new Rectangle(new Point(200, 300), new Size(10, 20)), true, true);
            Assert.AreEqual(res.Count, 2);
            Assert.AreEqual(res[0].ObjectId == building.ObjectId || res[0].ObjectId == bullet2.ObjectId, true);
            Assert.AreEqual(res[1].ObjectId == building.ObjectId || res[1].ObjectId == bullet2.ObjectId, true);
        }

        [TestMethod]
        public void GetPhysicalModelsInArea__OnlyChanged()
        {
            dataProvider.ClearAll();
            dataProvider.AddModel(building3);
            dataProvider.AddModel(bullet2);
            dataProvider.AddModel(building);
            dataProvider.AddModel(bullet4);
            dataProvider.AddModel(gamer0);
            dataProvider.BuildingSpaceCollection.ModelChangedCoords = new Dictionary<int, List<Building>>();
            dataProvider.GamerSpaceCollection.ModelChangedCoords = new Dictionary<int, List<Gamer>>();
            dataProvider.BulletSpaceCollection.ModelChangedCoords = new Dictionary<int, List<Bullet>>();
            dataProvider.BulletSpaceCollection.DeleteModel(bullet2);
            dataProvider.BuildingSpaceCollection.DeleteModel(building3);
            var res = dataProvider.GetPhysicalModelsInArea(new Rectangle(new Point(300, 300), new Size(200, 200)), true, false, true);
            Assert.AreEqual(res.Count, 2);
            Assert.AreEqual(res[0].ObjectId == building3.ObjectId || res[0].ObjectId == bullet2.ObjectId, true);
            Assert.AreEqual(res[1].ObjectId == building3.ObjectId || res[1].ObjectId == bullet2.ObjectId, true);
        }
    }
}