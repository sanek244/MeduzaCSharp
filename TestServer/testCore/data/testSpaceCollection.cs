﻿using System;
using MeduzaServer;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace TestServer
{
    [TestClass]
    public class TestSpaceCollection 
    {
        
        //*** Свойства ***//
        SpaceCollection<Building> spaceCollection;
        SpaceCollection<SoundObject> spaceCollectionSoundObject;
        SpaceCollection<Building> spaceCollectionBig;
        Building building;
        Building building2;
        Building building3;
        Building building4;
        Building building5;
        Building building6;
        Building building0;
        SoundObject soundCenter;
        SoundObject soundLeft;
        
        
        //*** Контруктор ***//
        public TestSpaceCollection()
        {
            spaceCollection = new SpaceCollection<Building>(new Size(1000, 2000));
            spaceCollectionSoundObject = new SpaceCollection<SoundObject>(new Size(1000, 2000));
            spaceCollectionBig = new SpaceCollection<Building>(new Size(1000, 2000), 100);

            building0 = new Building() {
                Name = "Building0",
                Rectangle = new RectangleRay(new Point(100, 200), new Size(25, 30)),
            };
            building = new Building() {
                Name = "Building1",
                Rectangle = new RectangleRay(new Point(200, 250), new Size(500, 700)),
                Health = 100
            };
            building2 = new Building() {
                Name = "Building2",
                Rectangle = new RectangleRay(new Point(200, 250), new Size(500, 100)),
                Health = 100
            };
            building3 = new Building() {
                Name = "Building3",
                Rectangle = new RectangleRay(new Point(210, 250), new Size(200, 200)),
                Health = 150
            };
            building4 = new Building() {
                Name = "Building4",
                Rectangle = new RectangleRay(new Point(210, 250), new Size(200, 400)),
                Health = 120
            };
            building5 = new Building() {
                Name = "Building5",
                Rectangle = new RectangleRay(new Point(220, 250), new Size(100, 100)),
                Health = 150
            };
            building6 = new Building() {
                Name = "Building6",
                Rectangle = new RectangleRay(new Point(190, 350), new Size(50, 350)),
                Health = 150
            };
            soundCenter = new SoundObject() {
                Rectangle = new Rectangle(new Point(500, 500)),
                Sound = new Sound { Radius = 400}
            };
            soundLeft = new SoundObject() {
                Rectangle = new Rectangle(new Point(250, 500)),
                Sound = new Sound { Radius = 400}
            };
        }
        
        
        //*** Методы ***//
        
        [TestMethod]
        public void GetNumberSquare__Outside()
        {
            Assert.AreEqual(spaceCollection.GetNumberSquare(1601, 67), spaceCollection.GetNumberSquare(1000, 67));
        }

        [TestMethod]
        public void GetNumberSquare__Outside_2()
        {
            Assert.AreEqual(spaceCollection.GetNumberSquare(-65756, 67), spaceCollection.GetNumberSquare(0, 67));
        }

        [TestMethod]
        public void GetNumberSquare__Outside_3()
        {
            Assert.AreEqual(spaceCollection.GetNumberSquare(456, -456), spaceCollection.GetNumberSquare(456, 0));
        }

        [TestMethod]
        public void GetNumberSquare__Outside_4()
        {
            Assert.AreEqual(spaceCollection.GetNumberSquare(456, 3000), spaceCollection.GetNumberSquare(456, 1000));
        }

        [TestMethod]
        public void GetNumberSquare__Ok()
        {
            Assert.AreEqual(spaceCollection.GetNumberSquare(0,0), 0);
        }

        [TestMethod]
        public void GetNumberSquare__Ok_2()
        {
            Assert.AreEqual(spaceCollection.GetNumberSquare(200, 0), 1);
        }

        [TestMethod]
        public void GetNumberSquare__Ok_3()
        {
            Assert.AreEqual(spaceCollection.GetNumberSquare(474, 0), 2);
        }

        [TestMethod]
        public void GetNumberSquare__Ok_4()
        {
            Assert.AreEqual(spaceCollection.GetNumberSquare(64, 300), 1000/200);
        }

        [TestMethod]
        public void GetNumberSquare__Ok_5()
        {
            Assert.AreEqual(spaceCollection.GetNumberSquare(364, 300), 1000/200 + 1);
        }


        [TestMethod]
        public void GetPositionSquare__1()
        {
            Assert.AreEqual(spaceCollection.GetPositionSquare(11).X, 200);
            Assert.AreEqual(spaceCollection.GetPositionSquare(11).Y, 400);
        }

        [TestMethod]
        public void GetPositionSquare__2()
        {
            Assert.AreEqual(spaceCollection.GetPositionSquare(18).X, 600);
            Assert.AreEqual(spaceCollection.GetPositionSquare(18).Y, 600);
        }

        [TestMethod]
        public void GetPositionSquare__3()
        {
            Assert.AreEqual(spaceCollection.GetPositionSquare(20).X, 0);
            Assert.AreEqual(spaceCollection.GetPositionSquare(20).Y, 800);
        }

        [TestMethod]
        public void GetPositionSquare__4()
        {
            Assert.AreEqual(spaceCollection.GetPositionSquare(14).X, 800);
            Assert.AreEqual(spaceCollection.GetPositionSquare(14).Y, 400);
        }

        [TestMethod]
        public void GetPositionSquare__Center_1()
        {
            Assert.AreEqual(spaceCollection.GetPositionSquare(11, true).X, 300);
            Assert.AreEqual(spaceCollection.GetPositionSquare(11, true).Y, 500);
        }

        [TestMethod]
        public void GetPositionSquare__Center_2()
        {
            Assert.AreEqual(spaceCollection.GetPositionSquare(18, true).X, 700);
            Assert.AreEqual(spaceCollection.GetPositionSquare(18, true).Y, 700);
        }

        [TestMethod]
        public void GetPositionSquare__Center_3()
        {
            Assert.AreEqual(spaceCollection.GetPositionSquare(20, true).X, 100);
            Assert.AreEqual(spaceCollection.GetPositionSquare(20, true).Y, 900);
        }

        [TestMethod]
        public void GetPositionSquare__Center_4()
        {
            Assert.AreEqual(spaceCollection.GetPositionSquare(14, true).X, 900);
            Assert.AreEqual(spaceCollection.GetPositionSquare(14, true).Y, 500);
        }

        [TestMethod]
        public void GetModelSquares__2_Squares()
        {
            var res = spaceCollection.GetModelSquares(building2).ToList();
            Assert.AreEqual(res.Count, 2);
            Assert.AreEqual(res[0], 6);
            Assert.AreEqual(res[1], 8);
        }

        [TestMethod]
        public void GetModelSquares__4_Squares()
        {
            var res = spaceCollection.GetModelSquares(building3).ToList();
            Assert.AreEqual(res.Count, 4);
            Assert.AreEqual(res[0], 6);
            Assert.AreEqual(res[1], 7);
            Assert.AreEqual(res[2], 12);
            Assert.AreEqual(res[3], 11);
        }

        [TestMethod]
        public void GetModelSquares__6_Squares()
        {
            var res = spaceCollection.GetModelSquares(building4).ToList();
            Assert.AreEqual(res.Count, 6);
            Assert.AreEqual(res[0], 6);
            Assert.AreEqual(res[1], 7);
            Assert.AreEqual(res[2], 11);
            Assert.AreEqual(res[3], 12);
            Assert.AreEqual(res[4], 16);
            Assert.AreEqual(res[5], 17);
        }

        [TestMethod]
        public void GetModelSquares__12_Squares()
        {
            var res = spaceCollection.GetModelSquares(building).ToList();
            Assert.AreEqual(res.Count, 12);
            Assert.AreEqual(res[0], 6);
            Assert.AreEqual(res[1], 7);
            Assert.AreEqual(res[2], 8);
            Assert.AreEqual(res[3], 11);
            Assert.AreEqual(res[4], 12);
            Assert.AreEqual(res[5], 13);
            Assert.AreEqual(res[6], 16);
            Assert.AreEqual(res[7], 17);
            Assert.AreEqual(res[8], 18);
            Assert.AreEqual(res[9], 21);
            Assert.AreEqual(res[10], 22);
            Assert.AreEqual(res[11], 23);
        }

        [TestMethod]
        public void GetModelSquares__Sound()
        {
            var res = spaceCollectionSoundObject.GetModelSquares(soundCenter).ToList();
            Assert.AreEqual(res.Count, 13);
            Assert.AreEqual(res.IndexOf(2) != -1, true);
            Assert.AreEqual(res.IndexOf(6) != -1, true);
            Assert.AreEqual(res.IndexOf(7) != -1, true);
            Assert.AreEqual(res.IndexOf(8) != -1, true);
            Assert.AreEqual(res.IndexOf(10) != -1, true);
            Assert.AreEqual(res.IndexOf(11) != -1, true);
            Assert.AreEqual(res.IndexOf(12) != -1, true);
            Assert.AreEqual(res.IndexOf(13) != -1, true);
            Assert.AreEqual(res.IndexOf(14) != -1, true);
            Assert.AreEqual(res.IndexOf(16) != -1, true);
            Assert.AreEqual(res.IndexOf(17) != -1, true);
            Assert.AreEqual(res.IndexOf(18) != -1, true);
            Assert.AreEqual(res.IndexOf(22) != -1, true);
        }

        [TestMethod]
        public void GetObjectSquares__1_Square()
        {
            var res = spaceCollection.GetObjectSquares(new Point(100, 200), new Size(25, 30)).ToList();
            Assert.AreEqual(res.Count, 1);
            Assert.AreEqual(res[0], 5);
        }

        [TestMethod]
        public void GetObjectSquares__2_Squares()
        {
            var res = spaceCollection.GetObjectSquares(new Point(200, 250), new Size(500, 100)).ToList();
            Assert.AreEqual(res.Count, 2);
            Assert.AreEqual(res[0], 6);
            Assert.AreEqual(res[1], 8);
        }

        [TestMethod]
        public void GetObjectSquares__4_Squares()
        {
            var res = spaceCollection.GetObjectSquares(new Point(210, 250), new Size(200, 200)).ToList();
            Assert.AreEqual(res.Count, 4);
            Assert.AreEqual(res[0], 6);
            Assert.AreEqual(res[1], 7);
            Assert.AreEqual(res[2], 12);
            Assert.AreEqual(res[3], 11);
        }

        [TestMethod]
        public void GetObjectSquares__6_Squares()
        {
            var res = spaceCollection.GetObjectSquares(new Point(210, 250), new Size(200, 400)).ToList();
            Assert.AreEqual(res.Count, 6);
            Assert.AreEqual(res[0], 6);
            Assert.AreEqual(res[1], 7);
            Assert.AreEqual(res[2], 11);
            Assert.AreEqual(res[3], 12);
            Assert.AreEqual(res[4], 16);
            Assert.AreEqual(res[5], 17);
        }

        [TestMethod]
        public void GetObjectSquares__12_Squares()
        {
            var res = spaceCollection.GetObjectSquares(new Point(200, 250), new Size(500, 700)).ToList();
            Assert.AreEqual(res.Count, 12);
            Assert.AreEqual(res[0], 6);
            Assert.AreEqual(res[1], 7);
            Assert.AreEqual(res[2], 8);
            Assert.AreEqual(res[3], 11);
            Assert.AreEqual(res[4], 12);
            Assert.AreEqual(res[5], 13);
            Assert.AreEqual(res[6], 16);
            Assert.AreEqual(res[7], 17);
            Assert.AreEqual(res[8], 18);
            Assert.AreEqual(res[9], 21);
            Assert.AreEqual(res[10], 22);
            Assert.AreEqual(res[11], 23);
        }

        [TestMethod]
        public void GetModelSquaresSound__Sound_Left_Border()
        {
            var res = spaceCollectionSoundObject.GetModelSquaresSound(soundLeft).ToList(); 
            var resAssert = new int[]{ 11, 12, 13, 20, 21, 22, 23, 24, 25, 30, 31, 32, 33, 34, 35, 40, 41, 42, 43, 44, 45,
                50, 51, 52, 53, 54, 55, 60, 61, 62, 63, 64, 65, 70, 71, 72, 73, 74, 75, 81, 82, 83 };

            Assert.AreEqual(res.Count, resAssert.Length);
            foreach(var x in resAssert)
            {
                Assert.AreEqual(res.IndexOf(x) != -1, true);
            }
        }

        [TestMethod]
        public void AddModel__Ok()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building0);

            Assert.AreEqual(spaceCollection.ModelCoords[5].Count, 1);
            Assert.AreEqual(spaceCollection.ModelCoords[5][0].ClassName, building0.ClassName);

            Assert.AreEqual(spaceCollection.ModelSquares[building0.ObjectId].Count, 1);
            Assert.AreEqual(spaceCollection.ModelSquares[building0.ObjectId].ToList()[0], 5);

            Assert.AreEqual(spaceCollection.Models[building0.ObjectId].ClassName, building0.ClassName);

            Assert.AreEqual(spaceCollection.ModelChangedCoords[5].Count, 1);
            Assert.AreEqual(spaceCollection.ModelChangedCoords[5][0].ClassName, building0.ClassName);
        }

        [TestMethod]
        public void AddModel__Ok_Big()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building);
            var squares = new int[] { 6, 7, 8, 11, 12, 13, 16, 17, 18, 21, 22, 23 };

            foreach(var square in squares)
            {
                 Assert.AreEqual(spaceCollection.ModelCoords.ContainsKey(square), true);
            }
            Assert.AreEqual(spaceCollection.ModelCoords[squares[0]][0].ClassName, building.ClassName);

            Assert.AreEqual(spaceCollection.ModelSquares[building.ObjectId].Count, squares.Length);
            Assert.AreEqual(spaceCollection.Models[building.ObjectId].ClassName, building.ClassName);

            foreach(var square in squares)
            {
                Assert.AreEqual(spaceCollection.ModelChangedCoords.ContainsKey(square), true);
            }
            Assert.AreEqual(spaceCollection.ModelChangedCoords[squares[0]][0].ClassName, building.ClassName);
        }

        [TestMethod]
        public void AddModelInChanged__Error()
        {
            var ok = false;
            try{
                spaceCollection.ModelSquares = new Dictionary<string, HashSet<int>>();
                spaceCollection.AddModelInChanged(building);
            }
            catch (Exception error){
                ok = true;
                Console.WriteLine(error.Message);
            }

            if(!ok){
                Assert.Fail();

            }
        }

        [TestMethod]
        public void AddModelInChanged__Ok_Big()
        {
            var squares = new HashSet<int>() { 6, 7, 8, 11, 12, 13, 16, 17, 18, 21, 22, 23 };
            spaceCollection.ModelChangedCoords = new Dictionary<int, List<Building>>();
            spaceCollection.ModelSquares[building.ObjectId] = squares;
            spaceCollection.AddModelInChanged(building);

            foreach(var square in squares)
            {
                Assert.AreEqual(spaceCollection.ModelChangedCoords.ContainsKey(square), true);
            }
            Assert.AreEqual(spaceCollection.ModelChangedCoords[squares.ToList()[0]][0].ClassName, building.ClassName);
        }

        [TestMethod]
        public void ClearChanges__Ok()
        {
            spaceCollection.ClearChanges();
            Assert.AreEqual(spaceCollection.ModelChangedCoords.Count, 0);
        }

        [TestMethod]
        public void UpdateModel__Equal()
        {
            spaceCollection.ModelCoords = new Dictionary<int, List<Building>>();
            spaceCollection.ModelSquares = new Dictionary<string, HashSet<int>>();
            spaceCollection.ModelChangedCoords = new Dictionary<int, List<Building>>();
            spaceCollection.AddModel(building3);
            Assert.AreEqual(spaceCollection.UpdateModel(building3), false);
        }

        [TestMethod]
        public void UpdateModel__Ok()
        {
            spaceCollection.ModelCoords = new Dictionary<int, List<Building>>();
            spaceCollection.ModelSquares = new Dictionary<string, HashSet<int>>();
            building3.Rectangle.Location = new Point(210, 250);
            spaceCollection.AddModel(building3);
            spaceCollection.ModelChangedCoords = new Dictionary<int, List<Building>>();
            building3.Rectangle.Location = new Point(390, 190);
            spaceCollection.UpdateModel(building3);
            var squaresAssert = new int[] { 1, 2, 7, 6 }; //было = [ 6, 7, 12, 11 ]
            var squaresChangesAssert = new int[] { 1, 2, 12, 11 };

            Assert.AreEqual(spaceCollection.ModelCoords.Count, squaresAssert.Length);
            foreach(var square in squaresAssert)
            {
                Assert.AreEqual(spaceCollection.ModelCoords.ContainsKey(square), true);
            }
            Assert.AreEqual(spaceCollection.ModelCoords[squaresAssert[0]][0].ClassName, building3.ClassName);

            Assert.AreEqual(spaceCollection.ModelSquares[building3.ObjectId].Count, squaresAssert.Length);

            Assert.AreEqual(spaceCollection.ModelChangedCoords.Count, squaresChangesAssert.Length);
            foreach(var square in squaresChangesAssert)
            {
                 Assert.AreEqual(spaceCollection.ModelChangedCoords.ContainsKey(square), true);
            }
            Assert.AreEqual(spaceCollection.ModelChangedCoords[squaresChangesAssert[0]][0].ClassName, building3.ClassName);
        }

        [TestMethod]
        public void DeleteModel__Ok()
        {
            spaceCollection.ModelCoords = new Dictionary<int, List<Building>>();
            spaceCollection.Models = new Dictionary<string, Building>();
            spaceCollection.ModelSquares = new Dictionary<string, HashSet<int>>();
            building3.Rectangle.Location = new Point(210, 250);
            spaceCollection.AddModel(building3);
            spaceCollection.ModelChangedCoords = new Dictionary<int, List<Building>>();
            spaceCollection.DeleteModel(building3);
            var squaresAssert = new int[] { 6, 7, 12, 11 };

            Assert.AreEqual(spaceCollection.ModelCoords.Count, 0);

            Assert.AreEqual(spaceCollection.ModelSquares.ContainsKey(building3.ObjectId), false);
            Assert.AreEqual(spaceCollection.Models.ContainsKey(building3.ObjectId), false);

            Assert.AreEqual(spaceCollection.ModelChangedCoords.Count, squaresAssert.Length);
            foreach(var square in squaresAssert)
            {
                Assert.AreEqual(spaceCollection.ModelChangedCoords.ContainsKey(square), true);
            }
            Assert.AreEqual(spaceCollection.ModelChangedCoords[squaresAssert[0]][0].ClassName, building3.ClassName);
        }

        [TestMethod]
        public void GetModel__Object_Id()
        {
            spaceCollection.AddModel(building3);
            Assert.AreEqual(spaceCollection.GetModel(building3.ObjectId).ClassName, building3.ClassName);
        }

        [TestMethod]
        public void GetModel__Null()
        {
            Assert.AreEqual(spaceCollection.GetModel(building.ObjectId), null);
        }

        [TestMethod]
        public void GetModel__Null_After_Remove()
        {
            spaceCollection.AddModel(building3);
            spaceCollection.DeleteModel(building3);
            Assert.AreEqual(spaceCollection.GetModel(building3.ObjectId), null);
        }

        [TestMethod]
        public void GetModels__0()
        {
            spaceCollection.ClearAll();
            Assert.AreEqual(spaceCollection.GetModels().Count, 0);
        }

        [TestMethod]
        public void GetModels__All()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3);
            spaceCollection.AddModel(building2);
            spaceCollection.AddModel(building);
            spaceCollection.AddModel(building4);
            spaceCollection.AddModel(building0);
            Assert.AreEqual(spaceCollection.GetModels().Count, 5);
        }


        [TestMethod]
        public void GetModelsInArea__Area_1()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3);
            spaceCollection.AddModel(building2);
            spaceCollection.AddModel(building);
            spaceCollection.AddModel(building4);
            spaceCollection.AddModel(building0);
            var res = spaceCollection.GetModelsInArea(new Rectangle(new Point(700, 900), new Size(200, 400)), true);
            Assert.AreEqual(res.Count, 1);
            Assert.AreEqual(res[0].ObjectId, building.ObjectId);
        }

        [TestMethod]
        public void GetModelsInArea__Area_2()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building6);
            var res = spaceCollection.GetModelsInArea(new Rectangle(new Point(150, 450), new Size(230, 205)), false);
            Assert.AreEqual(res.Count, 1);
        }

        [TestMethod]
        public void GetModelsInArea__Area_3()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building0);
            var res = spaceCollection.GetModelsInArea(new Rectangle(new Point(100, 300), new Size(30, 30)), true);
            Assert.AreEqual(res.Count, 1);
        }

        [TestMethod]
        public void GetModelsInArea__Exactly_1()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building0);
            var res = spaceCollection.GetModelsInArea(new Rectangle(new Point(100, 300), new Size(30, 30)), true, true);
            Assert.AreEqual(res.Count, 0);
        }

        [TestMethod]
        public void GetModelsInArea__Exactly_2()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building6);
            var res = spaceCollection.GetModelsInArea(new Rectangle(new Point(150, 450), new Size(230, 205)), false, true);
            Assert.AreEqual(res.Count, 1);
        }

        [TestMethod]
        public void GetModelsInArea__Exactly_3()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3);
            spaceCollection.AddModel(building2);
            spaceCollection.AddModel(building);
            spaceCollection.AddModel(building4);
            spaceCollection.AddModel(building0);
            var res = spaceCollection.GetModelsInArea(new Rectangle(new Point(200, 300), new Size(10, 20)), true, true);
            Assert.AreEqual(res.Count, 2);
            Assert.AreEqual(res[0].ObjectId == building.ObjectId || res[0].ObjectId == building2.ObjectId, true);
            Assert.AreEqual(res[1].ObjectId == building.ObjectId || res[1].ObjectId == building2.ObjectId, true);
        }

        [TestMethod]
        public void GetModelsInArea__OnlyChanged()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3);
            spaceCollection.AddModel(building2);
            spaceCollection.AddModel(building);
            spaceCollection.AddModel(building4);
            spaceCollection.AddModel(building0);
            spaceCollection.ModelChangedCoords = new Dictionary<int, List<Building>>();
            spaceCollection.DeleteModel(building2);
            spaceCollection.DeleteModel(building3);
            var res = spaceCollection.GetModelsInArea(new Rectangle(new Point(300, 300), new Size(200, 200)), true, false, true);
            Assert.AreEqual(res.Count, 2);
            Assert.AreEqual(res[0].ObjectId == building3.ObjectId || res[0].ObjectId == building2.ObjectId, true);
            Assert.AreEqual(res[1].ObjectId == building3.ObjectId || res[1].ObjectId == building2.ObjectId, true);
        }

        [TestMethod]
        public void GetPointIntersectionNetWithRay__Horizontal_Right()
        {
            var res = spaceCollection.GetPointIntersectionNetWithRay(new Ray(new Point(500, 300), new Point(510, 300)), 7);
            Assert.AreEqual(res.X, 600);
            Assert.AreEqual(res.Y, 300);
        }

        [TestMethod]
        public void GetPointIntersectionNetWithRay__Horizontal_Left()
        {
            var res = spaceCollection.GetPointIntersectionNetWithRay(new Ray(new Point(510, 300), new Point(500, 300)), 7);
            Assert.AreEqual(res.X, 400);
            Assert.AreEqual(res.Y, 300);
        }

        [TestMethod]
        public void GetPointIntersectionNetWithRay__Vertical_Top()
        {
            var res = spaceCollection.GetPointIntersectionNetWithRay(new Ray(new Point(500, 310), new Point(500, 300)), 7);
            Assert.AreEqual(res.X, 500);
            Assert.AreEqual(res.Y, 200);
        }

        [TestMethod]
        public void GetPointIntersectionNetWithRay__Vertical_Bottom()
        {
            var res = spaceCollection.GetPointIntersectionNetWithRay(new Ray(new Point(500, 300), new Point(500, 310)), 7);
            Assert.AreEqual(res.X, 500);
            Assert.AreEqual(res.Y, 400);
        }

        [TestMethod]
        public void GetPointIntersectionNetWithRay__Diagonal_Right_Top()
        {
            var res = spaceCollection.GetPointIntersectionNetWithRay(new Ray(new Point(500, 300), new Point(550, 250)), 7);
            Assert.AreEqual(res.X, 600);
            Assert.AreEqual(res.Y, 200);
        }

        [TestMethod]
        public void GetPointIntersectionNetWithRay__Diagonal_Left_Top()
        {
            var res = spaceCollection.GetPointIntersectionNetWithRay(new Ray(new Point(500, 300), new Point(450, 250)), 7);
            Assert.AreEqual(res.X, 400);
            Assert.AreEqual(res.Y, 200);
        }

        [TestMethod]
        public void GetPointIntersectionNetWithRay__Diagonal_Left_Bottom()
        {
            var res = spaceCollection.GetPointIntersectionNetWithRay(new Ray(new Point(500, 300), new Point(450, 350)), 7);
            Assert.AreEqual(res.X, 400);
            Assert.AreEqual(res.Y, 400);
        }

        [TestMethod]
        public void GetPointIntersectionNetWithRay__Diagonal_Right_Bottom()
        {
            var res = spaceCollection.GetPointIntersectionNetWithRay(new Ray(new Point(500, 300), new Point(550, 350)), 7);
            Assert.AreEqual(res.X, 600);
            Assert.AreEqual(res.Y, 400);
        }

        [TestMethod]
        public void GetMoveArea__Ok()
        {
            var res = SpaceCollection<Building>.GetMoveArea(building0, new Point(building0.Rectangle.Location.X + 500, building0.Rectangle.Location.Y + 300));
            Assert.AreEqual(res.Location.X, building0.Rectangle.Location.X);
            Assert.AreEqual(res.Location.Y, building0.Rectangle.Location.Y);
            Assert.AreEqual(res.Size.Width, building0.Rectangle.Size.Width + 500);
            Assert.AreEqual(res.Size.Height, building0.Rectangle.Size.Height + 300);
        }

        [TestMethod]
        public void GetMoveArea__Ok_2_Back()
        {
            var res = SpaceCollection<Building>.GetMoveArea(building0, new Point(building0.Rectangle.Location.X - 500, building0.Rectangle.Location.Y - 300));
            Assert.AreEqual(res.Location.X, building0.Rectangle.Location.X - 500);
            Assert.AreEqual(res.Location.Y, building0.Rectangle.Location.Y - 300);
            Assert.AreEqual(res.Size.Width, building0.Rectangle.Size.Width + 500);
            Assert.AreEqual(res.Size.Height, building0.Rectangle.Size.Height + 300);
        }

        [TestMethod]
        public void ClearAll__Ok()
        {
            spaceCollection.ClearAll();
            Assert.AreEqual(spaceCollection.ModelCoords.Count, 0);
            Assert.AreEqual(spaceCollection.ModelSquares.Count, 0);
            Assert.AreEqual(spaceCollection.Models.Count, 0);
            Assert.AreEqual(spaceCollection.ModelChangedCoords.Count, 0);
        }
    }
}
