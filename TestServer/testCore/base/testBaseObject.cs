﻿using System;
using MeduzaServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestBaseObject 
    {
        
        //*** Свойства ***//
        BaseObject baseObject;
        BaseObjectTest baseObjectTest;
        Sound sound;
        
        
        
        //*** Контруктор ***//
        public TestBaseObject()
        {
            baseObject = new BaseObject();
            baseObjectTest = new BaseObjectTest();
            sound = new Sound();
        }
        
        
        //*** Методы ***//
        [TestMethod]
        public void ClassName()
        {
            Assert.AreEqual(baseObject.ClassName, "BaseObject");
        }

        [TestMethod]
        public void ClassName__Через_Экземпляр_наследственно()
        {
            Assert.AreEqual(baseObjectTest.ClassName, "BaseObjectTest");
        }
        

        [TestMethod]
        public void Error()
        {
            var ok = false;
            try {
                (new BaseObject()).Error("TestMethod", "TestMessage");
            }
            catch (Exception error){
                Console.WriteLine(error.Message);
                if(Application.ProjectMode == TypeProjectMode.Production) {
                    Assert.Fail();
                }
                else{
                    ok = true;
                }
            }
            if(!ok) {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Log()
        {
            (new BaseObject()).Log("TestMethod", "TestMessage");
        }

        [TestMethod]
        public void Get()
        {
            Point a = new Point(25);
            Assert.AreEqual(int.Parse(a.Get("x") + ""), 25);
        }
    }
}
