﻿using MeduzaServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestEnum 
    {
        public void GetLabel()
        {
            Assert.AreEqual(EnumTest.labels[TypeTest.Test_field_1], "Тестовое поле 1");
        }
    }
}
