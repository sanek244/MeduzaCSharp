﻿using MeduzaServer;

namespace TestServer
{
    class ActiveModelTest : ActiveModel
    {
        private int testFieldObjectId = -1;

        public string Name;
        public int testField = 27;
        public int TestFieldObjectId
        {
            get => testFieldObjectId;
            set
            {
                testFieldObjectId = value;
                testFieldObject = Application.DBController.FindOne<ActiveModelTest2>(value);
            }
        }

        public ActiveModelTest2 testFieldObject;
    }
}