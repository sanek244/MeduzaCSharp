﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeTest { Test_field_1, Test_field_2 };

    public static class EnumTest
    {
        public static Dictionary<TypeTest, string> labels = new Dictionary<TypeTest, string>() {
            { TypeTest.Test_field_1, "Тестовое поле 1" },
            { TypeTest.Test_field_2, "Тестовое поле 2" },
        };
    }
}