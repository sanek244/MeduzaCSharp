﻿using System;
using MeduzaServer;
using System.Collections.Generic;

namespace TestServer
{
    class TestEffect : BaseMergeEffect
    {
        public TestEffect(SimpleGameObject model) {
            Model = model;
            TimeLife = 1000 * 3;
            ModifiableAttributes = new Dictionary<string, object>() {
                { "damage", 10 },
                { "health", -50 }
            };
        }

        public override void Start() {
            Console.WriteLine("start TestEffect");
            ModifiableAttributes["damage"] = 15;
        }
    }
}