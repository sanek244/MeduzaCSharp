﻿using System;
using MeduzaServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestSpeedSpaceCollection
    {
        static public void native()
        {
            Console.WriteLine("100 objects x 10 getModelContacts + updateModel");

            var dataProvider = new DataProvider(new Size(10000, 20000));

            var timeBegin2 = DateTime.Now;
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    dataProvider.GamerSpaceCollection.AddModel(new Gamer(){
                        Rectangle = new RectangleRay(new Point(20 * i, 30 * j), new Size(5, 5)),
                    });
                }
            }

            var timeBegin = DateTime.Now;
            for(int i = 0; i < 10; i++) {
                dataProvider.ClearChanges();
                var models = dataProvider.GamerSpaceCollection.GetModels();

                foreach(var model in models) {
                    var newLocation = new Point(
                        model.Rectangle.Location.X + (float)Helper.rnd.NextDouble(),
                        model.Rectangle.Location.X + (float)Helper.rnd.NextDouble());

                    dataProvider.GetModelContacts(model, newLocation);
                    dataProvider.GamerSpaceCollection.UpdateModel(model);
                }
            }

            Console.WriteLine("\nload data: " + (timeBegin - timeBegin2));
            Console.WriteLine("time complete: " + (DateTime.Now - timeBegin));
        }
    }
}
