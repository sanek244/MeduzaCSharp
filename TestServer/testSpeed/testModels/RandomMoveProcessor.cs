﻿using MeduzaServer;
using System.Collections.Generic;

namespace TestServer
{
    /// <summary>
    /// Обработка модели Gamer
    /// </summary>
    class RandomMoveProcessor : BaseObject, IProcessor
    {
        //*** Свойства ***//
        /// <summary>
        /// Модель, имеющая данный обработчик
        /// </summary>
        public SimpleGameObject Model { get; set; }


        //*** Конструкторы ***//
        public RandomMoveProcessor(SimpleGameObject model)
        {
            Model = model;
        }


        //*** Методы ***//
        /// <summary>
        /// Обработка действий модели
        /// </summary>
        /// <param name="dataProvider"></param>
        /// <param name="timeComplete">прошедшее время между обработкой событий</param>
        public void Process(DataProvider dataProvider, uint timeComplete)
        {
            //Перемещение модели
            var physicsMoveAbility = new PhysicsMoveAbility();
            physicsMoveAbility.Activation(dataProvider, Model, timeComplete, new Dictionary<string, object>() { { "dx", Helper.GetRandom(-1, 1) }, {"dy", 0 }});
        }
    }
}