﻿using System;
using MeduzaServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{

    /**
     * Цель:
     * 1. узнать количество кадров в секунду(среднее, миним, максим) на протяжении минуты
     */

    [TestClass]
    public class TestGameController_Speed 
    {
        
        //*** Свойства ***//
        GameController gameController;
        
        
        //*** Контруктор ***//
        public TestGameController_Speed()
        {
            gameController = new GameController(0); //0 - id тестового полигона
        }
        
        
        //*** Методы ***//
        
        [TestMethod]
        public void No_Acts()
        {
            var countSeconds = 5;
            var timeBegin2 = Helper.GetUnixTime();
            for(var i = 0; i < 10; i++){
                for(var j = 0; j < 10; j++){
                    gameController.AddModel(new Building() {
                        Rectangle = new RectangleRay(new Point(20 * i, 30 * j), new Size(5, 5)),
                    });
                }
            }

            var timeBegin = Helper.GetUnixTime();

            do {
                gameController.Process();
            } while(Helper.GetUnixTime() - timeBegin < (ulong)(1000 * countSeconds));

            Console.WriteLine("MinFPS: " + gameController.MinFPS);
            Console.WriteLine("MaxFPS: " + gameController.MaxFPS);
            Console.WriteLine("AvrFPS: " + gameController.SumFPS / countSeconds);
            Console.WriteLine("FPSGraph");
            foreach (var el in gameController.FPSGraph) {
                Console.Write(el + " ");
            }
            Console.WriteLine("Time added model: " + (timeBegin - timeBegin2));

            Assert.AreEqual(gameController.SumFPS / countSeconds > 50, true);
        }

        [TestMethod]
        public void Moved_100_Blocks()
        {
            var countSeconds = 5;
            var timeBegin2 = Helper.GetUnixTime();
            for(var i = 0; i < 10; i++){
                for(var j = 0; j < 10; j++){
                    var building = new Building() {
                        Rectangle = new RectangleRay(new Point(20 * i, 30 * j), new Size(5, 5)),
                    };
                    building.Processors.Add(new RandomMoveProcessor(building));
                    gameController.AddModel(building);
                }
            }

            var timeBegin = Helper.GetUnixTime();
            do {
                gameController.Process();
            } while(Helper.GetUnixTime() - timeBegin < (ulong)(1000 * countSeconds)); // 1 минута

            Console.WriteLine("MinFPS: " + gameController.MinFPS);
            Console.WriteLine("MaxFPS: " + gameController.MaxFPS);
            Console.WriteLine("AvrFPS: " + gameController.SumFPS / countSeconds);
            Console.WriteLine("FPSGraph");
            foreach (var el in gameController.FPSGraph) {
                Console.Write(el + " ");
            }
            Console.WriteLine("Time added model: " + (timeBegin - timeBegin2));

            Assert.AreEqual(gameController.SumFPS / countSeconds > 50, true);
        }

        [TestMethod]
        public void Moved_10_Blocks()
        {
            var countSeconds = 5;
            var timeBegin2 = Helper.GetUnixTime();
            for(var j = 0; j < 10; j++){
                var building = new Building() {
                    Rectangle = new RectangleRay(new Point(20 * j, 30), new Size(5, 5)),
                };
                building.Processors.Add(new RandomMoveProcessor(building));
                gameController.AddModel(building);
            }

            var timeBegin = Helper.GetUnixTime();
            do {
                gameController.Process();
            } while(Helper.GetUnixTime() - timeBegin < (ulong)(1000 * countSeconds)); // 1 минута

            Console.WriteLine("MinFPS: " + gameController.MinFPS);
            Console.WriteLine("MaxFPS: " + gameController.MaxFPS);
            Console.WriteLine("AvrFPS: " + gameController.SumFPS / countSeconds);
            Console.WriteLine("FPSGraph");
            foreach (var el in gameController.FPSGraph) {
                Console.Write(el + " ");
            }
            Console.WriteLine("Time added model: " + (timeBegin - timeBegin2));

            Assert.AreEqual(gameController.SumFPS / countSeconds > 50, true);
        }
    }
}