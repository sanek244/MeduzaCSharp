using MeduzaServer;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestBaseMergeEffect 
    {

        //*** �������� ***//
        BaseMergeEffect baseMergeEffect, baseMergeEffect2;


        //*** ���������� ***//
        public TestBaseMergeEffect()
        {
            baseMergeEffect = new BaseMergeEffect() {
                ModifiableAttributes = new Dictionary<string, object>() {
                    { "A", 343 },
                    { "B", "Weas" }
                },
                TimeLife = 1000
            };

            baseMergeEffect2 = new BaseMergeEffect() {
                ModifiableAttributes = new Dictionary<string, object>() {
                    { "A", 25 },
                    { "B", "Ss" }
                },
                TimeLife = 1500
            };
        }


        //*** ������ ***//

        [TestMethod]
        public void Merge__Ok()
        {
            baseMergeEffect.Merge(baseMergeEffect2);
            Assert.AreEqual(baseMergeEffect.TimeLife, 1500);
            Assert.AreEqual(baseMergeEffect.ModifiableAttributes["A"], 343 + 25);
            Assert.AreEqual(baseMergeEffect.ModifiableAttributes["B"], "Ss");
        }
    }
}
