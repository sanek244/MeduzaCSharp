﻿using MeduzaServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestRecoilEffect 
    {

        //*** Свойства ***//
        Gamer gamer;
        RecoilEffect recoilEffect;


        //*** Контруктор ***//
        public TestRecoilEffect()
        {
            gamer = new Gamer() {
                Force = 0
            };

            recoilEffect = new RecoilEffect(gamer) {
                ModifiableAttributes = {                    { "Recoil", 5}                },
                TimeLife = 1000
            };
        }
        

        //*** Методы ***//

        [TestMethod]
        public void Init__Ok()
        {
            RecoilEffect recoilEffect = new RecoilEffect(gamer);
            Assert.AreEqual(recoilEffect.ModifiableAttributes["Recoil"], 0);
        }

        [TestMethod]
        public void Process__Ok()
        {
            recoilEffect.ModifiableAttributes["Recoil"] = 5;
            Assert.AreEqual(recoilEffect.ModifiableAttributes["Recoil"], 5);

            recoilEffect.Process();

            Assert.AreEqual(recoilEffect.ModifiableAttributes["Recoil"], 4.9);
        }

        [TestMethod]
        public void Process__Force()
        {
            recoilEffect.ModifiableAttributes["Recoil"] = 5;
            Assert.AreEqual(recoilEffect.ModifiableAttributes["Recoil"], 5);

            gamer.Force = 10;
            recoilEffect.Process();

            Assert.AreEqual(recoilEffect.ModifiableAttributes["Recoil"], 4.7);
        }
    }
}