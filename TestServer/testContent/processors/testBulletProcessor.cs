﻿using MeduzaServer;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestBulletProcessor 
    {
        
        //*** Свойства ***//
        Gamer gamer;
        Gamer gamer2;
        Gamer gamer3;
        Bullet bullet;
        DataProvider dataProvider;
        BulletProcessor bulletProcessor;
        
        
        //*** Контруктор ***//
        public TestBulletProcessor()
        {
            gamer = new Gamer() {
                Rectangle = new RectangleRay(new Point(100, 200), new Size(25, 30)),
                Health = 100,
                Armor = 0,
            };
            
            gamer2 = new Gamer() {
                Rectangle = new RectangleRay(new Point(60, 200), new Size(25, 30)),
                Health = 100,
                Armor = 0,
            };
            
            gamer3 = new Gamer() {
                Rectangle = new RectangleRay(new Point(20, 200), new Size(25, 30)),
                Health = 100,
                Armor = 0,
            };
            
            bullet = new Bullet() {
                Health = 25,
                HealthStart = 25,
                DamageArmor = 0,
                Damage = 25,
                Rectangle = new RectangleRay(new Point(300, 200), new Size(5, 5), 180),
                ViewsClusterContactsId = 0
            };
            
            dataProvider = new DataProvider(new Size(1000, 2000));
            dataProvider.AddModel(gamer);
            dataProvider.AddModel(gamer2);
            dataProvider.AddModel(gamer3);
            dataProvider.AddModel(bullet);
            
            bulletProcessor = new BulletProcessor(bullet);
        }
        
        
        //*** Методы ***//
        
        [TestMethod]
        public void Process__No_Contacts()
        {
            bulletProcessor.Process(dataProvider, 10);

            Assert.AreEqual(bullet.Rectangle.Location.X, 200);
            Assert.AreEqual(bullet.Rectangle.Location.Y, 200);
            Assert.AreEqual(bullet.ContactsModelObjectId.Count, 0);
        }

        [TestMethod]
        public void Process__Contact()
        {
            bullet.Rectangle.Location.X = 150;
            bulletProcessor.Process(dataProvider, 10);

            Assert.AreEqual(bullet.Rectangle.Location.X, 125);
            Assert.AreEqual(bullet.Rectangle.Location.Y, 200);
            Assert.AreEqual(bullet.ContactsModelObjectId.Count, 1);
            Assert.AreEqual(dataProvider.VisualSpaceCollection.Models.Count, 1);
            Assert.AreEqual(bullet.Health, 0);
            Assert.AreEqual(gamer.Health, 75);
            Assert.AreEqual(bullet.IsDelete, true);
        }

        [TestMethod]
        public void Process__Contact_Add_Bullet_Armor()
        {
            bullet.Rectangle.Location.X = 150;
            bullet.IsDelete = false;
            bullet.Health = 50;
            bullet.HealthStart = 50;
            bullet.DamageArmor = 3;
            bullet.Damage = 25;
            bullet.ContactsModelObjectId = new HashSet<string>();

            gamer.Health = 100;

            dataProvider.ClearAll();
            dataProvider.AddModel(gamer);
            dataProvider.AddModel(gamer2);
            dataProvider.AddModel(gamer3);
            dataProvider.AddModel(bullet);

            bulletProcessor.Process(dataProvider, 10);

            Assert.AreEqual(bullet.Rectangle.Location.X, 85);
            Assert.AreEqual(bullet.Rectangle.Location.Y, 200);
            Assert.AreEqual(bullet.ContactsModelObjectId.Count, 2);
            Assert.AreEqual(dataProvider.VisualSpaceCollection.Models.Count, 2);
            Assert.AreEqual(bullet.Health, 0);
            Assert.AreEqual(gamer.Health, 45);
            Assert.AreEqual(gamer2.Health < 100, true);
            Assert.AreEqual(bullet.IsDelete, true);
        }

        [TestMethod]
        public void Process__Distance()
        {
            bullet.Rectangle.Location.X = 150;
            bullet.IsDelete = false;
            bullet.Health = 50;
            bullet.HealthStart = 50;
            bullet.DamageArmor = 3;
            bullet.Damage = 25;
            bullet.Distance = 150;
            bullet.Speed = 1;
            bullet.ContactsModelObjectId = new HashSet<string>();

            dataProvider.ClearAll();
            dataProvider.AddModel(bullet);

            bulletProcessor.Process(dataProvider, 100);
            bulletProcessor.Process(dataProvider, 100);

            Assert.AreEqual(bullet.IsDelete, true);
        }
    }
}
