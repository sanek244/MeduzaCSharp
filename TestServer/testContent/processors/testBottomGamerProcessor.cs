﻿using MeduzaServer;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestBottomGamerProcessor
    {

        //*** Свойства ***//
        Gamer gamer;
        BottomGamerProcessor bottomGamerProcessor;

        //*** Контруктор ***//
        public TestBottomGamerProcessor()
        {
            gamer = new Gamer();
            bottomGamerProcessor = new BottomGamerProcessor(gamer);
        }

        //*** Методы ***//

        [TestMethod]
        public void Process__Up()
        {
            gamer.Rectangle.RotateAngle = 270;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 270);
        }
        [TestMethod]
        public void Process__Up_2()
        {
            gamer.Rectangle.RotateAngle = 350;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 270);
        }
        [TestMethod]
        public void Process__Up_3()
        {
            gamer.Rectangle.RotateAngle = 190;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 270);
        }

        [TestMethod]
        public void Process__Up_Rotate_Back()
        {
            gamer.Rectangle.RotateAngle = 90;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 90);
        }
        [TestMethod]
        public void Process__Up_Rotate_Back_2()
        {
            gamer.Rectangle.RotateAngle = 10;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 90);
        }
        [TestMethod]
        public void Process__Up_Rotate_Back_3()
        {
            gamer.Rectangle.RotateAngle = 170;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 90);
        }



        [TestMethod]
        public void Process__Up_Right()
        {
            gamer.Rectangle.RotateAngle = 300;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 315);
        }
        [TestMethod]
        public void Process__Up_Right_2()
        {
            gamer.Rectangle.RotateAngle = 230;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 315);
        }
        [TestMethod]
        public void Process__Up_Right_3()
        {
            gamer.Rectangle.RotateAngle = 35;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 315);
        }

        [TestMethod]
        public void Process__Up_Right_Rotate_Back()
        {
            gamer.Rectangle.RotateAngle = 200;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 135);
        }
        [TestMethod]
        public void Process__Up_Right_Rotate_Back_2()
        {
            gamer.Rectangle.RotateAngle = 135;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 135);
        }
        [TestMethod]
        public void Process__Up_Right_Rotate_Back_3()
        {
            gamer.Rectangle.RotateAngle = 50;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 135);
        }

        [TestMethod]
        public void Process__Up_Left()
        {
            gamer.Rectangle.RotateAngle = 225;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 225);
        }
        [TestMethod]
        public void Process__Up_Left_2()
        {
            gamer.Rectangle.RotateAngle = 300;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 225);
        }
        [TestMethod]
        public void Process__Up_Left_3()
        {
            gamer.Rectangle.RotateAngle = 140;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 225);
        }

        [TestMethod]
        public void Process__Up_Left_Rotate_Back()
        {
            gamer.Rectangle.RotateAngle = 45;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 45);
        }
        [TestMethod]
        public void Process__Up_Left_Rotate_Back_2()
        {
            gamer.Rectangle.RotateAngle = 320;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 45);
        }
        [TestMethod]
        public void Process__Up_Left_Rotate_Back_3()
        {
            gamer.Rectangle.RotateAngle = 130;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(38, true);
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 45);
        }

        [TestMethod]
        public void Process__Down()
        {
            gamer.Rectangle.RotateAngle = 90;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 90);
        }
        [TestMethod]
        public void Process__Down_2()
        {
            gamer.Rectangle.RotateAngle = 15;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 90);
        }
        [TestMethod]
        public void Process__Down_3()
        {
            gamer.Rectangle.RotateAngle = 170;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 90);
        }

        [TestMethod]
        public void Process__Down_Rotate_Back()
        {
            gamer.Rectangle.RotateAngle = 270;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 270);
        }
        [TestMethod]
        public void Process__Down_Rotate_Back_2()
        {
            gamer.Rectangle.RotateAngle = 190;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 270);
        }
        [TestMethod]
        public void Process__Down_Rotate_Back_3()
        {
            gamer.Rectangle.RotateAngle = 350;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 270);
        }

        [TestMethod]
        public void Process__Down_Right()
        {
            gamer.Rectangle.RotateAngle = 45;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 45);
        }
        [TestMethod]
        public void Process__Down_Right_2()
        {
            gamer.Rectangle.RotateAngle = 320;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 45);
        }
        [TestMethod]
        public void Process__Down_Right_3()
        {
            gamer.Rectangle.RotateAngle = 130;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 45);
        }

        [TestMethod]
        public void Process__Down_Right_Rotate_Back()
        {
            gamer.Rectangle.RotateAngle = 225;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 225);
        }
        [TestMethod]
        public void Process__Down_Right_Rotate_Back_2()
        {
            gamer.Rectangle.RotateAngle = 140;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 225);
        }
        [TestMethod]
        public void Process__Down_Right_Rotate_Back_3()
        {
            gamer.Rectangle.RotateAngle = 310;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 225);
        }

        [TestMethod]
        public void Process__Down_Left()
        {
            gamer.Rectangle.RotateAngle = 135;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 135);
        }
        [TestMethod]
        public void Process__Down_Left_2()
        {
            gamer.Rectangle.RotateAngle = 49;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 135);
        }
        [TestMethod]
        public void Process__Down_Left_3()
        {
            gamer.Rectangle.RotateAngle = 220;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 135);
        }

        [TestMethod]
        public void Process__Down_Left_Rotate_Back()
        {
            gamer.Rectangle.RotateAngle = 315;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 315);
        }
        [TestMethod]
        public void Process__Down_Left_Rotate_Back_2()
        {
            gamer.Rectangle.RotateAngle = 229;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 315);
        }
        [TestMethod]
        public void Process__Down_Left_Rotate_Back_3()
        {
            gamer.Rectangle.RotateAngle = 40;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(40, true);
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 315);
        }

        [TestMethod]
        public void Process__Left()
        {
            gamer.Rectangle.RotateAngle = 180;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 180);
        }
        [TestMethod]
        public void Process__Left_2()
        {
            gamer.Rectangle.RotateAngle = 99;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 180);
        }
        [TestMethod]
        public void Process__Left_3()
        {
            gamer.Rectangle.RotateAngle = 260;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 180);
        }

        [TestMethod]
        public void Process__Left_Rotate_Back()
        {
            gamer.Rectangle.RotateAngle = 0;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 0);
        }
        [TestMethod]
        public void Process__Left_Rotate_Back_2()
        {
            gamer.Rectangle.RotateAngle = 80;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 0);
        }
        [TestMethod]
        public void Process__Left_Rotate_Back_3()
        {
            gamer.Rectangle.RotateAngle = 277;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(37, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 0);
        }


        [TestMethod]
        public void Process__Right()
        {
            gamer.Rectangle.RotateAngle = 0;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 0);
        }
        [TestMethod]
        public void Process__Right_2()
        {
            gamer.Rectangle.RotateAngle = 277;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 0);
        }
        [TestMethod]
        public void Process__Right_3()
        {
            gamer.Rectangle.RotateAngle = 80;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 0);
        }

        [TestMethod]
        public void Process__Right_Rotate_Back()
        {
            gamer.Rectangle.RotateAngle = 180;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 180);
        }
        [TestMethod]
        public void Process__Right_Rotate_Back_2()
        {
            gamer.Rectangle.RotateAngle = 99;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 180);
        }
        [TestMethod]
        public void Process__Right_Rotate_Back_3()
        {
            gamer.Rectangle.RotateAngle = 260;
            gamer.KeysMap = new List<ushort>();
            gamer.ToggleKey(39, true);
            bottomGamerProcessor.Process(null, 100);
            Assert.AreEqual(gamer.ItemsCluster.Bottom.Rectangle.RotateAngle, 180);
        }
    }
}
