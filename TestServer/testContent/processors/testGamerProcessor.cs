﻿using MeduzaServer;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestGamerProcessor 
    {
        
        //*** Свойства ***//
        DataProvider dataProvider;
        Gamer gamer;
        GamerProcessor gamerProcessor;
        
        
        
        //*** Контруктор ***//
        public TestGamerProcessor()
        {
            dataProvider = new DataProvider(new Size(1000, 2000));
            
            gamer = new Gamer() {
                Name = "gamer",
                Rectangle = new RectangleRay(new Point(100, 200), new Size(25, 30)),
                KeysMap = new List<ushort>() { 38 },
                Speed = 1
            };
            gamerProcessor = new GamerProcessor(gamer);
        }
        
        
        //*** Методы ***//
        
        [TestMethod]
        public void Process__Up_Relocation()
        {
            gamerProcessor.Process(dataProvider, 100);
            Assert.AreEqual(gamer.Rectangle.Location.X, 100);
            Assert.AreEqual(gamer.Rectangle.Location.Y, 100);
        }
    }
}
