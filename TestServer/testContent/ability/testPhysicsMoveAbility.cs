using MeduzaServer;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestPhysicsMoveAbility
    {

        //*** �������� ***//
        DataProvider dataProvider;
        Gamer gamer, gamer2;
        Building building, building2;
        PhysicsMoveAbility physicsMoveAbility;


        //*** ���������� ***//
        public TestPhysicsMoveAbility()
        {
            dataProvider = new DataProvider(new Size(1000, 2000));

            gamer = new Gamer() {
                Name = "gamer",
                Rectangle = new RectangleRay(new Point(100, 200), new Size(25, 30))
            };

            gamer2 = new Gamer() {
                Name = "gamer2",
                Rectangle = new RectangleRay(new Point(130, 200), new Size(25, 30))
            };

            building = new Building() {
                Name = "Building",
                Rectangle = new RectangleRay(new Point(50, 240), new Size(500, 700)),
            };

            building2 = new Building() {
                Name = "Building2",
                Rectangle = new RectangleRay(new Point(100, 185), new Size(10, 10)),
            };

            physicsMoveAbility = new PhysicsMoveAbility();

            dataProvider.AddModel(gamer);
            dataProvider.AddModel(gamer2);
            dataProvider.AddModel(building);
            dataProvider.AddModel(building2);
        }


        //*** ������ ***//

        [TestMethod]
        public void Activation__Contact()
        {
            physicsMoveAbility.Activation(dataProvider, gamer, 100, new Dictionary<string, object>() { { "dx", 0 }, { "dy", 0.3 } });
            Assert.AreEqual(gamer.Rectangle.Location.X, 100);
            Assert.AreEqual(gamer.Rectangle.Location.Y, 210);
        }

        [TestMethod]
        public void Activation__Empty_Space()
        {
            gamer.Rectangle.Location = new Point(100, 200);
            physicsMoveAbility.Activation(dataProvider, gamer, 100, new Dictionary<string, object>() { { "dx", -0.3 }, { "dy", 0 } });
            Assert.AreEqual(gamer.Rectangle.Location.X, 70);
            Assert.AreEqual(gamer.Rectangle.Location.Y, 200);
        }
    }
}