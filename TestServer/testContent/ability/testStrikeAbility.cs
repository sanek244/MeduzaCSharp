using System.Linq;
using MeduzaServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestStrikeAbility
    {

        //*** Свойства ***//
        Weapon weapon;
        StrikeAbility strikeAbility;
        DataProvider dataProvider;
        Gamer gamer;


        //*** Контруктор ***//
        public TestStrikeAbility()
        {
            weapon = new Weapon() {
                RapidityFire = 100,
                SoundClusterWeaponId = 0,
                ViewType = "ViewsClusterWeapon",
                ViewId = 0,
                Recoil = 100,
                ShotSpeed = 120,
                Damage = 20,
                BulletId = 0
            };

            dataProvider = new DataProvider(new Size(1000, 2000));

            gamer = new Gamer() {
                Name = "gamer",
                Rectangle = new RectangleRay(new Point(100, 200), new Size(25, 30))
            };

            strikeAbility = new StrikeAbility(weapon);
        }


        //*** Методы ***//

        [TestMethod]
        public void Activation__AttackLastTime()
        {
            dataProvider.ClearAll();
            strikeAbility.Weapon.HolderVolumeCurrent = 0;
            strikeAbility.Weapon.AttackLastTime = Helper.GetUnixTime();
            var res = strikeAbility.Activation(dataProvider, gamer, 1, null);
            Assert.AreEqual(res, false);
            Assert.AreEqual(dataProvider.SoundSpaceCollection.Models.Count, 0);
        }

        [TestMethod]
        public void Activation__AttackLastTime_2()
        {
            dataProvider.ClearAll();
            strikeAbility.Weapon.HolderVolumeCurrent = 0;
            strikeAbility.Weapon.AttackLastTime = Helper.GetUnixTime() - (uint)strikeAbility.Weapon.RapidityFire + 10;
            var res = strikeAbility.Activation(dataProvider, gamer, 1, null);
            Assert.AreEqual(res, false);
            Assert.AreEqual(dataProvider.SoundSpaceCollection.Models.Count, 0);
        }

        [TestMethod]
        public void Activation__AttackLastTime_3()
        {
            dataProvider.ClearAll();
            strikeAbility.Weapon.HolderVolumeCurrent = 0;
            strikeAbility.Weapon.AttackLastTime = Helper.GetUnixTime() - (uint)strikeAbility.Weapon.RapidityFire - 10;
            var res = strikeAbility.Activation(dataProvider, gamer, 1, null);
            Assert.AreEqual(res, false);
            Assert.AreEqual(dataProvider.SoundSpaceCollection.Models.Count, 1);
        }

        [TestMethod]
        public void Activation__HolderVolumeCurrent()
        {
            dataProvider.ClearAll();
            strikeAbility.Weapon.HolderVolumeCurrent = 0;
            var res = strikeAbility.Activation(dataProvider, gamer, 1, null);
            Assert.AreEqual(res, false);
            Assert.AreEqual(dataProvider.SoundSpaceCollection.Models.Count, 1);
        }

        [TestMethod]
        public void Activation__HolderVolumeCurrent_None()
        {
            dataProvider.ClearAll();
            var weapon2 = new Weapon() {
                RapidityFire = 100,
                SoundClusterWeaponId = 0
            };

            weapon2.SoundClusterWeapon.EmptyShotId = -1;
            StrikeAbility strikeAbility2 = new StrikeAbility(weapon2);
            strikeAbility2.Weapon.HolderVolumeCurrent = 0;
            var res = strikeAbility2.Activation(dataProvider, gamer, 1, null);
            Assert.AreEqual(res, false);
            Assert.AreEqual(dataProvider.SoundSpaceCollection.Models.Count, 0);
        }

        [TestMethod]
        public void Activation__Create()
        {
            dataProvider.ClearAll();
            strikeAbility.Weapon.HolderVolumeCurrent = 10;
            strikeAbility.Activation(dataProvider, gamer, 1, null);

            var keysVisualSpaceCollection = dataProvider.VisualSpaceCollection.Models;
            Assert.AreEqual(keysVisualSpaceCollection.Count, 1);
            Assert.AreEqual(dataProvider.VisualSpaceCollection.Models[keysVisualSpaceCollection.First().Value.ObjectId].ViewId, 1);
            var keysSoundSpaceCollection = dataProvider.SoundSpaceCollection.Models;
            Assert.AreEqual(keysSoundSpaceCollection.Count, 1);
            Assert.AreEqual(dataProvider.SoundSpaceCollection.Models[keysSoundSpaceCollection.First().Value.ObjectId].Sound.Name, "mp5_1_shot");
            Assert.AreEqual(gamer.Effects.Count, 1);
            Assert.AreEqual(gamer.Effects["RecoilEffect"].ClassName, "RecoilEffect");
            Assert.AreEqual(gamer.Effects["RecoilEffect"].ModifiableAttributes["Recoil"], strikeAbility.Weapon.Recoil);

            var keysBulletSpaceCollection = dataProvider.BulletSpaceCollection.Models.Keys.ToList();
            Assert.AreEqual(keysBulletSpaceCollection.Count, 1);
            Assert.AreEqual(dataProvider.BulletSpaceCollection.Models[keysBulletSpaceCollection[0]].ClassName, "Bullet");
            Assert.AreEqual(dataProvider.BulletSpaceCollection.Models[keysBulletSpaceCollection[0]].Speed, strikeAbility.Weapon.ShotSpeed);
            Assert.AreEqual(strikeAbility.Weapon.HolderVolumeCurrent, 9);
            Assert.AreEqual(strikeAbility.Weapon.AttackLastTime > Helper.GetUnixTime() - 10, true);
        }
    }
}
