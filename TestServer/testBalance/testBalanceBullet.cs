using System;
using MeduzaServer;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestServer
{
    [TestClass]
    public class TestBullet_Balance 
    {
        GameController gameController = new GameController(0); //0 - id тестового полигона
        List<Gamer> gamers = new List<Gamer>();


        [TestMethod]
        public void Damage_Without_Armor()
        {
            foreach(var x in new int[] { 200, 250, 300 })
            {
                gamers.Add(new Gamer() {                    Rectangle = new RectangleRay(new Point(x, 200), new Size(25, 25)),                    Health = 100,                    Armor = 0                });            }

            Bullet bullet = new Bullet() {                Rectangle = new RectangleRay(new Point(100, 200), new Size(5, 5)),                Health = 100,                DamageArmor = 5,                Damage = 15            };

            foreach(var gamer in gamers) {
                gameController.AddModel(gamer);
            }

            gameController.AddModel(bullet);
            gameController.LastTimeComplete = Helper.GetUnixTime() - 100;
            gameController.Process();

            foreach(var gamer in gamers)
            {
                Console.WriteLine(String.Format("Gamer хп:{0} armor:{1} x:{2} y:{3}", gamer.Health, gamer.Armor, gamer.Rectangle.Location.X, gamer.Rectangle.Location.Y));
            }
            Console.WriteLine(String.Format("Bulvar хп:{0} armorDamage:{1} damage:{2} x:{3} y:{4}", bullet.Health, bullet.DamageArmor, bullet.Damage, bullet.Rectangle.Location.X, bullet.Rectangle.Location.Y));
        }
    }
}