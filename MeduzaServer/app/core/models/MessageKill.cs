﻿namespace MeduzaServer
{
    public class MessageKill : SimpleGameObject
    {

        //*** Свойства ***//
        /// <summary>
        /// Убийца
        /// </summary>
        public string KillerName { get; set; }
        /// <summary>
        /// Жертва
        /// </summary>
        public string VictimName { get; set; }
        /// <summary>
        /// Чем убили
        /// </summary>
        public string KillObjectName { get; set; }
        /// <summary>
        /// Чем убили (картинка)
        /// </summary>
        public Img KillObjectImg { get; set; }


        //*** Конструкторы ***//
        public MessageKill()
        {
            ClientAttributes.Clear();
            ClientAttributes.Add("killerName");
            ClientAttributes.Add("victimName");
            ClientAttributes.Add("killObjectName");
            ClientAttributes.Add("KillObjectImg.path");
        }
    }
}
