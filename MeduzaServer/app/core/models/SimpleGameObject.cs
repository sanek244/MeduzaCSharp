﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace MeduzaServer
{
    /// <summary>
    /// Простой игровой объект, не имеет физической оболочки (не подчиняется игровым законам физики)
    /// </summary>
    public class SimpleGameObject : ActiveModel
    {
        private int viewId = -1;
        private string viewType;
        private string objectId;
        private List<IProcessor> processors;
        private List<string> clientAttributes;

        //*** Свойства ***//
        /// <summary>
        /// Прямоугольник модели
        /// </summary>
        public virtual Rectangle Rectangle { get; set; }
        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public string ObjectId { get => objectId; }
        /// <summary>
        /// Имя объекта
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Русскоязычная надпись описывающая сущность
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// Пора удалять?
        /// </summary>
        public bool IsDelete { get; set; }
        /// <summary>
        /// id визуального представления (Animation, Img, ViewsCluster, ...)
        /// </summary>
        public int ViewId
        {
            get => viewId;
            set
            {
                viewId = value;
                if(ViewType != "") {
                    View = JsonConvert.DeserializeObject<Img>(Application.DBController.FindOne(ViewType, value).ToString());
                }
            }
        }
        /// <summary>
        /// Тип визуального представления (Animation, Img, ViewsCluster, ...) 
        /// ClassName
        /// </summary>
        public string ViewType
        {
            get => viewType;
            set
            {
                viewType = value;
                if (ViewId != -1) {
                    View = JsonConvert.DeserializeObject<Img>(Application.DBController.FindOne(value, ViewId).ToString());
                }
            }
        }
        /// <summary>
        /// Визуальное представление
        /// </summary>
        public Img View { get; set; }
        /// <summary>
        /// Состояние объекта (бег, открытие двери,удар с левой, ...)
        /// </summary>
        public TypeState State { get; set; }
        /// <summary>
        /// Обработчики модели
        /// </summary>
        public List<IProcessor> Processors { get => processors; }
        /// <summary>
        /// Атрибуты для отправки на клиент
        /// </summary>
        public List<string> ClientAttributes { get => clientAttributes; }
        

        //*** Конструкторы ***//
        public SimpleGameObject()
        {
            objectId = Helper.GenerateUid();
            Rectangle = new Rectangle();
            Name = "";
            Id = -1;
            Label = "";
            IsDelete = false;
            ViewType = "";
            State = TypeState.Base;
            processors = new List<IProcessor>();
            clientAttributes = new List<string>() {
                 "objectId",
                 "isDelete",
                 "rectangle.rotateAngle",
                 "rectangle.location.x",
                 "rectangle.location.y",
                 "viewId",
                 "viewType",
                 "state",
            };
        }


        //*** Методы ***//
        /// <summary>
        /// Изменились ли аттрибуты, отправляемые клиенту?
        /// </summary>
        /// <returns></returns>
        public bool IsChangedSendAttributes()
        {
            foreach(string attributeName in ClientAttributes) {
                if (СhangesAttributes.IndexOf(attributeName) > -1) {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Переводит аттрибуты модели, указанные в clientAttributes, в JSON строку для отправки на клиент
        /// </summary>
        /// <returns></returns>
        public string ToJSONinStringForClient()
        {
            string res = "{";

            foreach (string clientAttribute in ClientAttributes) {
                var value = Get(clientAttribute);
                string separateValue = value.GetType() == typeof(string) ? "\"" : "";
                res += "\"" + clientAttribute.Split('.').Last() + "\":" + separateValue + value + separateValue + ",";
            }

            return res.Substring(0, res.Length - 1) + "}";
        }


        //Alias Алиасы
        /// <summary>
        /// Содержится ли точка внутри данной модели?
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool IsContainsPoint(Point point)
        {
            return Rectangle.IsContainsPoint(point);
        }
        /// <summary>
        /// Проверка на содержание модели внутри себя
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool IsContainsModel(SimpleGameObject model)
        {
            return Rectangle.IsContainsModel(model);
        }
        /// <summary>
        /// Проверка на пересечение с моделью
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool IsIntersectionOrContainsModel(SimpleGameObject model)
        {
            return Rectangle.IsIntersectionOrContainsModel(model);
        }
    }
}
