﻿
namespace MeduzaServer
{
    /// <summary>
    /// Персонаж
    /// </summary>
    public class Personage : GameObject
    {
        private int itemsClusterId = -1;

        //*** Свойства ***//
        /// <summary>
        /// Расса - люди, монстры, инопланетяне
        /// </summary>
        public TypeRace Race { get; set; }
        /// <summary>
        /// Отдача
        /// </summary>
        public float Recoil { get; set; }
        /// <summary>
        /// id набора вещей
        /// </summary>
        public int ItemsClusterId
        {
            get => itemsClusterId;
            set
            {
                itemsClusterId = value;
                ItemsCluster = Application.DBController.FindOne<ItemsCluster>(value);
            }
        }
        /// <summary>
        /// Набор вещей + части тела
        /// </summary>
        public ItemsCluster ItemsCluster { get; set; }
        

        //*** Контроллеры ***//
        public Personage()
        {
            Material = TypeMaterial.Bio;
            ClientAttributes.Add("recoil");
            ItemsCluster = new ItemsCluster();
        }
    }
}
