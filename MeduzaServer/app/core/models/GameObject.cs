﻿using System.Collections.Generic;
using System.Linq;

namespace MeduzaServer
{
    /// <summary>
    /// Игровой объект имеющий физическую оболочку (подчиняется игровым законам физики)
    /// </summary>
    abstract public class GameObject : SimpleGameObject, IAim
    {
        //*** Свойства ***//
        /// <summary>
        /// Здоровье
        /// </summary>
        public float Health { get; set; }
        /// <summary>
        /// Начальный уровень здоровья
        /// </summary>
        public int HealthStart { get; set; }
        /// <summary>
        /// Урон
        /// </summary>
        public float Damage { get; set; }
        /// <summary>
        /// Бронебойность
        /// </summary>
        public float DamageArmor { get; set; }
        /// <summary>
        /// Радиус урона
        /// </summary>
        public float DamageRadius { get; set; }
        /// <summary>
        /// Броня
        /// </summary>
        public float Armor { get; set; }
        /// <summary>
        /// Сила - для толканий и смещений при налёте на объекты
        /// </summary>
        public int Force { get; set; }
        /// <summary>
        /// Скорость
        /// </summary>
        public float Speed { get; set; }
        /// <summary>
        /// Материал объекта
        /// </summary>
        public TypeMaterial Material { get; set; }
        /// <summary>
        /// Наложенные эффекты
        /// </summary>
        public Dictionary<string, BaseEffect> Effects { get; set; }
        /// <summary>
        /// Способности
        /// Ассоциативный массив вида: [key => Ability]
        /// </summary>
        public Dictionary<ushort, BaseAbility> Abilities { get; set; }
        /// <summary>
        /// Точка прицеливания
        /// </summary>
        public virtual Point AimPoint { get; set; }
        /// <summary>
        /// Прямоугольная область обозначающая объект с лучами по краям
        /// </summary>
        public new RectangleRay Rectangle { get; set; }


        //*** Конструкторы ***//
        public GameObject()
        {
            Rectangle = new RectangleRay();
            Effects = new Dictionary<string, BaseEffect>();
            Abilities = new Dictionary<ushort, BaseAbility>();
            AimPoint = new Point();

            ClientAttributes.Add("health");
            ClientAttributes.Add("healthStart");
        }


        //*** Методы ***//
        /// <summary>
        /// Получает точки пересечения границ объекта с лучём
        /// </summary>
        /// <param name="ray">входящий луч</param>
        /// <param name="isRaySegment">Входящий луч отрезок?</param>
        /// <returns></returns>
        public List<Point> GetPointsIntersectionRays(Ray ray, bool isRaySegment = false)
        {
            return new List<Point>() {
                Rectangle.TopRay.GetPointIntersectionRay(ray, true, isRaySegment),
                Rectangle.BottomRay.GetPointIntersectionRay(ray, true, isRaySegment),
                Rectangle.LeftRay.GetPointIntersectionRay(ray, true, isRaySegment),
                Rectangle.RightRay.GetPointIntersectionRay(ray, true, isRaySegment),
            }
            .Where(point => point != null)
            .ToList();
        }

        /// <summary>
        /// Получает наиближайшую точку пересечения границы объекта с лучём
        /// </summary>
        /// <param name="ray">входящий луч</param>
        /// <param name="isRaySegment">Входящий луч отрезок?</param>
        /// <returns></returns>
        public Point GetBesidePointIntersectionRays(Ray ray, bool isRaySegment = false)
        {
            var angleRotate = Helper.GetRotateAngle(ray.StartPoint, ray.SecondPoint);
            var contacts = new List<Point>();

            if (angleRotate <= 90) {
                contacts.Add(Rectangle.TopRay.GetPointIntersectionRay(ray, true, isRaySegment));
                contacts.Add(Rectangle.LeftRay.GetPointIntersectionRay(ray, true, isRaySegment));
            }
            else if (angleRotate > 90 && angleRotate <= 180) {
                contacts.Add(Rectangle.TopRay.GetPointIntersectionRay(ray, true, isRaySegment));
                contacts.Add(Rectangle.RightRay.GetPointIntersectionRay(ray, true, isRaySegment));
            }
            else if (angleRotate > 180 && angleRotate <= 270) {
                contacts.Add(Rectangle.BottomRay.GetPointIntersectionRay(ray, true, isRaySegment));
                contacts.Add(Rectangle.RightRay.GetPointIntersectionRay(ray, true, isRaySegment));
            }
            else if (angleRotate > 270) {
                contacts.Add(Rectangle.BottomRay.GetPointIntersectionRay(ray, true, isRaySegment));
                contacts.Add(Rectangle.LeftRay.GetPointIntersectionRay(ray, true, isRaySegment));
            }

            contacts = contacts.Where(point => point != null).ToList();

            return contacts.Count != 0
                ? contacts[0]
                : null;
        }

        /// <summary>
        /// Добавить эффект
        /// </summary>
        /// <param name="effect"></param>
        public void AddEffect(BaseEffect effect)
        {
            effect.Model = this;
            effect.Start();

            //поиск уже добавленного такого эффекта
            if (Effects.ContainsKey(effect.ClassName)){
                var res = Effects[effect.ClassName].Merge(effect);
                if (res == false) {
                    string newId = "";
                    do {
                        newId = effect.ClassName + '_' + Helper.rnd.Next();
                    } while (Effects.ContainsKey(newId));
                    Effects.Add(newId, effect);
                }
            }
            else{
                Effects.Add(effect.ClassName, effect);
            }
        }


        /// <summary>
        /// Уничтожение объекта (можно тут запускать анимации гибели объекта)
        /// </summary>
        public virtual void Destroy()
        {

        }
    }
}
