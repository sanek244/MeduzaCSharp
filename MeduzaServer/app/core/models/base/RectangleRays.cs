﻿
namespace MeduzaServer
{
    /// <summary>
    /// Прямоугольная область с лучами - границами
    /// </summary>
    public class RectangleRay : Rectangle
    {
        //для нахождения пересечений с ругими моделями
        private Ray leftRay;
        private Ray rightRay;
        private Ray topRay;
        private Ray bottomRay;

        private Point location;
        private Size size;
        private float rotateAngle;

        //*** Свойства ***//
        /// <summary>
        /// Левый луч траектории объекта
        /// </summary>
        public Ray LeftRay
        {
            get
            {
                if(leftRay == null) {
                    leftRay = new Ray(Location, Helper.GetAngleRightTriangle(Location, RotateAngle, Size.Width));
                }

                return leftRay;
            }
        }
        /// <summary>
        /// Правый луч траектории объекта
        /// </summary>
        public Ray RightRay
        {
            get
            {
                if (rightRay == null) {
                    rightRay = new Ray(TopRay.SecondPoint, Helper.GetAngleRightTriangle(TopRay.SecondPoint, RotateAngle + 90, Size.Height));
                }

                return rightRay;
            }
        }
        /// <summary>
        /// Верхний луч траектории объекта
        /// </summary>
        public Ray TopRay
        {
            get
            {
                if (topRay == null) {
                    topRay = new Ray(RightRay.SecondPoint, Helper.GetAngleRightTriangle(RightRay.SecondPoint, RotateAngle + 180, Size.Width));
                }

                return topRay;
            }
        }
        /// <summary>
        /// Нижний луч траектории объекта
        /// </summary>
        public Ray BottomRay
        {
            get
            {
                if (bottomRay == null) {
                    bottomRay = new Ray(BottomRay.SecondPoint, Location);
                }

                return bottomRay;
            }
        }
        /// <summary>
        /// Функция вызываемая при изменении значений
        /// </summary>
        public CallbackChange Callback { get; set; }

        public new Point Location
        {
            get => location;
            set
            {
                location = new Point(ClearRays, value);
                ClearRays();
            }
        }
        public new Size Size
        {
            get => size;
            set
            {
                size = new Size(ClearRays, value);
                ClearRays();
            }
        }
        public new float RotateAngle
        {
            get => rotateAngle;
            set
            {
                rotateAngle = value;
                ClearRays();
            }
        }


        //*** Конструкторы ***//
        public RectangleRay()
        {
            Callback = () => { };
            Location = new Point();
            Size = new Size();
        }
        public RectangleRay(SimpleGameObject simpleGameObject) : this()
        {
            Location.X = simpleGameObject.Rectangle.Location.X;
            Location.Y = simpleGameObject.Rectangle.Location.Y;
            Size.Width = simpleGameObject.Rectangle.Size.Width;
            Size.Height = simpleGameObject.Rectangle.Size.Height;
            RotateAngle = simpleGameObject.Rectangle.RotateAngle;
        }
        public RectangleRay(Point location) : this()
        {
            Location.X = location.X;
            Location.Y = location.Y;
        }
        public RectangleRay(Point location, Size size) : this(location)
        {
            Size.Width = size.Width;
            Size.Height = size.Height;
        }
        public RectangleRay(Point location, Size size, float rotateAngle) : this(location, size)
        {
            RotateAngle = rotateAngle;
        }
        public RectangleRay(CallbackChange callback) : this()
        {
            Callback = callback;
        }
        public RectangleRay(CallbackChange callback, RectangleRay rectangle) : this(callback)
        {
            location = new Point(ClearRays, rectangle.Location);
            size = new Size(ClearRays, rectangle.Size);
            rotateAngle = rectangle.RotateAngle;
        }


        //*** Методы ***//
        /// <summary>
        /// Очистка значений лучей + вызов callback функции
        /// </summary>
        public void ClearRays()
        {
            leftRay = null;
            rightRay = null;
            topRay = null;
            bottomRay = null;
            Callback();
        }
    }
}
