﻿using System;

namespace MeduzaServer
{
    /// <summary>
    /// Луч
    /// </summary>
    public class Ray
    {
        //*** Закрытые переменные ***//
        /// <summary>
        /// Коэфициент из уравнения a*x + b*y + c
        /// </summary>
        private double a;
        /// <summary>
        /// Коэфициент из уравнения a*x + b*y + c
        /// </summary>
        private double b;
        /// <summary>
        /// Коэфициент из уравнения a*x + b*y + c
        /// </summary>
        private double c;

        /// <summary>
        /// Прирост луча по x между начальной и второй точкой
        /// </summary>
        private double dx;
        /// <summary>
        /// Прирост луча по y между начальной и второй точкой
        /// </summary>
        private double dy;

        /// <summary>
        /// Точка начала луча
        /// </summary>
        private Point startPoint;
        /// <summary>
        /// Вторая точка луча (для создания прямой)
        /// </summary>
        private Point secondPoint;


        //*** Свойства ***//
        public Point StartPoint
        {
            set {
                startPoint = value;
                ChangeKofs();
            }
            get { return startPoint; }
        }
        public Point SecondPoint
        {
            set
            {
                secondPoint = value;
                ChangeKofs();
            }
            get { return secondPoint; }
        }

        public double Dx { get { return dx; } }
        public double Dy { get { return dy; } }


        //*** Конструкторы ***//
        public Ray()
        {
            StartPoint = new Point();
            SecondPoint = new Point();
        }
        public Ray(Point startPoint, Point secondPoint) : this()
        {
            SecondPoint = secondPoint;
            StartPoint = startPoint;
        }


        //*** Методы ***//
        /// <summary>
        /// Возвращает x - точку пересечения ординаты (y) с лучём
        /// </summary>
        /// <param name="y"></param>
        /// <returns></returns>
        public double GetX(double y)
        {
            return (-b * y - c) / a;
        }
        /// <summary>
        /// Возвращает y - точку пересечения абсциссы(x) с лучём
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public double GetY(double x)
        {
            return (-a * x - c) / b;
        }

        /// <summary>
        /// Возвращает расстояние от прямой до точки
        /// </summary>
        /// <param name="point">Точка, до которой измеряется расстояние</param>
        /// <returns></returns>
        public double GetDistanceToPoint(Point point)
        {
            return Math.Abs(a * point.X + b * point.Y + c) / Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
        }

        /// <summary>
        /// Возвращает точку пересечения прямых
        /// </summary>
        /// <param name="ray">Луч с которым надо найти пересечение</param>
        /// <param name="isThisSegment">текущий луч - отрезок с границами в startPoint и secondPoint?</param>
        /// <param name="isRaySegment">входящий луч - отрезок с границами в startPoint и secondPoint?</param>
        /// <returns>null, если прямые параллельны или совпадают</returns>
        public Point GetPointIntersectionRay(Ray ray, bool isThisSegment = false, bool isRaySegment = false)
        {
            //делитель
            double divider = a * ray.b - ray.a * b;
            if (divider == 0) {
                return null;
            }

            //Поиск адекватного x и y
            double y = (ray.a * c - ray.c * a) / (-ray.a * b + ray.b * a);
            double x = (-b * y - c) / a;

            if (double.IsNaN(x) || double.IsNaN(y)) {
                y = (a * ray.c - c * ray.a) / (-a * ray.b + b * ray.a);
                x = (-ray.b * y - ray.c) / ray.a;
            }

            if (double.IsNaN(x) || double.IsNaN(y)) {
                y = -(c * ray.b - ray.c * b / divider);
                x = -(a * ray.c - ray.a * c / divider);
            }

            //точка пересечения лучей
            Point point = new Point((float)x, (float)y);

            //Если текущий луч - отрезок
            if (isThisSegment) {
                float minX = Math.Min(startPoint.X, secondPoint.X);
                float maxX = Math.Max(startPoint.X, secondPoint.X);
                float minY = Math.Min(startPoint.Y, secondPoint.Y);
                float maxY = Math.Max(startPoint.Y, secondPoint.Y);

                //точка пересечения лучей за границами текущего отрезка?
                if (point.X > maxX || point.X < minX || point.Y > maxY || point.Y < minY) {
                    return null;
                }
            }

            //Если входящий луч - отрезок
            if (isRaySegment) {
                float minX = Math.Min(ray.startPoint.X, ray.secondPoint.X);
                float maxX = Math.Max(ray.startPoint.X, ray.secondPoint.X);
                float minY = Math.Min(ray.startPoint.Y, ray.secondPoint.Y);
                float maxY = Math.Max(ray.startPoint.Y, ray.secondPoint.Y);

                //точка пересечения лучей за границами входящего отрезка?
                if (point.X > maxX || point.X < minX || point.Y > maxY || point.Y < minY) {
                    return null;
                }
            }

            return point;
        }


        /// <summary>
        /// Изменение коэффициентов
        /// </summary>
        private void ChangeKofs()
        {
            //B = x1 - x2
            //A = y2 - y1
            //C = x2 * y1 - x1 * y2
            b = startPoint.X - secondPoint.X;
            c = secondPoint.X * startPoint.Y - startPoint.X * secondPoint.Y;
            a = secondPoint.Y - startPoint.Y;

            double distance = Helper.Distance(startPoint, secondPoint);
            dx = (secondPoint.X - startPoint.X) / distance;
            dy = (secondPoint.Y - startPoint.Y) / distance;
        }
    }
}
