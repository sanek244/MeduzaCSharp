﻿namespace MeduzaServer
{
    /// <summary>
    /// Звук
    /// </summary>
    public class Sound : ActiveModel
    {

        //*** Свойства ***//
        //rectangle.Location = центр звука
        /// <summary>
        /// Путь до звука
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Расширние файла
        /// </summary>
        public string Extension { get; set; }
        /// <summary>
        /// Радиус
        /// </summary>
        public int Radius { get; set; }
        /// <summary>
        /// Мощность
        /// </summary>
        public int Volume { get; set; }
        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }
    }
}
