﻿
namespace MeduzaServer
{
    /// <summary>
    /// Размер
    /// </summary>
    public class Size : BaseObject
    {
        private float width;
        private float height;


        //*** Свойства ***//
        /// <summary>
        /// Ширина
        /// </summary>
        public float Width
        {
            get => width;
            set
            {
                width = value;
                Callback();
            }
        }
        /// <summary>
        /// Высота
        /// </summary>
        public float Height
        {
            get => height;
            set
            {
                height = value;
                Callback();
            }
        }
        /// <summary>
        /// Функция вызываемая при изменении значений
        /// </summary>
        public CallbackChange Callback { get; set; }


        //*** Конструкторы ***//
        public Size()
        {
            Callback = () => { };
        }
        public Size(float width) : this()
        {
            Width = width;
        }
        public Size(float Width, float height) : this(Width)
        {
            Height = height;
        }
        public Size(CallbackChange callback)
        {
            Callback = callback;
        }
        public Size(CallbackChange callback, Size size) : this(callback)
        {
            width = size.Width;
            height = size.Height;
        }
    }
}
