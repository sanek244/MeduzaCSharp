﻿
namespace MeduzaServer
{
    /// <summary>
    /// Изображение
    /// </summary>
    public class Img : ActiveModel
    {

        //*** Свойства ***//
        /// <summary>
        /// Путь до папки с изображением
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Приоритет в прорисовке
        /// Приоритет: earth, items, transport, bottom, body, (weapon, bullet), backBody, head, flyAnimals
        /// </summary>
        public int ZIndex { get; set; }
        /// <summary>
        /// Размер
        /// </summary>
        public Size Size { get; set; }
        /// <summary>
        /// Расширение файла
        /// </summary>
        public string FileExtension { get; set; }
        /// <summary>
        /// Тип изображения (для клиента через ToJSONString)
        /// </summary>
        public string Type { get => ClassName; }
        /// <summary>
        /// Сдвиг в расположении (для изменения в коде)
        /// </summary>
        public Point ShiftLocation { get; set; }
    }
}
