﻿
namespace MeduzaServer
{
    /// <summary>
    /// Квадрат
    /// </summary>
    public class Square
    {
        //*** Свойства ***//
        /// <summary>
        /// Верхний левый угол
        /// </summary>
        public Point LeftTop { get; set; }
        /// <summary>
        /// Верхний правый угол
        /// </summary>
        public Point RightTop { get; set; }
        /// <summary>
        /// Нижний левый угол
        /// </summary>
        public Point LeftBottom { get; set; }
        /// <summary>
        /// Нижний правый угол
        /// </summary>
        public Point RightBottom { get; set; }
    }
}
