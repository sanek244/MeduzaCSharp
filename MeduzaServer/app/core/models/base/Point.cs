﻿
namespace MeduzaServer
{
    /// <summary>
    /// Точка в пространстве
    /// </summary>
    public class Point : BaseObject
    {
        private float x;
        private float y;


        //*** Свойства ***//
        public float X
        {
            get => x;
            set
            {
                x = value;
                Callback();
            }
        }
        public float Y
        {
            get => y;
            set
            {
                y = value;
                Callback();
            }
        }
        /// <summary>
        /// Функция вызываемая при изменении значений
        /// </summary>
        public CallbackChange Callback { get; set; }


        //*** Конструкторы ***//
        public Point()
        {
            Callback = () => { };
        }
        public Point(float x) : this()
        {
            X = x;
        }
        public Point(float x, float y) : this(x)
        {
            Y = y;
        }
        public Point(CallbackChange callback)
        {
            Callback = callback;
        }
        public Point(CallbackChange callback, Point point) : this(callback)
        {
            x = point.X;
            y = point.Y;
        }
        public Point(Point point) : this()
        {
            X = point.X;
            Y = point.Y;
        }


        //*** Операторы ***//
        /// <summary>
        /// Оператор сложения
        /// </summary>
        /// <param name="pointA"></param>
        /// <param name="pointB"></param>
        /// <returns></returns>
        static public Point operator + (Point pointA, Point pointB)
        {
            return new Point(pointA.X + pointB.X, pointA.Y + pointB.Y);
        }
    }
}
