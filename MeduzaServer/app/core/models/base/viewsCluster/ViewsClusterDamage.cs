﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Набор изображений на разные уровни жизни
    /// </summary>
    public class ViewsClusterDamage : ViewsCluster
    {

        //*** Свойства ***//
        /// <summary>
        /// Оторажения на разные уровни здоровья
        /// </summary>
        public List<Img> Damages { get; set; }
    }
}
