﻿namespace MeduzaServer
{
    /// <summary>
    /// Набор изображений столкновений пули
    /// </summary>
    public class ViewsClusterBulletContacts : ActiveModel
    {
        private Animation concrete;
        private Animation tree;
        private Animation bio;
        private Animation iron;

        private int concreteId = -1;
        private int treeId = -1;
        private int bioId = -1;
        private int ironId = -1;


        //*** Свойства ***///
        /// <summary>
        /// id изображения столкновения с бетоном
        /// </summary>
        public int ConcreteId {
            get => concreteId;
            set
            {
                concreteId = value;
                concrete = Application.DBController.FindOne<Animation>(value);
            }
        }
        /// <summary>
        /// Изображение столкновения пули с бетоном
        /// </summary>
        public Animation Concrete { get => concrete; }

        /// <summary>
        /// id изображения столкновения с деревом
        /// </summary>
        public int TreeId
        {
            get => treeId;
            set
            {
                treeId = value;
                tree = Application.DBController.FindOne<Animation>(value);
            }
        }
        /// <summary>
        /// Изображение столкновения пули с деревом
        /// </summary>
        public Animation Tree { get => tree; }

        /// <summary>
        /// id изображения столкновения с биологической сущностью
        /// </summary>
        public int BioId
        {
            get => bioId;
            set
            {
                bioId = value;
                bio = Application.DBController.FindOne<Animation>(value);
            }
        }
        /// <summary>
        /// Изображение столкновения пули с биологической сущностью
        /// </summary>
        public Animation Bio { get => bio; }

        /// <summary>
        /// id изображения столкновения с металлом
        /// </summary>
        public int IronId
        {
            get => ironId;
            set
            {
                ironId = value;
                iron = Application.DBController.FindOne<Animation>(value);
            }
        }
        /// <summary>
        /// Изображение столкновения пули с металлом
        /// </summary>
        public Animation Iron { get => iron; }
    }
}
