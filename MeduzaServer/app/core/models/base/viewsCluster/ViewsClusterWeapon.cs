﻿namespace MeduzaServer
{
    /// <summary>
    /// Набор изображений оружия
    /// </summary>
    public class ViewsClusterWeapon : ViewsCluster
    {
        private Animation shot;
        private int shotId = -1;


        //*** Свойства ***///
        /// <summary>
        /// id выстрела
        /// </summary>
        public int ShotId
        {
            get => shotId;
            set
            {
                shotId = value;
                shot = Application.DBController.FindOne<Animation>(value);
            }
        }
        /// <summary>
        /// Изображение выстрела
        /// </summary>
        public Animation Shot { get => shot; }
    }
}
