﻿
namespace MeduzaServer
{
    /// <summary>
    /// Столкновение моделей
    /// </summary>
    public class Contact : BaseObject
    {

        //*** Свойства ***//
        /// <summary>
        /// Модель с которой было совершено столкновение
        /// </summary>
        public GameObject Model { get; set; }
        /// <summary>
        /// Точка столкновения
        /// </summary>
        public Point PointContact { get; set; }
        /// <summary>
        /// Расстояние от места старта модели, до точки столкновения
        /// </summary>
        public float Distance { get; set; }


        //*** Конструкторы ***//
        public Contact(GameObject model)
        {
            Model = model;
        }
        public Contact(GameObject model, Point pointContact) : this(model)
        {
            PointContact = pointContact;
        }
        public Contact(GameObject model, Point pointContact, float distance) : this(model, pointContact)
        {
            Distance = distance;
        }
    }
}
