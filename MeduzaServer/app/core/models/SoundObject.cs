﻿
using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Объект звука на карте
    /// </summary>
    public class SoundObject : SimpleGameObject
    {
        //*** Свойства ***//
        /// <summary>
        /// Звук
        /// </summary>
        public Sound Sound { get; set; }


        //*** Конструкторы ***//
        public SoundObject()
        {
            Sound = new Sound();
            ClientAttributes.Clear();
            ClientAttributes.Add("objectId");
            ClientAttributes.Add("sound.volume");
            ClientAttributes.Add("sound.id");
            ClientAttributes.Add("sound.radius");
            ClientAttributes.Add("rectangle.location.x");
            ClientAttributes.Add("rectangle.location.y");
            ClientAttributes.Add("isDelete");
        }
        public SoundObject(Sound sound) : this()
        {
            Sound = sound;
        }
    }
}
