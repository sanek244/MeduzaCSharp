﻿
namespace MeduzaServer
{
    /// <summary>
    /// Позиция появления игроков 
    /// </summary>
    public class StartPosition : SimpleGameObject
    {

        //*** Свойства ***//
        /// <summary>
        /// Раса
        /// Расса - люди, монстры, инопланетяне
        /// </summary>
        public TypeRace Race { get; set; }
        /// <summary>
        /// Свободено место?
        /// </summary>
        public bool IsFree { get; set; }
    }
}
