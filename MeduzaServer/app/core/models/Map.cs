﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Карта
    /// </summary>
    public class Map : ActiveModel
    {
        private int viewId = -1;


        //*** Свойства ***//
        /// <summary>
        /// Имя объекта
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Русскоязычная надпись описывающая сущность
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// id изображения
        /// </summary>
        public int ViewId
        {
            get => viewId;
            set
            {
                viewId = value;
                View = Application.DBController.FindOne<Img>(value);
            }
        }
        /// <summary>
        /// Изображение
        /// </summary>
        public Img View { get; set; }
        /// <summary>
        /// Описание карты
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Максимальное количество игроков
        /// </summary>
        public int MaxPlayers { get; set; }
        /// <summary>
        /// Размер карты
        /// </summary>
        public Size Size { get; set; }
        /// <summary>
        /// Точки появления игроков
        /// </summary>
        public List<StartPosition> StartPoints { get; set; }

        /// <summary>
        /// Выдаёт объекты расположенные на карте по умолчанию
        /// </summary>
        /// <returns></returns>
        public List<object> GetDefaultObjects()
        {
            return Application.DBController.LoadMap(Name);
        }
    }
}
