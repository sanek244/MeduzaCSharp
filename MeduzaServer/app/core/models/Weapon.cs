﻿
namespace MeduzaServer
{
    /// <summary>
    /// Оружие
    /// </summary>
    public class Weapon : SimpleGameObject
    {
        private int soundClusterWeaponId = -1;
        private int bulletId = -1;

        //*** Свойства ***//
        /// <summary>
        /// Урон
        /// </summary>
        public int Damage { get; set; }
        /// <summary>
        /// Радиус урона
        /// </summary>
        public int DamageRadius { get; set; }
        /// <summary>
        /// Скорострельность - миллисекунд между выстрелами
        /// </summary>
        public int RapidityFire { get; set; }
        /// <summary>
        /// Скорость выстрела (кол-во пикселей в секунду)
        /// </summary>
        public int ShotSpeed { get; set; }
        /// <summary>
        /// Объём обоймы
        /// </summary>
        public int HolderVolume { get; set; }
        /// <summary>
        /// Количество патрон в обойме
        /// </summary>
        public int HolderVolumeCurrent { get; set; }
        /// <summary>
        /// Время перезарядки (в миллисекундах)
        /// </summary>
        public int ReloadingTime { get; set; }
        /// <summary>
        /// Точка создания боеприпаса
        /// </summary>
        public Point BulletCreatePoint { get; set; }
        /// <summary>
        /// Тип разброса
        /// </summary>
        public TypeScatter ScatterType { get; set; }
        /// <summary>
        /// Максимальный разброс
        /// </summary>
        public int ScatterMax { get; set; }
        /// <summary>
        /// Разброс
        /// </summary>
        public int Scatter { get; set; }
        /// <summary>
        /// Отдача
        /// </summary>
        public int Recoil { get; set; }
        /// <summary>
        /// Тип перезарядки
        /// </summary>
        public TypeRecharge RechargeType { get; set; }
        /// <summary>
        /// Тип держания
        /// </summary>
        public TypeHolding HoldingType { get; set; }
        /// <summary>
        /// id боеприпаса
        /// </summary>
        public int BulletId { get => BulletId; set => bulletId = value; }
        /// <summary>
        /// Время последней атаки
        /// </summary>
        public ulong AttackLastTime { get; set; }
        /// <summary>
        /// id кластера звуков
        /// </summary>
        public int SoundClusterWeaponId
        {
            get => soundClusterWeaponId;
            set
            {
                soundClusterWeaponId = value;
                SoundClusterWeapon = Application.DBController.FindOne<SoundClusterWeapon>(value);
            }
        }
        /// <summary>
        /// Кластер звуков
        /// </summary>
        public SoundClusterWeapon SoundClusterWeapon { get; set; }


        //*** Конструкторы ***//
        public Weapon()
        {
            BulletCreatePoint = new Point();
        }
    }
}
