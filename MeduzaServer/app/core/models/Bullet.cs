﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public class Bullet : GameObject
    {
        private int viewsClusterContactsId = -1;
        private int soundClusterContactsId = -1;
        private ViewsClusterBulletContacts viewsClusterContacts;
        private SoundClusterBulletContacts soundClusterContacts;

        //*** Свойства ***//
        /// <summary>
        /// Точка создания
        /// </summary>
        public Point PointStart { get; set; }
        /// <summary>
        /// Дальность полёта
        /// </summary>
        public int Distance { get; set; }
        /// <summary>
        /// id кластера изображений столкновения
        /// </summary>
        public int ViewsClusterContactsId
        {
            get => viewsClusterContactsId;
            set
            {
                viewsClusterContactsId = value;
                viewsClusterContacts = Application.DBController.FindOne<ViewsClusterBulletContacts>(value);
            }
        }
        /// <summary>
        /// Кластер изображений столкновения
        /// </summary>
        public ViewsClusterBulletContacts ViewsClusterContacts { get => viewsClusterContacts; }
        /// <summary>
        /// id кластера звуков столкновения
        /// </summary>
        public int SoundClusterContactsId
        {
            get => soundClusterContactsId;
            set
            {
                soundClusterContactsId = value;
                soundClusterContacts = Application.DBController.FindOne<SoundClusterBulletContacts>(value);
            }
        }
        /// <summary>
        /// Кластер звуков столкновения
        /// </summary>
        public SoundClusterBulletContacts SoundClusterContacts { get => soundClusterContacts; }
        /// <summary>
        /// ObjectId объектов в которые было совершено попадание
        /// Игнор список, если пуля уцелеет. 
        /// </summary>
        public HashSet<string> ContactsModelObjectId { get; set; }

        //*** Конструкторы ***//
        public Bullet()
        {
            Processors.Add(new BulletProcessor(this));
        }


        //*** Методы ***//
        /// <summary>
        /// Возвращает бронебойность пули после попадания в модель
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public float GetDamageArmorBeforeClashModel(GameObject model)
        {
            return Bullet.GetDamageArmorBeforeClashModel(DamageArmor, model);
        }

        /// <summary>
        /// Возвращает бронебойность пули после попадания в модель
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static float GetDamageArmorBeforeClashModel(float damageArmorBullet, GameObject model)
        {
            var damageArmor = damageArmorBullet - (model.Armor + model.Health / 100);
            return damageArmor < 0 ? 0 : damageArmor;
        }
    }
}
