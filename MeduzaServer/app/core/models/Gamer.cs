﻿using System;
using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Игрок
    /// </summary>
    public class Gamer : Personage
    {
        //*** Свойства ***//
        /// <summary>
        /// Имя игрока
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// Время последнего действия
        /// </summary>
        public ulong LastTimeAction { get; set; }
        /// <summary>
        /// id сокет соединения
        /// </summary>
        public string SocketId { get; set; }
        /// <summary>
        /// Нажатые клавиши
        /// </summary>
        public List<ushort> KeysMap { get; set; }
        /// <summary>
        /// Точка прицеливания
        /// </summary>
        public override Point AimPoint
        {
            get => base.AimPoint;
            set
            {
                base.AimPoint = value;

                var difX = Rectangle.Location.X - AimPoint.X;
                var difY = Rectangle.Location.Y - AimPoint.Y;
                var rotateAngle = (Math.Atan(difY / difX) + (difX >= 0 ? Math.PI : 0)) * 180 / Math.PI;
                Rectangle.RotateAngle = (float)rotateAngle;
            }
        }


        //*** Конструкторы ***//
        public Gamer()
        {
            Rectangle = new RectangleRay(ChangeBodyParts);
            ClientAttributes.Add("nickname");
            Processors.Add(new GamerProcessor(this));
            Processors.Add(new BottomGamerProcessor(this));
        }


        //*** Методы ***//
        /// <summary>
        /// Изменение положения частей тела в зависимости от текущей модели
        /// </summary>
        public void ChangeBodyParts()
        {
            ChangeBodyPart(ItemsCluster.BackBody);
            ChangeBodyPart(ItemsCluster.Body);
            ChangeBodyPart(ItemsCluster.Bottom, false);
            ChangeBodyPart(ItemsCluster.Head);
            ChangeBodyPart(ItemsCluster.LeftHand);
            ChangeBodyPart(ItemsCluster.RightHand);
            ChangeBodyPart(ItemsCluster.Hands);
        }
        /// <summary>
        /// Изменение положения части тела в зависимости от текущей модели
        /// </summary>
        /// <param name="bodyPart">Изменяемая часть тела</param>
        /// <param name="isChangeRotate">Повернуть?</param>
        public void ChangeBodyPart(SimpleGameObject bodyPart, bool isChangeRotate = true)
        {
            if (bodyPart.ViewId != -1) {
                var shiftLocation = Helper.GetAngleRightTriangle(
                    Rectangle.Location,
                    Rectangle.RotateAngle + Helper.GetRotateAngle(Rectangle.Location, View.ShiftLocation),
                    (float)Helper.Distance(Rectangle.Location, View.ShiftLocation));

                bodyPart.Rectangle.Location = shiftLocation + Rectangle.Location;

                if (isChangeRotate) {
                    bodyPart.Rectangle.RotateAngle = Rectangle.RotateAngle;
                }
            }
        }

        /// <summary>
        /// Переключение клавиш
        /// </summary>
        /// <param name="keyCode">код клавишы</param>
        /// <param name="isAdd">добавить?</param>
        public void ToggleKey(ushort keyCode, bool isAdd)
        {
            if (isAdd) {
                if (KeysMap.IndexOf(keyCode) == -1) {
                    KeysMap.Add(keyCode);
                }
            }
            else {
                if (KeysMap.IndexOf(keyCode) != -1) {
                    KeysMap.Remove(keyCode);
                }
            }

            LastTimeAction = Helper.GetUnixTime();
        }

        /// <summary>
        /// Загрузка данных персонажа
        /// </summary>
        /// <param name="personageId"></param>
        public void LoadPersonage(int personageId)
        {
            var personage = Application.DBController.FindOne<Personage>(personageId);
            Race = personage.Race;
            ViewType = personage.ViewType;
            ViewId = personage.ViewId;
            ItemsClusterId = personage.ItemsClusterId;
            Speed = personage.Speed;
            HealthStart = personage.HealthStart;
            Health = personage.Health;
            Armor = personage.Armor;
            Damage = personage.Damage;
            DamageArmor = personage.DamageArmor;
            DamageRadius = personage.DamageRadius;
            Force = personage.Force;
            Abilities = personage.Abilities;
        }
    }
}
