﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeScatter { Increasing, Equable };

    public static class EnumScatter
    {
        public static Dictionary<TypeScatter, string> labels = new Dictionary<TypeScatter, string>() {
            { TypeScatter.Increasing, "Возрастающий" },
            { TypeScatter.Equable, "Равномерный" },
        };
    }
}
