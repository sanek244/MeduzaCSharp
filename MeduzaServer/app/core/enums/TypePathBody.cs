﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypePathBody { Body, Head, Bottom, BackBody };

    public static class EnumPathBody
    {
        public static Dictionary<TypePathBody, string> labels = new Dictionary<TypePathBody, string>() {
            { TypePathBody.Body, "Тело" },
            { TypePathBody.Head, "Голова" },
            { TypePathBody.Bottom, "Ноги" },
            { TypePathBody.BackBody, "Сзади тела (хвост, плащ)" }, 
        };
    }
}