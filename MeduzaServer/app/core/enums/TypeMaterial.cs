﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeMaterial { Tree, Bio, Iron, Concrete };

    public static class EnumMaterial
    {
        public static Dictionary<TypeMaterial, string> labels = new Dictionary<TypeMaterial, string>() {
            { TypeMaterial.Tree, "Дерево" },
            { TypeMaterial.Bio, "Органика" },
            { TypeMaterial.Iron, "Железо" },
            { TypeMaterial.Concrete, "Бетон" },
        };
    }
}