﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeState { Base, Run, BlowLateral, BlowDirect, Recoil, Run_recoil };

    public static class EnumState
    {
        public static Dictionary<TypeState, string> labels = new Dictionary<TypeState, string>() {
            { TypeState.Base, "Базовое" },
            { TypeState.Run, "Бег" },
            { TypeState.BlowLateral, "Отдача" },
            { TypeState.BlowDirect, "Боковой удар" },
            { TypeState.Recoil, "Прямой удар" },
            { TypeState.Run_recoil, "Бег и отдача" },
        };
    }
}