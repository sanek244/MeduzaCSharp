﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeProjectMode { Development, Production };

    public static class EnumProjectMode
    {
        public static Dictionary<TypeProjectMode, string> labels = new Dictionary<TypeProjectMode, string>() {
            { TypeProjectMode.Development, "Проект в разработке" },
            { TypeProjectMode.Production, "Проект опубликован" },
        };
    }
}
