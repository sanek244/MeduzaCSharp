﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Абстракция для контроллеров базы данных
    /// </summary>
    public abstract class DBController
    {
        //*** Свойства ***//
        /// <summary>
        /// Возвращает версию данных
        /// </summary>
        /// <returns></returns>
        public abstract ulong VersionData { get; }


        //*** Методы ***//
        /// <summary>
        /// Возвращает все объекты T, удовлетворящие условию where
        /// </summary>
        /// <typeparam name="T">Тип искомого объекта</typeparam>
        /// <param name="where">Условие отбора</param>
        /// <returns></returns>
        public abstract List<T> Find<T>(Dictionary<string, string> where) where T : ActiveModel, new();

        /// <summary>
        /// Возвращает первый найденный объект T, удовлетворящий условию where
        /// </summary>
        /// <typeparam name="T">Тип искомого объекта</typeparam>
        /// <param name="where">Условие отбора</param>
        /// <returns></returns>
        public abstract T FindOne<T>(Dictionary<string, string> where) where T : ActiveModel, new();

        /// <summary>
        /// Возвращает первый найденный объект T, суказанным id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="idModel">id модели</param>
        /// <returns></returns>
        public abstract T FindOne<T>(int idModel) where T : ActiveModel, new();

        /// <summary>
        /// Возвращает первый найденный объект T, суказанным id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName">Имя таблицы откуда взять модель</param>
        /// <param name="idModel">id модели</param>
        /// <returns></returns>
        public abstract object FindOne(string tableName, int idModel);

        /// <summary>
        /// Добавляет модель в бд
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        public abstract void AddModel<T>(T model) where T : ActiveModel, new();

        /// <summary>
        /// Загрузка объектов на карте
        /// </summary>
        /// <param name="name"></param>
        public abstract List<object> LoadMap(string name);
    }
}
