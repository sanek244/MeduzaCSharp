﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace MeduzaServer
{
    /// <summary>
    /// Чтение данных из файлового хранилища
    /// </summary>
    public class FileController : DBController
    {
        /// <summary>
        /// Версия данных
        /// </summary>
        private ulong versionData = 0;


        //*** Свойства ***//
        /// <summary>
        /// Версия данных
        /// </summary>
        public override ulong VersionData {
            get
            {
                if (versionData != 0) {
                    return versionData;
                }

                //Ищем последний изменённый файл
                string currentDirectory = Directory.GetCurrentDirectory();
                string pathDirectory = currentDirectory.Substring(0, currentDirectory.IndexOf("TestServer")) + "MeduzaServer\\fileDb\\";

                foreach (string pathFile in Directory.GetFiles(pathDirectory)) {
                    var mTime = Helper.ToUnixTime(File.GetLastWriteTime(pathFile));
                    if (mTime > versionData) {
                        versionData = mTime;
                    }
                }

                return versionData;
            }
        }


        //*** Методы ***//
        /// <summary>
        /// Возвращает все объекты T, удовлетворящие условию where
        /// </summary>
        /// <typeparam name="T">Тип искомого объекта</typeparam>
        /// <param name="where">Условие отбора</param>
        /// <returns></returns>
        public override List<T> Find<T>(Dictionary<string, string> where)
        {
            List<T> items = LoadFile<T>();
            List<T> result = new List<T>();

            foreach (T item in items) {
                bool ok = true;

                foreach(var whereEl in where) {
                    if(item.Get(whereEl.Key) + "" != whereEl.Value) {
                        ok = false;
                        break;
                    }
                }

                if (ok) {
                    result.Add(item);
                }
            }

            return result;
        }

        /// <summary>
        /// Возвращает первый найденный объект T, удовлетворящий условию where
        /// </summary>
        /// <typeparam name="T">Тип искомого объекта</typeparam>
        /// <param name="where">Условие отбора</param>
        /// <returns></returns>
        public override T FindOne<T>(Dictionary<string, string> where)
        {
            List<T> items = LoadFile<T>();
            List<T> result = new List<T>();

            foreach (T item in items) {
                bool ok = true;

                if(where != null) {
                    foreach (var whereEl in where) {
                        if (item.Get(whereEl.Key) + "" != whereEl.Value) {
                            ok = false;
                            break;
                        }
                    }
                }

                if (ok) {
                    return item;
                }
            }

            return null;
        }

        /// <summary>
        /// Возвращает первый найденный объект T, суказанным id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="idModel">id модели</param>
        /// <returns></returns>
        public override T FindOne<T>(int idModel)
        {
            return FindOne<T>(new Dictionary<string, string>() { { "id", idModel + "" } });
        }

        /// <summary>
        /// Возвращает первый найденный объект T, суказанным id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tableName">Имя таблицы откуда взять модель</param>
        /// <param name="idModel">id модели</param>
        /// <returns></returns>
        public override object FindOne(string tableName, int idModel)
        {
            List<object> items = LoadFile(tableName);
            List<object> result = new List<object>();

            foreach (object item in items) {
                 
                if (JsonConvert.DeserializeObject<ActiveModel>(item.ToString()).Id == idModel) {
                    return item;
                }
            }

            return null;
        }

        /// <summary>
        /// Добавляет модель в файл
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        public override void AddModel<T>(T model)
        {
            var models = LoadFile<T>();
            models.Add(model);
            SaveFile(models);
        }

        /// <summary>
        /// Загрузка объектов на карте
        /// </summary>
        /// <param name="name"></param>
        public override List<object> LoadMap(string name)
        {
            List<object> items = null;
            string currentDirectory = Directory.GetCurrentDirectory();
            string pathFile = currentDirectory.Substring(0, currentDirectory.IndexOf("TestServer")) + "MeduzaServer\\fileDb\\objectsMap\\" + name + ".json";

            using (var stream = new FileStream(pathFile, FileMode.Open)) {
                using (StreamReader r = new StreamReader(stream)) {
                    items = JsonConvert.DeserializeObject<List<object>>(r.ReadToEnd());
                }
            }

            return items;
        }


        /// <summary>
        /// Загрузка содержимого JSON файла
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private List<T> LoadFile<T>() where T : ActiveModel, new()
        {
            List<T> items = null;
            string currentDirectory = Directory.GetCurrentDirectory();
            string pathFile = currentDirectory.Substring(0, currentDirectory.IndexOf("TestServer")) + "MeduzaServer\\fileDb\\" + (new T()).TableName + ".json";

            using (var stream = new FileStream(pathFile, FileMode.Open)) {
                using (StreamReader r = new StreamReader(stream)) {
                    string file = r.ReadToEnd();
                    items = JsonConvert.DeserializeObject<List<T>>(file);
                }
            }

            return items;
        }

        /// <summary>
        /// Загрузка содержимого JSON файла
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        private List<object> LoadFile(string tableName)
        {
            List<object> items = null;
            string currentDirectory = Directory.GetCurrentDirectory();
            string pathFile = currentDirectory.Substring(0, currentDirectory.IndexOf("TestServer")) + "MeduzaServer\\fileDb\\" + tableName + ".json";

            using (var stream = new FileStream(pathFile, FileMode.Open)) {
                using (StreamReader r = new StreamReader(stream)) {
                    items = JsonConvert.DeserializeObject<List<object>>(r.ReadToEnd());
                }
            }

            return items;
        }

        /// <summary>
        /// Сохраняет набор экземпляров в файл
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        private void SaveFile<T>(List<T> items) where T : ActiveModel, new()
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            string pathFile = currentDirectory.Substring(0, currentDirectory.IndexOf("TestServer")) + "MeduzaServer\\fileDb\\" + (new T()).TableName + ".json";

            using (var stream = new FileStream(pathFile, FileMode.Open)) {
                using (StreamWriter r = new StreamWriter(stream)) {
                    r.WriteLine(JsonConvert.SerializeObject(items));
                }
            }
        }
    }
}
