﻿'use strict';

const BaseObject = require('../base/BaseObject');
const Point = require('../models/base/Point');
const GameController = require('./GameController');
const TypeImportanceMessage = require('../enums/TypeImportanceMessage');
const Helper = require('./../Helper');
const Map = require('./../models/Map');
const Gamer = require('./../models/Gamer');
const App = require('../base/Application');
const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

/** @var {number} timeReturn - время на возврат в игру после потери соединения (миллисекунды) */
const timeReturn = 3 * 1000;

/**
 * Контроллирует все подключения и передачу данных
 */
class SocketController extends BaseObject {

	/**
	 * @param {string} dirname
	 * @param {number} port
	 * @param {number} mapId
	 */
	init(dirname, port, mapId){

		// Routing
        app.use(express.static(dirname + '/public_html'));

        app.get('/', (req, res) => {
            res.sendFile(dirname + '/public_html/index.html');
        });

        //Подключаем сокеты и ставим на прослушивание порта
        http.listen(port, function(){
            console.log('listening on *:3000');
        });

		/** @type {object} Игровой контроллер */
		this.gameController = new GameController(mapId);

		//установка всех событий
		this.setEvents();


        this.ping = this.ping.bind(this);
        this.sendAllData = this.sendAllData.bind(this);

        setInterval(this.ping, 1000); //пинг всех каждую секунду
        setInterval(this.sendAllData, 8); //отправка данных игрокам (125 fps)
	}

	/**
	 * Устанавливает все обытия взаимодействия сервера с клиентом при его подключении
	 */
	setEvents(){
		//Событие подключения нового клиента
		io.on('connection', socket => {
			//запрос на получение данных
            socket.on('getMediaData', msg => {
				if(typeof msg === 'number' && msg !== App.get('db').getVersionData()){
					//отправляем игроку все необходимые данные(картинки, звуки)
                    socket.emit('getMediaData', this.gameController.getData());
				}
				else{
					//данные актуальны
                    socket.emit('getMediaData', false);
				}
			});

			//ответ клиента на пинг
            socket.on('ping', msg => {
				const gamer = this.gameController.getGamer(socket.id);
				if(gamer && msg){
					gamer.set('lastTimeAction', new Date().getTime());
				}
			});

			//подключившийся клиент прислал хэш код из cookie
            socket.on('hash', msg => {
				//Проверка msg
				if(msg !== '' && (typeof msg !== 'string' || msg.length !== 36))
				{
                    socket.emit('errors', 'Некорретные данные(хеш код) Очистите cookie и попробуйте снова!');
                    socket.disconnect();

					this.error('constructor', Helper.getDateShort() + 'error! invalid hash: ' + msg, 'error', 'invalid');
				}

				let gamer = this.gameController.getGamerById(msg);

				//если хэша нету или он нормальной длины, но нету модели игрока
				if(msg === '' || !gamer)
				{
					//добавляем модель игрока
					gamer = this.gameController.addGamer(0, 'New name gamer');

					//отправляем игроку его хеш код
                    socket.emit('hash', gamer.get('objectId'));
					//видимые данные
                    socket.emit('view', this.gameController.getView(gamer, true));

					this.log('constructor', Helper.getDateShort() + 'connect - ' + gamer.get('nickname'), TypeImportanceMessage.INFO, 'socket');
				}
				else
				{
					//вторая вкладка или украли печеньки
					if(gamer.get('lastTimeAction') + 10 * 1000 > new Date().getTime())
					{
                        socket.emit('errors', 'Ошибка! Возможно вы открыли игру во второй вкладке браузера! Разрешено играть только в 1 вкладке!');
						this.log('constructor', Helper.getDateShort() + 'Message! multiplay: ' + msg, TypeImportanceMessage.INFO, 'socket');
					}
					else
					{
						//игрок закрыл вкладку/потеря соединения и заново подключается
						gamer.set('lastTimeAction', new Date().getTime());
						gamer.set('socketId', socket.id);

						//видимые данные
                        socket.emit('view', this.gameController.getView(gamer, true));

						this.log('constructor', Helper.getDateShort() + 'reconnect - ' + gamer.get('nickname'), TypeImportanceMessage.INFO, 'socket');
					}
				}
			});

			//Нажатие или отпускание клавиши
            socket.on('keyControl', (msg) => {
				const gamer = this.gameController.getGamer(socket.id);
				if(gamer){
					//максимум код клавиши меньше 1000, всё что больше - мусор и спам или глюки.
					if(typeof msg !== 'string' || msg.length > 11 || msg.indexOf(':') === -1 || msg.split(':')[1].length > 3 || parseInt(msg.split(':')[1]) > 1000 || !(parseInt(msg.split(':')[1]) > 0))
					{
                        socket.emit('errors', 'Некорретные данные(код клавиши)');
						this.gameController.deleteGamer(gamer);
                        socket.disconnect();
						this.log('constructor', Helper.getDateShort() + 'error! invalid keyControl: ' + msg, TypeImportanceMessage.WARRING, 'socket');
					}
					else
					{
						//нажатие клавиши
						const data = msg.split(':');
                        gamer.toggleKey(data[1], data[0] === 'keydown');
					}
				}
			});

			//Нажатие или отпускание клавиши мыши
            socket.on('click', (msg) => {
				const gamer = this.gameController.getGamer(socket.id);
				if(gamer)
				{
					//максимум разрешение экрана 5к по всем осям => 4цифры + 3 знака разделителя + 4 цифры + 3 цифры + 10 букв = 23 знака, всё что больше - мусор и спам или глюки.
					if(msg.length > 23 || msg.indexOf(':') === -1)
					{
                        socket.emit('errors', 'Некорретные данные(координаты мыши)');
						this.gameController.deleteGamer(gamer);
                        socket.disconnect();
						this.log('constructor', Helper.getDateShort() + 'error! invalid click: ' + msg, TypeImportanceMessage.WARRING, 'socket');
					}
					else
					{
						//клик мышкой
						const data = msg.split(':');
						let keysMap = gamer.get('keysMap');
						keysMap[data[3]] = (data[2] === 'mousedown');
						gamer.set('keysMap', keysMap);
						gamer.set('lastTimeAction', new Date().getTime());
						gamer.set('aimPoint', new Point({x: data[0], y: data[1]}));
					}
				}
			});

			//Перемешение мыши
            socket.on('mousemove', (msg) => {
				const gamer = this.gameController.getGamer(socket.id);
				if(gamer)
				{
					//максимум разрешение экрана 5к по всем осям => 4цифры + 1 знак разделитель + 4 цифры = 9 знаков, всё что больше - мусор и спам или глюки.
					if(msg.length > 9 || msg.indexOf(':') === -1)
					{
                        socket.emit('errors', 'Некорретные данные(координаты мыши при перемещении)');
						this.gameController.deleteGamer(gamer);
                        socket.disconnect();
						this.log('constructor', Helper.getDateShort() + 'error! invalid mousemove: ' + msg, TypeImportanceMessage.WARRING, 'socket');
					}
					else
					{
						//перемещение мыши
						const data = msg.split(':');
						gamer.set('lastTimeAction', new Date().getTime());
						gamer.set('aimPoint', new Point({x: data[0], y: data[1]}));
					}
				}
			});

			//Изменение размера окна
            socket.on('resize', () => {
				const gamer = this.gameController.getGamer(socket.id);
				if(gamer)
				{
					gamer.set('keysMap', {});
                    socket.emit('view', this.gameController.getView(gamer, true));
				}
			});
		});
	}

	//Отправка данных игрокам
	sendAllData(){
		//запуск обработки всего
		this.gameController.process();

		this.gameController.getGamers().map(gamerModel => {
			//Визуальное представление
			let data = this.gameController.getViewGamer(gamerModel);
			if(data){
				io.to(gamerModel.get('socketId')).emit('view', data);
			}
		});
	}

	//Пилингование всех пользователей
	ping(){
		this.gameController.getGamers().map(gamerModel => {
			//если не помечен, как вылетивший
			if(gamerModel.get('lastTimeAction') > timeReturn)
			{
				// через 5 сек молчания оповещаем всех
				if(gamerModel.get('lastTimeAction') + 5000 < new Date().getTime()){
					io.emit('disconnect', gamerModel.get('nickname'));
				}

				//пилингуем
				if(gamerModel.get('lastTimeAction') + 1000 < new Date().getTime()){
					const socket = io.to(gamerModel.get('socketId'));
					if(socket){
                        socket.emit('ping', true);
					}
					else
					{
						//если соединения нет
						io.emit('disconnect', gamerModel.get('nickname'));
						this.log('ping', Helper.getDateShort() + 'off: ' + gamerModel.get('nickname'), TypeImportanceMessage.INFO, 'socket');
						//даём время на возврат
						gamerModel.set('lastTimeAction', timeReturn);
					}
				}
			}
			else if(gamerModel.get('lastTimeAction') <= 0) //не успел вернуться
			{
				this.gameController.deleteGamer(gamerModel);
			}
			else //время на возврат
			{
				gamerModel.set('lastTimeAction', gamerModel.get('lastTimeAction') - 1000);
			}
		});
	}
}

module.exports = SocketController; //делаем файл модулем