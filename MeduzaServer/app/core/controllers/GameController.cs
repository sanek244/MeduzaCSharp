﻿using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace MeduzaServer
{
    /// <summary>
    /// Содержит интерфейс для взаимодействия с игрой + обработка всех действий внутренних механизмов
    /// </summary>
    public class GameController : BaseObject
    {
        private DataProvider dataProviderGame;

        //*** Свойства ***//
        /// <summary>
        /// Текущая игровая карта
        /// </summary>
        public Map MapGame { get; set; }
        /// <summary>
        /// Данные (физ объекты + изображения и анимации + звуки + сообщения(игроков и об убийствах))
        /// </summary>
        public DataProvider DataProviderGame { get => dataProviderGame; }
        /// <summary>
        /// Время прошлого выполнения кода
        /// </summary>
        public ulong LastTimeComplete { get; set; }

		/* Для статистики производительности игры */
        /// <summary>
        /// Время запуска контроллера
        /// </summary>
		public ulong TimeStartController { get; set; }
        /// <summary>
        /// Минимальный FPS за всю работу контроллера
        /// </summary>
        public uint MinFPS { get; set; }
        /// <summary>
        /// Максимальный FPS за всю работу контроллера
        /// </summary>
        public uint MaxFPS { get; set; }
        /// <summary>
        /// Сумма FPS за 3 минуты
        /// </summary>
        public uint SumFPS { get; set; }
        /// <summary>
        /// Граф FPS
        /// </summary>
        public List<uint> FPSGraph { get; set; }
        /// <summary>
        /// Время последнего подсчёта FPS за 3 минуты
        /// </summary>
        public ulong LastTimeSecond { get; set; }

        //*** Конструкторы ***//
        public GameController(int mapId)
        {
            MapGame = Application.DBController.FindOne<Map>(mapId);
            dataProviderGame = new DataProvider(MapGame.Size);
            LastTimeComplete = Helper.GetUnixTime();

            //Загрузка объектов карты
            foreach(var objectModel in MapGame.GetDefaultObjects()){
                DataProviderGame.AddModel(objectModel);
            }

            //Для статистики производительности игры
            TimeStartController = Helper.GetUnixTime();
            LastTimeSecond = Helper.GetUnixTime();
            MinFPS = 4000000000;
        }


        //*** Методы ***//
        /// <summary>
        /// Добавление модели в коллекцию
        /// </summary>
        /// <param name="model"></param>
        public void AddModel(object model)
        {
            DataProviderGame.AddModel(model);

            string className = ((ActiveModel)model).ClassName;
            //Добавление частей тела в dataProvider
            if (className == "Personage" || className == "Gamer") {
                Personage personage = model.GetType() == typeof(string) 
                    ? JsonConvert.DeserializeObject<Personage>(model.ToString())
                    : personage = (Personage)model;

                if(personage.ItemsCluster.BackBody != null) {
                    DataProviderGame.AddModel(personage.ItemsCluster.BackBody);
                }
                if (personage.ItemsCluster.Body != null) {
                    DataProviderGame.AddModel(personage.ItemsCluster.BackBody);
                }
                if (personage.ItemsCluster.Bottom != null) {
                    DataProviderGame.AddModel(personage.ItemsCluster.BackBody);
                }
                if (personage.ItemsCluster.Hands != null) {
                    DataProviderGame.AddModel(personage.ItemsCluster.BackBody);
                }
                if (personage.ItemsCluster.Head != null) {
                    DataProviderGame.AddModel(personage.ItemsCluster.BackBody);
                }
                if (personage.ItemsCluster.LeftHand != null) {
                    DataProviderGame.AddModel(personage.ItemsCluster.BackBody);
                }
                if (personage.ItemsCluster.RightHand != null) {
                    DataProviderGame.AddModel(personage.ItemsCluster.BackBody);
                }
            }
        }

        /// <summary>
        /// Добавление нового игрока с никнеймом
        /// </summary>
        /// <param name="personageId">id персонажа</param>
        /// <param name="nickname">имя игрока</param>
        public Gamer AddGamer(int personageId, string nickname)
        {
            var gamer = new Gamer() {
                NickName = nickname
            };

		    gamer.LoadPersonage(personageId);
		    AddModel(gamer);

            //задаём точку появления игрока
            var startPoint = MapGame.StartPoints.Where(point => point.Race == gamer.Race && point.IsFree).First();
            startPoint.IsFree = false;

            gamer.Rectangle.Location = new Point(startPoint.Rectangle.Location);

		    return gamer;
	    }

        /// <summary>
        /// Удаление игрока
        /// </summary>
        /// <param name="gamer"></param>
        public void DeleteGamer(Gamer gamer)
        {
            gamer.IsDelete = true;
        }

        /// <summary>
        /// Возвращает игрока по id соединению
        /// </summary>
        /// <param name="socketId"> id сокета</param>
        /// <returns></returns>
        public Gamer GetGamer(string socketId)
        {
            return DataProviderGame.GamerSpaceCollection.Models.First(el => el.Value.SocketId == socketId).Value;
        }

        /// <summary>
        /// Возвращает игрока по глобальному id
        /// </summary>
        /// <param name="objectId"></param>
        /// <returns></returns>
        public Gamer GetGamerById(string objectId)
        {
            return DataProviderGame.GamerSpaceCollection.Models[objectId];
        }

        /// <summary>
        /// Возвращает всех игроков
        /// </summary>
        /// <returns></returns>
        public List<Gamer> GetGamers()
        {
            return DataProviderGame.GamerSpaceCollection.Models.Values.ToList();
        }

        /// <summary>
        /// Все данные игры (картинки, звуки)
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, List<object>> GetData()
        {
            return new Dictionary<string, List<object>>() {
                { "img", Application.DBController.Find<Img>(null).ToList<object>() },
                { "animation", Application.DBController.Find<Animation>(null).ToList<object>() },
                { "viewsCluster", Application.DBController.Find<ViewsCluster>(null).ToList<object>() },
                { "viewsClusterDamage", Application.DBController.Find<ViewsClusterDamage>(null).ToList<object>() },
                { "viewsClusterPeople", Application.DBController.Find<ViewsClusterPeople>(null).ToList<object>() },
                { "viewsClusterWeapon", Application.DBController.Find<ViewsClusterWeapon>(null).ToList<object>() },
                { "sound", Application.DBController.Find<Sound>(null).ToList<object>() }
            };
        }

        /// <summary>
        /// Очистка всех изменений
        /// </summary>
        public void ClearChanges()
        {
            DataProviderGame.ClearChanges();
        }

        /// <summary>
        /// Получение нового визуального представления для игрка, возвращает пустоту, если представление не изменилось
        /// </summary>
        /// <param name="gamer">Игрок</param>
        /// <param name="force">Всё вернуть?</param>
        /// <returns></returns>
        public Dictionary<string, List<string>> GetViewGamer(Gamer gamer, bool force = false)
        {
            //TODO
            /*const data = this.dataProvider.getView(gamer, force);

            //clear empty category data and toObjectClient
            var empty = true;
		    for(var key in data) {
                if (data.hasOwnProperty(key)) {
                    if (data[key].length === 0) {
                        delete data[key];
                    }
                    else {
                        data[key] = data[key].map(model => model.toObjectClient());
                        empty = false;
                    }
                }
            }

            if (empty) {
                return false;
            }

            if ('sound' in data){
                data.sound = data.sound
                    .map(sound => {
                        const distance = Helper.distance(gamer.get('rectangle.location'), new Point(sound['x'], sound['y']));
                        if (distance > sound['radius']) {
                            return false;
                        }
                        sound['volume'] = (sound['radius'] - distance) / sound['radius'] * sound['volume'];
                        delete sound['x'];
                        delete sound['y'];
                        delete sound['radius'];
                        return sound;
                    })
                    .filter(sound => sound);
            }

            return data;*/

            return new Dictionary<string, List<string>>();
        }

        /// <summary>
        /// Обработка объектов
        /// </summary>
        public void Process()
        {
            //TODO
            /** @type {number} время прошедшее между выполнением кода */
            /*const timeComplete = new Date().getTime() - this.lastTimeComplete;

            this.lastTimeComplete = new Date().getTime();

            this.clearChanges();
            const models = this.dataProvider.physicsSpaceCollection.getModels();

            models.map(model => {
                //обнуление изменений
                model.changesAttributes = [];
                model.set('dx', 0);
                model.set('dy', 0);
                model.set('state', TypeStates.BASE);

                //эффекты
                _forIn(model.get('effects'), effect => {
                    if (effect.get('createdTime') + effect.get('timeLife') < new Date().getTime()) {
                        model.endEffect(effect);
                        effect.end(this.dataProvider);
                    }
                    else {
                        effect.process();
                    }
                });

                //процессоры
                model.get('processors').map(processor => processor.process(this.dataProvider, timeComplete));

                //модель на удаление
                if (model.get('isDelete')) {
                    model.destroy();
                    this.dataProvider.deleteModel(model);
                    return;
                }

                //изменённые модели в особый массив
                if (model.isChangedSendAttributes()) {
                    this.dataProvider.addModelInChanged(model);
                }

                //обновляем квадраты местоположения модели
                if (model.changesAttributes.indexOf('rectangle.location') === 0) {
                    this.dataProvider.updateModel(model);
                }
            });*/

            /* Для статистики производительности игры */
            /*this.sumSecondFPS++;
            if (new Date().getTime() - this.lastTimeSecond > 1000) {
                this.sumFPS += this.sumSecondFPS;
                this.FPSGraph.push(this.sumSecondFPS);
                if (this.sumSecondFPS > this.maxFPS) {
                    this.maxFPS = this.sumSecondFPS;
                }
                if (this.sumSecondFPS < this.minFPS || this.minFPS === null) {
                    this.minFPS = this.sumSecondFPS;
                }
                this.sumSecondFPS = 0;
                this.lastTimeSecond = new Date().getTime();
            }*/
        }
    }
}
