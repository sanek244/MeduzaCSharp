﻿namespace MeduzaServer
{
    public delegate void CallbackChange();
    public delegate T CallbackMap<T>(T item);
}
