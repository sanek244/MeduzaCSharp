﻿using System.Reflection;
using System;

namespace MeduzaServer
{
    /// <summary>
    /// Базовый объект
    /// </summary>
    public class BaseObject
    {
        //*** Свойства ***//
        /// <summary>
        /// Возвращает имя класса
        /// </summary>
        /// <returns></returns>
        public string ClassName
        {
            get => GetType().Name;
        }


        //*** Методы ***//
        /// <summary>
        /// Геттер для всех полей
        /// Использовать только при наличии составного имени!
        /// </summary>
        /// <param name="name">название поля</param>
        /// <returns></returns>
        public object Get(string name)
        {
            //простое
            if (name.IndexOf('.') == -1) {
                //поле
                if (GetType().GetField(name) != null) {
                    return GetType().GetField(name).GetValue(this);
                }
                //свойство
                name = Helper.ToUpperFirst(name);
                return GetType().GetProperty(name).GetValue(this);
            }


            //Имеется цепочка вызовов: model.size.width
            string nameModel = name.Substring(0, name.IndexOf('.'));

            BaseObject model = null;
            if (GetType().GetField(nameModel) != null) {
                //поле
                model = (BaseObject)GetType().GetField(nameModel).GetValue(this);
            }
            else {
                //свойство
                nameModel = Helper.ToUpperFirst(nameModel);
                model = (BaseObject)GetType().GetProperty(nameModel).GetValue(this);
            }

            //рекурсия
            return model.Get(name.Substring(name.IndexOf('.') + 1));
        }

        /// <summary>
        /// Генерация ошибки 
        /// </summary>
        /// <param name="method">Метод вызвавший ошибку</param>
        /// <param name="message">Сообщение</param>
        public void Error(string method, string message)
        {
            if(Application.ProjectMode == TypeProjectMode.Development) {
                throw new Exception(message);
            }
            else {
                Log(method, message, "error");
            }
        }

        /// <summary>
        /// Генерация лога
        /// </summary>
        /// <param name="method">Метод вызвавший логирование</param>
        /// <param name="message">Сообщение</param>
        /// <param name="type">Тип лога</param>
        public void Log(string method, string message, string type = "")
        {
            Application.DBController.AddModel(new Log(ClassName, method, message, type));
        }
    }
}
