﻿using System;

namespace MeduzaServer
{
    public class Log : ActiveModel
    {
        //*** Свойства ***//
        public string Method { get; set; }
        public string Message { get; set; }
        public string Class { get; set; }
        public string Type { get; set; }

        public DateTime CreateTime { get; set; }


        //*** Конструкторы ***//
        public Log()
        {
            CreateTime = DateTime.Now;
            Class = "";
            Method = "";
            Message = "";
            Type = "";
        }
        public Log(string className, string method, string message, string type) : this()
        {
            Class = className;
            Method = method;
            Message = message;
            Type = type;
        }
    }
}
