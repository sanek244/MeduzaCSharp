﻿
namespace MeduzaServer
{
    /// <summary>
    /// Класс приложения - содержит набор классов из конфигураций и параметры конфигурации
    /// </summary>
    public static class Application
    {
        private static DBController dBController = new FileController();
        private static TypeProjectMode projectMode = TypeProjectMode.Development;


        //*** Свойства ***//
        /// <summary>
        /// Контроллер базы данных
        /// </summary>
        public static DBController DBController { get { return dBController; } }

        /// <summary>
        /// Состояние проекта
        /// </summary>
        public static TypeProjectMode ProjectMode { get { return projectMode; } }
    }
}
