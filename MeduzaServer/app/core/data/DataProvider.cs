﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace MeduzaServer
{
    /// <summary>
    ///  Содержит в себе коллекции данных (физ объектов и не физ объектов) и осуществляет оптимальную работу с данными.
    /// </summary>
    public class DataProvider : BaseObject
    {
        //Физические коллекции
        private SpaceCollection<Building> buildingSpaceCollection;
        private SpaceCollection<Bullet> bulletSpaceCollection;
        private SpaceCollection<Gamer> gamerSpaceCollection;

        private SpaceCollection<SimpleGameObject> visualSpaceCollection;
        private SpaceCollection<SoundObject> soundSpaceCollection;
        private SpaceCollection<MessageKill> killMessageSpaceCollection;

        //*** Свойства ***//
        /// <summary>
        /// Коллекция строений
        /// </summary>
        public SpaceCollection<Building> BuildingSpaceCollection { get => buildingSpaceCollection; }
        /// <summary>
        /// Коллекци пуль
        /// </summary>
        public SpaceCollection<Bullet> BulletSpaceCollection { get => bulletSpaceCollection; }

        /// <summary>
        /// Коллекция игроков
        /// </summary>
        public SpaceCollection<Gamer> GamerSpaceCollection { get => gamerSpaceCollection; }
         

        /// <summary>
        /// Коллекция визуальных данных (картинки, анимации)
        /// </summary>
        public SpaceCollection<SimpleGameObject> VisualSpaceCollection { get => visualSpaceCollection; }
        /// <summary>
        /// Коллекция активированных звуков 
        /// </summary>
        public SpaceCollection<SoundObject> SoundSpaceCollection { get => soundSpaceCollection; }
        /// <summary>
        /// Сообщения об убийстве игроками других игроков
        /// </summary>
        public SpaceCollection<MessageKill> KillMessageSpaceCollection { get => killMessageSpaceCollection; }


        //*** Конструкторы ***//
        public DataProvider(Size mapSize)
        {
            buildingSpaceCollection = new SpaceCollection<Building>(mapSize);
            bulletSpaceCollection = new SpaceCollection<Bullet>(mapSize);
            gamerSpaceCollection = new SpaceCollection<Gamer>(mapSize);
            visualSpaceCollection = new SpaceCollection<SimpleGameObject>(mapSize);
            soundSpaceCollection = new SpaceCollection<SoundObject>(mapSize);
            killMessageSpaceCollection = new SpaceCollection<MessageKill>(mapSize);
        }


        //*** Методы ***//
        /// <summary>
        /// Добавление модели в коллекцию
        /// </summary>
        /// <param name="model">Добавляемая модель</param>
        public void AddModel(object model)
        {
            //определяем что пришло - модель или строка в JSON формате
            bool isJSON = false;
            string className = "";
            if (model.ToString()[0] == '{') {
                isJSON = true;
            }
            else {
                className = ((ActiveModel)model).ClassName;
            }

            //Добавляем в нужную коллекцию
            switch (className) {
                case "MessageKill":
                    KillMessageSpaceCollection.AddModel(isJSON ? JsonConvert.DeserializeObject<MessageKill>(model.ToString()) : (MessageKill)model);
                    break;

                case "SoundObject":
                    SoundSpaceCollection.AddModel(isJSON ? JsonConvert.DeserializeObject<SoundObject>(model.ToString()) : (SoundObject)model);
                    break;

                case "SimpleGameObject":
                    VisualSpaceCollection.AddModel(isJSON ? JsonConvert.DeserializeObject<SimpleGameObject>(model.ToString()) : (SimpleGameObject)model);
                    break;

                case "Gamer":
                    GamerSpaceCollection.AddModel(isJSON ? JsonConvert.DeserializeObject<Gamer>(model.ToString()) : (Gamer)model);
                    break;

                case "Building":
                    BuildingSpaceCollection.AddModel(isJSON ? JsonConvert.DeserializeObject<Building>(model.ToString()) : (Building)model);
                    break;

                case "Bullet":
                    BulletSpaceCollection.AddModel(isJSON ? JsonConvert.DeserializeObject<Bullet>(model.ToString()) : (Bullet)model);
                    break;
            }
        }

        /// <summary>
        /// Очистка изменений
        /// </summary>
        public void ClearChanges()
        {
            buildingSpaceCollection.ClearChanges();
            bulletSpaceCollection.ClearChanges();
            gamerSpaceCollection.ClearChanges();
            visualSpaceCollection.ClearChanges();
            soundSpaceCollection.ClearChanges();
            killMessageSpaceCollection.ClearChanges();
        }

        /// <summary>
        /// Очистка всей коллекции
        /// </summary>
        public void ClearAll()
        {
            buildingSpaceCollection.ClearAll();
            bulletSpaceCollection.ClearAll();
            gamerSpaceCollection.ClearAll();
            visualSpaceCollection.ClearAll();
            soundSpaceCollection.ClearAll();
            killMessageSpaceCollection.ClearAll();
        }

        /// <summary>
        /// Возвращает количество всех моделей в коллекции
        /// </summary>
        /// <returns></returns>
        public int GetCountAllObjects()
        {
            return buildingSpaceCollection.Models.Count +
                bulletSpaceCollection.Models.Count +
                gamerSpaceCollection.Models.Count +
                visualSpaceCollection.Models.Count +
                soundSpaceCollection.Models.Count +
                killMessageSpaceCollection.Models.Count;
        }


        /// <summary>
        /// Возвращает представление для выбранного игрока
        /// </summary>
        /// <param name="gamer"></param>
        /// <param name="isforce">вернуть всё? иначе только изменения</param>
        /// <returns></returns>
        public Dictionary<string, List<string>> GetView(Gamer gamer, bool isforce = false)
        {
            var rectangleGamer = new Rectangle(gamer.Rectangle.Location, new Size(1920, 1080));

            var gamers = GamerSpaceCollection.GetObjectModelsInArea(rectangleGamer, true, false, !isforce);
            var bullets = BulletSpaceCollection.GetObjectModelsInArea(rectangleGamer, true, false, !isforce);
            var buildings = BuildingSpaceCollection.GetObjectModelsInArea(rectangleGamer, true, false, !isforce);
            var visualObjects = visualSpaceCollection.GetObjectModelsInArea(rectangleGamer, true, false, !isforce);

            Size sizeAreaKillMessage = new Size(
                rectangleGamer.Size.Width * 10, //увеличиваем площадь сбора сообщений в 10 раз
                rectangleGamer.Size.Height * 10);

            return new Dictionary<string, List<string>>() {
                { "sound", SoundSpaceCollection.GetObjectModelsInArea(rectangleGamer, true, false, !isforce) },
                { "objects", gamers.Concat(bullets).Concat(buildings).Concat(visualObjects).ToList() },
                { "messagesKill", KillMessageSpaceCollection.GetObjectModelsInArea(
                    new Rectangle(gamer.Rectangle.Location, sizeAreaKillMessage), true, false, !isforce) }
            };
        }

        /// <summary>
        /// Возвращает все физические объекты под абстрактным классом GameObject
        /// </summary>
        /// <returns></returns>
        public List<GameObject> GetPhysicalModelsInArea(Rectangle area, bool isAreaLocationCenter = false, bool isExactly = false, bool isOnlyChanged = false, string excludeObjectId = "")
        {
            var gamers = GamerSpaceCollection.GetObjectModelsInArea(area, isAreaLocationCenter, isExactly, isOnlyChanged, excludeObjectId);
            var bullets = BulletSpaceCollection.GetObjectModelsInArea(area, isAreaLocationCenter, isExactly, isOnlyChanged, excludeObjectId);
            var buildings = BuildingSpaceCollection.GetObjectModelsInArea(area, isAreaLocationCenter, isExactly, isOnlyChanged, excludeObjectId);
            var physicalObjects = new List<GameObject>();

            foreach(var model in gamers.Concat(bullets).Concat(buildings)) {
                physicalObjects.Add(JsonConvert.DeserializeObject<GameObject>(model));
            }

            return physicalObjects;
        }

        /// <summary>
        /// Возвращает первые модели с которыми было совершено столкновение при переходе модели из текущео положения в новое
        /// а так же местоположение модели при столкновении(соприкасающее модель с другими моделями)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="newLocation">Точка куда модель необходимо передвинуть</param>
        /// <returns></returns>
        public Contacts GetModelContacts(Gamer model, Point newLocation)
        {
            if (model.Rectangle.Location.X == newLocation.X && model.Rectangle.Location.Y == newLocation.Y) {
                return new Contacts(newLocation);
            }

            //область перемещения модели
            var moveArea = SpaceCollection<Gamer>.GetMoveArea(model, newLocation);

            //получаем все модели в области перемещения
            var ModelsArea = GetPhysicalModelsInArea(moveArea, false, true, false, model.ObjectId);

            if (ModelsArea.Count == 0) {
                return new Contacts(newLocation);
            }

            var rotateAngleArea = Helper.GetRotateAngle(model.Rectangle.Location, newLocation);

            Point pointContact;

            //двигаемся прямо
            if (rotateAngleArea % 90 == 0) {

                //1 модель на пути
                if (ModelsArea.Count == 1) {
                    pointContact = new Point(model.Rectangle.Location.X, model.Rectangle.Location.Y);

                    switch (rotateAngleArea) {
                        case 0: pointContact.X = ModelsArea[0].Rectangle.Location.X - model.Rectangle.Size.Width; break;
                        case 180: pointContact.X = ModelsArea[0].Rectangle.Location.X + ModelsArea[0].Rectangle.Size.Width; break;
                        case 90: pointContact.Y = ModelsArea[0].Rectangle.Location.Y - model.Rectangle.Size.Height; break;
                        case 270: pointContact.Y = ModelsArea[0].Rectangle.Location.Y + ModelsArea[0].Rectangle.Size.Height; break;
                    }

                    return new Contacts(ModelsArea, pointContact);
                }

                //ищем наиближайшие модели
                List<GameObject> nearModelsArea = new List<GameObject>();
                float minDistance = 0,
                    distance = 0;
                string axis = "x",
                    size = "width";

                foreach (var modelArea in ModelsArea) {
                    if (model.Rectangle.Location.X == newLocation.X) {
                        //движение по вертикали
                        axis = "y";
                        size = "height";
                    }

                    //180 напрвление или 270 ?
                    distance = (float)model.Rectangle.Location.Get(axis) > (float)modelArea.Rectangle.Location.Get(axis)
                        ? (float)model.Rectangle.Location.Get(axis) - (float)modelArea.Rectangle.Location.Get(axis) - (float)modelArea.Rectangle.Size.Get(size)
                        : (float)modelArea.Rectangle.Location.Get(axis) - (float)model.Rectangle.Location.Get(axis) + (float)model.Rectangle.Size.Get(size);


                    if (nearModelsArea.Count == 0 || distance < minDistance) {
                        minDistance = distance;
                        nearModelsArea = new List<GameObject>() { modelArea };
                    }
                    else if (distance == minDistance) {
                        nearModelsArea.Add(modelArea);
                    }
                }

                pointContact = new Point(model.Rectangle.Location.X, model.Rectangle.Location.Y);

                switch (rotateAngleArea) {
                    case 0: pointContact.X = nearModelsArea[0].Rectangle.Location.X - model.Rectangle.Size.Width; break;
                    case 180: pointContact.X = nearModelsArea[0].Rectangle.Location.X + nearModelsArea[0].Rectangle.Size.Width; break;
                    case 90: pointContact.Y = nearModelsArea[0].Rectangle.Location.Y - model.Rectangle.Size.Height; break;
                    case 270: pointContact.Y = nearModelsArea[0].Rectangle.Location.Y + nearModelsArea[0].Rectangle.Size.Height; break;
                }

                return new Contacts(nearModelsArea, pointContact);
            }

            //движение по диагонали
            float steps = (float)Math.Max(2, Helper.Distance(model.Rectangle.Location, newLocation));
            float dx = (newLocation.X - model.Rectangle.Location.X) / steps;
            float dy = (newLocation.Y - model.Rectangle.Location.Y) / steps;

            //за n шагов достигаем конечной точки
            Contacts contacts = new Contacts(newLocation);
            var positionModel = new Rectangle(model);
            for (var i = 0; i < steps; i++) {
                foreach (var modelArea in ModelsArea) {
                    if (positionModel.IsIntersectionOrContainsModel(modelArea)) {
                        contacts.Add(modelArea);
                    }
                }

                if (contacts.Models.Count > 0) {
                    contacts.LastlocationModel = new Point(positionModel.Location.X - dx, positionModel.Location.Y - dy);

                    return contacts;
                }

                positionModel.Location.X = positionModel.Location.X + dx;
                positionModel.Location.Y = positionModel.Location.Y + dy;
            }

            return contacts;
        }

        /// <summary>
        /// Возвращает все модели с которыми было совершено столкновение при переходе пули из текущео положения в новое
        /// </summary>
        /// <param name="model"></param>
        /// <param name="newLocation">Точка куда модель необходимо передвинуть</param>
        /// <returns></returns>
        public Contacts GetModelContacts(Bullet model, Point newLocation)
        {
            //Трассировка
            var positionBullet = new Rectangle(model);
            var contacts = new Contacts(); //все найденные контакты
            var allContactKeys = new HashSet<string>(); //объект для быстрого доступа ко всем найденным контактам
            List<Contact> localContacts; //контакты в квадрате
            var damageArmorBullet = model.DamageArmor; //бронебойность пули
            var distanceToEnd = Helper.Distance(model.Rectangle.Location, newLocation); //дистанция от начала до конца
            var squareEnd = BulletSpaceCollection.GetNumberSquareByPoint(newLocation);
            int numberSquare;

            do {
                localContacts = new List<Contact>();
                numberSquare = BulletSpaceCollection.GetNumberSquareByPoint(positionBullet.Location);
                var modelsArea = GetPhysicalModelsInArea(positionBullet, true, false, false, model.ObjectId);

                foreach (var modelArea in modelsArea) {
                    //если в модель ещё не было совершено попадание
                    if (!allContactKeys.Contains(modelArea.ObjectId) && !model.ContactsModelObjectId.Contains(modelArea.ObjectId)) {
                        //проверка на пересечение линии пули и граней модели
                        Point contact = modelArea.GetBesidePointIntersectionRays(model.Rectangle.TopRay);

                        if (contact == null) {
                            var distanceToContact = Helper.Distance(model.Rectangle.Location, contact);
                            if (distanceToContact < distanceToEnd) {
                                allContactKeys.Add(modelArea.ObjectId);
                                localContacts.Add(new Contact(modelArea, contact, (float)distanceToContact));
                            }
                        }
                    }
                }

                //сортируем по расстоянию от точки выстрела
                localContacts.Sort((a, b) => a.Distance > b.Distance ? 1 : (a.Distance < b.Distance ? -1 : 0));

                //уменьшаем потенциал пули
                foreach (var localContact in localContacts) {
                    contacts.Add(localContact);
                    damageArmorBullet = Bullet.GetDamageArmorBeforeClashModel(damageArmorBullet, localContact.Model);
                    //пуля утратила свой потенциал
                    if (damageArmorBullet <= 0) {
                        contacts.LastlocationModel = localContact.PointContact;
                        return contacts;
                    }
                }

                //место пересечения траектории пули с сеткой
                var pointIntersection = BulletSpaceCollection.GetPointIntersectionNetWithRay(model.Rectangle.TopRay, numberSquare);
                positionBullet.Location = new Point(
                    (float)(pointIntersection.X + model.Rectangle.TopRay.Dx),
                    (float)(pointIntersection.Y + model.Rectangle.TopRay.Dy));

                //пуля не достигла квадрат назначения?
            } while (numberSquare != squareEnd);

            if (contacts.Models.Count == 0) {
                contacts.LastlocationModel = newLocation;
            }
            else {
                contacts.LastlocationModel = positionBullet.Location;
            }

            return contacts;
        }
    }
}
