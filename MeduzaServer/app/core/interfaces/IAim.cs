﻿
namespace MeduzaServer
{
    /// <summary>
    /// Прицеливание - обязывает иметь точку прицеливания 
    /// Пример: Турель, монстры, нпс
    /// </summary>
    public interface IAim
    {
        Point AimPoint { get; set; }
        Rectangle Rectangle { get; set; }
    }
}
