﻿
namespace MeduzaServer
{
    /// <summary>
    /// Процессор - занимается обработкой действий моделей в ядре */
    /// Пример: оптимизированная обработка летящей пули - отличается от перемещения обычных объектов (их скорость намного ниже пули)
    /// </summary>
    public interface IProcessor
    {
        string ClassName { get; }

        /// <summary>
        /// Обработка действий модели
        /// </summary>
        /// <param name="dataProvider"></param>
        /// <param name="timeComplete">прошедшее время между обработкой событий</param>
        void Process(DataProvider dataProvider, uint timeComplete);
    }
}
