﻿
namespace MeduzaServer
{
    /// <summary>
    /// Обработка модели отвечающего за отображения ног людей
    /// </summary>
    public class BottomGamerProcessor : BaseObject, IProcessor
    {

        //*** Свойства ***//
        /// <summary>
        /// Модель, имеющая данный обработчик
        /// </summary>
        public Gamer Model { get; set; }


        //*** Конструкторы ***//
        public BottomGamerProcessor(Gamer model)
        {
            Model = model;
        }


        //*** Методы ***//
        /// <summary>
        /// Обработка действий модели
        /// </summary>
        /// <param name="dataProvider"></param>
        /// <param name="timeComplete">прошедшее время между обработкой событий</param>
        public void Process(DataProvider dataProvider, uint timeComplete)
        {
            //Поворот нижней части
            if (Model.KeysMap.IndexOf(38) != -1 || Model.KeysMap.IndexOf(87) != -1) //w
            {
                if (Model.KeysMap.IndexOf(39) != -1 || Model.KeysMap.IndexOf(68) != -1) //d
                {
                    if (Model.Rectangle.RotateAngle > 45 && Model.Rectangle.RotateAngle < 225) {
                        ChangeBottom(135);
                    }
                    else {
                        ChangeBottom(315);
                    }
                }
                else if (Model.KeysMap.IndexOf(37) != -1 || Model.KeysMap.IndexOf(65) != -1) //a
                {
                    if (Model.Rectangle.RotateAngle < 315 && Model.Rectangle.RotateAngle > 135) {
                        ChangeBottom(225);
                    }
                    else {
                        ChangeBottom(45);
                    }
                }
                else {
                    if (Model.Rectangle.RotateAngle >= 0 && Model.Rectangle.RotateAngle < 180) {
                        ChangeBottom(90);
                    }
                    else {
                        ChangeBottom(270);
                    }
                }
            }
            else if (Model.KeysMap.IndexOf(40) != -1 || Model.KeysMap.IndexOf(83) != -1) //s
            {
                if (Model.KeysMap.IndexOf(39) != -1 || Model.KeysMap.IndexOf(68) != -1) //d
                {
                    if (Model.Rectangle.RotateAngle <= 135 || Model.Rectangle.RotateAngle > 315) {
                        ChangeBottom(45);
                    }
                    else {
                        ChangeBottom(225);
                    }
                }
                else if (Model.KeysMap.IndexOf(37) != -1 || Model.KeysMap.IndexOf(65) != -1) //a
                {
                    if (Model.Rectangle.RotateAngle > 45 && Model.Rectangle.RotateAngle < 225) {
                        ChangeBottom(135);
                    }
                    else {
                        ChangeBottom(315);
                    }
                }
                else {
                    if (Model.Rectangle.RotateAngle > 0 && Model.Rectangle.RotateAngle < 180) {
                        ChangeBottom(90);
                    }
                    else {
                        ChangeBottom(270);
                    }
                }
            }
            else if (Model.KeysMap.IndexOf(39) != -1 || Model.KeysMap.IndexOf(68) != -1) //d
            {
                if (Model.Rectangle.RotateAngle > 90 && Model.Rectangle.RotateAngle < 270) {
                    ChangeBottom(180);
                }
                else {
                    ChangeBottom(0);
                }
            }
            else if (Model.KeysMap.IndexOf(37) != -1 || Model.KeysMap.IndexOf(65) != -1) //a
            {
                if (Model.Rectangle.RotateAngle < 90 || Model.Rectangle.RotateAngle > 270) {
                    ChangeBottom(0);
                }
                else {
                    ChangeBottom(180);
                }
            }
        }

        /// <summary>
        /// Изменение поворота ног
        /// </summary>
        /// <param name="rotateAngleBottom"></param>
        private void ChangeBottom(float rotateAngleBottom)
        {
            Model.ItemsCluster.Bottom.Rectangle.RotateAngle = rotateAngleBottom;
        }
    }
}
