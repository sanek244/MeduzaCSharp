﻿
namespace MeduzaServer
{
    /// <summary>
    /// Обработчик модели Bullet
    /// </summary>
    public class BulletProcessor : BaseObject, IProcessor
    {

        //*** Свойства ***//
        /// <summary>
        /// Модель, имеющая данный обработчик
        /// </summary>
        public Bullet Model { get; set; }


        //*** Конструкторы ***//
        public BulletProcessor(Bullet model)
        {
            Model = model;
        }


        //*** Методы ***//
        /// <summary>
        /// Обработка действий модели
        /// </summary>
        /// <param name="dataProvider"></param>
        /// <param name="timeComplete">прошедшее время между обработкой событий</param>
        public void Process(DataProvider dataProvider, uint timeComplete)
        {
            var dx = Model.Rectangle.TopRay.Dx * Model.Speed * timeComplete;
            var dy = Model.Rectangle.TopRay.Dy * Model.Speed * timeComplete;

            //Передвигаем модель и смотрим, есть ли столкновения
            var contacts = dataProvider.GetModelContacts(Model,
                    new Point(
                        (float)(Model.Rectangle.Location.X + dx),
                        (float)(Model.Rectangle.Location.Y + dy)
                    ));

            //Проход по всем объектам в которые было совершено попадание
            foreach(var contact in contacts.Models) {
                //добавляем анимацию попадания
                Animation animationContact = (Animation)Model.ViewsClusterContacts.Get(contact.Model.Material.ToString());
                if (animationContact != null) {
                    dataProvider.AddModel(new SimpleGameObject() {
                        ViewType = animationContact.ClassName,
                        ViewId = animationContact.Id,
                        Rectangle = new Rectangle(contact.PointContact, animationContact.Size)
                    });
                }

                //добавляем звук попадания
                Sound soundContact = (Sound)Model.SoundClusterContacts.Get(contact.Model.Material.ToString());
                if (soundContact != null) {
                    dataProvider.AddModel(new SoundObject() {
                        Sound = soundContact,
                        Rectangle = new Rectangle(contact.PointContact)
                    });
                }

                //наносим урон (урон пули в % от оставшегося здоровья) - броня игрока * 10 + бронебойность пули * 10
                var damage = (Model.Health / Model.HealthStart * Model.Damage) - contact.Model.Armor * 10 + Model.DamageArmor * 10;

                //уменьшаем жизнь пули (10 - обязательный урон + хп жертвы / 10 * бронебойность
                var difArm = Model.DamageArmor * 100 - contact.Model.Armor * 100;
                var damageHealthBullet = Model.Health - 10 - contact.Model.Health / (Model.DamageArmor + 1) + (difArm < 0 ? difArm : 0);
                Model.Health = damageHealthBullet < 0 ? 0 : damageHealthBullet;
                Model.Damage = Model.Damage - damage / 10;

                //уменьшаем бронебойность пули
                Model.DamageArmor = Model.GetDamageArmorBeforeClashModel(contact.Model);

                if (damage > 0) {
                    var newHealth = contact.Model.Health - damage;
                    contact.Model.Health = newHealth;
                    if (newHealth <= 0) {
                        contact.Model.IsDelete = true;
                    }
                }

                //смерть пули
                Model.ContactsModelObjectId.Add(contact.Model.ObjectId);
                if (Model.Health <= 0) {
                    Model.IsDelete = true;
                    Model.Rectangle.Location = contact.PointContact;
                    return;
                }
            }

            Model.Rectangle.Location = contacts.LastlocationModel;

            //ограничение по дистанции полёта
            if (Helper.Distance(Model.PointStart, contacts.LastlocationModel) >= Model.Distance) {
                Model.IsDelete = true;
            }
        }
    }
}
