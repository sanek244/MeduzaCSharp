﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public class GamerProcessor : BaseObject, IProcessor
    {
        //*** Свойства ***//
        /// <summary>
        /// Модель, имеющая данный обработчик
        /// </summary>
        public Gamer Model { get; set; }


        //*** Конструкторы ***//
        public GamerProcessor(Gamer model)
        {
            Model = model;
        }


        //*** Методы ***//
        /// <summary>
        /// Обработка действий модели
        /// </summary>
        /// <param name="dataProvider"></param>
        /// <param name="timeComplete">прошедшее время между обработкой событий</param>
        public void Process(DataProvider dataProvider, uint timeComplete)
        {
            double dx = 0, dy = 0;

            //обработка нажатых клавиш
            foreach(ushort key in Model.KeysMap) {
                switch(key) {
                    case 38: //w
                    case 87: //w
                        dy -= Model.Speed;
                        break;

                    case 39: //d
                    case 68: //D
                        dx += Model.Speed;
                        break;

                    case 40: //s
                    case 83: //S
                        dy += Model.Speed;
                        break;

                    case 37: //a
                    case 65: //A
                        dx -= Model.Speed;
                        break;
                }

                //активация способностей
                if (Model.Abilities.ContainsKey(key)){
                    Model.Abilities[key].Activation(dataProvider, Model, timeComplete, null);
                }
            }

            //Перемещение модели
            var physicsMoveAbility = new PhysicsMoveAbility();
            physicsMoveAbility.Activation(dataProvider, Model, timeComplete, new Dictionary<string, object> { { "dx", dx}, { "dy", dy} });
        }
    }
}
