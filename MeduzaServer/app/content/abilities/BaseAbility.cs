﻿
using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Способность - то, что можно активировать. Данный объект имеет право лишь создавать новые объекты или накладывать эффекты. */
    /// Пример: супер сила не 10 секунд, выстрел из ружья, удар саблей
    /// </summary>
    public abstract class BaseAbility : BaseObject
    {

        //*** Методы ***//
        /// <summary>
        /// Активация способности
        /// </summary>
        /// <param name="dataProvider">прошедшее время между обработкой событий</param>
        /// <param name="model"></param>
        /// <param name="timeComplete"></param>
        /// <param name="param"></param>
        public abstract void Activation(DataProvider dataProvider, SimpleGameObject model, uint timeComplete, Dictionary<string, object> param);
    }
}
