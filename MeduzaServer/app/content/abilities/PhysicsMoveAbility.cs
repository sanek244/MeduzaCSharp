﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    ///  Способность: Перемещение физической сущности
    /// </summary>
    public class PhysicsMoveAbility : BaseAbility
    {

        //*** Методы ***//
        /// <summary>
        /// Активация способности
        /// </summary>
        /// <param name="dataProvider">прошедшее время между обработкой событий</param>
        /// <param name="model"></param>
        /// <param name="timeComplete"></param>
        /// <param name="param"></param>
        public override void Activation(DataProvider dataProvider, SimpleGameObject model, uint timeComplete, Dictionary<string, object> param)
        {
            Point newLocation = new Point(
                model.Rectangle.Location.X + (float)param["dx"] * timeComplete,
                model.Rectangle.Location.Y + (float)param["dy"] * timeComplete
            );

            //Передвигаем модель и смотрим, есть ли столкновения
            var contacts = dataProvider.GetModelContacts((Gamer)model, newLocation);

            //измененяем модель
            model.Rectangle.Location = contacts.LastlocationModel;
        }
    }
}
