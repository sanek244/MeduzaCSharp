﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Способность: Стрельба
    /// </summary>
    public class StrikeAbility : BaseAbility
    {

        //*** Свойства ***//
        /// <summary>
        /// Оружие обладающее данной способностью
        /// </summary>
        public Weapon Weapon { get; set; }

        //*** Конструкторы ***//
        public StrikeAbility(Weapon weapon)
        {
            Weapon = weapon;
        }


        //*** Методы ***//
        /// <summary>
        /// Активация способности
        /// </summary>
        /// <param name="dataProvider">прошедшее время между обработкой событий</param>
        /// <param name="model"></param>
        /// <param name="timeComplete"></param>
        /// <param name="param"></param>
        public bool Activation(DataProvider dataProvider, Gamer gamer, uint timeComplete, Dictionary<string, object> param)
        {
            //проверка скорострельности
            if (Weapon.AttackLastTime + (ulong)Weapon.RapidityFire > Helper.GetUnixTime()) {
                return false;
            }

            //проверка количества патрон в обойме
            if (Weapon.HolderVolumeCurrent <= 0) {
                if (Weapon.SoundClusterWeaponId > -1 && Weapon.SoundClusterWeapon.EmptyShotId > -1) {
                    //Создаём звук пустого счелчка
                    var soundEmptyShot = new SoundObject(Weapon.SoundClusterWeapon.EmptyShot);
                    soundEmptyShot.Rectangle.Location = Weapon.BulletCreatePoint;
                    dataProvider.AddModel(soundEmptyShot);
                }
                return false;
            }

            //Создаём анимацию огня от выстрела (берём её у оружия)
            Animation shotAnimation = ((ViewsClusterWeapon)Weapon.View).Shot;
            if (shotAnimation != null) {
                dataProvider.AddModel(new SimpleGameObject() {
                    Rectangle = new Rectangle(Weapon.BulletCreatePoint, shotAnimation.Size, Weapon.Rectangle.RotateAngle),
                    ViewType = shotAnimation.ClassName,
                    ViewId = shotAnimation.Id
                });
            }

            //Создаём звук выстрела
            if (Weapon.SoundClusterWeapon.Shot != null) {
                var soundShot = new SoundObject(Weapon.SoundClusterWeapon.Shot);
                soundShot.Rectangle.Location = Weapon.BulletCreatePoint;
                dataProvider.AddModel(soundShot);
            }

            //На игрока накладываем эффект отдачи
            gamer.AddEffect(new RecoilEffect(gamer) {
                ModifiableAttributes = new Dictionary<string, object>() { { "recoil", Weapon.Recoil } }
            });

            //Создаём пулю с траекторией данной от типа разброса оружия
            var bullet = Application.DBController.FindOne<Bullet>(Weapon.BulletId);
            //TODO: доделать (учесть поворот и где вообще должна появлятся пуля)
            bullet.Rectangle.Location = new Point(Weapon.BulletCreatePoint.X + gamer.Rectangle.Location.X, Weapon.BulletCreatePoint.Y + gamer.Rectangle.Location.Y);
            bullet.PointStart = new Point(bullet.Rectangle.Location);
            bullet.Damage = bullet.Damage * Weapon.Damage;
            bullet.Speed = Weapon.ShotSpeed;

            var scatterAngle = Helper.GetScatterWeapon(Weapon, gamer);
            bullet.Rectangle.RotateAngle = scatterAngle;
            dataProvider.AddModel(bullet);


            //Меняем weapon
            Weapon.AttackLastTime = Helper.GetUnixTime();
            Weapon.HolderVolumeCurrent -= 1;

            return true;
        }
        public override void Activation(DataProvider dataProvider, SimpleGameObject model, uint timeComplete, Dictionary<string, object> param)
        {
            Error("Activation", "Вызван метод не с тем параметром! Должен быть с Gamer-ом");
        }
    }
}
