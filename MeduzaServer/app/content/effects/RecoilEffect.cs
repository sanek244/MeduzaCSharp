﻿
namespace MeduzaServer
{
    /// <summary>
    /// эффект отдачи
    /// </summary>
    public class RecoilEffect : BaseEffect
    {

        //*** Свойства ***//
        /// <summary>
        /// Модель, имеющая данный эффект
        /// </summary>
        public new Gamer Model { get; set; }


        //*** Конструкторы ***//
        public RecoilEffect(Gamer model)
        {
            ModifiableAttributes.Add("recoil", 0);
            Model = model;
        }


        //*** Методы ***//
        /// <summary>
        /// Действие эффекта
        /// </summary>
        public override void Process()
        {
            ModifiableAttributes["recoil"] = (float)ModifiableAttributes["recoil"] - (0.1 + Model.Force / 50);
        }
    }
}
