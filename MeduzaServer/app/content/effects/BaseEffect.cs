﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Эффект - то, что временно меняет характеристики объекта (накладывается на объекты через способности, другие объекты) */
    /// Пример: заморозка игрока при поподании в него из заморажвающего луча
    /// </summary>
    public class BaseEffect : BaseObject
    {
        private ulong createdTime = Helper.GetUnixTime();

        //*** Свойства ***//
        /// <summary>
        /// Изменяемые атрибуты
        /// </summary>
        public Dictionary<string, object> ModifiableAttributes { get; set; }
        /// <summary>
        /// Время создания (unix time)
        /// </summary>
        public ulong CreatedTime { get => createdTime; set => createdTime = value; }
        /// <summary>
        /// Время жизни (миллисекунды)
        /// </summary>
        public uint TimeLife { get; set; }
        /// <summary>
        /// Модель, имеющая данный эффект
        /// </summary>
        public SimpleGameObject Model { get; set; }


        //*** Методы ***//
        /// <summary>
        ///  Начало действия эффекта (срабатывает при добавлении эффекта модели)
        /// </summary>
        public virtual void Start()
        {

        }

        /// <summary>
        /// Конец действия эффекта (если конец не связан со временем, то нужно его вручную вызывать в process)
        /// </summary>
        /// <param name="dataProvider"></param>
        public virtual void End(DataProvider dataProvider)
        {

        }

        /// <summary>
        /// Действие эффекта
        /// </summary>
        public virtual void Process()
        {

        }

        /// <summary>
        /// Объединение эффектов
        /// </summary>
        /// <param name="effect"></param>
        /// <returns>false - объединение не произошло</returns>
        public virtual bool Merge(BaseEffect effect)
        {
            return false;
        }
    }
}
