﻿using System;

namespace MeduzaServer
{
    /// <summary>
    /// основа для соединяемых эффектов
    /// Пример: отдача сущноти
    /// </summary>
    public class BaseMergeEffect : BaseEffect
    {

        //*** Методы ***//
        /// <summary>
        /// Объединение эффектов
        /// </summary>
        /// <param name="effect"></param>
        /// <returns>если false, то объединение не происходит</returns>
        public override bool Merge(BaseEffect effect)
        {
            foreach(var effectModifiableAttribute in effect.ModifiableAttributes) {
                var typeValue = effectModifiableAttribute.Value.GetType();
                if (typeValue == typeof(int)) {
                    ModifiableAttributes[effectModifiableAttribute.Key] = 
                        (int)ModifiableAttributes[effectModifiableAttribute.Key] + (int)effectModifiableAttribute.Value;
                }
                else if(typeValue == typeof(float)) {
                    ModifiableAttributes[effectModifiableAttribute.Key] =
                        (float)ModifiableAttributes[effectModifiableAttribute.Key] + (float)effectModifiableAttribute.Value;
                }
                else if (typeValue == typeof(double)) {
                    ModifiableAttributes[effectModifiableAttribute.Key] =
                        (double)ModifiableAttributes[effectModifiableAttribute.Key] + (double)effectModifiableAttribute.Value;
                } 
                else {
                    ModifiableAttributes[effectModifiableAttribute.Key] = effectModifiableAttribute.Value;
                }
            }

            TimeLife = Math.Max(TimeLife, effect.TimeLife);

            return true;
        }
    }
}
