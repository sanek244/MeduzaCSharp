﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public static class ListModifaed
    {

        public static List<T> Map<T>(this List<T> list, CallbackMap<T> callback)
        {
            for (int i = 0; i < list.Count; i++) {
                list[i] = callback(list[i]);
            }

            return list;
        }
    }
}
