﻿using System;

namespace Meduza
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.ReadKey();
        }

        static void log(params object[] objs)
        {
            Console.WriteLine();
            foreach(var obj in objs) {
                Console.Write(obj + " ");
            }
        }
    }
}