﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestSpeed
{
    class TestSpaceCollection
    {
        static public void native()
        {
            Console.WriteLine("100 objects x 1000 getModelContacts + updateModel");

            const spaceCollection = new SpaceCollection(new Size(10000, 20000));

            let timeBegin2 = new Date().getTime();
            for (let i = 0; i < 10; i++) {
                for (let j = 0; j < 10; j++) {
                    spaceCollection.addModel(new SimpleGameObject({
            rectangle: new Rectangle(new Point(20 * i, 30 * j), new Size(5, 5)),
        }));
        }
    }

    let timeBegin = new Date().getTime();
for(let i = 0; i< 10; i++) {
    spaceCollection.clearChanges();
    const models = spaceCollection.getModels();

    models.map(model => {
        const newLocation = new Point(
            model.rectangle.location.x + Math.random(),
            model.rectangle.location.x + Math.random());

    spaceCollection.getModelContacts(model, newLocation);
        spaceCollection.updateModel(model);
    });
}

console.log('\nload data', timeBegin - timeBegin2);
console.log('time complete', new Date().getTime() - timeBegin);
        }
    }
}
