﻿using System;
using MeduzaTest;

namespace TestSpeed
{
    class TestSpaceCollection
    {
        static public void native()
        {
            Console.WriteLine("100 objects x 1000 getModelContacts + updateModel");

            var spaceCollection = new SpaceCollection(new Size(10000, 20000));

            var timeBegin2 = DateTime.Now;
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    spaceCollection.AddModel(new SimpleGameObject(){
                        rectangle = new Rectangle(new Point(20 * i, 30 * j), new Size(5, 5)),
                    });
                }
            }

            var timeBegin = DateTime.Now;
            for(int i = 0; i< 10; i++) {
                spaceCollection.ClearChanges();
                var models = spaceCollection.GetModels();

                models.map(model => {
                    const newLocation = new Point(
                        model.rectangle.location.x + Math.random(),
                        model.rectangle.location.x + Math.random());

                spaceCollection.getModelContacts(model, newLocation);
                    spaceCollection.updateModel(model);
                });
            }

            Console.WriteLine("\nload data", timeBegin - timeBegin2);
            Console.WriteLine("time complete", new DateTime.Now - timeBegin);
        }
    }
}
