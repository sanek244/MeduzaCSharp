
/**
 * Чтение данных из файлового хранилища
 * @testOk
 */
class FileController{

    /**
     * Возвращает версию данных
     * @testOk
     */
    getVersionData(){
        const statFile = fs.statSync(directoryDB + filePath);
        const mTime = new Date(statFile.mtime).getTime();
        if (mTime > this.versionData) {
            this.versionData = new Date(statFile.mtime).getTime();
        }

        return this.versionData;
    }
}

module.exports = FileController;