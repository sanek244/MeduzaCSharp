﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace MeduzaServer
{
    /// <summary>
    /// Простой игровой объект
    /// </summary>
    public class SimpleGameObject : BaseObject
    {
        /// <summary>
        /// Прямоугольник модели
        /// </summary>
        public Rectangle rectangle;

        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public string objectId;

        /// <summary>
        /// Имя объекта
        /// </summary>
        public string name;

        /// <summary>
        /// Изменённые атрибуты
        /// </summary>
        public List<string> changesAttributes;

        /// <summary>
        /// Id экземпляра в бд
        /// </summary>
        public int id;

        /// <summary>
        /// Русскоязычная надпись описывающая сущность
        /// </summary>
        public string label;


        //*** Конструкторы ***//
        public SimpleGameObject()
        {
            objectId = Helper.GenerateUid();
            rectangle = new Rectangle();
            changesAttributes = new List<string>();
            name = "";
        }



        /// <summary>
        /// Возвращает строку JSON
        /// </summary>
        /// <param name="isAll">Вернуть все аттрибуты? false - только изменённые </param>
        /// <returns></returns>
        public string ToJSONString(bool isAll = false)
        {
            if (isAll) {
                return JsonConvert.SerializeObject(this);
            }

            string res = "{";

            foreach(string attribute in changesAttributes) {
                var value = Get(attribute);
                string separateValue = value.GetType() == typeof(string) ? "\"" : "";
                res += "\"" + attribute + "\":" + separateValue + value + separateValue + ",";
            }

            return res.Substring(0, res.Length - 1) + "}";
        }
    }
}
