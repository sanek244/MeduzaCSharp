﻿
namespace MeduzaServer
{
    /// <summary>
    /// Изображение
    /// </summary>
    public class Img : ActiveRecord
    {

        //*** Свойства ***//
        /// <summary>
        /// Путь до папки с картинкой
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// z расположение
        /// </summary>
        public int ZIndex { get; set; }
        /// <summary>
        /// Размер
        /// </summary>
        public Size Size { get; set; }
        /// <summary>
        /// Расширение файла
        /// </summary>
        public string FileExtension { get; set; }
        /// <summary>
        /// Тип изображения (для клиента через ToJSONString)
        /// </summary>
        public string Type { get => ClassName(); }
    }
}
