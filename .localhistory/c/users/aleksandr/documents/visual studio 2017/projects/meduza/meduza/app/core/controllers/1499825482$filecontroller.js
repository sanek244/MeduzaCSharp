'use strict';

const fs = require('fs');
const App = require('../base/Application');

/**
 * Чтение данных из файлового хранилища
 * @testOk
 */
class FileController{
    constructor(){
        /** @var {int} versionData */
        this.versionData = 0;
    }
    
    
    static find(tableName, where, isOne = false){
        const tableData = require('../../../fileDb/' + tableName);

        if(!where){
            if(isOne){
                return tableData.length > 0
                    ? tableData[0]
                    : null;
            }
            return tableData;
        }

        if(typeof(where) !== 'object'){
            where = {id: where};
        }

        let result = [];
        //по всем моделям
        for(const iModel in tableData)
        {
            if(tableData.hasOwnProperty(iModel))
            {
                let ok = true;

                //проверяем на равенство всех условий
                for(let key in where){
                    if(where.hasOwnProperty(key)){
                        if(tableData[iModel].get(key) !== where[key]){
                            ok = false;
                            break;
                        }
                    }
                }

                //модель прошла условия
                if(ok){
                    if(isOne){
                        return tableData[iModel];
                    }
                    result.push(tableData[iModel]);
                }
            }
        }

        if(isOne){
            return null;
        }
        return result;
    }

    /**
     * Возвращает версию данных
     * @testOk
     */
    getVersionData(){
        if(!this.versionData){
            const directoryDB = App.params.directProject + '/fileDb/';
            const files = fs.readdirSync(directoryDB);
            files.map(filePath => {
                if(filePath !== '.' && filePath !== '..' && filePath !== 'objectsMap'){
                    const statFile = fs.statSync(directoryDB + filePath);
                    const mTime = new Date(statFile.mtime).getTime();
                    if(mTime > this.versionData){
                        this.versionData = new Date(statFile.mtime).getTime();
                    }
                }
            });
        }

        return this.versionData;
    }
}

module.exports = FileController;