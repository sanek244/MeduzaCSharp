﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MeduzaServer
{
    /// <summary>
    /// Пространственная коллекция данных - оптимизированная сущность для работы с набором данных в пространстве
    /// </summary>
    public class SpaceCollection
    {
        //*** Свойства ***//
        /// <summary>
        /// номер квадрата => [model] (для быстрого получения модели в квадрате)
        /// </summary>
        public Dictionary<int, List<SimpleGameObject>> ModelCoords { get; set; }

        /// <summary>
        /// objectId => массив координат (Модель может быть на несколько квадратов)
        /// </summary>
        public Dictionary<string, HashSet<int>> ModelSquares { get; set; }

        /// <summary>
        /// objectId => модель
        /// </summary>
        public Dictionary<string, SimpleGameObject> Models { get; set; }

        /// <summary>
        /// Изменённые модели вида: номер квадрата=>[model, ...] (частично заполненный)
        /// Заполняет по мере формирования визуального вида для конкретного пользователя
        /// Алгоритм: При формировании вида для игроков, смотрится, есть ли здесь интересующий квадрат
        /// Если нету, то высчитываются изменённые модели и заносятся сюда.
        ///
        /// Обнуляется после отправки всех данных
        /// </summary>
        public Dictionary<int, List<SimpleGameObject>> ModelChangedCoords { get; set; }

        /// <summary>
        /// Размер сетки (в пикселях)
        /// </summary>
        public int SquareSize { get; set; }

        /// <summary>
        /// Размер карты в квадратах 
        /// </summary>
        public Size SquareMapSize { get; set; }

        /// <summary>
        /// Размер карты
        /// </summary>
        public Size MapSize { get; set; }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="MapSize">Размер карты</param>
        /// <param name="SquareSize">размер квадрата</param>
        public SpaceCollection(Size mapSize, int squareSize = 200)
        {
            ModelCoords = new Dictionary<int, List<SimpleGameObject>>();
            ModelSquares = new Dictionary<string, HashSet<int>>();
            Models = new Dictionary<string, SimpleGameObject>();
            ModelChangedCoords = new Dictionary<int, List<SimpleGameObject>>();

            this.SquareSize = squareSize;
            this.MapSize = mapSize;

            SquareMapSize = new Size() {
                width = (float)Math.Ceiling(MapSize.width / SquareSize),
                height = (float)Math.Ceiling(MapSize.height / SquareSize)
            };
        }



        
        /// <summary>
        /// Очистка изменений
        /// </summary>
        public void ClearChanges()
        {
            ModelChangedCoords.Clear();
        }

        /// <summary>
        /// Очистка всей коллекции
        /// </summary>
        public void ClearAll()
        {
            ModelCoords.Clear();
            ModelSquares.Clear();
            Models.Clear();
            ModelChangedCoords.Clear();
        }

        /// <summary>
        /// Возвращает номер квадрата по точке в пространстве
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int GetNumberSquare(float x, float y)
        {
            x = x < 0 ? 0 : x;
            y = y < 0 ? 0 : y;
            x = x > MapSize.width ? MapSize.width : x;
            y = y > MapSize.width ? MapSize.width : y;

            return (int)((int)(x / SquareSize) + ((int)(y / SquareSize)) * SquareMapSize.width);
        }
        /// <summary>
        /// Возвращает номер квадрата по точке в пространстве
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public int GetNumberSquareByPoint(Point point)
        {
            return GetNumberSquare(point.x, point.y);
        }

        /// <summary>
        /// Возвращает координаты квадрата
        /// </summary>
        /// <param name="numberSquare">номер квадрата</param>
        /// <param name="isGetCenter">вернуть центр квадрата?</param>
        /// <returns></returns>
        public Point GetPositionSquare(int numberSquare, bool isGetCenter = false)
        {
            float x = numberSquare % SquareMapSize.width * SquareSize;
            float y = (int)(numberSquare / SquareMapSize.width) * SquareSize;

            if (isGetCenter) {
                x += SquareSize / 2;
                y += SquareSize / 2;
            }

            return new Point(x, y);
        }


        /// <summary>
        /// Возвращает квадраты, в которых находится модель
        /// </summary>
        /// <param name="model">модель</param>
        /// <returns>массив квадратов</returns>
        public HashSet<int> GetModelSquares(SimpleGameObject model)
        {
            if (model is Sound) {
                return GetModelSquaresSound(model as Sound);
            }

            //номера квадратов, в которых находятся углы
            HashSet<int> numberSquares = new HashSet<int>() {
                GetNumberSquareByPoint(model.rectangle.location),
                GetNumberSquare(model.rectangle.location.x + model.rectangle.size.width, model.rectangle.location.y),
                GetNumberSquare(model.rectangle.location.x + model.rectangle.size.width, model.rectangle.location.y + model.rectangle.size.height),
                GetNumberSquare(model.rectangle.location.x, model.rectangle.location.y + model.rectangle.size.height),
            };

            //если 1 квадрат, то сразу возвращаем его
            if (numberSquares.Count == 1) {
                return numberSquares;
            }

            int squareStartNumber = numberSquares.Min();
            int squareEndNumber = numberSquares.Max();

            //если квадраты углов - смежные
            if ((squareEndNumber - squareStartNumber) <= SquareMapSize.width + 1) {
                return numberSquares;
            }

            //находим ширину занимаемой области (второй минимум)
            numberSquares.Remove(squareStartNumber);
            int squaresWidth = numberSquares.Min() - squareStartNumber;
            numberSquares.Clear();

            //Собираем полный список квадратов
            for (int y = squareStartNumber; y <= squareEndNumber; y += (int)SquareMapSize.width) { //высота
                for (int x = y; x <= y + squaresWidth; x++) { //ширина
                    numberSquares.Add(x);
                }
            }

            return numberSquares;
        }
        public HashSet<int> GetModelSquaresSound(Sound model)
        {
            //Если это звук, то проверку на радиус с размытым краями (погрешност в пол квадрата)
            Point soundLocation = model.rectangle.location;
            float soundRadius = model.sound.radius;
            int widthMap = (int)SquareMapSize.width;

            var numberSquares = new List<int>();
            int centerSquare = GetNumberSquareByPoint(soundLocation);
            int centerVertical = centerSquare;
            int centerHorizontal = centerSquare;

            for (int i = centerSquare; true; i -= widthMap + 1) {
                for (int j = i; true; j--) {
                    float distanceToCenterSquare = (float)Helper.Distance(soundLocation, GetPositionSquare(j, true));
                    float distanceToCenterSquare2 = (float)Helper.Distance(soundLocation, GetPositionSquare(2 * centerSquare - j, true));
                    if (distanceToCenterSquare > soundRadius && distanceToCenterSquare2 > soundRadius) {
                        //end
                        if (j == i) {
                            numberSquares = numberSquares.Where(x => Helper.Distance(soundLocation, GetPositionSquare(x, true)) <= soundRadius).ToList();
                            return FindResiduesSquaresSound(numberSquares, soundLocation, soundRadius, centerSquare, centerVertical, centerHorizontal, widthMap, j);
                        }
                        break;
                    }

                    numberSquares.Add(j); //(2 четверть)
                    if (j != centerSquare) {
                        int differentV = centerVertical - j;

                        //симметрия справа (1 четверть)
                        int p2 = 2 * centerVertical - j;
                        //симметрия снизу  (3 четверть)
                        int p3 = centerHorizontal + differentV * widthMap;
                        //симметрия по диагонали (2 четверть)
                        int p4 = j == i
                            ? p3 - j + p2
                            : centerHorizontal - differentV * widthMap;

                        numberSquares.Add(p2);
                        numberSquares.Add(p3);
                        numberSquares.Add(p4);

                        if (j != i && i != centerSquare) {
                            //симметрия по диагонали (4 четверть)
                            int p5 = 2 * centerSquare - centerHorizontal + differentV * widthMap;
                            //симметрия снизу по диагонали (3 четверть)
                            int p6 = 2 * centerSquare - j;

                            numberSquares.Add(p5);
                            numberSquares.Add(p6);
                            numberSquares.Add(p6 - p2 + j);
                            numberSquares.Add(p4 + p5 - p3);
                        }
                    }
                }

                centerVertical -= widthMap;
                centerHorizontal--;
            }
        }
        /// <summary>
        /// Поиск остатков ненайденных квадратов вокруг окружности (максимум с двух сторон в связи с отклонениями центра)
        /// </summary>
        /// <param name="numberSquares"></param>
        /// <param name="soundLocation"></param>
        /// <param name="soundRadius"></param>
        /// <param name="centerSquare"></param>
        /// <param name="centerVertical"></param>
        /// <param name="centerHorizontal"></param>
        /// <param name="widthMap"></param>
        /// <param name="leftTopDiagonal"></param>
        /// <returns></returns>
        private HashSet<int> FindResiduesSquaresSound(List<int> numberSquares, Point soundLocation, float soundRadius, int centerSquare, int centerVertical, int centerHorizontal, int widthMap, int leftTopDiagonal)
        {
            numberSquares.Sort();

            //Граничные по диагонали элементы
            int rightTopDiagonal = 2 * centerVertical - leftTopDiagonal;
            if (Helper.Distance(soundLocation, GetPositionSquare(rightTopDiagonal, true)) < soundRadius && numberSquares.IndexOf(rightTopDiagonal) == -1) {
                numberSquares.Add(rightTopDiagonal);
            }
            int bottomLeftDiagonal = 2 * centerVertical - leftTopDiagonal;
            if (Helper.Distance(soundLocation, GetPositionSquare(bottomLeftDiagonal, true)) < soundRadius && numberSquares.IndexOf(bottomLeftDiagonal) == -1) {
                numberSquares.Add(bottomLeftDiagonal);
            }


            //Поиск ненайденных квадратов (на двух сторонах окружности)
            //Верх
            do {
                centerVertical -= widthMap;
            } while (numberSquares.IndexOf(centerVertical) != -1);

            bool ok = true;

            //Если на верху норм, то переходим на низ
            if (Helper.Distance(soundLocation, GetPositionSquare(centerVertical, true)) > soundRadius) {
                centerVertical = 2 * centerSquare - centerVertical; //низ

                if (Helper.Distance(soundLocation, GetPositionSquare(centerVertical, true)) > soundRadius) {
                    ok = false;
                }
            }

            if (ok) {
                numberSquares.Add(centerVertical);
                int i = centerVertical;
                bool isFound = false;
                do {
                    isFound = false;
                    i--;

                    if (Helper.Distance(soundLocation, GetPositionSquare(i, true)) <= soundRadius) {
                        numberSquares.Add(i);
                        isFound = true;
                    }
                    if (Helper.Distance(soundLocation, GetPositionSquare(2 * centerVertical - i, true)) <= soundRadius) {
                        numberSquares.Add(2 * centerVertical - i);
                        isFound = true;
                    }
                } while (isFound);
            }

            //Лево
            do {
                centerHorizontal--;
            } while (numberSquares.IndexOf(centerHorizontal) != -1);

            ok = true;

            //Если слева норм, то переходим в право
            if (Helper.Distance(soundLocation, GetPositionSquare(centerHorizontal, true)) > soundRadius) {
                centerHorizontal = 2 * centerSquare - centerHorizontal; //право

                if (Helper.Distance(soundLocation, GetPositionSquare(centerHorizontal, true)) > soundRadius) {
                    ok = false;
                }
            }

            if (ok) {
                numberSquares.Add(centerHorizontal);
                int i = centerHorizontal;
                bool isFound = false;
                do {
                    isFound = false;
                    i -= widthMap;
                    if (Helper.Distance(soundLocation, GetPositionSquare(i, true)) <= soundRadius) {
                        numberSquares.Add(i);
                        isFound = true;
                    }
                    if (Helper.Distance(soundLocation, GetPositionSquare(2 * centerHorizontal - i, true)) <= soundRadius) {
                        numberSquares.Add(2 * centerHorizontal - i);
                        isFound = true;
                    }
                } while (isFound);
            }

            return new HashSet<int>(numberSquares);
        }

        /// <summary>
        /// Возвращает квадраты, в которых находится объект
        /// </summary>
        /// <param name="location">местоположение</param>
        /// <param name="size">размер</param>
        /// <param name="rotateAngle">угол поворота объекта</param>
        /// <returns>массив квадратов</returns>
        public HashSet<int> GetObjectSquares(Point location, Size size, float rotateAngle = 0)
        {
            return GetModelSquares(new SimpleGameObject(){
                rectangle = new Rectangle(location, size, rotateAngle)
            });
        }

        /// <summary>
        /// Добавление модели в коллекцию
        /// </summary>
        /// <param name="model"></param>
        public void AddModel(SimpleGameObject model)
        {
            HashSet<int> squares = GetModelSquares(model);

            //добавление в координаты
            foreach(var square in squares) { 
                //ещё нету такого квадрата
                if (!ModelCoords.ContainsKey(square)) {
                    ModelCoords.Add(square, new List<SimpleGameObject>());
                }

                ModelCoords[square].Add(model);
            }

            //добавление номеров квадратов в ассоциативный массив моделей
            ModelSquares.Add(model.objectId, squares);
            Models.Add(model.objectId, model);

            AddModelInChanged(model);
        }

        /// <summary>
        /// Добавление модели в координаты изменений
        /// </summary>
        /// <param name="model"></param>
        public void AddModelInChanged(SimpleGameObject model)
        {
            //Проходим по квадратам модели и добавляем модель в массив изменений по тем же квадратам
            foreach(var square in ModelSquares[model.objectId]) {
                if (!ModelChangedCoords.ContainsKey(square)) {
                    ModelChangedCoords.Add(square, new List<SimpleGameObject>() { model });
                }
                else if (ModelChangedCoords[square].IndexOf(model) == -1) {
                    ModelChangedCoords[square].Add(model);
                }
            }
        }

        /// <summary>
        /// Обновление координат модели
        /// </summary>
        /// <param name="model"></param>
        public bool UpdateModel(SimpleGameObject model)
        {
            HashSet<int> squares = GetModelSquares(model);

            if (ModelSquares[model.objectId].Except(squares).Count() == 0 &&
                squares.Except(ModelSquares[model.objectId]).Count() == 0) 
            {
                return false;
            }

            //удаление из ушедших координат
            IEnumerable<int> removeSquares = ModelSquares[model.objectId].Except(squares);

            foreach(int square in removeSquares) {
                //если по этим координатам только 1 объект - это модель
                if (ModelCoords[square].Count == 1) {
                    //удаляем координату
                    ModelCoords.Remove(square);
                }
                else {
                    //для быстроты находим номер модели в списке
                    ModelCoords[square].Remove(model);
                }

                //изменения
                if (!ModelChangedCoords.ContainsKey(square)) {
                    ModelChangedCoords.Add(square, new List<SimpleGameObject>() { model });
                }
                else if (ModelChangedCoords[square].IndexOf(model) == -1) {
                    ModelChangedCoords[square].Add(model);
                }
            }

            //добавление в новые координаты
            IEnumerable<int> addSquares = squares.Except(ModelSquares[model.objectId]);
            foreach (int square in addSquares) {
                if (!ModelCoords.ContainsKey(square)) {
                    ModelCoords.Add(square, new List<SimpleGameObject>() { model });
                }
                else if (ModelCoords[square].IndexOf(model) == -1) {
                    ModelCoords[square].Add(model);
                }

                //изменения
                if (!ModelChangedCoords.ContainsKey(square)) {
                    ModelChangedCoords.Add(square, new List<SimpleGameObject>() { model });
                }
                else if (ModelChangedCoords[square].IndexOf(model) == -1) {
                    ModelChangedCoords[square].Add(model);
                }
            }

            //добавление номеров квадратов в ассоциативный массив моделей
            ModelSquares[model.objectId] = squares;
            return true;
        }

        /// <summary>
        /// Удаление модели из коллекции
        /// </summary>
        /// <param name="model"></param>
        public void DeleteModel(SimpleGameObject model)
        {
            AddModelInChanged(model);

            //удаление в координатах
            foreach(var square in ModelSquares[model.objectId]) {
                if (ModelCoords[square].Count == 1) {
                    ModelCoords.Remove(square);
                }
                else {
                    //для быстроты находим номер модели в списке
                    ModelCoords[square].Remove(model);
                }
            }

            //удаление из ассоциативного массива моделей
            ModelSquares.Remove(model.objectId);
            Models.Remove(model.objectId);
        }


        /// <summary>
        /// Возвращает модель по глобальному id
        /// </summary>
        /// <param name="objectId"></param>
        /// <returns></returns>
        public object GetModel(string objectId)
        {
            if (!Models.ContainsKey(objectId)) {
                return null;
            }

            return Models[objectId];
        }

        /// <summary>
        /// Возвращает все модели
        /// </summary>
        /// <returns></returns>
        public List<SimpleGameObject> GetModels()
        {
            return Models.Values.ToList();
        }


        /// <summary>
        /// Выборка по данным
        /// </summary>
        /// <param name="className">Класс, экземпляры которого нужно вернуть</param>
        /// <returns></returns>
        public List<SimpleGameObject> Find(string className)
        {
            return Models.Values.Where(model => model.ClassName() == className).ToList(); 
        }

        /// <summary>
        /// Возвращает все модели находящиеся в выбранной области
        /// </summary>
        /// <param name="area"></param>
        /// <param name="isAreaLocationCenter">считать location в area - центром area?</param>
        /// <param name="isExactly">точно в заданной области? (иначе модели в тех же квадратах)</param>
        /// <param name="isOnlyChanged">вернуть только изменённые модели?</param>
        /// <param name="excludeObjectId">исклюить заданный objectId</param>
        /// <returns></returns>
        public List<SimpleGameObject> GetModelsInArea(Rectangle area, bool isAreaLocationCenter = false, bool isExactly = false, bool isOnlyChanged = false, string excludeObjectId = "")
        {
            var pLeftTop = isAreaLocationCenter
                ? new Point(area.location.x - area.size.width / 2, area.location.y - area.size.height / 2)
                : area.location;

            var pRightTop = isAreaLocationCenter
                ? new Point(area.location.x + area.size.width / 2, area.location.y - area.size.height / 2)
                : new Point(area.location.x + area.size.width, area.location.y);

            var pRightBottom = isAreaLocationCenter
                ? new Point(area.location.x + area.size.width / 2, area.location.y + area.size.height / 2)
                : new Point(area.location.x + area.size.width, area.location.y + area.size.height);

            //переводим area в квадраты
            var squareStartNumber = GetNumberSquareByPoint(pLeftTop); //верхний левый угол (начало)
            var areaSquaresWidth = GetNumberSquareByPoint(pRightTop) - squareStartNumber; //ширина
            var squareEndNumber = GetNumberSquareByPoint(pRightBottom); //нижний правый угол

            //достаём все модели
            var res = new List<SimpleGameObject>();
            for (int y = squareStartNumber; y <= squareEndNumber; y += (int)SquareMapSize.width) { //высота
                for (int x = y; x <= y + areaSquaresWidth; x++) { //ширина
                    if (isOnlyChanged) {
                        //Изменённые модели
                        if (ModelChangedCoords.ContainsKey(x)) {
                            res = res.Concat(ModelChangedCoords[x]).ToList();
                        }
                    }
                    else if (ModelCoords.ContainsKey(x)) { //такой квадрат есть
                        //Все модели квадрата
                        res = res.Concat(ModelCoords[x]).ToList();
                    }
                }
            }

            if(excludeObjectId != "") {
                res.Remove(res.Find(x => x.objectId == excludeObjectId));
            }

            //отсеиваем модели не лежащие в заданной области
            if (isExactly){
                Rectangle leftTopArea = null;

                if (isAreaLocationCenter){
                    leftTopArea = new Rectangle(
                        new Point(area.location.x - area.size.width / 2, area.location.y - area.size.height / 2),
                        area.size);
                }
                else {
                    leftTopArea = area;
                }

                res = res.Where(model => leftTopArea.IsIntersectionOrContainsModel(model)).ToList();
            }

            return new HashSet<SimpleGameObject>(res).ToList();
        }


        /// <summary>
        /// Возвращает первые модели с которыми было совершено столкновение при переходе модели из текущео положения в новое
        /// а так же местоположение модели при столкновении(соприкасающее модель с другими моделями)
        /// </summary>
        /// <param name="model"></param>
        /// <param name="newLocation">Точка куда модель необходимо передвинуть</param>
        /// <returns></returns>
        public Contacts GetModelContacts(SimpleGameObject model, Point newLocation)
        {
            if (model.rectangle.location.x == newLocation.x && model.rectangle.location.y == newLocation.y) {
                return new Contacts(newLocation);
            }

            //область перемещения модели
            var moveArea = GetMoveArea(model, newLocation);

            //получаем все модели в области перемещения
            var ModelsArea = GetModelsInArea(moveArea, false, true, false, model.objectId);

            if (ModelsArea.Count == 0) {
                return new Contacts(newLocation);
            }

            if (model is Bullet) {
                return GetBulletContacts(model as Bullet, newLocation);
            }

            var rotateAngleArea = Helper.GetRotateAngle(model.rectangle.location, newLocation);

            Point pointContact;

            //двигаемся прямо
            if (rotateAngleArea % 90 == 0) {

                //1 модель на пути
                if (ModelsArea.Count == 1) {
                    pointContact = new Point(model.rectangle.location.x, model.rectangle.location.y);

                    switch (rotateAngleArea) {
                        case 0: pointContact.x = ModelsArea[0].rectangle.location.x - model.rectangle.size.width; break;
                        case 180: pointContact.x = ModelsArea[0].rectangle.location.x + ModelsArea[0].rectangle.size.width; break;
                        case 90: pointContact.y = ModelsArea[0].rectangle.location.y - model.rectangle.size.height; break;
                        case 270: pointContact.y = ModelsArea[0].rectangle.location.y + ModelsArea[0].rectangle.size.height; break;
                    }

                    return new Contacts(ModelsArea, pointContact);
                }

                //ищем наиближайшие модели
                List<SimpleGameObject> nearModelsArea = new List<SimpleGameObject>();
                float minDistance = 0,
                    distance = 0;
                string axis = "x",
                    size = "width";

                foreach(var modelArea in ModelsArea) {
                    if (model.rectangle.location.x == newLocation.x) {
                        //движение по вертикали
                        axis = "y";
                        size = "height";
                    }

                    //180 напрвление или 270 ?
                    distance = (float)model.rectangle.location.Get(axis) > (float)modelArea.rectangle.location.Get(axis)
                        ? (float)model.rectangle.location.Get(axis) - (float)modelArea.rectangle.location.Get(axis) - (float)modelArea.rectangle.size.Get(size)
                        : (float)modelArea.rectangle.location.Get(axis) - (float)model.rectangle.location.Get(axis) + (float)model.rectangle.size.Get(size);


                    if (nearModelsArea.Count == 0 || distance < minDistance) {
                        minDistance = distance;
                        nearModelsArea = new List<SimpleGameObject>() { modelArea };
                    }
                    else if (distance == minDistance) {
                        nearModelsArea.Add(modelArea);
                    }
                }

                pointContact = new Point(model.rectangle.location.x, model.rectangle.location.y);

                switch (rotateAngleArea) {
                    case 0: pointContact.x = nearModelsArea[0].rectangle.location.x - model.rectangle.size.width; break;
                    case 180: pointContact.x = nearModelsArea[0].rectangle.location.x + nearModelsArea[0].rectangle.size.width; break;
                    case 90: pointContact.y = nearModelsArea[0].rectangle.location.y - model.rectangle.size.height; break;
                    case 270: pointContact.y = nearModelsArea[0].rectangle.location.y + nearModelsArea[0].rectangle.size.height; break;
                }

                return new Contacts(nearModelsArea, pointContact);
            }

            //движение по диагонали
            float steps = (float)Math.Max(2, Helper.Distance(model.rectangle.location, newLocation));
            float dx = (float)((newLocation.x - model.rectangle.location.x) / steps);
            float dy = (float)((newLocation.y - model.rectangle.location.y) / steps);

            //за n шагов достигаем конечной точки
            Contacts contacts = new Contacts(newLocation);
            var positionModel = new Rectangle(model);
            for (var i = 0; i < steps; i++) {
                foreach(SimpleGameObject modelArea in ModelsArea) {
                    if (positionModel.IsIntersectionOrContainsModel(modelArea)) {
                        contacts.Add(modelArea);
                    }
                }

                if (contacts.contacts.Count > 0) {
                    contacts.lastlocationModel = new Point(positionModel.location.x - dx, positionModel.location.y - dy);

                    return contacts;
                }

                positionModel.location.x = positionModel.location.x + dx;
                positionModel.location.y = positionModel.location.y + dy;
            }

            return contacts;
        }

        /// <summary>
        /// Возвращает все модели с которыми было совершено столкновение при переходе пули из текущео положения в новое
        /// </summary>
        /// <param name="model"></param>
        /// <param name="newLocation">Точка куда модель необходимо передвинуть</param>
        /// <returns></returns>
        private Contacts GetBulletContacts(SimpleGameObject model, Point newLocation)
        {
            return null;
        }

        /// <summary>
        /// Возвращает область перемещения модели
        /// </summary>
        /// <param name="model"></param>
        /// <param name="newLocation">Точка куда модель необходимо передвинуть</param>
        /// <returns></returns>
        static public Rectangle GetMoveArea(SimpleGameObject model, Point newLocation)
        {

            //Находим минимальные и максимальные точки
            var arrayX = new float[4]{
                model.rectangle.location.x,
                model.rectangle.location.x + model.rectangle.size.width,
                newLocation.x,
                newLocation.x + model.rectangle.size.width,
            };

            var arrayY = new float[4]{
                model.rectangle.location.y,
                model.rectangle.location.y + model.rectangle.size.height,
                newLocation.y,
                newLocation.y + model.rectangle.size.height,

            };

            var xMin = arrayX.Min();
            var yMin = arrayY.Min();
            var xMax = arrayX.Max();
            var yMax = arrayY.Max();

            //Область передвижения
            return new Rectangle(new Point(xMin, yMin), new Size(xMax - xMin, yMax - yMin));
        }
    }
}
