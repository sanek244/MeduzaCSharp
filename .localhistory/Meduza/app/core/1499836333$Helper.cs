﻿using System;
using System.Text.RegularExpressions;

namespace MeduzaServer
{
    /// <summary>
    /// Вспомогательный класс
    /// </summary>
    public static class Helper
    {
        public static Random rnd = new Random();

        /// <summary>
        /// Рандомное число
        /// </summary>
        /// <param name="min">минимальное число включительно</param>
        /// <param name="max">максимальное число включительно</param>
        /// <returns></returns>
        static public float GetRandom(float min, float max)
        {
            return (float)(rnd.NextDouble() * (max - min) + min);
        }

        /// <summary>
        /// Расстояние между точками
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <returns></returns>
        static public double Distance(Point point1, Point point2)
        {
            return Math.Sqrt(Math.Pow(point1.x - point2.x, 2) + Math.Pow(point1.y - point2.y, 2));
        }

        /// <summary>
        /// Возвращает угол поворота между относительной прямой паралельной оси X и проведённой через точку А, и точкой Б
        /// Угол считается относительно положительной оси Х по часовой стрелке
        /// </summary>
        /// <param name="pointCenter">Центральная точка</param>
        /// <param name="pointB">Точка Б</param>
        /// <returns>Угол между точкой Б и параллельной к очи X прямой, проходящей через точку А</returns>
        static public float GetRotateAngle(Point pointCenter, Point pointB)
        {
            float x = pointCenter.x - pointB.x;
            float y = pointCenter.y - pointB.y;

            if (x == 0 && y == 0) {
                return 0;
            }

            float angle = (float)((Math.Atan(y / x) + (x >= 0 ? Math.PI : 0)) * 180 / Math.PI);

            return angle >= 0 ? angle : 360 + angle;
        }

        /// <summary>
        /// Возвращает сгенерированный 'уникальный' идентификатор
        /// </summary>
        /// <returns></returns>
        static public string GenerateUid()
        {
            MatchEvaluator myEvaluator = new MatchEvaluator((c) => {
                var r = (int)(rnd.NextDouble() * 16) | 0;
                var v = c.Value == "x" ? r : (r & 0x3 | 0x8);

                return Convert.ToString(v, 16);
            });

            return Regex.Replace("xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx", "[xy]", myEvaluator);
        }

        /// <summary>
        /// Преобразует DateTime в unix время
        /// </summary>
        /// <param name="date">Дата, которую нужно кнвертировать</param>
        /// <returns></returns>
        static public uint ToUnixTime(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date - origin;
            return (uint)Math.Floor(diff.TotalSeconds);
        }
        /// <summary>
        /// Возвращает текущее время в unit time 
        /// </summary>
        /// <returns></returns>
        static public uint GetUnixTime()
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = DateTime.Now - origin;
            return (uint)Math.Floor(diff.TotalSeconds);
        }

        //*** Разбросы  ***//
        /// <summary>
        /// Нарастающий разброс
        /// </summary>
        /// <param name="rotateAngle">текущий угол точки прицеливания</param>
        /// <param name="max">максимальное значение разброса</param>
        /// <param name="timeLast">время последнего вызова (unix-время)</param>
        /// <param name="forceGamer">сила игрока</param>
        /// <param name="isGetMax">вернуть максимальное отклонение?</param>
        /// <returns></returns>
        static public float GetScatterIncreasing(float rotateAngle, float max, uint timeLast = 0, int forceGamer = 1, bool isGetMax = false)
        {
            //полное время восстановления (миллисекунды)
            var timeRecovery = 1000 / forceGamer;

            //сила времени
            var forceTime = timeLast > GetUnixTime() - timeRecovery
                ? timeLast - (GetUnixTime() - timeRecovery)
                : 0;

            //(forceTime / timeRecovery) - процент от 0 до 1
            var newMax = forceTime / timeRecovery / forceGamer * max;

            if (isGetMax) {
                return rotateAngle + newMax;
            }

            return GetRandom(rotateAngle - newMax, rotateAngle + newMax);
        }
        /// <summary>
        /// Равномерный разброс
        /// </summary>
        /// <param name="rotateAngle">текущий угол точки прицеливания</param>
        /// <param name="max">максимальное значение разброса</param>
        /// <param name="forceGamer">сила игрока</param>
        /// <returns></returns>
        static public float GetScatterUniform(float rotateAngle, float max, int forceGamer)
        {
            var newMax = max / forceGamer;

            return GetRandom(rotateAngle - newMax, rotateAngle + newMax);
        }

        /**
         * Возвращает разброс оружия
         * @param {Weapon} weapon
         * @param {Gamer} gamer
         * @returns {number}
         * @testOk
         */
        static public float GetScatterWeapon(Weapon weapon, Gamer gamer)
        {
            if (weapon.get('scatterType') == TypeScatter.INCREASING) {
                return Helper.getScatterIncreasing(
                    weapon.get('rectangle.rotateAngle'),
                    weapon.get('scatterMax'),
                    weapon.get('attackLastTime'),
                    gamer.get('force')
                );
            }

            return Helper.getScatterUniform(
                weapon.get('rectangle.rotateAngle'),
                weapon.get('scatterMax'),
                gamer.get('force')
            );
        }
    }
}
