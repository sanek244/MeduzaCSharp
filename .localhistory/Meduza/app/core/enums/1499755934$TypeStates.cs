﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeState { Base, Run, BlowLateral, BlowDirect, Recoil, Run_recoil };

    public static class EnumState
    {
        public static Dictionary<TypeState, string> labels = new Dictionary<TypeState, string>() {
            { TypeState.Automaton, "Дерево" },
            { TypeState.RightElongatedHanded, "Органика" },
            { TypeState.LeftElongatedHanded, "Железо" },
            { TypeState.RightHanded, "Бетон" },
        };
    }
}
[BASE]: 'Базовое',
    [RUN]: 'Бег',
    [RECOIL]: 'Отдача',
    [BLOW_LATERAL]: 'Боковой удар',
    [BLOW_DIRECT]: 'Прямой удар',
    [RUN_RECOIL]: 'Бег и отдача'