﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeRace { People, Monsters, Aliens };

    public static class EnumRace
    {
        public static Dictionary<TypeRace, string> labels = new Dictionary<TypeRace, string>() {
            { TypeRace.People, "Люди" },
            { TypeRace.Monsters, "Монстры" },
            { TypeRace.Aliens, "Пришельцы" },
        };
    }
}
