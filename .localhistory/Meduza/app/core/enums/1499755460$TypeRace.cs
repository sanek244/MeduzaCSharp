﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeRace { Human, Monster, Alien };

    public static class EnumRace
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeRace.Human.ToString(), "Люди" },
            { TypeRace.Monster.ToString(), "Монстры" },
            { TypeRace.Alien.ToString(), "Пришельцы" },
        };
    }
}
[HUMAN]: 'Люди',
    [MONSTER]: 'Монстры',
    [ALIENS]: 'Пришельцы'