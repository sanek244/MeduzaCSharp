﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeRace { People, Monsters, Aliens };

    public static class EnumRace
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeRace.People.ToString(), "Люди" },
            { TypeRace.Monsters.ToString(), "Монстры" },
            { TypeRace.Aliens.ToString(), "Пришельцы" },
        };
    }
}
[HUMAN]: 'Люди',
    [MONSTER]: 'Монстры',
    [ALIENS]: 'Пришельцы'