﻿using System.Collections.Generic;

namespace MeduzaServer
{
    const BASE = 'base';
    const RUN = 'run';
    const BLOW_LATERAL = 'blowLateral';
    const BLOW_DIRECT = 'blowDirect';
    const RECOIL = 'recoil';
    const RUN_RECOIL = 'run_recoil';

    public enum TypeState { Tree, Bio, Iron, Concrete };

    public static class EnumState
    {
        public static Dictionary<TypeState, string> labels = new Dictionary<TypeState, string>() {
            { TypeState.Automaton, "Дерево" },
            { TypeState.RightElongatedHanded, "Органика" },
            { TypeState.LeftElongatedHanded, "Железо" },
            { TypeState.RightHanded, "Бетон" },
        };
    }
}