﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeMaterial { Automaton, Pistol, TommyGun, TwoPistol };

    public static class EnumMaterial
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeHolding.Automaton.ToString(), "Дерево" },
            { TypeHolding.RightElongatedHanded.ToString(), "Органика" },
            { TypeHolding.LeftElongatedHanded.ToString(), "Железо" },
            { TypeHolding.RightHanded.ToString(), "Бетон" },
        };
    }
}
[AUTOMATON]: 'Автомат',
    [PISTOL]: 'Пистолет',
    [TOMMY_GUN]: 'Пистолет-пулемёт',
    [TWO_PISTOL]: 'Два пистолета'