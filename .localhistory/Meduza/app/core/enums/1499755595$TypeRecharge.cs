﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeRecharge { Automaton, Pistol, TommyGun, TwoPistol };

    public static class EnumRecharge
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeRecharge.Automaton.ToString(), "Дерево" },
            { TypeRecharge.RightElongatedHanded.ToString(), "Органика" },
            { TypeRecharge.LeftElongatedHanded.ToString(), "Железо" },
            { TypeRecharge.RightHanded.ToString(), "Бетон" },
        };
    }
}
[AUTOMATON]: 'Автомат',
    [PISTOL]: 'Пистолет',
    [TOMMY_GUN]: 'Пистолет-пулемёт',
    [TWO_PISTOL]: 'Два пистолета'