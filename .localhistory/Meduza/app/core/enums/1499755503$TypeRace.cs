﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeRecharge { People, Monsters, Aliens };

    public static class EnumRecharge
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeRace.People.ToString(), "Люди" },
            { TypeRace.Monsters.ToString(), "Монстры" },
            { TypeRace.Aliens.ToString(), "Пришельцы" },
        };
    }
}
