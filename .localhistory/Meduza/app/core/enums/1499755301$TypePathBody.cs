﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeMaterial { Body, Head, Bottom, BackBody };

    public static class EnumMaterial
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeHolding.Automaton.ToString(), "Дерево" },
            { TypeHolding.RightElongatedHanded.ToString(), "Органика" },
            { TypeHolding.LeftElongatedHanded.ToString(), "Железо" },
            { TypeHolding.RightHanded.ToString(), "Бетон" },
        };
    }
}
TypePathBody.labels = {
    [BODY]: 'Тело',
    [HEAD]: 'Голов',
    [BOTTOM]: 'Ноги',
    [BACK_BODY]: 'Сзади тела' //хвост, плащ
};