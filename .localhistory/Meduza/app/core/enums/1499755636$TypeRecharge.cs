﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeRecharge { Automaton, Pistol, TommyGun, TwoPistol };

    public static class EnumRecharge
    {
        public static Dictionary<TypeRecharge, string> labels = new Dictionary<string, string>() {
            { TypeRecharge.Automaton, "Автомат" },
            { TypeRecharge.Pistol, "Пистолет" },
            { TypeRecharge.TommyGun, "Пистолет-пулемёт" },
            { TypeRecharge.TwoPistol, "Два пистолета" },
        };
    }
}