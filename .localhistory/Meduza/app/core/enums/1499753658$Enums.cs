﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    public static class Enums
    {
        public enum TypeProjectMode { Development, Production };
        //Development - режим разработки
        //Production - опубликованный проект

        public static Dictionary<string, string> LabelsTypeProgramMode = new Dictionary<string, string>() {
            { TypeProjectMode.Development.ToString(), "Проект в разработке" },
            { TypeProjectMode.Development.ToString(), "Проект опубликован" },
        };
    }
}
