﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    const AUTOMATON = 'automaton';
    const RIGHT_ELONGATED_HANDED = 'rightElongatedHanded';
    const LEFT_ELONGATED_HANDED = 'leftElongatedHanded';
    const RIGHT_HANDED = 'rightHanded';
    const LEFT_HANDED = 'leftHanded';
    const TWO_HANDED_PISTOL = 'twoHandedPistol';
    const MINIGUN = 'minigun';
    const FLAMETHROWER = 'flamethrower';
    public enum TypeHolding { automaton, rightElongatedHanded, leftElongatedHanded, rightHanded, leftHanded, twoHandedPistol, minigun, flamethrower };

    public static class EnumHolding
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeProjectMode.Development.ToString(), "Проект в разработке" },
            { TypeProjectMode.Production.ToString(), "Проект опубликован" },
        };
    }
}
