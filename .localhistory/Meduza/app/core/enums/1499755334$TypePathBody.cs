﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypePathBody { Body, Head, Bottom, BackBody };

    public static class EnumPathBody
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypePathBody.Automaton.ToString(), "Дерево" },
            { TypePathBody.RightElongatedHanded.ToString(), "Органика" },
            { TypePathBody.LeftElongatedHanded.ToString(), "Железо" },
            { TypePathBody.RightHanded.ToString(), "Бетон" },
        };
    }
}
TypePathBody.labels = {
    [BODY]: 'Тело',
    [HEAD]: 'Голов',
    [BOTTOM]: 'Ноги',
    [BACK_BODY]: 'Сзади тела' //хвост, плащ
};