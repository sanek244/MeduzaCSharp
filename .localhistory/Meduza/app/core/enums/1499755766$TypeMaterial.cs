﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeMaterial { Tree, Bio, Iron, Concrete };

    public static class EnumMaterial
    {
        public static Dictionary<TypeMaterial, string> labels = new Dictionary<TypeMaterial, string>() {
            { TypeMaterial.Automaton, "Дерево" },
            { TypeMaterial.RightElongatedHanded, "Органика" },
            { TypeMaterial.LeftElongatedHanded, "Железо" },
            { TypeMaterial.RightHanded, "Бетон" },
        };
    }
}