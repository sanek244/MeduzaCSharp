﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeState { Tree, Bio, Iron, Concrete };

    public static class EnumState
    {
        public static Dictionary<TypeState, string> labels = new Dictionary<TypeState, string>() {
            { TypeState.Automaton, "Дерево" },
            { TypeState.RightElongatedHanded, "Органика" },
            { TypeState.LeftElongatedHanded, "Железо" },
            { TypeState.RightHanded, "Бетон" },
        };
    }
}