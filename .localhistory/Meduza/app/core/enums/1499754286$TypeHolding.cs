﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    public enum TypeHolding { Automaton, RightElongatedHanded, LeftElongatedHanded, RightHanded, LeftHanded, TwoHandedPistol, Minigun, Flamethrower };

    public static class EnumHolding
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeHolding.Automaton.ToString(), "Проект" },
            { TypeHolding.RightElongatedHanded.ToString(), "Проект" },
            { TypeHolding.LeftElongatedHanded.ToString(), "Проект" },
            { TypeHolding.RightHanded.ToString(), "Проект" },
            { TypeHolding.LeftHanded.ToString(), "Проект" },
            { TypeHolding.TwoHandedPistol.ToString(), "Проект" },
            { TypeHolding.Minigun.ToString(), "Проект" },
            { TypeHolding.Flamethrower.ToString(), "Проект" },
        };
    }
}
