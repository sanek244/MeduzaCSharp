﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypePathBody { Body, Head, Bottom, BackBody };

    public static class EnumPathBody
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypePathBody.Body.ToString(), "Тело" },
            { TypePathBody.Head.ToString(), "Голова" },
            { TypePathBody.Bottom.ToString(), "Железо" },
            { TypePathBody.BackBody.ToString(), "Бетон" },
        };
    }
}
TypePathBody.labels = {
    [BODY]: 'Тело',
    [HEAD]: 'Голов',
    [BOTTOM]: 'Ноги',
    [BACK_BODY]: 'Сзади тела' //хвост, плащ
};