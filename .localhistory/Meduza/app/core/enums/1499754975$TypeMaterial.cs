﻿using System.Collections.Generic;

namespace MeduzaServer
{
    const TREE = 'tree';
    const BIO = 'bio';
    const IRON = 'iron';
    const CONCRETE = 'concrete';
    public enum TypeHolding { Tree, Bio, Iron, Concrete };

    public static class EnumMaterial
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeHolding.Automaton.ToString(), "Автомат" },
            { TypeHolding.RightElongatedHanded.ToString(), "Правая вытянутая рука" },
            { TypeHolding.LeftElongatedHanded.ToString(), "Левая вытянутая рука" },
            { TypeHolding.RightHanded.ToString(), "Правая рука" },
            { TypeHolding.LeftHanded.ToString(), "Левая рука" },
            { TypeHolding.TwoHandedPistol.ToString(), "Пистолет двумя руками" },
            { TypeHolding.Minigun.ToString(), "Миниган" },
            { TypeHolding.Flamethrower.ToString(), "Огнемёт" },
        };
    }
}

[TREE]: 'Дерево',
    [BIO]: 'Органика',
    [IRON]: 'Железо',
    [CONCRETE]: 'Бетон',