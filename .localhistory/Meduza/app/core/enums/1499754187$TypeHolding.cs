﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    public static class TypeHolding
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeProjectMode.Development.ToString(), "Проект в разработке" },
            { TypeProjectMode.Production.ToString(), "Проект опубликован" },
        };
    }
}
