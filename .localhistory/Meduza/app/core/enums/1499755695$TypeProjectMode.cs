﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeProjectMode { Development, Production };

    public static class EnumProjectMode
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeProjectMode.Development(), "Проект в разработке" },
            { TypeProjectMode.Production(), "Проект опубликован" },
        };
    }
}
