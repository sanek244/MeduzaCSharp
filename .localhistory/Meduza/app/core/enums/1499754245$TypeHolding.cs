﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    public enum TypeHolding { automaton, rightElongatedHanded, leftElongatedHanded, rightHanded, leftHanded, twoHandedPistol, minigun, flamethrower };

    public static class EnumHolding
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeProjectMode.Development.ToString(), "Проект в разработке" },
            { TypeProjectMode.Production.ToString(), "Проект опубликован" },
        };
    }
}
