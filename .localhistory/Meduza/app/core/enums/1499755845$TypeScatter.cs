﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeScatter { Increasing, Equable };

    public static class EnumScatter
    {
        public static Dictionary<TypeScatter, string> labels = new Dictionary<TypeScatter, string>() {
            { TypeScatter.Increasing, "Дерево" },
            { TypeScatter.Equable, "Органика" },
        };
    }
}
[INCREASING]: 'Возрастающий',
    [EQUABLE]: 'Равномерный',