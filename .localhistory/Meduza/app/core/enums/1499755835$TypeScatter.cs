﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeScatter { Tree, Bio, Iron, Concrete };

    public static class EnumScatter
    {
        public static Dictionary<TypeScatter, string> labels = new Dictionary<TypeScatter, string>() {
            { TypeScatter.Automaton, "Дерево" },
            { TypeScatter.RightElongatedHanded, "Органика" },
            { TypeScatter.LeftElongatedHanded, "Железо" },
            { TypeScatter.RightHanded, "Бетон" },
        };
    }
}