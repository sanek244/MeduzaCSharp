﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeScatter { Tree, Bio, Iron, Concrete };

    public static class EnumScatter
    {
        public static Dictionary<TypeScatter, string> labels = new Dictionary<TypeScatter, string>() {
            { TypeHolding.Automaton, "Дерево" },
            { TypeHolding.RightElongatedHanded, "Органика" },
            { TypeHolding.LeftElongatedHanded, "Железо" },
            { TypeHolding.RightHanded, "Бетон" },
        };
    }
}