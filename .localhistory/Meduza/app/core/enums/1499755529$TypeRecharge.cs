﻿using System.Collections.Generic;

namespace MeduzaServer
{
    const AUTOMATON = 'automaton';
    const PISTOL = 'pistol';
    const TOMMY_GUN = 'tommyGun';
    const TWO_PISTOL = 'twoPistol';
    public enum TypeMaterial { Tree, Bio, Iron, Concrete };

    public static class EnumMaterial
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeHolding.Automaton.ToString(), "Дерево" },
            { TypeHolding.RightElongatedHanded.ToString(), "Органика" },
            { TypeHolding.LeftElongatedHanded.ToString(), "Железо" },
            { TypeHolding.RightHanded.ToString(), "Бетон" },
        };
    }
}
[AUTOMATON]: 'Автомат',
    [PISTOL]: 'Пистолет',
    [TOMMY_GUN]: 'Пистолет-пулемёт',
    [TWO_PISTOL]: 'Два пистолета'