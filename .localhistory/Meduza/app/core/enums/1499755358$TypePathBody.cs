﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypePathBody { Body, Head, Bottom, BackBody };

    public static class EnumPathBody
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypePathBody.Body.ToString(), "Тело" },
            { TypePathBody.Head.ToString(), "Голова" },
            { TypePathBody.Bottom.ToString(), "Ноги" },
            { TypePathBody.BackBody.ToString(), "Сзади тела" }, //хвост, плащ
        };
    }
}