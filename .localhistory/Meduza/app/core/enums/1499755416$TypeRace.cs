﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeRace { Auman, Aonster, Aliens };

    public static class EnumRace
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeRace.RightElongatedHanded.ToString(), "Органика" },
            { TypeRace.LeftElongatedHanded.ToString(), "Железо" },
            { TypeRace.RightHanded.ToString(), "Бетон" },
        };
    }
}