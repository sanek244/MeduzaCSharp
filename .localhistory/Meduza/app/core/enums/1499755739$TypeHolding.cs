﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeHolding { Automaton, RightElongatedHanded, LeftElongatedHanded, RightHanded, LeftHanded, TwoHandedPistol, Minigun, Flamethrower };

    public static class EnumHolding
    {
        public static Dictionary<TypeHolding, string> labels = new Dictionary<TypeHolding, string>() {
            { TypeHolding.Automaton(), "Автомат" },
            { TypeHolding.RightElongatedHanded(), "Правая вытянутая рука" },
            { TypeHolding.LeftElongatedHanded(), "Левая вытянутая рука" },
            { TypeHolding.RightHanded(), "Правая рука" },
            { TypeHolding.LeftHanded(), "Левая рука" },
            { TypeHolding.TwoHandedPistol(), "Пистолет двумя руками" },
            { TypeHolding.Minigun(), "Миниган" },
            { TypeHolding.Flamethrower(), "Огнемёт" },
        };
    }
}