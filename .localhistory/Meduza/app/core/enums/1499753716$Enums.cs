﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    public static class Enums
    {
        public enum TypeProjectMode { Development, Production };

        public static Dictionary<string, string> LabelsTypeProjectMode = new Dictionary<string, string>() {
            { TypeProjectMode.Development.ToString(), "Проект в разработке" },
            { TypeProjectMode.Production.ToString(), "Проект опубликован" },
        };
    }
}
