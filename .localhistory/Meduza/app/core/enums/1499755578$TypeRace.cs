﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeRecharge { People, Monsters, Aliens };

    public static class EnumRecharge
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeRecharge.People.ToString(), "Люди" },
            { TypeRecharge.Monsters.ToString(), "Монстры" },
            { TypeRecharge.Aliens.ToString(), "Пришельцы" },
        };
    }
}
