﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeMaterial { Tree, Bio, Iron, Concrete };

    public static class EnumMaterial
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeHolding.Automaton.ToString(), "Дерево" },
            { TypeHolding.RightElongatedHanded.ToString(), "Органика" },
            { TypeHolding.LeftElongatedHanded.ToString(), "Железо" },
            { TypeHolding.RightHanded.ToString(), "Бетон" },
        };
    }
}

[TREE]: 'Дерево',
    [BIO]: 'Органика',
    [IRON]: 'Железо',
    [CONCRETE]: 'Бетон',