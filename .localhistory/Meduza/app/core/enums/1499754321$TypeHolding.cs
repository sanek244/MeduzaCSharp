﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    public enum TypeHolding { Automaton, RightElongatedHanded, LeftElongatedHanded, RightHanded, LeftHanded, TwoHandedPistol, Minigun, Flamethrower };

    public static class EnumHolding
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeHolding.Automaton.ToString(), "Автомат" },
            { TypeHolding.RightElongatedHanded.ToString(), "Правая вытянутая рука" },
            { TypeHolding.LeftElongatedHanded.ToString(), "Левая вытянутая рука" },
            { TypeHolding.RightHanded.ToString(), "Правая рука" },
            { TypeHolding.LeftHanded.ToString(), "Левая рука" },
            { TypeHolding.TwoHandedPistol.ToString(), "Пистолет двумя руками" },
            { TypeHolding.Minigun.ToString(), "Миниган" },
            { TypeHolding.Flamethrower.ToString(), "Огнемёт" },
        };
    }
}