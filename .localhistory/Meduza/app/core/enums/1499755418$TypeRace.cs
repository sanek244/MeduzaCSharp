﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeRace { Human, Monster, Aliens };

    public static class EnumRace
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeRace.Human.ToString(), "Органика" },
            { TypeRace.Monster.ToString(), "Железо" },
            { TypeRace.Aliens.ToString(), "Бетон" },
        };
    }
}