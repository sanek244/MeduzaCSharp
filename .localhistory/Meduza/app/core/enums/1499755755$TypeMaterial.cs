﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeMaterial { Tree, Bio, Iron, Concrete };

    public static class EnumMaterial
    {
        public static Dictionary<TypeMaterial, string> labels = new Dictionary<TypeMaterial, string>() {
            { TypeHolding.Automaton, "Дерево" },
            { TypeHolding.RightElongatedHanded, "Органика" },
            { TypeHolding.LeftElongatedHanded, "Железо" },
            { TypeHolding.RightHanded, "Бетон" },
        };
    }
}