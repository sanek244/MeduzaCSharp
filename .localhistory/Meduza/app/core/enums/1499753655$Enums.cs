﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    public static class Enums
    {
        public enum TypeProgramMode { Development, Production };
        //Development - режим разработки
        //Production - опубликованный проект

        public static Dictionary<string, string> LabelsTypeProgramMode = new Dictionary<string, string>() {
            { TypeProgramMode.Development.ToString(), "Проект в разработке" },
            { TypeProgramMode.Development.ToString(), "Проект опубликован" },
        };
    }
}
