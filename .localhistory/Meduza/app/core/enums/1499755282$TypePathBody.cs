﻿using System.Collections.Generic;

namespace MeduzaServer
{
    const BODY = 'body';
    const HEAD = 'head';
    const BOTTOM = 'bottom';
    const BACK_BODY = 'backBody';
    public enum TypeMaterial { body, head, bottom, backBody };

    public static class EnumMaterial
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeHolding.Automaton.ToString(), "Дерево" },
            { TypeHolding.RightElongatedHanded.ToString(), "Органика" },
            { TypeHolding.LeftElongatedHanded.ToString(), "Железо" },
            { TypeHolding.RightHanded.ToString(), "Бетон" },
        };
    }
}
TypePathBody.labels = {
    [BODY]: 'Тело',
    [HEAD]: 'Голов',
    [BOTTOM]: 'Ноги',
    [BACK_BODY]: 'Сзади тела' //хвост, плащ
};