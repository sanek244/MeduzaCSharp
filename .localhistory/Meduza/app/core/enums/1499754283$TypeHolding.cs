﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    public enum TypeHolding { Automaton, RightElongatedHanded, LeftElongatedHanded, RightHanded, LeftHanded, TwoHandedPistol, Minigun, Flamethrower };

    public static class EnumHolding
    {
        public static Dictionary<string, string> labels = new Dictionary<string, string>() {
            { TypeProjectMode.Development.ToString(), "Проект в разработке" },
            { TypeProjectMode.Production.ToString(), "Проект опубликован" },
        };
    }
}
