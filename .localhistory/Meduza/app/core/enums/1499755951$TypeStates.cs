﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeState { Base, Run, BlowLateral, BlowDirect, Recoil, Run_recoil };

    public static class EnumState
    {
        public static Dictionary<TypeState, string> labels = new Dictionary<TypeState, string>() {
            { TypeState.Base, "Дерево" },
            { TypeState.Run, "Органика" },
            { TypeState.BlowLateral, "Железо" },
            { TypeState.BlowDirect, "Бетон" },
            { TypeState.Recoil, "Бетон" },
            { TypeState.Run_recoil, "Бетон" },
        };
    }
}
[BASE]: 'Базовое',
    [RUN]: 'Бег',
    [RECOIL]: 'Отдача',
    [BLOW_LATERAL]: 'Боковой удар',
    [BLOW_DIRECT]: 'Прямой удар',
    [RUN_RECOIL]: 'Бег и отдача'