﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public enum TypeRecharge { Automaton, Pistol, TommyGun, TwoPistol };

    public static class EnumRecharge
    {
        public static Dictionary<TypeRecharge, string> labels = new Dictionary<string, string>() {
            { TypeRecharge.Automaton.ToString(), "Автомат" },
            { TypeRecharge.Pistol.ToString(), "Пистолет" },
            { TypeRecharge.TommyGun.ToString(), "Пистолет-пулемёт" },
            { TypeRecharge.TwoPistol.ToString(), "Два пистолета" },
        };
    }
}