﻿using System;

namespace MeduzaServer
{
    public enum TypeProgramMode { Development, Production };
    //Development - режим разработки
    //Production - опубликованный проект

    public Dictionary<string, string> LabelsTypeProgramMode = new Dictionary<string, string>();
}
