﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    /// <summary>
    /// Класс приложения - содержит набор классов из конфигураций и параметры конфигурации
    /// </summary>
    public static class Application
    {
        /// <summary>
        /// Контроллер базы данных
        /// </summary>
        public static DBController DBController = new FileController();
    }
}
