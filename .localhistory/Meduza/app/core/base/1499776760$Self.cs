﻿using System;

namespace MeduzaServer
{
    public class Self<TSelfReferenceType>
    {
        public static Type ClassNamee()
        {
            return typeof(TSelfReferenceType);
        }
    }
}
