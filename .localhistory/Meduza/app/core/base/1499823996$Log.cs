﻿using System;

namespace MeduzaServer
{
    public class Log : ActiveRecord
    {
        public string method;

        public string message;

        public string className;

        public string type;

        public DateTime createTime;

        public Log()
        {
            createTime = DateTime.Now;
            className = "";
            method = "";
            message = "";
            type = "";
            tableName = "logs";
        }
        public Log(string className, string method, string message, string type) : this()
        {
            this.className = className;
            this.method = method;
            this.message = message;
            this.type = type;
        }
    }
}
