﻿using System.Reflection;
using System;

namespace MeduzaServer
{
    /// <summary>
    /// Базовый объект
    /// </summary>
    public class BaseObject
    {
        //*** Методы ***//
        /// <summary>
        /// Возвращает имя класса
        /// </summary>
        /// <returns></returns>
        public string ClassName()
        {
            return GetType().Name;
        }

        /// <summary>
        /// Геттер для всех полей
        /// Использовать только при наличии составного имени!
        /// </summary>
        /// <param name="name">название поля</param>
        /// <returns></returns>
        public object Get(string name)
        {
            return GetType().GetField(name).GetValue(this);
        }

        /// <summary>
        /// Генерация ошибки 
        /// </summary>
        /// <param name="method">Метод вызвавший ошибку</param>
        /// <param name="message">Сообщение</param>
        public void Error(string method, string message)
        {
            if(Application.projectMode == TypeProjectMode.Development) {
                throw new Exception(message);
            }
            else {
                Log(method, message, "error");
            }
        }

        /// <summary>
        /// Генерация лога
        /// </summary>
        /// <param name="method">Метод вызвавший логирование</param>
        /// <param name="message">Сообщение</param>
        /// <param name="type">Тип лога</param>
        public void Log(string method, string message, string type = "")
        {
            Application.DBController.AddModel(new Log(ClassName(), method, message, type));
        }
    }
}
