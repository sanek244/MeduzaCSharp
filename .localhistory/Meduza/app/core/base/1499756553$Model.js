'use strict';

const BaseObject = require('./BaseObject');
const BaseInterface = require('./BaseInterface');
const Rule = require('./Rule');
const _forIn = require('lodash/forIn');
const config = require('../../../config');

/** Базовый объект предоставляет className для логов и rules с get и set для типизации и контроля + интерфейсы
 * @property changesAttributes {array} - изменённые атрибуты
 * @testOk
 * */
class Model extends BaseObject{

    init() {
        super.init(...arguments);

        /**
         * Набор правил в виде объекта для быстрого доступа по имени (через find по array работает колосально долго - см тесты)
         * @type {object}
         */
        this.rules = {};
        this.constructor.rules().map(rule => this.rules[rule.name] = rule);

        /** @var changesAttributes {array} - изменённые атрибуты */
        this.changesAttributes = [];

        //Создание переменных из правил
        this._createFields();
    }

    /**
     * @private
     * @param data
     * @testOk
     */
    afterInit(data) {
        //Загрузка данных из конструктора
        this.load(data, config.params.mode === 'development');
    }

    /**
     * Набор правил описывающих поля и тип полей класса + default значения
     * @returns {[*]}
     */
    static rules(){
        return [
            new Rule({callClass: Model.className(), name: 'interfaces', type: 'object', label: 'Интерфейсы класса'})
        ];
    }

    /**
     * Проверка на наличие интерфейса
     * @param name {string}
     * @returns {boolean}
     * @testOk
     */
    existInterface(name){
        return name in this.get('interfaces');
    }

    /**
     * Добавление интерфейса
     * @param _interface {BaseInterface}
     * @internale - только для наследников и данного класса
     * @testOk
     */
    addInterface(_interface){
        if(_interface.classNameParent().indexOf(BaseInterface.className()) === -1){
            return this.error('addInterface', 'Добавляемый интерфейс не унаследован от "{0}". Class: "{1}"'.format(BaseInterface, _interface))
        }

        if(_interface.checkClass(this)){
            this.interfaces[_interface.className()] = _interface;
        }
    }


    /**
     * Геттер. Объекты и экземпляры классов отдаются по ссылке!!!
     * Значительно повышает контроль при разработке и спасает от множества багов и недочётов разработчика.
     * @param {string} name - Название поля
     * @returns {*}
     * @testOk
     */
    get(name){
        //простое
        if(name.indexOf('.') === -1){
            if(name in this.rules){
                if(this[name] === null){
                    this[name] = this.rules[name].createDefaultValue(true);
                }

                return this[name];
            }

            this.error('get', 'Попытка получить отсутствующее поле: "{0}" Объект: "{1}"'.format(name, this));
            return null;
        }

        //имеется цепочка вызовов: model.size.width
        let names = name.split('.');
        name = names.splice(0, 1)[0];

        if(name in this.rules){
            if(this[name] === null){
                this[name] = this.rules[name].createDefaultValue(true);
            }

            if(this[name].get){
                return this[name].get(names.join('.'));
            }

            if(typeof this[name] === 'object'){
                let res = this[name];

                names.map(name => res = res[name]);

                return res;
            }
        }

        this.error('get', 'Попытка получить отсутствующее поле: "{0}" Объект: "{1}"'.format(name, this));
        return null;
    }

    /**
     * Сеттер. Объекты и экземпляры классов записваются по ссылке!!!
     * Значительно повышает контроль при разработке и спасает от множества багов и недочётов разработчика.
     * @param {string} name - название поля
     * @param value
     * @testOk
     */
    set(name, value){
        if(name.indexOf('.') === -1) {
            const rule = this.rules[name];

            if(!rule){
                return this.error('set', 'Попытка записать "' + value + '" в отсутствующее поле: ' + name);
            }

            if (rule.checkValue(value)) {
                this[name] = value;
                this.changesAttributes.push(name);
            }
        }
        else{
            //имеется цепочка вызовов: model.size.width
            let names = name.split('.');
            name = names.splice(0, 1)[0];

            this.changesAttributes.push(name);

            if(this[name] === null){
                this[name] = this.rules[name].createDefaultValue(true);
            }

            if(this[name].set){
                return this[name].set(names.join('.'), value);
            }
            else if(typeof this[name] === 'object'){
                if(names.length === 1){
                    return this[name][names[0]] = value;
                }

                let res = this[name];

                names.map(name => {
                    if(typeof res[name] === 'object'){
                        res = res[name]
                    }
                });

                return res[names[names.length - 1]] = value;
            }
        }
    }

    /**
     * Загрузка данных в модель
     * @param {object} data
     * @param {boolean} withErrors - выдават ошибки при отсутствии полей в модели?
     * @testOk
     */
    load(data, withErrors = false){
        if(data && typeof data === 'object' && !('className' in data)){
            _forIn(data, (value, key) => {
                if(key in this.rules || key.indexOf('.') && key.split('.')[0] in this.rules){
                    this.set(key, value);
                }
                else if(withErrors){
                    this.error('load', 'Попытка записать "{0}" в отсутствующее поле: "{1}"'.format(value, key));
                }
            })
        }
    }

    /**
     * Создание переменных на основе правил
     * @private
     * @testOk
     */
    _createFields(){
        //create fields
        _forIn(this.rules, (rule, ruleName) => {
            this[ruleName] = rule.getDefaultValue();
        });

        //set default data
        _forIn(this.rules, (rule, ruleName) => {
            this.set(ruleName, rule.getDefaultValue());
        });
    }

    /**
     * В объект (только поля, созданные по правилам(Rules))
     * @params {boolean} isAll - всё вернуть?
     * @returns {object}
     * @testOk
     */
    toObject(isAll = false){
        let object = {};

        _forIn(this.rules, (rule, ruleName) => {
            if(this.get(ruleName) !== null && this.get(ruleName).toObject){
                if(isAll){
                    object[ruleName] = this.get(ruleName).toObject();
                }
            }
            else{
                object[ruleName] = this.get(ruleName);
            }
        });

        return object;
    }
}
module.exports = Model;