﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer.app.core.base
{
    public class Self<TSelfReferenceType>
    {
        public static Type GetType()
        {
            return typeof(TSelfReferenceType);
        }
    }
}
