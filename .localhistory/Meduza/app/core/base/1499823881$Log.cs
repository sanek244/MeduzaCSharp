﻿using System;

namespace MeduzaServer
{
    public class Log
    {
        public string method;

        public string message;

        public string className;

        public string type;

        public DateTime createTime;

        public Log()
        {
            createTime = DateTime.Now;
            className = "";
            method = "";
            message = "";
            type = "";
        }
        public Log(string className, string method, string message, string type) : this()
        {

        }
    }
}
