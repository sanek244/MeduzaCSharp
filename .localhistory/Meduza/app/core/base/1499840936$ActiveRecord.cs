﻿
namespace MeduzaServer
{
    public class ActiveRecord : BaseObject
    {
        //*** Свойства ***//
        /// <summary>
        /// Название таблицы с данными этого класса
        /// </summary>
        public string TableName { get; set; }


        //*** Конструкторы ***//
        public ActiveRecord()
        {
            TableName = "not specified!";
        }





        /// <summary>
        /// Возвращает строку JSON
        /// </summary>
        /// <param name="isAll">Вернуть все аттрибуты? false - только изменённые </param>
        /// <returns></returns>
        public string ToJSONString(bool isAll = false)
        {
            if (isAll) {
                return JsonConvert.SerializeObject(this);
            }

            string res = "{";

            foreach (string attribute in changesAttributes) {
                var value = Get(attribute);
                string separateValue = value.GetType() == typeof(string) ? "\"" : "";
                res += "\"" + attribute + "\":" + separateValue + value + separateValue + ",";
            }

            return res.Substring(0, res.Length - 1) + "}";
        }
    }
}
