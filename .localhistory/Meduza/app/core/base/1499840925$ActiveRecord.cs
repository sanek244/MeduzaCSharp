﻿
namespace MeduzaServer
{
    public class ActiveRecord : BaseObject
    {
        //*** Свойства ***//
        /// <summary>
        /// Название таблицы с данными этого класса
        /// </summary>
        public string TableName { get; set; }


        //*** Конструкторы ***//
        public ActiveRecord()
        {
            TableName = "not specified!";
        }
    }
}
