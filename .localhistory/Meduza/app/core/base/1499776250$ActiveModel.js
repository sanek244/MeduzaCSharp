'use strict';

const Model = require('./Model');
const Rule = require('./Rule');
const App = require('./Application');
const Helper = require('../Helper');

/** Активная модель - модель имеющая связь с базой данных
 * @testOk */
class ActiveModel extends Model{
    init(){
        super.init(...arguments);

        this.tableName = this.constructor.tableName;
    }

    /**
     * Возвращает набор правил: Rules
     * @returns {[Rule]}
     */
    static rules(){
        return [
            new Rule({callClass: this.className(), name: 'id', type: 'integer', label: 'id модели', default: -1}),
            new Rule({callClass: this.className(), name: 'objectId', type: 'string', label: 'глобальный id объекта'}),
            new Rule({callClass: this.className(), name: 'name', type: 'string', label: 'Название объекта (CamelCase)'}),
            new Rule({callClass: this.className(), name: 'label', type: 'string', label: 'Отображаемое название'}),
        ].concat(super.rules());
    }

    /**
     * Поиск экземпляра совподающего с условием
     * @param {string|number|object|null} condition - условия в ассоциативном виде либо как число\строка равная id модели
     * @return {null|ActiveModel}
     * @testOk
     */
    static findOne(condition = null){
        return this.find(condition, true);
    }

    /**
     * Поиск экземпляторв совпадающих с условием
     * @param {string|number|object|null} condition - условия в ассоциативном виде либо как число\строка равная id модели
     * @param {boolean} isOne - вернуть 1 экземпляр?
     * @return {null|ActiveModel|[ActiveModel]}
     * @testOk
     */
    static find(condition = null, isOne = false){
        if(this.tableName === ActiveModel.tableName){
            this.error('find', '"tableName" модели не задан!');
            return null;
        }
        return App.get('db').find(this.tableName, condition, isOne);
    }
    /**
     * В объект (только поля, созданные по правилам(Rules))
     * @params {boolean} isAll - всё вернуть?
     * @returns {object}
     * @testOk
     */
    toObject(isAll = false) {
        let object = {};

        _forIn(this.rules, (rule, ruleName) => {
            if (this.get(ruleName) !== null && this.get(ruleName).toObject) {
                if (isAll) {
                    object[ruleName] = this.get(ruleName).toObject();
                }
            }
            else {
                object[ruleName] = this.get(ruleName);
            }
        });

        return object;
    }
}
ActiveModel.tableName = 'not specified!';
module.exports = ActiveModel;