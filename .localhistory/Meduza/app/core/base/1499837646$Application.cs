﻿
namespace MeduzaServer
{
    /// <summary>
    /// Класс приложения - содержит набор классов из конфигураций и параметры конфигурации
    /// </summary>
    public static class Application
    {
        /// <summary>
        /// Контроллер базы данных
        /// </summary>
        public static DBController DBController = new FileController();

        /// <summary>
        /// Состояние проекта
        /// </summary>
        public static TypeProjectMode projectMode = TypeProjectMode.Development;
    }
}
