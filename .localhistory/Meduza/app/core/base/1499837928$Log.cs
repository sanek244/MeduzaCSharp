﻿using System;

namespace MeduzaServer
{
    public class Log : ActiveRecord
    {
        //*** Свойства ***//
        public string method { get; set; }

        public string message;

        public string className;

        public string type;

        public DateTime createTime;


        //*** Конструкторы ***//
        public Log()
        {
            createTime = DateTime.Now;
            className = "";
            method = "";
            message = "";
            type = "";
            TableName = "log";
        }
        public Log(string className, string method, string message, string type) : this()
        {
            this.className = className;
            this.method = method;
            this.message = message;
            this.type = type;
        }
    }
}
