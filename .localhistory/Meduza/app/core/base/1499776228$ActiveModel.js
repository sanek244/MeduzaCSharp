'use strict';

const Model = require('./Model');
const Rule = require('./Rule');
const App = require('./Application');
const Helper = require('../Helper');

/** Активная модель - модель имеющая связь с базой данных
 * @testOk */
class ActiveModel extends Model{
    init(){
        super.init(...arguments);

        this.tableName = this.constructor.tableName;
    }

    /**
     * Возвращает набор правил: Rules
     * @returns {[Rule]}
     */
    static rules(){
        return [
            new Rule({callClass: this.className(), name: 'id', type: 'integer', label: 'id модели', default: -1}),
            new Rule({callClass: this.className(), name: 'objectId', type: 'string', label: 'глобальный id объекта'}),
            new Rule({callClass: this.className(), name: 'name', type: 'string', label: 'Название объекта (CamelCase)'}),
            new Rule({callClass: this.className(), name: 'label', type: 'string', label: 'Отображаемое название'}),
        ].concat(super.rules());
    }

    /**
     * Поиск экземпляра совподающего с условием
     * @param {string|number|object|null} condition - условия в ассоциативном виде либо как число\строка равная id модели
     * @return {null|ActiveModel}
     * @testOk
     */
    static findOne(condition = null){
        return this.find(condition, true);
    }

    /**
     * Поиск экземпляторв совпадающих с условием
     * @param {string|number|object|null} condition - условия в ассоциативном виде либо как число\строка равная id модели
     * @param {boolean} isOne - вернуть 1 экземпляр?
     * @return {null|ActiveModel|[ActiveModel]}
     * @testOk
     */
    static find(condition = null, isOne = false){
        if(this.tableName === ActiveModel.tableName){
            this.error('find', '"tableName" модели не задан!');
            return null;
        }
        return App.get('db').find(this.tableName, condition, isOne);
    }

    /**
     * Сеттер (добавляем relation по id)
     * @param {string} name - название поля
     * @param value
     * @testOk
     */
    set(name, value){
        super.set(name, value);

        //Если изменилось поле с id
        if(name.indexOf('Id') > 0){
            //название связанного поля
            const nameRelated = name.substr(0, name.length -2);

            if(nameRelated in this){
                //название связанной модели
                const modelRelated = this.rules[nameRelated].type;

                //установка нового значения
                if(value === -1){
                    this.set(nameRelated, null);
                }
                else {
                    if(modelRelated !== 'mixed'){
                        this.set(nameRelated, modelRelated.findOne(value));
                    }
                    else{
                        //mixed - смотрим наличие переменной с постфиксом Type, и наличие в ней класса
                        if((nameRelated + 'Type') in this.rules && this.get(nameRelated + 'Type') && this.get(nameRelated + 'Type').className){
                            this.set(nameRelated, this.get(nameRelated + 'Type').findOne(value));
                        }
                    }
                }
            }
        }
    }
}
ActiveModel.tableName = 'not specified!';
module.exports = ActiveModel;