﻿using System.Reflection;

namespace Meduza
{
    /// <summary>
    /// Базовый объект
    /// </summary>
    public class BaseObject
    {
        /// <summary>
        /// Return class name
        /// </summary>
        /// <returns></returns>
        public string ClassName()
        {
            return GetType().Name;
        }

        static public string ClassNamee()
        {
            return typeof(base).Name;
        }

        public object Get(string name)
        {
            return GetType().GetField(name).GetValue(this);
        }
    }
}
