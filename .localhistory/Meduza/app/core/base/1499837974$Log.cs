﻿using System;

namespace MeduzaServer
{
    public class Log : ActiveRecord
    {
        //*** Свойства ***//
        public string Method { get; set; }
        public string Message { get; set; }
        public string Class { get; set; }
        public string Type { get; set; }

        public DateTime CreateTime { get; set; }


        //*** Конструкторы ***//
        public Log()
        {
            createTime = DateTime.Now;
            className = "";
            method = "";
            message = "";
            type = "";
            TableName = "log";
        }
        public Log(string className, string method, string message, string type) : this()
        {
            this.className = className;
            this.method = method;
            this.message = message;
            this.type = type;
        }
    }
}
