﻿
using System.Collections.Generic;
using Newtonsoft.Json;

namespace MeduzaServer
{
    /// <summary>
    /// Сущности связанные с бд
    /// </summary>
    public class ActiveRecord : BaseObject
    {
        //*** Свойства ***//
        /// <summary>
        /// Название таблицы с данными этого класса
        /// </summary>
        public virtual string TableName { get => ClassName(); }
        /// <summary>
        /// Изменённые атрибуты
        /// </summary>
        public List<string> СhangesAttributes { get; set; }


        //*** Конструкторы ***//
        public ActiveRecord()
        {
            СhangesAttributes = new List<string>();
        }


        //*** Методы ***//
        /// <summary>
        /// Возвращает строку JSON
        /// </summary>
        /// <param name="isAll">Вернуть все аттрибуты? false - только изменённые </param>
        /// <returns></returns>
        public virtual string ToJSONString(bool isAll = false)
        {
            if (isAll) {
                return JsonConvert.SerializeObject(this);
            }

            string res = "{";

            foreach (string attribute in СhangesAttributes) {
                var value = Get(attribute);
                string separateValue = value.GetType() == typeof(string) ? "\"" : "";
                res += "\"" + attribute + "\":" + separateValue + value + separateValue + ",";
            }

            return res.Substring(0, res.Length - 1) + "}";
        }
    }
}
