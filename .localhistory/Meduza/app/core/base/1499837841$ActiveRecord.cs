﻿
namespace MeduzaServer
{
    public class ActiveRecord : BaseObject
    {
        /// <summary>
        /// Название таблицы с данными этого класса
        /// </summary>
        public string TableName { get; set; }

        public ActiveRecord()
        {
            TableName = "not specified!";
        }
    }
}
