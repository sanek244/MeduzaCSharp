﻿using System.Reflection;

namespace Meduza
{
    /// <summary>
    /// Базовый объект
    /// </summary>
    public class BaseObject
    {
        /// <summary>
        /// Возвращает имя класса
        /// </summary>
        /// <returns></returns>
        public string ClassName()
        {
            return GetType().Name;
        }

        /// <summary>
        /// Геттер для всех полей
        /// Использовать только при наличии составного имени!
        /// </summary>
        /// <param name="name">название поля</param>
        /// <returns></returns>
        public object Get(string name)
        {
            return GetType().GetField(name).GetValue(this);
        }

        /// <summary>
        /// Генерация ошибки 
        /// </summary>
        /// <param name="message">Сообщение</param>
        public void Error(string message)
        {
            
        }

        /**
     * Добавление лога
     * @param {string} method
     * @param {string} message
     * @param {string} importance
     * @param {string} type
     * @testOk
     */
        public void Log(method, message,type = '')
        {
            const Log = require('./Log');
            new Log(this.className(), method, message, importance, type);
        }
    }
}
