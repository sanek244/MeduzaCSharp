﻿using System;

namespace MeduzaServer
{
    public class Self<T>
    {
        public static string ClassNamee()
        {
            return typeof(T).Name;
        }

        public static new Type GetType()
        {
            return typeof(T);
        }
    }
}
