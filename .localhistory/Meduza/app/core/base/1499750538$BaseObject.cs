﻿using System.Reflection;

namespace Meduza
{
    /// <summary>
    /// Базовый объект
    /// </summary>
    public class BaseObject
    {
        /// <summary>
        /// Возвращает имя класса
        /// </summary>
        /// <returns></returns>
        public string ClassName()
        {
            return GetType().Name;
        }

        /// <summary>
        /// Геттер для всех полей
        /// Использовать только при наличии составного имени!
        /// </summary>
        /// <param name="name">название поля</param>
        /// <returns></returns>
        public object Get(string name)
        {
            return GetType().GetField(name).GetValue(this);
        }

        /**
     * Добавление ошибки
     * @param {string} method
     * @param {string} message
     * @param {string} importance
     * @param {string} type
     * @testOk
     */
        static error(method, message, importance = 'error', type = '')
        {
            const Error = require('./Error');
            new Error(this.className(), method, message, importance, type);
        }
        public void Error(string message)
        {

        }

        /**
     * Добавление лога
     * @param {string} method
     * @param {string} message
     * @param {string} importance
     * @param {string} type
     * @testOk
     */
        static log(method, message, importance = 'log', type = '')
        {
            const Log = require('./Log');
            new Log(this.className(), method, message, importance, type);
        }
    }
}
