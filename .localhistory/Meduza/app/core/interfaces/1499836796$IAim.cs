﻿
namespace MeduzaServer
{
    /** Прицеливание - обязывает иметь точку прицеливания */
    //Пример: Турель, монстры, нпс
    public interface IAim
    {
        Point aimPoint { get; set; };
    }
}
