﻿
using System.Collections.Generic;

namespace MeduzaServer
{
    public interface IAbility
    {
        void activation(DataProvider dataProvider, SimpleGameObject model, int timeComplete, Dictionary<string, object> params);
    }
}
