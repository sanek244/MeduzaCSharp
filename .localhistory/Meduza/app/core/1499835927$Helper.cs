﻿using System;
using System.Text.RegularExpressions;

namespace MeduzaServer
{
    /// <summary>
    /// Вспомогательный класс
    /// </summary>
    public static class Helper
    {
        public static Random rnd = new Random();

        /// <summary>
        /// Расстояние между точками
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <returns></returns>
        static public double Distance(Point point1, Point point2)
        {
            return Math.Sqrt(Math.Pow(point1.x - point2.x, 2) + Math.Pow(point1.y - point2.y, 2));
        }

        /// <summary>
        /// Возвращает угол поворота между относительной прямой паралельной оси X и проведённой через точку А, и точкой Б
        /// Угол считается относительно положительной оси Х по часовой стрелке
        /// </summary>
        /// <param name="pointCenter">Центральная точка</param>
        /// <param name="pointB">Точка Б</param>
        /// <returns>Угол между точкой Б и параллельной к очи X прямой, проходящей через точку А</returns>
        static public float GetRotateAngle(Point pointCenter, Point pointB)
        {
            float x = pointCenter.x - pointB.x;
            float y = pointCenter.y - pointB.y;

            if (x == 0 && y == 0) {
                return 0;
            }

            float angle = (float)((Math.Atan(y / x) + (x >= 0 ? Math.PI : 0)) * 180 / Math.PI);

            return angle >= 0 ? angle : 360 + angle;
        }

        /// <summary>
        /// Возвращает сгенерированный 'уникальный' идентификатор
        /// </summary>
        /// <returns></returns>
        static public string GenerateUid()
        {
            MatchEvaluator myEvaluator = new MatchEvaluator((c) => {
                var r = (int)(rnd.NextDouble() * 16) | 0;
                var v = c.Value == "x" ? r : (r & 0x3 | 0x8);

                return Convert.ToString(v, 16);
            });

            return Regex.Replace("xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx", "[xy]", myEvaluator);
        }

        /// <summary>
        /// Преобразует DateTime в unix время
        /// </summary>
        /// <param name="date">Дата, которую нужно кнвертировать</param>
        /// <returns></returns>
        static public uint ToUnixTime(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date - origin;
            return (uint)Math.Floor(diff.TotalSeconds);
        }


        //*** Разбросы  ***//
        /**
         * Нарастающий разброс
         * @param {number} rotateAngle - текущий угол точки прицеливания
         * @param {number} max - максимальное значение разброса
         * @param {number} timeLast - время последнего вызова (unix-время)
         * @param {number} forceGamer - сила игрока
         * @param {boolean} maxGet - вернуть максимальное отклонение
         * @returns {number}
         * @testOk
         */
        static public GetScatterIncreasing(float rotateAngle, float max, uing timeLast = 0, forceGamer = 1, maxGet = false)
        {
            //полное время восстановления (миллисекунды)
            const timeRecovery = 1000 / forceGamer;

            //сила времени
            const forceTime = timeLast > new Date().getTime() - timeRecovery
                ? timeLast - (new Date().getTime() - timeRecovery)
                : 0;

            //(forceTime / timeRecovery) - процент от 0 до 1
            const newMax = forceTime / timeRecovery / forceGamer * max;

            if (maxGet) {
                return rotateAngle + newMax;
            }

            return Helper.getRandom(rotateAngle - newMax, rotateAngle + newMax);
        }

        /**
         * Равномерный разброс
         * @param {number} rotateAngle - текущий угол точки прицеливания
         * @param {number} max - максимальное значение разброса
         * @param {number} forceGamer - сила игрока
         * @returns {number}
         * @testOk
         */
        static public getScatterUniform(rotateAngle, max, forceGamer)
        {
            const newMax = max / forceGamer;

            return Helper.getRandom(rotateAngle - newMax, rotateAngle + newMax);
        }

        /**
         * Возвращает разброс оружия
         * @param {Weapon} weapon
         * @param {Gamer} gamer
         * @returns {number}
         * @testOk
         */
        static public getScatterWeapon(weapon, gamer)
        {
            if (weapon.get('scatterType') === TypeScatter.INCREASING) {
                return Helper.getScatterIncreasing(
                    weapon.get('rectangle.rotateAngle'),
                    weapon.get('scatterMax'),
                    weapon.get('attackLastTime'),
                    gamer.get('force')
                );
            }

            return Helper.getScatterUniform(
                weapon.get('rectangle.rotateAngle'),
                weapon.get('scatterMax'),
                gamer.get('force')
            );
        }
    }
}
