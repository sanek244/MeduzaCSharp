﻿
using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Простой игровой объект
    /// </summary>
    public class SimpleGameObject : BaseObject
    {
        /// <summary>
        /// Прямоугольник модели
        /// </summary>
        public Rectangle rectangle;

        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public string objectId;

        /// <summary>
        /// Имя объекта
        /// </summary>
        public string name;

        /// <summary>
        /// Изменённые атрибуты
        /// </summary>
        public List<string> changesAttributes;

        public int id;

        public string label;


        /// <summary>
        /// Название таблицы с данными этого класса
        /// </summary>
        public static string tableName = "not specified!";

        //*** Конструкторы ***//
        public SimpleGameObject()
        {
            objectId = Helper.GenerateUid();
            rectangle = new Rectangle();
            changesAttributes = new List<string>();
            name = "";
        }
    }
}
