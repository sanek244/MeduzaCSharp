﻿using System.Collections.Generic;
using N.Wtonsoft.Json;

namespace MeduzaServer
{
    /// <summary>
    /// Простой игровой объект
    /// </summary>
    public class SimpleGameObject : ActiveRecord
    {
        private string objectId;

        //*** Свойства ***//
        /// <summary>
        /// Прямоугольник модели
        /// </summary>
        public Rectangle Rectangle { get; set; }
        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public string ObjectId { get => objectId }
        /// <summary>
        /// Имя объекта
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Id экземпляра в бд
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Русскоязычная надпись описывающая сущность
        /// </summary>
        public string Label { get; set; }


        //*** Конструкторы ***//
        public SimpleGameObject()
        {
            objectId = Helper.GenerateUid();
            Rectangle = new Rectangle();
            Name = "";
        }
    }
}
