
/** Активная модель - модель имеющая связь с базой данных
 * @testOk */
class ActiveModel extends Model{
    /**
     * Возвращает набор правил: Rules
     * @returns {[Rule]}
     */
    static rules(){
        return [
            new Rule({callClass: this.className(), name: 'id', type: 'integer', label: 'id модели', default: -1}),
            new Rule({callClass: this.className(), name: 'objectId', type: 'string', label: 'глобальный id объекта'}),
            new Rule({callClass: this.className(), name: 'name', type: 'string', label: 'Название объекта (CamelCase)'}),
            new Rule({callClass: this.className(), name: 'label', type: 'string', label: 'Отображаемое название'}),
        ].concat(super.rules());
    }

    /**
     * В объект (только поля, созданные по правилам(Rules))
     * @params {boolean} isAll - всё вернуть?
     * @returns {object}
     * @testOk
     */
    toObject(isAll = false) {
        let object = {};

        _forIn(this.rules, (rule, ruleName) => {
            if (this.get(ruleName) !== null && this.get(ruleName).toObject) {
                if (isAll) {
                    object[ruleName] = this.get(ruleName).toObject();
                }
            }
            else {
                object[ruleName] = this.get(ruleName);
            }
        });

        return object;
    }
}
