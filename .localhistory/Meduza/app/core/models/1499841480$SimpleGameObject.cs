﻿using System.Collections.Generic;
using N.Wtonsoft.Json;

namespace MeduzaServer
{
    /// <summary>
    /// Простой игровой объект
    /// </summary>
    public class SimpleGameObject : ActiveRecord
    {
        //*** Свойства ***//
        /// <summary>
        /// Прямоугольник модели
        /// </summary>
        public Rectangle Rectangle { get; set; }
        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public string ObjectId { get; set; }
        /// <summary>
        /// Имя объекта
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Id экземпляра в бд
        /// </summary>
        public int Ide { get; set; }
        /// <summary>
        /// Русскоязычная надпись описывающая сущность
        /// </summary>
        public string Labele { get; set; }


        //*** Конструкторы ***//
        public SimpleGameObject()
        {
            ObjectId = Helper.GenerateUid();
            Rectangle = new Rectangle();
            ChangesAttributes = new List<string>();
            Name = "";
        }
    }
}
