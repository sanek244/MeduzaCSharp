﻿using System.Collections.Generic;
using N.Wtonsoft.Json;

namespace MeduzaServer
{
    /// <summary>
    /// Простой игровой объект
    /// </summary>
    public class SimpleGameObject : ActiveRecord
    {
        //*** Свойства ***//
        /// <summary>
        /// Прямоугольник модели
        /// </summary>
        public Rectangle Rectangle;
        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public string ObjectId;
        /// <summary>
        /// Имя объекта
        /// </summary>
        public string Name;
        /// <summary>
        /// Id экземпляра в бд
        /// </summary>
        public int Id;
        /// <summary>
        /// Русскоязычная надпись описывающая сущность
        /// </summary>
        public string Label;


        //*** Конструкторы ***//
        public SimpleGameObject()
        {
            ObjectId = Helper.GenerateUid();
            Rectangle = new Rectangle();
            ChangesAttributes = new List<string>();
            Name = "";
        }
    }
}
