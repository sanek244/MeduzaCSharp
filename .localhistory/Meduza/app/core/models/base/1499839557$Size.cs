﻿
namespace MeduzaServer
{
    /// <summary>
    /// Размер
    /// </summary>
    public class Size : BaseObject
    {
        //*** Свойства ***//
        public float Width { get; set; }
        public float Height { get; set; }

        public Size()
        {}
        public Size(float Width)
        {
            this.Width = Width;
        }
        public Size(float Width, float height) : this(Width)
        {
            Height = height;
        }
    }
}
