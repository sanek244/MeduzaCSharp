﻿
namespace MeduzaServer
{
    /// <summary>
    /// Прямоугольная область с лучами - границами
    /// </summary>
    public class RectangleRay : Rectangle
    {
        //для нахождения пересечений с ругими моделями
        private Ray leftRay;
        private Ray rightRay;
        private Ray topRay;
        private Ray bottomRay;

        private Point location;
        private Size size;
        private float rotateAngle;


        //*** Свойства ***//
        /// <summary>
        /// Левый луч траектории объекта
        /// </summary>
        public Ray LeftRay
        {
            get
            {
                if(leftRay == null) {
                    leftRay = new Ray(Location, Helper.GetAngleRightTriangle(Location, RotateAngle, Size.Width));
                }

                return leftRay;
            }
        }
        /// <summary>
        /// Правый луч траектории объекта
        /// </summary>
        public Ray RightRay
        {
            get
            {
                if (rightRay == null) {
                    rightRay = new Ray(TopRay.SecondPoint, Helper.GetAngleRightTriangle(TopRay.SecondPoint, RotateAngle + 90, Size.Height));
                }

                return rightRay;
            }
        }
        /// <summary>
        /// Верхний луч траектории объекта
        /// </summary>
        public Ray TopRay
        {
            get
            {
                if (topRay == null) {
                    topRay = new Ray(RightRay.SecondPoint, Helper.GetAngleRightTriangle(RightRay.SecondPoint, RotateAngle + 180, Size.Width));
                }

                return topRay;
            }
        }
        /// <summary>
        /// Нижний луч траектории объекта
        /// </summary>
        public Ray BottomRay
        {
            get
            {
                if (bottomRay == null) {
                    bottomRay = new Ray(BottomRay.SecondPoint, Location);
                }

                return bottomRay;
            }
        }

        /// <summary>
        /// Углы повёрнутого прямоугольника
        /// </summary>
        public Square AnglesLocation { get; set; }

        public override Point Location
        {
            get => location;
            set
            {
                location = value;
                leftRay = null;
                rightRay = null;
                topRay = null;
                bottomRay = null;
            }
        }
        public override Size Size
        {
            get => size;
            set
            {
                size = value;
                leftRay = null;
                rightRay = null;
                topRay = null;
                bottomRay = null;
            }
        }
        public override float RotateAngle
        {
            get => rotateAngle;
            set
            {
                rotateAngle = value;
                leftRay = null;
                rightRay = null;
                topRay = null;
                bottomRay = null;
            }
        }
    }
}
