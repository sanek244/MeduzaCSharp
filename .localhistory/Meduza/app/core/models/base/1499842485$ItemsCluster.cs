﻿
namespace MeduzaServer
{
    public class ItemsCluster : ActiveRecord
    {
        //*** Свойства ***//
        /// <summary>
        /// Нижняя часть (ноги, лапы)
        /// </summary>
        public SimpleGameObject bottom', type: SimpleGameObject, label: 'Нижняя часть (ноги, лапы)'}),
        /// <summary>
        /// 
        /// </summary>
        public SimpleGameObject head', type: SimpleGameObject, label: 'Голова и всё, что на ней'}),
        /// <summary>
        /// Голова и всё, что на ней
        /// </summary>
        public SimpleGameObject backBody', type: SimpleGameObject, label: 'Сзади тела(хвост, плащ)'}),
        /// <summary>
        /// Сзади тела(хвост, плащ)
        /// </summary>
        public SimpleGameObject body', type: SimpleGameObject, label: 'На теле (бронежилет)'}),
        /// <summary>
        /// На теле (бронежилет)
        /// </summary>
        public SimpleGameObject leftHand', type: SimpleGameObject, label: 'Левая рука'}),
        /// <summary>
        /// Левая рука
        /// </summary>
        public SimpleGameObject rightHand', type: SimpleGameObject, label: 'Правая рука'}),
        /// <summary>
        /// Правая рука
        /// </summary>
        public SimpleGameObject hands', type: SimpleGameObject, label: 'Руки (двуручные оружия)'}),

        /// <summary>
        /// Руки (двуручные оружия)
        /// </summary>
        public int backpackSize', type: 'int', label: 'Размер рюкзака'}),

        /// <summary>
        /// Размер рюкзака
        /// </summary>
        public List< backpackItems', type: 'array', label: 'Вещи в рюкзаке'}),
    }
}
