﻿
namespace MeduzaServer
{
    public class Img
    {
        //*** Свойства ***//
        /// <summary>
        /// Путь до папки с картинкой
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// z расположение картинки
        /// </summary>
        public int ZIndex { get; set; }
        /// <summary>
        /// Размер
        /// </summary>
        public Size Size { get; set; }
        /// <summary>
        /// Расширение файла
        /// </summary>
        public string FileExtension { get; set; }
    }
}
