﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    /// <summary>
    /// Луч
    /// </summary>
    public class Ray
    {
        /// <summary>
        /// Точка начала луча
        /// </summary>
        public Point startPoint;
        /// <summary>
        /// Вторая точка луча (для создания прямой)
        /// </summary>
        public Point secondPoint;


        public double b;
        public double c;
        public double a;
        public double dx;
        public double dy;

        public Ray(Point startPoint, Point secondPoint)
        {
            this.secondPoint = secondPoint;
            this.startPoint = startPoint;
        }
    }
}
