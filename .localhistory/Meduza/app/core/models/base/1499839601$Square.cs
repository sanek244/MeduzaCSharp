﻿
namespace MeduzaServer
{
    /// <summary>
    /// Квадрат
    /// </summary>
    public class Square
    {
        /// <summary>
        /// Верхний левый угол
        /// </summary>
        public Point leftTop;
        /// <summary>
        /// Верхний правый угол
        /// </summary>
        public Point rightTop;
        /// <summary>
        /// Нижний левый угол
        /// </summary>
        public Point leftBottom;
        /// <summary>
        /// Нижний правый угол
        /// </summary>
        public Point rightBottom;
    }
}
