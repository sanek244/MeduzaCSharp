﻿
using System.Collections.Generic;

namespace MeduzaServer
{
    public class ItemsCluster : ActiveRecord
    {
        //*** Свойства ***//
        /// <summary>
        /// Нижняя часть (ноги, лапы)
        /// </summary>
        public SimpleGameObject Bottom { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public SimpleGameObject Head { get; set; }
        /// <summary>
        /// Голова и всё, что на ней
        /// </summary>
        public SimpleGameObject BackBody { get; set; }
        /// <summary>
        /// Сзади тела(хвост, плащ)
        /// </summary>
        public SimpleGameObject Body { get; set; }
        /// <summary>
        /// На теле (бронежилет)
        /// </summary>
        public SimpleGameObject LeftHand { get; set; }
        /// <summary>
        /// Левая рука
        /// </summary>
        public SimpleGameObject RightHand { get; set; }
        /// <summary>
        /// Правая рука
        /// </summary>
        public SimpleGameObject Hands { get; set; }

        /// <summary>
        /// Руки (двуручные оружия)
        /// </summary>
        public int BackpackSize { get; set; }

        /// <summary>
        /// Размер рюкзака
        /// </summary>
        public List<Item> BackpackItems { get; set; }
    }
}
