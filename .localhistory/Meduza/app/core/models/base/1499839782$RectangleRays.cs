﻿
namespace MeduzaServer
{
    /// <summary>
    /// Прямоугольная область с лучами - границами
    /// </summary>
    public class RectangleRay : Rectangle
    {

        //*** Свойства ***//
        /// <summary>
        /// Углы повёрнутого прямоугольника
        /// </summary>
        public Square anglesLocation;

        //для нахождения пересечений с ругими моделями
        /// <summary>
        /// Левый луч траектории объекта
        /// </summary>
        public Ray leftRay;
        /// <summary>
        /// Правый луч траектории объекта
        /// </summary>
        public Ray rightRay;
        /// <summary>
        /// Верхний луч траектории объекта
        /// </summary>
        public Ray topRay;
        /// <summary>
        /// Нижний луч траектории объекта
        /// </summary>
        public Ray bottomRay;

        public Ray LeftRay
        {
            set { leftRay = value; }
            get
            {
                if(leftRay != null) {
                    return leftRay;
                }
                leftRay = new Ray(location, Helper.GetAngleRightTriangle(location, rotateAngle, size.Width));

                return leftRay;
            }
        }
    }
}
