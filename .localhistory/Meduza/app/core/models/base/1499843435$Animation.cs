﻿
namespace MeduzaServer
{
    public class Animation
    {
        //для сохранения объёма данных, картинки анимации грузят по имени анимации + номера от 1 до frameCount включительно
        /// <summary>
        /// Количество картинок
        /// </summary>
        public int frameCount', type: 'number', label: 'Количество картинок'}),
        /// <summary>
        /// 
        /// </summary>
        public int frameChangeTime', type: 'int', label: 'Время между кадрами (миллисекунды)', default: 100}),
        /// <summary>
        /// 
        /// </summary>
        public int lifeTime', type: 'int', label: 'Время жизни (миллисекунды)', default: 3000}),
        /// <summary>
        /// 
        /// </summary>
        public int hiddenTime', type: 'int', label: 'Время исчезновения (миллисекунды)', default: 1000}),
        /// <summary>
        /// 
        /// </summary>
        public int currentFrame', type: 'int', label: 'Номер показываемого кадра', default: 1}),
        /// <summary>
        /// 
        /// </summary>
        public int isRandomFirst', type: 'boolean', label: 'Первый кадр рандомный?'}),
        /// <summary>
        /// 
        /// </summary>
        public int isRevert', type: 'boolean', label: 'В обратном направлении показывать?'}),
    }
}
