﻿namespace MeduzaServer
{
    /// <summary>
    /// Набор звуков столкновений пули
    /// </summary>
    public class SoundClusterWeapon : ActiveRecord
    {
        private Sound recharge;
        private Sound shot;
        private Sound emptyShot;

        private int rechargeId = -1;
        private int shotId = -1;
        private int emptyShotId = -1;

        new Rule({ callClass: this.className(), name: 'rechargeId', type: 'int', label: 'id перезарядки'}),
            new Rule({ callClass: this.className(), name: 'recharge', type: BaseSound, label: 'Перезарядка'}),

            new Rule({ callClass: this.className(), name: 'shotId', type: 'int', label: 'id выстрела'}),
            new Rule({ callClass: this.className(), name: 'shot', type: BaseSound, label: 'Выстрел'}),

            new Rule({ callClass: this.className(), name: 'emptyShotId', type: 'int', label: 'id пустого щелчка'}),
            new Rule({ callClass: this.className(), name: 'emptyShot', type: BaseSound, label: 'Пустой щелчок'}),

        //*** Свойства ***///
        /// <summary>
        /// id перезарядки
        /// </summary>
        public int RechargeId
        {
            get => rechargeId;
            set
            {
                rechargeId = value;
                recharge = Application.DBController.FindOne<Sound>(value);
            }
        }
        /// <summary>
        /// Звук перезарядки
        /// </summary>
        public Sound Recharge { get; }

        /// <summary>
        /// id звука столкновения с деревом
        /// </summary>
        public int TreeId
        {
            get => treeId;
            set
            {
                treeId = value;
                tree = Application.DBController.FindOne<Sound>(value);
            }
        }
        /// <summary>
        /// Звук столкновения пули с деревом
        /// </summary>
        public Sound Tree { get; }

        /// <summary>
        /// id звука столкновения с биологической сущностью
        /// </summary>
        public int BioId
        {
            get => bioId;
            set
            {
                bioId = value;
                bio = Application.DBController.FindOne<Sound>(value);
            }
        }
        /// <summary>
        /// Звук столкновения пули с биологической сущностью
        /// </summary>
        public Sound Bio { get; }
    }
}
