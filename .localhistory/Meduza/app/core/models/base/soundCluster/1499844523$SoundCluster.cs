﻿namespace MeduzaServer
{
    /// <summary>
    /// Набор звуков на один объект
    /// </summary>
    public class SoundCluster
    {
        public Dictionary<string, int> States { get; set; };
        return [
            //Пример: {openDoor: ...} ассоциативный массив вида: [состояние => idBaseSound]
            new Rule({ callClass: this.className(), name: 'states', type: 'object', label: 'На разные типы состояний'}),
    }
}
