﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Набор звуков на один объект
    /// </summary>
    public class SoundCluster
    {
        /// <summary>
        /// На разные типы состояний
        /// Пример: {openDoor: ...} ассоциативный массив вида: [состояние => idBaseSound]
        /// </summary>
        public Dictionary<string, int> States { get; set; }
        return [
            new Rule({ callClass: this.className(), name: 'states', type: 'object', label: 'На разные типы состояний'}),
    }
}
