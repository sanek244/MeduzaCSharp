﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Набор звуков столкновений пули
    /// </summary>
    public class SoundClusterBulletContacts : ActiveRecord
    {
        /// <summary>
        /// На разные типы состояний
        /// Пример: {openDoor: ...} ассоциативный массив вида: [состояние => idBaseSound]
        /// </summary>
        public Dictionary<string, int> States { get; set; }
    }
}
