﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Набор звуков столкновений пули
    /// </summary>
    public class SoundClusterBulletContacts : ActiveRecord
    {
        private Sound concrete;
        private Sound tree;
        private Sound bio;
        private Sound iron;

        private int concreteId = -1;
        private int treeId = -1;
        private int bioId = -1;
        private int ironId = -1;


        //*** Свойства ***///
        /// <summary>
        /// id звука столкновения с бетоном
        /// </summary>
        public int ConcreteId {
            get => concreteId;
            set
            {
                concreteId = value;
                concrete = Application.DBController.FindOne<Sound>(new { { "id", value + "" } });
            }
        }
        public Sound Concrete { get; }
        new Rule({ callClass: this.className(), name: TypeMaterial.Concrete + 'Id', type: 'int', label: 'id звука столкновения с бетоном', default: -1}),
            new Rule({ callClass: this.className(), name: TypeMaterial.CONCRETE, type: BaseSound, label: 'звук столкновения с бетоном'}),

            new Rule({ callClass: this.className(), name: TypeMaterial.TREE + 'Id', type: 'int', label: 'id звука столкновения с деревом', default: -1}),
            new Rule({ callClass: this.className(), name: TypeMaterial.TREE, type: BaseSound, label: 'звук столкновения с деревом'}),

            new Rule({ callClass: this.className(), name: TypeMaterial.BIO + 'Id', type: 'int', label: 'id звука столкновения с существом', default: -1}),
            new Rule({ callClass: this.className(), name: TypeMaterial.BIO, type: BaseSound, label: 'звук столкновения с существом'}),

            new Rule({ callClass: this.className(), name: TypeMaterial.IRON + 'Id', type: 'int', label: 'id звука столкновения с железом', default: -1}),
            new Rule({ callClass: this.className(), name: TypeMaterial.IRON, type: BaseSound, label: 'звук столкновения с железом'}),
    }
}
