﻿
namespace MeduzaServer
{
    /// <summary>
    /// Размер
    /// </summary>
    public class Size : BaseObject
    {
        //*** Свойства ***//
        public float Width { get; set; }
        public float Height { get; set; }

        public Size()
        {}
        public Size(float width)
        {
            Width = width;
        }
        public Size(float Width, float height) : this(Width)
        {
            Height = height;
        }
    }
}
