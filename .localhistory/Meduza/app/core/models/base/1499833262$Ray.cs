﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    /// <summary>
    /// Луч
    /// </summary>
    public class Ray
    {
        /// <summary>
        /// Точка начала луча
        /// </summary>
        public Point startPoint;
        /// <summary>
        /// Вторая точка луча (для создания прямой)
        /// </summary>
        public Point secondPoint;

        /// <summary>
        /// Коэфициент из уравнения a*x + b*y + c
        /// </summary>
        public double a;
        /// <summary>
        /// Коэфициент из уравнения a*x + b*y + c
        /// </summary>
        public double b;
        /// <summary>
        /// Коэфициент из уравнения a*x + b*y + c
        /// </summary>
        public double c;

        /// <summary>
        /// Прирост луча по x между начальной и второй точкой
        /// </summary>
        public double dx;
        /// <summary>
        /// Прирост луча по x между начальной и второй точкой
        /// </summary>
        public double dy;

        public Ray(Point startPoint, Point secondPoint)
        {
            this.secondPoint = secondPoint;
            this.startPoint = startPoint;
        }
    }
}
