﻿
namespace MeduzaServer
{
    /// <summary>
    /// Прямоугольная область с лучами - границами
    /// </summary>
    public class RectangleRay : Rectangle
    {

        public Square anglesLocation', type: Square, label: 'Угоы повёрнутого прямоугольника'}),


        public Ray leftRay', type: Ray, label: 'Левый луч траектории объекта', default: null}),
        public Ray rightRay', type: Ray, label: 'Правый луч траектории объекта', default: null}),
        //для нахождения пересечений с ругими моделями
        public Ray topRay', type: Ray, label: 'Верхний луч траектории объекта', default: null}),
        public Ray bottomRay', type: Ray, label: 'Нижний луч траектории объекта', default: null}),
    }
}
