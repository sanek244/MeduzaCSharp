﻿using System.Collections.Generic;


namespace MeduzaServer
{
    /// <summary>
    /// Модель возвращаемая в SpaceCollection в функциях столкновения моделей
    /// </summary>
    public class Contacts : BaseObject
    {
        //*** Свойства ***//
        /// <summary>
        /// Столкновения
        /// </summary>
        public List<Contact> contacts;
        /// <summary>
        /// Последнее местоположение модели
        /// </summary>
        public Point lastlocationModel;


        //*** Конструкторы ***//
        public Contacts()
        {
            contacts = new List<Contact>();
        }
        public Contacts(Point lastlocationModel) : this()
        {
            this.lastlocationModel = lastlocationModel;
        }
        public Contacts(List<SimpleGameObject> models, Point lastlocationModel) : this(lastlocationModel)
        {
            foreach(var model in models) {
                contacts.Add(new Contact(model));
            }
        }


        //*** Методы ***//
        public void Add(SimpleGameObject model)
        {
            contacts.Add(new Contact(model));
        }
    }
}
