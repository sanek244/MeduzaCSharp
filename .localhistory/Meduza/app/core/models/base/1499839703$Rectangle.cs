﻿
namespace MeduzaServer
{
    /// <summary>
    /// Прямоугольная область
    /// </summary>
    public class Rectangle : BaseObject
    {
        //*** Свойства ***//
        /// <summary>
        /// Расположение (верхний левый угол)
        /// </summary>
        public Point Location { get; set; }
        /// <summary>
        /// Размер
        /// </summary>
        public Size Size { get; set; }
        /// <summary>
        /// Угол поворота
        /// </summary>
        public float RotateAngle { get; set; }


        //*** Конструкторы ***//
        public Rectangle() {
            Location = new Point();
            Size = new Size();
        }
        public Rectangle(SimpleGameObject simpleGameObject) : this()
        {
            Location.X = simpleGameObject.Rectangle.Location.X;
            Location.Y = simpleGameObject.Rectangle.Location.Y;
            Size.Width = simpleGameObject.Rectangle.Size.Width;
            Size.Height = simpleGameObject.Rectangle.Size.Height;
            RotateAngle = simpleGameObject.Rectangle.RotateAngle;
        }
        public Rectangle(Point location) : this() 
        {
            this.Location.X = location.X;
            this.Location.Y = location.Y;
        }
        public Rectangle(Point location, Size size) : this(location)
        {
            Size.Width = size.Width;
            Size.Height = size.Height;
        }
        public Rectangle(Point location, Size size, float rotateAngle) : this(location, size)
        {
            RotateAngle = rotateAngle;
        }


        //*** Методы ***//
        /// <summary>
        /// Является ли наклон прямоугольника прямым? 0, 90, 180, 270, 360 градусов
        /// </summary>
        /// <returns></returns>
        public bool IsRightAngle()
        {
            return RotateAngle % 90 == 0;
        }

        /// <summary>
        /// Содержится ли точка внутри данной модели?
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool IsContainsPoint(Point point)
        {
            return !(
                point.X > Location.X + Size.Width ||
                point.Y > Location.Y + Size.Height ||
                point.X < Location.X ||
                point.Y < Location.Y
            );
        }

        /// <summary>
        /// Проверка на содержание модели внутри себя (без пересечений и лежаний на границы)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool IsContainsModel(SimpleGameObject model)
        {
            return model.Rectangle.Location.X > Location.X
                && model.Rectangle.Location.X + model.Rectangle.size.Width < Location.X + Size.Width
                && model.Rectangle.Location.Y > Location.Y
                && model.Rectangle.Location.Y + model.Rectangle.size.Height < Location.Y + Size.Height;
        }

        /// <summary>
        /// Проверка на пересечение с моделью или её содержание внутри
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool IsIntersectionOrContainsModel(SimpleGameObject model)
        {
            //по x линия модели внутри области
            bool xInside = model.Rectangle.Location.X <= this.Location.X + this.size.Width
                && model.Rectangle.Location.X >= this.Location.X
                || model.Rectangle.Location.X + model.Rectangle.size.Width <= this.Location.X + this.size.Width
                && model.Rectangle.Location.X + model.Rectangle.size.Width >= this.Location.X;

            //по y линия модели внутри области
            bool yInside = model.Rectangle.Location.Y <= this.Location.Y + this.size.Height
                && model.Rectangle.Location.Y >= this.Location.Y
                || model.Rectangle.Location.Y + model.Rectangle.size.Height <= this.Location.Y + this.size.Height
                && model.Rectangle.Location.Y + model.Rectangle.size.Height >= this.Location.Y;

            //модель внутри или пересекает её двумя сторонами
            if (xInside && yInside) {
                return true;
            }

            //по x стороны модели вокруг области
            bool xOutside = model.Rectangle.Location.X <= this.Location.X
                && model.Rectangle.Location.X + model.Rectangle.size.Width >= this.Location.X + this.size.Width;

            //по y стороны модели вокруг области
            bool yOutside = model.Rectangle.Location.Y <= this.Location.Y
                && model.Rectangle.Location.Y + model.Rectangle.size.Height >= this.Location.Y + this.size.Height;

            return xInside && yOutside || yInside && xOutside;
        }
    }
}
