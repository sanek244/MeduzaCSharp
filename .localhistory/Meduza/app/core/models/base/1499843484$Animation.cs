﻿
namespace MeduzaServer
{
    public class Animation
    {
        //*** Свойства ***//
        //для сохранения объёма данных, картинки анимации грузят по имени анимации + номера от 1 до frameCount включительно
        /// <summary>
        /// Количество картинок
        /// </summary>
        public int FrameCount { get; set; }
        /// <summary>
        /// Время между кадрами (миллисекунды)
        /// </summary>
        public int FrameChangeTime { get; set; }
        /// <summary>
        /// Время жизни (миллисекунды)
        /// </summary>
        public int LifeTime { get; set; }
        /// <summary>
        /// Время исчезновения (миллисекунды)
        /// </summary>
        public int HiddenTime { get; set; }
        /// <summary>
        /// Номер показываемого кадра
        /// </summary>
        public int CurrentFrame { get; set; }
        /// <summary>
        /// Первый кадр рандомный?
        /// </summary>
        public int IsRandomFirst { get; set; }
        /// <summary>
        /// В обратном направлении показывать?
        /// </summary>
        public int IsRevert { get; set; }
    }
}
