﻿
namespace MeduzaServer
{
    /// <summary>
    /// Прямоугольная область
    /// </summary>
    public class Rectangle : BaseObject
    {
        //*** Свойства ***//
        /// <summary>
        /// Расположение (верхний левый угол)
        /// </summary>
        public Point Location { get; set; }
        /// <summary>
        /// Размер
        /// </summary>
        public Size Size { get; set; }
        /// <summary>
        /// Угол поворота
        /// </summary>
        public float RotateAngle { get; set; }


        //*** Конструкторы ***//
        public Rectangle() {
            Location = new Point();
            Size = new Size();
        }
        public Rectangle(SimpleGameObject simpleGameObject) : this()
        {
            Location.X = simpleGameObject.rectangle.Location.X;
            Location.Y = simpleGameObject.rectangle.Location.Y;
            Size.Width = simpleGameObject.rectangle.Size.Width;
            Size.Height = simpleGameObject.rectangle.Size.Height;
            rotateAngle = simpleGameObject.rectangle.rotateAngle;
        }
        public Rectangle(Point Location) : this() 
        {
            this.Location.X = Location.X;
            this.Location.Y = Location.Y;
        }
        public Rectangle(Point Location, Size size) : this(Location)
        {
            this.size.Width = size.Width;
            this.size.height = size.height;
        }
        public Rectangle(Point Location, Size size, float rotateAngle) : this(Location, size)
        {
            this.rotateAngle = rotateAngle;
        }


        //*** Методы ***//
        /// <summary>
        /// Является ли наклон прямоугольника прямым? 0, 90, 180, 270, 360 градусов
        /// </summary>
        /// <returns></returns>
        public bool IsRightAngle()
        {
            return rotateAngle % 90 == 0;
        }

        /// <summary>
        /// Содержится ли точка внутри данной модели?
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool IsContainsPoint(Point point)
        {
            return !(
                point.X > Location.X + size.Width ||
                point.Y > Location.Y + size.height ||
                point.X < Location.X ||
                point.Y < Location.Y
            );
        }

        /// <summary>
        /// Проверка на содержание модели внутри себя (без пересечений и лежаний на границы)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool IsContainsModel(SimpleGameObject model)
        {
            return model.rectangle.Location.X > Location.X
                && model.rectangle.Location.X + model.rectangle.size.Width < Location.X + size.Width
                && model.rectangle.Location.Y > Location.Y
                && model.rectangle.Location.Y + model.rectangle.size.height < Location.Y + size.height;
        }

        /// <summary>
        /// Проверка на пересечение с моделью или её содержание внутри
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool IsIntersectionOrContainsModel(SimpleGameObject model)
        {
            //по x линия модели внутри области
            bool xInside = model.rectangle.Location.X <= this.Location.X + this.size.Width
                && model.rectangle.Location.X >= this.Location.X
                || model.rectangle.Location.X + model.rectangle.size.Width <= this.Location.X + this.size.Width
                && model.rectangle.Location.X + model.rectangle.size.Width >= this.Location.X;

            //по y линия модели внутри области
            bool yInside = model.rectangle.Location.Y <= this.Location.Y + this.size.height
                && model.rectangle.Location.Y >= this.Location.Y
                || model.rectangle.Location.Y + model.rectangle.size.height <= this.Location.Y + this.size.height
                && model.rectangle.Location.Y + model.rectangle.size.height >= this.Location.Y;

            //модель внутри или пересекает её двумя сторонами
            if (xInside && yInside) {
                return true;
            }

            //по x стороны модели вокруг области
            bool xOutside = model.rectangle.Location.X <= this.Location.X
                && model.rectangle.Location.X + model.rectangle.size.Width >= this.Location.X + this.size.Width;

            //по y стороны модели вокруг области
            bool yOutside = model.rectangle.Location.Y <= this.Location.Y
                && model.rectangle.Location.Y + model.rectangle.size.height >= this.Location.Y + this.size.height;

            return xInside && yOutside || yInside && xOutside;
        }
    }
}
