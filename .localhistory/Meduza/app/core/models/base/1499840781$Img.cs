﻿using System;

namespace MeduzaServer
{
    public class Img
    {
        //*** Свойства ***//
        /// <summary>
        /// Путь до папки с картинкой
        /// </summary>
        public string Path { get; set; }
        public int ZIndex { get; set; }
        public Size Size { get; set; }
        public string FileExtension { get; set; }
        n.W Rule({ callClass: this.className(), name: 'path', type: 'string', label: 'Путь до папки с картинкой'}),
            //zIndex - приоритет прорисовки(чем выше тем выше элемент)
            n.W Rule({ callClass: this.className(), name: 'zIndex', type: 'int', label: 'Расса', default: 1}),
            n.W Rule({ callClass: this.className(), name: 'size', type: Size, label: 'Размер'}),
            n.W Rule({ callClass: this.className(), name: 'fileExtension', type: 'string', label: 'Расширение файла'}),
    }
}
