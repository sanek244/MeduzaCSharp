﻿
namespace Meduza
{
    /// <summary>
    /// Столкновение моделей
    /// </summary>
    public class Contact : BaseObject
    {
        /// <summary>
        /// Модель с которой было совершено столкновение
        /// </summary>
        public SimpleGameObject model;
        /// <summary>
        /// Точка столкновения
        /// </summary>
        public Point pointContact;
        /// <summary>
        /// Расстояние от места старта модели, до точки столкновения
        /// </summary>
        public float distance;

        public Contact(SimpleGameObject model)
        {
            this.model = model;
        }
    }
}
