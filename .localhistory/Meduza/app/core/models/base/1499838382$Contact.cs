﻿
namespace MeduzaServer
{
    /// <summary>
    /// Столкновение моделей
    /// </summary>
    public class Contact : BaseObject
    {
        /// <summary>
        /// Модель с которой было совершено столкновение
        /// </summary>
        public SimpleGameObject Model { get; set; }
        /// <summary>
        /// Точка столкновения
        /// </summary>
        public Point PointContact { get; set; }
        /// <summary>
        /// Расстояние от места старта модели, до точки столкновения
        /// </summary>
        public float Distance { get; set; }


        public Contact(SimpleGameObject model)
        {
            this.model = model;
        }
    }
}
