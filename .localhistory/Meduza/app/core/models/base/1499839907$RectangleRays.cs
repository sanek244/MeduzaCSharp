﻿
namespace MeduzaServer
{
    /// <summary>
    /// Прямоугольная область с лучами - границами
    /// </summary>
    public class RectangleRay : Rectangle
    {
        //для нахождения пересечений с ругими моделями
        /// <summary>
        /// Левый луч траектории объекта
        /// </summary>
        private Ray leftRay;
        /// <summary>
        /// Правый луч траектории объекта
        /// </summary>
        private Ray rightRay;
        /// <summary>
        /// Верхний луч траектории объекта
        /// </summary>
        private Ray topRay;
        /// <summary>
        /// Нижний луч траектории объекта
        /// </summary>
        private Ray bottomRay;

        //*** Свойства ***//
        public Ray LeftRay
        {
            get
            {
                if(leftRay == null) {
                    leftRay = new Ray(Location, Helper.GetAngleRightTriangle(Location, RotateAngle, Size.Width));
                }

                return leftRay;
            }
        }
        public Ray rightRay
        {
            get
            {
                if (leftRay == null) {
                    leftRay = new Ray(Location, Helper.GetAngleRightTriangle(Location, RotateAngle, Size.Width));
                }

                return leftRay;
            }
        }
        public Ray LeftRay
        {
            get
            {
                if (leftRay == null) {
                    leftRay = new Ray(Location, Helper.GetAngleRightTriangle(Location, RotateAngle, Size.Width));
                }

                return leftRay;
            }
        }
        public Ray LeftRay
        {
            get
            {
                if (leftRay == null) {
                    leftRay = new Ray(Location, Helper.GetAngleRightTriangle(Location, RotateAngle, Size.Width));
                }

                return leftRay;
            }
        }

        /// <summary>
        /// Углы повёрнутого прямоугольника
        /// </summary>
        public Square AnglesLocation { get; set; }
    }
}
