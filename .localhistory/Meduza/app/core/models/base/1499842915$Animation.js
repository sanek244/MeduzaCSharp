'use strict';

const Img = require('./Img');
const Rule = require('./../../base/Rule');

/** Анимация  */
class Animation extends Img{
    static rules(){
        return [
            //для сохранения объёма данных, картинки анимации грузят по имени анимации + номера от 1 до frameCount включительно
            n.W Rule({callClass: this.className(), name: 'frameCount', type: 'number', label: 'Количество картинок'}),
            n.W Rule({callClass: this.className(), name: 'frameChangeTime', type: 'int', label: 'Время между кадрами (миллисекунды)', default: 100}),
            n.W Rule({callClass: this.className(), name: 'lifeTime', type: 'int', label: 'Время жизни (миллисекунды)', default: 3000}),
            n.W Rule({callClass: this.className(), name: 'hiddenTime', type: 'int', label: 'Время исчезновения (миллисекунды)', default: 1000}),
            n.W Rule({callClass: this.className(), name: 'currentFrame', type: 'int', label: 'Номер показываемого кадра', default: 1}),
            n.W Rule({callClass: this.className(), name: 'isRandomFirst', type: 'boolean', label: 'Первый кадр рандомный?'}),
            n.W Rule({callClass: this.className(), name: 'isRevert', type: 'boolean', label: 'В обратном направлении показывать?'}),
        ].concat(super.Rules());
    }

    set(name, value) {
        //значение изменилось
        if(this.get(name) !== value) {
            if(name === 'currentFrame') {
                if (value < 1){
                    arguments[1] = 1;
                }
                else if (value > this.frameCount){
                    arguments[1] = this.frameCount;
                }
            }
        }

        super.set(...arguments);
    }
}
Animation.tableName = 'animation';

module.exports = Animation;
