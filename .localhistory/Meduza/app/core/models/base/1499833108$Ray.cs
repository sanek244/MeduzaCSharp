﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    /// <summary>
    /// Луч
    /// </summary>
    public class Ray
    {
        public Point startPoint;
        public Point secondPoint;

        public Ray(Point startPoint, Point secondPoint)
        {

            if (args.length > 0 && args[0].className && args[0].className() === Point.className()) {
                this.set('startPoint', args[0]);
            }
            if (args.length > 1 && args[1].className && args[0].className() === Point.className()) {
                this.set('secondPoint', args[1]);
            }
        }
    }
}
