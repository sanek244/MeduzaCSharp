﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    public class Square
    {
        /// <summary>
        /// Верхний левый угол
        /// </summary>
        public Point leftTop;
        /// <summary>
        /// Верхний правый угол
        /// </summary>
        public Point rightTop;
        /// <summary>
        /// Нижний левый угол
        /// </summary>
        public Point leftBottom;
        /// <summary>
        /// Нижний правый угол
        /// </summary>
        public Point rightBottom;
    }
}
