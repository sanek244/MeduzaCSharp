﻿
namespace MeduzaServer
{
    public class ItemsCluster : ActiveRecord
    {
        //*** Свойства ***//
        public SimpleGameObject bottom', type: SimpleGameObject, label: 'Нижняя часть (ноги, лапы)'}),
        public SimpleGameObject head', type: SimpleGameObject, label: 'Голова и всё, что на ней'}),
        public SimpleGameObjectbackBody', type: SimpleGameObject, label: 'Сзади тела(хвост, плащ)'}),
        public SimpleGameObject body', type: SimpleGameObject, label: 'На теле (бронежилет)'}),
        public SimpleGameObject leftHand', type: SimpleGameObject, label: 'Левая рука'}),
        public SimpleGameObject rightHand', type: SimpleGameObject, label: 'Правая рука'}),
        public SimpleGameObject hands', type: SimpleGameObject, label: 'Руки (двуручные оружия)'}),

        public int backpackSize', type: 'int', label: 'Размер рюкзака'}),

        public List< backpackItems', type: 'array', label: 'Вещи в рюкзаке'}),
    }
}
