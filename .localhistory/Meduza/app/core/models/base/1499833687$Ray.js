
class Ray extends Model {

            // A * x + B * y + C
            new Rule({callClass: this.className(), name: 'B', type: 'number'}),
            new Rule({callClass: this.className(), name: 'C', type: 'number'}),
            new Rule({callClass: this.className(), name: 'A', type: 'number'}),

            new Rule({callClass: this.className(), name: 'dx', type: 'number'}),
            new Rule({callClass: this.className(), name: 'dy', type: 'number'}),
        ].concat(super.rules());
    }

    set(name, value){
        super.set(name, value);

        //Если изменились открытые поля
        if(name.indexOf('startPoint') !== -1 || name.indexOf('secondPoint') !== -1){
            const startPoint = this.get('startPoint');
            const secondPoint = this.get('secondPoint');
            //B = x1 - x2
            //A = y2 - y1
            //C = x2 * y1 - x1 * y2
            this.set('B', startPoint.get('x') - secondPoint.get('x'));
            this.set('C', secondPoint.get('x') * startPoint.get('y') - startPoint.get('x') * secondPoint.get('y'));
            this.set('A', secondPoint.get('y') - startPoint.get('y'));

            const distance = Helper.distance(startPoint, secondPoint);
            this.set('dx', (secondPoint.get('x') - startPoint.get('x')) / distance);
            this.set('dy', (secondPoint.get('y') - startPoint.get('y')) / distance);
        }
    }

    /**
     * Функция получения значения x - точки пересечения ординаты (y) с лучём
     * @param {int} y
     * @returns {number}
     * @testOk
     */
    fx(y){
        // A * x = -B * y - C
        // x = (-B * y - C) / A
        return ( -this.get('B') * y - this.get('C')) / this.get('A');
    }
    /**
     * Функция получения значения y - точки пересечения абсциссы(x) с лучём
     * @param {int} x
     * @returns {number}
     * @testOk
     */
    fy(x){
        // B * y = -A * x - C
        // y = (-A * x - C) / B
        return ( -this.get('A') * x - this.get('C')) / this.get('B');
    }

    /**
     * Возвращает расстояние от прямой до точки
     * @param {Point} point
     * @testOk
     */
    getDistanceToPoint(point){
        // |A * x + B * y + C| / (корень из (A ^ 2 + B ^ 2))
        return Math.abs(
                this.get('A') * point.get('x') +
                this.get('B') * point.get('y') +
                this.get('C'))
            /
            Math.sqrt(
                Math.pow(this.get('A'), 2) +
                Math.pow(this.get('B'), 2));
    }

    /**
     * Возвращает точку пересечения прямых
     * @param {Ray} ray
     * @param {boolean} isThisSegment - текущий луч - отрезок с границами в startPoint и secondPoint
     * @param {boolean} isRaySegment - входящий луч - отрезок с границами в startPoint и secondPoint
     * @return null|Point, null, если прямые параллельны или совпадают
     * @testOk
     */
    getPointIntersectionRay(ray, isThisSegment = false, isRaySegment = false){
        //делитель
        const divider = this.get('A') * ray.get('B') - ray.get('A') * this.get('B');
        if(divider === 0){
            return null;
        }

        let y = (ray.get('A') * this.get('C') - ray.get('C') * this.get('A')) / (- ray.get('A') * this.get('B') + ray.get('B') * this.get('A'));
        let x = (-this.get('B') * y - this.get('C')) / this.get('A');

        //точка пересечения лучей
        let point = new Point(x, y);

        if(Number.isNaN(x) || Number.isNaN(y)){
            y = (this.get('A') * ray.get('C') - this.get('C') * ray.get('A')) / (- this.get('A') * ray.get('B') + this.get('B') * ray.get('A'));
            x = (-ray.get('B') * y - ray.get('C')) / ray.get('A');

            //точка пересечения лучей
            point = new Point(x, y);
        }

        if(Number.isNaN(x) || Number.isNaN(y)){
            //точка пересечения лучей
            point = new Point(
                -(this.get('C') * ray.get('B') - ray.get('C') * this.get('B') / divider),
                -(this.get('A') * ray.get('C') - ray.get('A') * this.get('C') / divider));
        }

        //Если текущий луч - отрезок
        if(isThisSegment){
            const minX = Math.min(this.get('startPoint.x'), this.get('secondPoint.x'));
            const maxX = Math.max(this.get('startPoint.x'), this.get('secondPoint.x'));
            const minY = Math.min(this.get('startPoint.y'), this.get('secondPoint.y'));
            const maxY = Math.max(this.get('startPoint.y'), this.get('secondPoint.y'));

            //точка пересечения лучей за границами текущего отрезка?
            if(point.get('x') > maxX || point.get('x') < minX || point.get('y') > maxY || point.get('y') < minY){
                return null;
            }
        }

        //Если входящий луч - отрезок
        if(isRaySegment){
            const minX = Math.min(ray.get('startPoint.x'), ray.get('secondPoint.x'));
            const maxX = Math.max(ray.get('startPoint.x'), ray.get('secondPoint.x'));
            const minY = Math.min(ray.get('startPoint.y'), ray.get('secondPoint.y'));
            const maxY = Math.max(ray.get('startPoint.y'), ray.get('secondPoint.y'));

            //точка пересечения лучей за границами входящего отрезка?
            if(point.get('x') > maxX || point.get('x') < minX || point.get('y') > maxY || point.get('y') < minY){
                return null;
            }
        }

        return point;
    }
}

module.exports = Ray;
