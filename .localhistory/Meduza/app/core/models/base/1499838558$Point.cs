﻿
namespace MeduzaServer
{
    /// <summary>
    /// Точка в пространстве
    /// </summary>
    public class Point : BaseObject
    {
        //*** Свойства ***//
        public float X { get; set; }
        public float Y { get; set; }


        //*** Конструкторы ***//
        public Point() { }
        public Point(float x)
        {
            X = x;
        }
        public Point(float x, float y) : this(x)
        {
            Y = y;
        }

        /// <summary>
        /// Оператор сложения
        /// </summary>
        /// <param name="pointA"></param>
        /// <param name="pointB"></param>
        /// <returns></returns>
        static public Point operator + (Point pointA, Point pointB)
        {
            return new Point(pointA.x + pointB.x, pointA.y + pointB.y);
        }
    }
}
