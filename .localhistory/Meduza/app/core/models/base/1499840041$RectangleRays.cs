﻿
namespace MeduzaServer
{
    /// <summary>
    /// Прямоугольная область с лучами - границами
    /// </summary>
    public class RectangleRay : Rectangle
    {
        //для нахождения пересечений с ругими моделями
        /// <summary>
        /// Левый луч траектории объекта
        /// </summary>
        private Ray leftRay;
        /// <summary>
        /// Правый луч траектории объекта
        /// </summary>
        private Ray rightRay;
        /// <summary>
        /// Верхний луч траектории объекта
        /// </summary>
        private Ray topRay;
        /// <summary>
        /// Нижний луч траектории объекта
        /// </summary>
        private Ray bottomRay;

        //*** Свойства ***//
        public Ray LeftRay
        {
            get
            {
                if(leftRay == null) {
                    leftRay = new Ray(Location, Helper.GetAngleRightTriangle(Location, RotateAngle, Size.Width));
                }

                return leftRay;
            }
        }
        public Ray RightRay
        {
            get
            {
                if (rightRay == null) {
                    rightRay = new Ray(topRay.SecondPoint, Helper.GetAngleRightTriangle(topRay.SecondPoint, RotateAngle + 90, Size.Height));
                }

                return rightRay;
            }
        }
        public Ray TopRay
        {
            get
            {
                if (topRay == null) {
                    topRay = new Ray(rightRay.SecondPoint, Helper.GetAngleRightTriangle(rightRay.SecondPoint, RotateAngle + 180, Size.Width));
                }

                return topRay;
            }
        }
        public Ray BottomRay
        {
            get
            {
                if (bottomRay == null) {
                    bottomRay = new Ray(bottomRay.SecondPoint, Location);
                }

                return bottomRay;
            }
        }

        /// <summary>
        /// Углы повёрнутого прямоугольника
        /// </summary>
        public Square AnglesLocation { get; set; }
    }
}
