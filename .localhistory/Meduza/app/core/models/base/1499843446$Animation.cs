﻿
namespace MeduzaServer
{
    public class Animation
    {
        //для сохранения объёма данных, картинки анимации грузят по имени анимации + номера от 1 до frameCount включительно
        /// <summary>
        /// Количество картинок
        /// </summary>
        public int frameCount,
        /// <summary>
        /// Время между кадрами (миллисекунды)
        /// </summary>
        public int frameChangeTime,
        /// <summary>
        /// Время жизни (миллисекунды)
        /// </summary>
        public int lifeTime,
        /// <summary>
        /// Время исчезновения (миллисекунды)
        /// </summary>
        public int hiddenTime,
        /// <summary>
        /// Номер показываемого кадра
        /// </summary>
        public int currentFrame,
        /// <summary>
        /// Первый кадр рандомный?
        /// </summary>
        public int isRandomFirst,
        /// <summary>
        /// В обратном направлении показывать?
        /// </summary>
        public int isRevert,
    }
}
