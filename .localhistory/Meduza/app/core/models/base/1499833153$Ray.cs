﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    /// <summary>
    /// Луч
    /// </summary>
    public class Ray
    {
        public Point startPoint;
        public Point secondPoint;

        public Ray(Point startPoint, Point secondPoint)
        {
            this.secondPoint = secondPoint;
            this.startPoint = startPoint;
        }
    }
}
