﻿
namespace MeduzaServer
{
    public class Img : ActiveRecord
    {
        //*** Свойства ***//
        /// <summary>
        /// Путь до папки с картинкой
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// z расположение картинки
        /// </summary>
        public int ZIndex { get; set; }
        /// <summary>
        /// Размер
        /// </summary>
        public Size Size { get; set; }
        /// <summary>
        /// Расширение файла
        /// </summary>
        public string FileExtension { get; set; }


        //*** Конструкторы ***//
        public Img()
        {
            TableName = "img";
        }


        /**
         * В объект (только поля, созданные по правилам(Rules))
         * @returns {{}}
         */
        public string ToJSONString()
        {
            var res = base.ToJSONString();

            object['type'] = this.className();

            return object;
        }
    }
}
