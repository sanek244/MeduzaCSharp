﻿namespace MeduzaServer
{
    /// <summary>
    /// Звук
    /// </summary>
    public class Sound
    {
        //rectangle.Location = центр звука
        /// <summary>
        /// Путь до звука
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// Расширние файла
        /// </summary>
        public string Extension { get; set; }
        /// <summary>
        /// Радиус
        /// </summary>
        public int Radius { get; set; }
        /// <summary>
        /// Расширние файла
        /// </summary>
        public int Path { get; set; }
            new Rule({ callClass: this.className(), name: 'radius', type: 'int', label: 'Радиус', default: 10}),
            //Мощность - в процентах от начального
            new Rule({ callClass: this.className(), name: 'volume', type: 'int', label: 'Мощность', default: 100}),
    }
}
