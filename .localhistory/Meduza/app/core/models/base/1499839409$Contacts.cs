﻿using System.Collections.Generic;


namespace MeduzaServer
{
    /// <summary>
    /// Модель возвращаемая в SpaceCollection в функциях столкновения моделей
    /// </summary>
    public class Contacts : BaseObject
    {
        //*** Свойства ***//
        /// <summary>
        /// Столкновения
        /// </summary>
        public List<Contact> Models { get; set; }
        /// <summary>
        /// Последнее местоположение модели
        /// </summary>
        public Point LastlocationModel { get; set; }


        //*** Конструкторы ***//
        public Contacts()
        {
            Models = new List<Contact>();
        }
        public Contacts(Point lastlocationModel) : this()
        {
            LastlocationModel = lastlocationModel;
        }
        public Contacts(List<SimpleGameObject> models, Point lastlocationModel) : this(lastlocationModel)
        {
            foreach(var model in models) {
                Models.Add(new Contact(model));
            }
        }


        //*** Методы ***//
        public void Add(SimpleGameObject model)
        {
            Models.Add(new Contact(model));
        }
    }
}
