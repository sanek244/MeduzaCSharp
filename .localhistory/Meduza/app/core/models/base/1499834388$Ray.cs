﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    /// <summary>
    /// Луч
    /// </summary>
    public class Ray
    {
        /// <summary>
        /// Точка начала луча
        /// </summary>
        public Point startPoint;
        /// <summary>
        /// Вторая точка луча (для создания прямой)
        /// </summary>
        public Point secondPoint;

        /// <summary>
        /// Коэфициент из уравнения a*x + b*y + c
        /// </summary>
        public double a;
        /// <summary>
        /// Коэфициент из уравнения a*x + b*y + c
        /// </summary>
        public double b;
        /// <summary>
        /// Коэфициент из уравнения a*x + b*y + c
        /// </summary>
        public double c;

        /// <summary>
        /// Прирост луча по x между начальной и второй точкой
        /// </summary>
        public double dx;
        /// <summary>
        /// Прирост луча по y между начальной и второй точкой
        /// </summary>
        public double dy;

        public Ray()
        {
            startPoint = new Point();
            secondPoint = new Point();
            a = 0;
            b = 0;
            c = 0;
            dx = 0;
            dy = 0;
        }
        public Ray(Point startPoint, Point secondPoint) : this()
        {
            this.secondPoint = secondPoint;
            this.startPoint = startPoint;
        }


        /// <summary>
        /// Возвращает x - точку пересечения ординаты (y) с лучём
        /// </summary>
        /// <param name="y"></param>
        /// <returns></returns>
        public double GetX(double y)
        {
            return (-b * y - c) / a;
        }
        /// <summary>
        /// Возвращает y - точку пересечения абсциссы(x) с лучём
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public double GetY(double x)
        {
            return (-a * x - c) / b;
        }

        /// <summary>
        /// Возвращает расстояние от прямой до точки
        /// </summary>
        /// <param name="point">Точка, до которой измеряется расстояние</param>
        /// <returns></returns>
        public double GetDistanceToPoint(Point point)
        {
            return Math.Abs(a * point.x + b * point.y + c) / Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
        }

        /**
         * Возвращает точку пересечения прямых
         * @param {Ray} ray
         * @param {boolean} isThisSegment - текущий луч - отрезок с границами в startPoint и secondPoint
         * @param {boolean} isRaySegment - входящий луч - отрезок с границами в startPoint и secondPoint
         * @return null|Point, null, если прямые параллельны или совпадают
         * @testOk
         */
        /// <summary>
        /// Возвращает точку пересечения прямых
        /// </summary>
        /// <param name="ray">Луч с которым надо найти пересечение</param>
        /// <param name="isThisSegment"></param>
        /// <param name="isRaySegment"></param>
        /// <returns></returns>
        public Point GetPointIntersectionRay(Ray ray, bool isThisSegment = false, bool isRaySegment = false)
        {
            //делитель
            double divider = a * ray.b - ray.a * b;
            if (divider == 0) {
                return null;
            }

            //Поиск адекватного x и y
            double y = (ray.a * c - ray.c * a) / (-ray.a * b + ray.b * a);
            double x = (-b * y - c) / a;

            if (double.IsNaN(x) || double.IsNaN(y)) {
                y = (a * ray.c - c * ray.a) / (-a * ray.b + b * ray.a);
                x = (-ray.b * y - ray.c) / ray.a;
            }

            if (double.IsNaN(x) || double.IsNaN(y)) {
                y = -(c * ray.b - ray.c * b / divider);
                x = -(a * ray.c - ray.a * c / divider);
            }

            //точка пересечения лучей
            Point point = new Point((float)x, (float)y);

            //Если текущий луч - отрезок
            if (isThisSegment) {
                float minX = Math.Min(startPoint.x, secondPoint.x);
                float maxX = Math.Max(startPoint.x, secondPoint.x);
                float minY = Math.Min(startPoint.y, secondPoint.y);
                float maxY = Math.Max(startPoint.y, secondPoint.y);

                //точка пересечения лучей за границами текущего отрезка?
                if (point.x > maxX || point.x < minX || point.y > maxY || point.y < minY) {
                    return null;
                }
            }

            //Если входящий луч - отрезок
            if (isRaySegment) {
                float minX = Math.Min(ray.startPoint.x, ray.secondPoint.x);
                float maxX = Math.Max(ray.startPoint.x, ray.secondPoint.x);
                float minY = Math.Min(ray.startPoint.y, ray.secondPoint.y);
                float maxY = Math.Max(ray.startPoint.y, ray.secondPoint.y);

                //точка пересечения лучей за границами входящего отрезка?
                if (point.x > maxX || point.x < minX || point.y > maxY || point.y < minY) {
                    return null;
                }
            }

            return point;
        }
    }
}
