﻿
namespace Meduza
{
    /// <summary>
    /// Размер
    /// </summary>
    public class Size : BaseObject
    {
        public float width;
        public float height;

        public Size()
        {}
        public Size(float width)
        {
            this.width = width;
        }
        public Size(float width, float height) : this(width)
        {
            this.height = height;
        }
    }
}
