﻿
namespace MeduzaServer
{
    public class ItemsCluster : ActiveRecord
    {
        //*** Свойства ***//

        n.W Rule({ callClass: this.className(), name: 'bottom', type: SimpleGameObject, label: 'Нижняя часть (ноги, лапы)'}),
            n.W Rule({ callClass: this.className(), name: 'head', type: SimpleGameObject, label: 'Голова и всё, что на ней'}),
            n.W Rule({ callClass: this.className(), name: 'backBody', type: SimpleGameObject, label: 'Сзади тела(хвост, плащ)'}),
            n.W Rule({ callClass: this.className(), name: 'body', type: SimpleGameObject, label: 'На теле (бронежилет)'}),
            n.W Rule({ callClass: this.className(), name: 'leftHand', type: SimpleGameObject, label: 'Левая рука'}),
            n.W Rule({ callClass: this.className(), name: 'rightHand', type: SimpleGameObject, label: 'Правая рука'}),
            n.W Rule({ callClass: this.className(), name: 'hands', type: SimpleGameObject, label: 'Руки (двуручные оружия)'}),

            n.W Rule({ callClass: this.className(), name: 'backpackSize', type: 'int', label: 'Размер рюкзака'}),

            n.W Rule({ callClass: this.className(), name: 'backpackItems', type: 'array', label: 'Вещи в рюкзаке'}),


        //*** Конструкторы ***//
        public ItemsCluster()
        {
            TableName = "itemsCluster";
        }
    }
}
