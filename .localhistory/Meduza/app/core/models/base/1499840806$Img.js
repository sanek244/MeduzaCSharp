'use strict';

const ActiveModel = require('./../../base/ActiveModel');
const Rule = require('./../../base/Rule');
const Size = require('./../base/Size');

/** Изображения - набор картинок, показ которых контроллируется ядром и зависит от связанных физических объектов */
class Img extends ActiveModel{
    static rules(){
        return [
            n.W Rule({callClass: this.className(), name: 'path', type: 'string', label: 'Путь до папки с картинкой'}),
            //zIndex - приоритет прорисовки(чем выше тем выше элемент)
            n.W Rule({callClass: this.className(), name: 'zIndex', type: 'int', label: 'Расса', default: 1}),
            n.W Rule({callClass: this.className(), name: 'size', type: Size, label: 'Размер'}),
            n.W Rule({callClass: this.className(), name: 'fileExtension', type: 'string', label: 'Расширение файла'}),
        ].concat(super.Rules());
    }

    /**
     * В объект (только поля, созданные по правилам(Rules))
     * @returns {{}}
     */
    toObject(){
        let object = super.toObject();

        object['type'] = this.className();

        return object;
    }
}
Img.tableName = 'img';

module.exports = Img;
