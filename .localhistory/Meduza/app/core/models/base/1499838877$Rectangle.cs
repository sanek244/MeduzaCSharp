﻿
namespace MeduzaServer
{
    /// <summary>
    /// Прямоугольная область
    /// </summary>
    public class Rectangle : BaseObject
    {
        //*** Свойства ***//
        /// <summary>
        /// Расположение (верхний левый угол)
        /// </summary>
        public Point Location { get; set; }
        /// <summary>
        /// Размер
        /// </summary>
        public Size Size { get; set; }
        /// <summary>
        /// Угол поворота
        /// </summary>
        public float RotateAngle { get; set; }

        public Rectangle() {
            location = new Point();
            size = new Size();
        }
        public Rectangle(SimpleGameObject simpleGameObject) : this()
        {
            location.x = simpleGameObject.rectangle.location.x;
            location.y = simpleGameObject.rectangle.location.y;
            size.width = simpleGameObject.rectangle.size.width;
            size.height = simpleGameObject.rectangle.size.height;
            rotateAngle = simpleGameObject.rectangle.rotateAngle;
        }
        public Rectangle(Point location) : this() 
        {
            this.location.x = location.x;
            this.location.y = location.y;
        }
        public Rectangle(Point location, Size size) : this(location)
        {
            this.size.width = size.width;
            this.size.height = size.height;
        }
        public Rectangle(Point location, Size size, float rotateAngle) : this(location, size)
        {
            this.rotateAngle = rotateAngle;
        }

        /// <summary>
        /// Является ли наклон прямоугольника прямым? 0, 90, 180, 270, 360 градусов
        /// </summary>
        /// <returns></returns>
        public bool IsRightAngle()
        {
            return rotateAngle % 90 == 0;
        }

        /// <summary>
        /// Содержится ли точка внутри данной модели?
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public bool IsContainsPoint(Point point)
        {
            return !(
                point.x > location.x + size.width ||
                point.y > location.y + size.height ||
                point.x < location.x ||
                point.y < location.y
            );
        }

        /// <summary>
        /// Проверка на содержание модели внутри себя (без пересечений и лежаний на границы)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool IsContainsModel(SimpleGameObject model)
        {
            return model.rectangle.location.x > location.x
                && model.rectangle.location.x + model.rectangle.size.width < location.x + size.width
                && model.rectangle.location.y > location.y
                && model.rectangle.location.y + model.rectangle.size.height < location.y + size.height;
        }

        /// <summary>
        /// Проверка на пересечение с моделью или её содержание внутри
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool IsIntersectionOrContainsModel(SimpleGameObject model)
        {
            //по x линия модели внутри области
            bool xInside = model.rectangle.location.x <= this.location.x + this.size.width
                && model.rectangle.location.x >= this.location.x
                || model.rectangle.location.x + model.rectangle.size.width <= this.location.x + this.size.width
                && model.rectangle.location.x + model.rectangle.size.width >= this.location.x;

            //по y линия модели внутри области
            bool yInside = model.rectangle.location.y <= this.location.y + this.size.height
                && model.rectangle.location.y >= this.location.y
                || model.rectangle.location.y + model.rectangle.size.height <= this.location.y + this.size.height
                && model.rectangle.location.y + model.rectangle.size.height >= this.location.y;

            //модель внутри или пересекает её двумя сторонами
            if (xInside && yInside) {
                return true;
            }

            //по x стороны модели вокруг области
            bool xOutside = model.rectangle.location.x <= this.location.x
                && model.rectangle.location.x + model.rectangle.size.width >= this.location.x + this.size.width;

            //по y стороны модели вокруг области
            bool yOutside = model.rectangle.location.y <= this.location.y
                && model.rectangle.location.y + model.rectangle.size.height >= this.location.y + this.size.height;

            return xInside && yOutside || yInside && xOutside;
        }
    }
}
