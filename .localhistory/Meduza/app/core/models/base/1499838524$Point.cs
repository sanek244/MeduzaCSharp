﻿
namespace MeduzaServer
{
    /// <summary>
    /// Точка в пространстве
    /// </summary>
    public class Point : BaseObject
    {
        public float x;
        public float y;

        public Point() { }
        public Point(float x)
        {
            this.x = x;
        }
        public Point(float x, float y) : this(x)
        {
            this.y = y;
        }

        /// <summary>
        /// Оператор сложения
        /// </summary>
        /// <param name="pointA"></param>
        /// <param name="pointB"></param>
        /// <returns></returns>
        static public Point operator + (Point pointA, Point pointB)
        {
            return new Point(pointA.x + pointB.x, pointA.y + pointB.y);
        }
    }
}
