﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    public class Square
    {
        public Point leftTop;
        public Point rightTop;
        public Point leftBottom;
        public Point rightBottom;
        new Rule({ callClass: this.className(), name: 'leftTop', type: Point, label: 'Верхний левый угол'}),
            new Rule({ callClass: this.className(), name: 'rightTop', type: Point, label: 'Верхний правый угол'}),
            new Rule({ callClass: this.className(), name: 'leftBottom', type: Point, label: 'Нижний левый угол'}),
            new Rule({ callClass: this.className(), name: 'rightBottom', type: Point, label: 'Нижний правый угол'}),
    }
}
