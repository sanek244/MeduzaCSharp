﻿
namespace MeduzaServer
{
    public class Animation
    {
        //для сохранения объёма данных, картинки анимации грузят по имени анимации + номера от 1 до frameCount включительно
        new Rule({ callClass: this.className(), name: 'frameCount', type: 'number', label: 'Количество картинок'}),
        new Rule({ callClass: this.className(), name: 'frameChangeTime', type: 'int', label: 'Время между кадрами (миллисекунды)', default: 100}),
        new Rule({ callClass: this.className(), name: 'lifeTime', type: 'int', label: 'Время жизни (миллисекунды)', default: 3000}),
        new Rule({ callClass: this.className(), name: 'hiddenTime', type: 'int', label: 'Время исчезновения (миллисекунды)', default: 1000}),
        new Rule({ callClass: this.className(), name: 'currentFrame', type: 'int', label: 'Номер показываемого кадра', default: 1}),
        new Rule({ callClass: this.className(), name: 'isRandomFirst', type: 'boolean', label: 'Первый кадр рандомный?'}),
        new Rule({ callClass: this.className(), name: 'isRevert', type: 'boolean', label: 'В обратном направлении показывать?'}),
    }
}
