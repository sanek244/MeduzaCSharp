﻿
namespace MeduzaServer
{
    /// <summary>
    /// Квадрат
    /// </summary>
    public class Square
    {
        //*** Свойства ***//
        /// <summary>
        /// Верхний левый угол
        /// </summary>
        public Point leftTop { get; set; }
        /// <summary>
        /// Верхний правый угол
        /// </summary>
        public Point rightTop { get; set; }
        /// <summary>
        /// Нижний левый угол
        /// </summary>
        public Point leftBottom { get; set; }
        /// <summary>
        /// Нижний правый угол
        /// </summary>
        public Point rightBottom { get; set; }
    }
}
