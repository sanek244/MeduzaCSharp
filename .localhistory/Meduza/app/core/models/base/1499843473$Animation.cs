﻿
namespace MeduzaServer
{
    public class Animation
    {
        //для сохранения объёма данных, картинки анимации грузят по имени анимации + номера от 1 до frameCount включительно
        /// <summary>
        /// Количество картинок
        /// </summary>
        public int frameCount { get; set; }
        /// <summary>
        /// Время между кадрами (миллисекунды)
        /// </summary>
        public int frameChangeTime { get; set; }
        /// <summary>
        /// Время жизни (миллисекунды)
        /// </summary>
        public int lifeTime { get; set; }
        /// <summary>
        /// Время исчезновения (миллисекунды)
        /// </summary>
        public int hiddenTime { get; set; }
        /// <summary>
        /// Номер показываемого кадра
        /// </summary>
        public int currentFrame { get; set; }
        /// <summary>
        /// Первый кадр рандомный?
        /// </summary>
        public int isRandomFirst { get; set; }
        /// <summary>
        /// В обратном направлении показывать?
        /// </summary>
        public int isRevert { get; set; }
    }
}
