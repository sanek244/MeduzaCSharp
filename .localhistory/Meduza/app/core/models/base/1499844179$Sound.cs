﻿namespace MeduzaServer
{
    /// <summary>
    /// Звук
    /// </summary>
    public class Sound
    {
        //rectangle.Location = центру звука
        /// <summary>
        /// Путь до звука
        /// </summary>
        public string Path', type: 'string', label: 'Путь до звука'}),
            new Rule({ callClass: this.className(), name: 'extension', type: 'string', label: 'Расширние файла'}),
            new Rule({ callClass: this.className(), name: 'radius', type: 'int', label: 'Радиус', default: 10}),
            //Мощность - в процентах от начального
            new Rule({ callClass: this.className(), name: 'volume', type: 'int', label: 'Мощность', default: 100}),
    }
}
