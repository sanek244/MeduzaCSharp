﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    /// <summary>
    /// Луч
    /// </summary>
    public class Ray
    {
        /// <summary>
        /// Точка начала луча
        /// </summary>
        public Point startPoint;
        /// <summary>
        /// Вторая точка луча (для создания прямой)
        /// </summary>
        public Point secondPoint;

        public
        new Rule({ callClass: this.className(), name: 'B', type: 'number'}),
            new Rule({ callClass: this.className(), name: 'C', type: 'number'}),
            new Rule({ callClass: this.className(), name: 'A', type: 'number'}),

            new Rule({ callClass: this.className(), name: 'dx', type: 'number'}),
            new Rule({ callClass: this.className(), name: 'dy', type: 'number'}),

        public Ray(Point startPoint, Point secondPoint)
        {
            this.secondPoint = secondPoint;
            this.startPoint = startPoint;
        }
    }
}
