﻿namespace MeduzaServer
{
    /// <summary>
    /// Набор изображений оружия
    /// </summary>
    public class ViewsClusterWeapon : ViewsCluster
    {
        private Img shot;

        private int shotId = -1;


        //*** Свойства ***///
        /// <summary>
        /// id перезарядки
        /// </summary>
        public int RechargeId
        {
            get => rechargeId;
            set
            {
                rechargeId = value;
                recharge = Application.DBController.FindOne<Sound>(value);
            }
        }
        /// <summary>
        /// Звук перезарядки
        /// </summary>
        public Sound Recharge { get; }

        /// <summary>
        /// id выстрела
        /// </summary>
        public int ShotId
        {
            get => shotId;
            set
            {
                shotId = value;
                shot = Application.DBController.FindOne<Sound>(value);
            }
        }
        /// <summary>
        /// Звук выстрела
        /// </summary>
        public Sound Shot { get; }

        /// <summary>
        /// id пустого щелчка
        /// </summary>
        public int EmptyShotId
        {
            get => emptyShotId;
            set
            {
                emptyShotId = value;
                emptyShot = Application.DBController.FindOne<Sound>(value);
            }
        }
        /// <summary>
        /// Звук пустого щелчка
        /// </summary>
        public Sound EmptyShot { get; }
    }
}
