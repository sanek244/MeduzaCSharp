﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Набор звуков на один объект
    /// </summary>
    public class ViewsCluster : Img
    {

        //*** Свойства ***//
        /// <summary>
        /// Для показа в меню/магазине/редакторе
        /// </summary>
        public Img Preview { get; set; }

        /// <summary>
        /// Сдвиг в расположении (для изменения в коде)
        /// </summary>
        public Point ShiftLocation { get; set; }

        /// <summary>
        /// На разные типы состояний
        /// Пример: {run: ..., openDoor: ...} ассоциативный массив вида: [состояние => Img/Animation]
        /// </summary>
        public Dictionary<string, Img> States { get; set; }
    }
}
