﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Набор звуков на один объект
    /// </summary>
    public class ViewsCluster : Img
    {
        //Может быть как IMG, так и Animation
        new Rule({ callClass: this.className(), name: 'preview', type: 'object', label: 'Для показа в меню'}),
        //Пример: {run: ..., openDoor: ...} ассоциативный массив вида: [состояние => Img/Animation]
        new Rule({ callClass: this.className(), name: 'states', type: 'object', label: 'Оторажения на состояния'}),
        //Сдвиг в расположении (для изменения в коде)
        new Rule({ callClass: this.className(), name: 'shiftLocation', type: Point, label: 'Сдвиг в расположении'}),

        /// <summary>
        /// На разные типы состояний
        /// Пример: {run: ..., openDoor: ...} ассоциативный массив вида: [состояние => Img/Animation]
        /// </summary>
        public Dictionary<string, Img> States { get; set; }
    }
}
