﻿namespace MeduzaServer
{
    /// <summary>
    /// Набор изображений оружия
    /// </summary>
    public class ViewsClusterWeapon : ViewsCluster
    {
        private Img shot;

        private int shotId = -1;


        //*** Свойства ***///
        /// <summary>
        /// id выстрела
        /// </summary>
        public int ShotId
        {
            get => shotId;
            set
            {
                shotId = value;
                shot = Application.DBController.FindOne<Sound>(value);
            }
        }
        /// <summary>
        /// Звук выстрела
        /// </summary>
        public Sound Shot { get; }
    }
}
