﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Набор изображений на разные уровни жизни
    /// </summary>
    public class ViewsClusterDamage : ViewsCluster
    {

        //*** Свойства ***//
        /// <summary>
        /// Оторажения на разные уровни здоровья
        /// Пример: {run: ..., openDoor: ...} ассоциативный массив вида: [состояние => Img/Animation]
        /// </summary>
        public Dictionary<string, Img> Damages { get; set; }
    }
}
