﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Набор звуков на один объект
    /// </summary>
    public class ViewsCluster : ActiveRecord
    {
        /// <summary>
        /// На разные типы состояний
        /// Пример: {openDoor: ...} ассоциативный массив вида: [состояние => idBaseSound]
        /// </summary>
        public Dictionary<string, int> States { get; set; }
    }
}
