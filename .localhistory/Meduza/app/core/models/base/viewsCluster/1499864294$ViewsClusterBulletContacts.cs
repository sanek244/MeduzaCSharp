﻿namespace MeduzaServer
{
    /// <summary>
    /// Набор изображений столкновений пули
    /// </summary>
    public class ViewsClusterBulletContacts : ActiveRecord
    {
        private Sound concrete;
        private Sound tree;
        private Sound bio;
        private Sound iron;

        private int concreteId = -1;
        private int treeId = -1;
        private int bioId = -1;
        private int ironId = -1;


        //*** Свойства ***///
        /// <summary>
        /// id звука столкновения с бетоном
        /// </summary>
        public int ConcreteId {
            get => concreteId;
            set
            {
                concreteId = value;
                concrete = Application.DBController.FindOne<Sound>(value);
            }
        }
        /// <summary>
        /// Звук столкновения пули с бетоном
        /// </summary>
        public Sound Concrete { get; }

        /// <summary>
        /// id звука столкновения с деревом
        /// </summary>
        public int TreeId
        {
            get => treeId;
            set
            {
                treeId = value;
                tree = Application.DBController.FindOne<Sound>(value);
            }
        }
        /// <summary>
        /// Звук столкновения пули с деревом
        /// </summary>
        public Sound Tree { get; }

        /// <summary>
        /// id звука столкновения с биологической сущностью
        /// </summary>
        public int BioId
        {
            get => bioId;
            set
            {
                bioId = value;
                bio = Application.DBController.FindOne<Sound>(value);
            }
        }
        /// <summary>
        /// Звук столкновения пули с биологической сущностью
        /// </summary>
        public Sound Bio { get; }

        /// <summary>
        /// id звука столкновения с металлом
        /// </summary>
        public int IronId
        {
            get => ironId;
            set
            {
                ironId = value;
                iron = Application.DBController.FindOne<Sound>(value);
            }
        }
        /// <summary>
        /// Звук столкновения пули с металлом
        /// </summary>
        public Sound Iron { get; }
    }
}
