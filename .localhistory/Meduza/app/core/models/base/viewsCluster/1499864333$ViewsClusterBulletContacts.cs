﻿namespace MeduzaServer
{
    /// <summary>
    /// Набор изображений столкновений пули
    /// </summary>
    public class ViewsClusterBulletContacts : ActiveRecord
    {
        private Img concrete;
        private Img tree;
        private Img bio;
        private Img iron;

        private int concreteId = -1;
        private int treeId = -1;
        private int bioId = -1;
        private int ironId = -1;


        //*** Свойства ***///
        /// <summary>
        /// id звука столкновения с бетоном
        /// </summary>
        public int ConcreteId {
            get => concreteId;
            set
            {
                concreteId = value;
                concrete = Application.DBController.FindOne<Img>(value);
            }
        }
        /// <summary>
        /// Звук столкновения пули с бетоном
        /// </summary>
        public Img Concrete { get; }

        /// <summary>
        /// id звука столкновения с деревом
        /// </summary>
        public int TreeId
        {
            get => treeId;
            set
            {
                treeId = value;
                tree = Application.DBController.FindOne<Img>(value);
            }
        }
        /// <summary>
        /// Звук столкновения пули с деревом
        /// </summary>
        public Img Tree { get; }

        /// <summary>
        /// id звука столкновения с биологической сущностью
        /// </summary>
        public int BioId
        {
            get => bioId;
            set
            {
                bioId = value;
                bio = Application.DBController.FindOne<Img>(value);
            }
        }
        /// <summary>
        /// Звук столкновения пули с биологической сущностью
        /// </summary>
        public Img Bio { get; }

        /// <summary>
        /// id звука столкновения с металлом
        /// </summary>
        public int IronId
        {
            get => ironId;
            set
            {
                ironId = value;
                iron = Application.DBController.FindOne<Img>(value);
            }
        }
        /// <summary>
        /// Звук столкновения пули с металлом
        /// </summary>
        public Img Iron { get; }
    }
}
