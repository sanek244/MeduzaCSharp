﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Набор изображений человека
    /// </summary>
    public class ViewsClusterPeople : ViewsCluster
    {

        //*** Свойства ***//
        /// <summary>
        /// Оторажения на разные типы держания оружия
        /// </summary>
        public Dictionary<string, Img> HoldingWeapon { get; set; }
        /// <summary>
        /// Оторажения на разные типы отдачи оружия
        /// </summary>
        public Dictionary<string, Img> RecoilWeapon { get; set; }
    }
}
