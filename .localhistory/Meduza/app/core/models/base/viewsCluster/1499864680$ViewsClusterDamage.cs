﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Набор изображений на разные уровни жизни
    /// </summary>
    public class ViewsClusterDamage : ViewsCluster
    {

        //*** Свойства ***//
        /// <summary>
        /// На разные типы состояний
        /// Пример: {run: ..., openDoor: ...} ассоциативный массив вида: [состояние => Img/Animation]
        /// </summary>
        public Dictionary<string, Img> States { get; set; }
    }
}
