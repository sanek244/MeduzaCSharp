﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Набор изображений человека
    /// </summary>
    public class ViewsClusterPeople : ViewsCluster
    {

        //*** Свойства ***//
        /// <summary>
        /// Оторажения на разные уровни здоровья
        /// </summary>
        public List<Img> Damages { get; set; }
        new Rule({ callClass: this.className(), name: 'holdingWeapon', type: 'object', label: 'Оторажения на разные типы держания оружия'}),
        new Rule({ callClass: this.className(), name: 'recoilWeapon', type: 'object', label: 'Оторажения на разные типы отдачи оружия'}),
    }
}
