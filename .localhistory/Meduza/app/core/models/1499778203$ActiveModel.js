class ActiveModel extends Model{
    /**
     * В объект (только поля, созданные по правилам(Rules))
     * @params {boolean} isAll - всё вернуть?
     * @returns {object}
     * @testOk
     */
    toObject(isAll = false) {
        let object = {};

        _forIn(this.rules, (rule, ruleName) => {
            if (this.get(ruleName) !== null && this.get(ruleName).toObject) {
                if (isAll) {
                    object[ruleName] = this.get(ruleName).toObject();
                }
            }
            else {
                object[ruleName] = this.get(ruleName);
            }
        });

        return object;
    }
}
