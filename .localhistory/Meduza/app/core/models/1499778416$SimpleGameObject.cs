﻿
using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Простой игровой объект
    /// </summary>
    public class SimpleGameObject : BaseObject
    {
        /// <summary>
        /// Прямоугольник модели
        /// </summary>
        public Rectangle rectangle;

        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public string objectId;

        /// <summary>
        /// Имя объекта
        /// </summary>
        public string name;

        /// <summary>
        /// Изменённые атрибуты
        /// </summary>
        public List<string> changesAttributes;

        /// <summary>
        /// Id экземпляра в бд
        /// </summary>
        public int id;

        /// <summary>
        /// Русскоязычная надпись описывающая сущность
        /// </summary>
        public string label;


        /// <summary>
        /// Название таблицы с данными этого класса
        /// </summary>
        public static string tableName = "not specified!";



        //*** Конструкторы ***//
        public SimpleGameObject()
        {
            objectId = Helper.GenerateUid();
            rectangle = new Rectangle();
            changesAttributes = new List<string>();
            name = "";
        }




        ToJSON(isAll = false)
        {
            let object = { };

            _forIn(this.rules, (rule, ruleName) => {
                if (this.get(ruleName) !== null && this.get(ruleName).toObject) {
                    if (isAll) {
                        object[ruleName] = this.get(ruleName).toObject();
                    }
                }
                else {
                    object[ruleName] = this.get(ruleName);
                }
            });

            return object;
        }
    }
}
