﻿using System.Collections.Generic;
using N.Wtonsoft.Json;

namespace MeduzaServer
{
    /// <summary>
    /// Простой игровой объект
    /// </summary>
    public class SimpleGameObject : ActiveRecord
    {
        /// <summary>
        /// Прямоугольник модели
        /// </summary>
        public Rectangle rectangle;

        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public string objectId;

        /// <summary>
        /// Имя объекта
        /// </summary>
        public string name;


        /// <summary>
        /// Id экземпляра в бд
        /// </summary>
        public int id;

        /// <summary>
        /// Русскоязычная надпись описывающая сущность
        /// </summary>
        public string label;


        //*** Конструкторы ***//
        public SimpleGameObject()
        {
            objectId = Helper.GenerateUid();
            rectangle = new Rectangle();
            changesAttributes = new List<string>();
            name = "";
        }
    }
}
