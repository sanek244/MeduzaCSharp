﻿
namespace MeduzaServer
{
    /// <summary>
    /// Простой игровой объект
    /// </summary>
    public class SimpleGameObject : BaseObject
    {
        /// <summary>
        /// Прямоугольник модели
        /// </summary>
        public Rectangle rectangle;

        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public string objectId;

        public string name;

        public SimpleGameObject()
        {
            objectId = Helper.GenerateUid();
        }
    }
}
