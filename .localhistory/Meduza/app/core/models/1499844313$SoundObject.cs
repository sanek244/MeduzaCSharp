﻿
namespace MeduzaServer
{
    /// <summary>
    /// Объект звука на карте
    /// </summary>
    public class SoundObject : SimpleGameObject
    {
        public Sound sound;

        public SoundObject()
        {
            sound = new BaseSound();
        }
    }
}
