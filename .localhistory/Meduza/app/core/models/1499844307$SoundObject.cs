﻿
namespace MeduzaServer
{
    /// <summary>
    /// Объект звука на карте
    /// </summary>
    public class SoundObject : SimpleGameObject
    {
        public BaseSound sound;

        public SoundObject()
        {
            sound = new BaseSound();
        }
    }
}
