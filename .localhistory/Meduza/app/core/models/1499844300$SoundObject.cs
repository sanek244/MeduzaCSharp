﻿
namespace MeduzaServer
{
    public class SoundObject : SimpleGameObject
    {
        public BaseSound sound;

        public SoundObject()
        {
            sound = new BaseSound();
        }
    }
}
