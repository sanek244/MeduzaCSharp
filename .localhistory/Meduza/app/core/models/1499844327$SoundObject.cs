﻿
namespace MeduzaServer
{
    /// <summary>
    /// Объект звука на карте
    /// </summary>
    public class SoundObject : SimpleGameObject
    {
        /// <summary>
        /// Звук
        /// </summary>
        public Sound Sound { get; set; }

        public SoundObject()
        {
            sound = new BaseSound();
        }
    }
}
