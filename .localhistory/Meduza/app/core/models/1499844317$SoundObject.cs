﻿
namespace MeduzaServer
{
    /// <summary>
    /// Объект звука на карте
    /// </summary>
    public class SoundObject : SimpleGameObject
    {
        public Sound Sound { get; set; }

        public SoundObject()
        {
            sound = new BaseSound();
        }
    }
}
