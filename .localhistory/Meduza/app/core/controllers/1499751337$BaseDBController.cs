﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    public abstract class BaseDBController
    {
        public abstract List<T> Find<T>(string tableName, Dictionary<string, string> where, bool isOne = false);

        public abstract T FindOne<T>(string tableName, Dictionary<string, string> where);
    }
}
