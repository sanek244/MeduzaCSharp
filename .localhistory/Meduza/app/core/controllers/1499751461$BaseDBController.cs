﻿using System.Collections.Generic;

namespace MeduzaServer
{
    public abstract class BaseDBController
    {
        /// <summary>
        /// Возвращает все объекты T, удовлетворящие условию where
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="where">Условие отбора</param>
        /// <param name="isOne"></param>
        /// <returns></returns>
        public abstract List<T> Find<T>(Dictionary<string, string> where, bool isOne = false);

        public abstract T FindOne<T>(Dictionary<string, string> where);

        /// <summary>
        /// Возвращает версию данных
        /// </summary>
        /// <returns></returns>
        public abstract string GetVersionData();
    }
}
