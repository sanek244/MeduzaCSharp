﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace MeduzaServer
{
    /// <summary>
    /// Чтение данных из файлового хранилища
    /// </summary>
    public class FileController : DBController
    {
        /// <summary>
        /// Возвращает все объекты T, удовлетворящие условию where
        /// </summary>
        /// <typeparam name="T">Тип искомого объекта</typeparam>
        /// <param name="where">Условие отбора</param>
        /// <returns></returns>
        public override List<T> Find<T>(Dictionary<string, string> where)
        {
            List<T> items = LoadFile<T>();
            List<T> result = new List<T>();

            foreach (T item in items) {
                bool ok = true;

                foreach(var whereEl in where) {
                    if(item.Get(whereEl.Key) + "" != whereEl.Value) {
                        ok = false;
                        break;
                    }
                }

                if (ok) {
                    result.Add(item);
                }
            }

            return result;
        }

        /// <summary>
        /// Возвращает первый найденный объект T, удовлетворящий условию where
        /// </summary>
        /// <typeparam name="T">Тип искомого объекта</typeparam>
        /// <param name="where">Условие отбора</param>
        /// <returns></returns>
        public override T FindOne<T>(Dictionary<string, string> where)
        {
            List<T> items = LoadFile<T>();
            List<T> result = new List<T>();

            foreach (T item in items) {
                bool ok = true;

                foreach (var whereEl in where) {
                    if (item.Get(whereEl.Key) + "" != whereEl.Value) {
                        ok = false;
                        break;
                    }
                }

                if (ok) {
                    return item;
                }
            }

            return null;
        }



        /// <summary>
        /// Загрузка содержимого JSON файла
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private List<T> LoadFile<T>() where T : ActiveRecord, new()
        {
            List<T> items = null;
            string pathFile = Directory.GetCurrentDirectory() + "\\fileDb\\" + (new T()).tableName + ".json";

            using (var stream = new FileStream(pathFile, FileMode.Open)) {
                using (StreamReader r = new StreamReader(stream)) {
                    items = JsonConvert.DeserializeObject<List<T>>(r.ReadToEnd());
                }
            }

            return items;
        }

        /// <summary>
        /// Сохраняет набор экземпляров в файл
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        private void SaveFile<T>(List<T> items) where T : ActiveRecord, new()
        {
            string pathFile = Directory.GetCurrentDirectory() + "\\fileDb\\" + (new T()).tableName + ".json";

            using (var stream = new FileStream(pathFile, FileMode.Open)) {
                using (StreamWriter r = new StreamWriter(stream)) {
                    r.WriteLine(JsonConvert.SerializeObject(items));
                }
            }
        }

        /// <summary>
        /// Добавляет модель в файл
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        public override void AddModel<T>(T model) where T : ActiveRecord, new()
        {
            var models = LoadFile<T>();
            models.Add(model);
            SaveFile<T>(models);
        }



        /// <summary>
        /// Возвращает версию данных
        /// </summary>
        /// <returns></returns>
        public override string GetVersionData()
        {

        }


        /// <summary>
        /// Добавление лога
        /// </summary>
        /// <param name="className"> Имя класса вызвавшего логирование</param>
        /// <param name="method">Имя метода в котором произошло создание лога</param>
        /// <param name="message">Сообщение лога</param>
        /// <param name="type">Тип лога</param>
        public override void AddLog(string className, string method, string message, string type)
        {
            AddModel(new Log(className, method, message, type));
        }
    }
}
