﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    public class FileController : DBController
    {
        /// <summary>
        /// Возвращает все объекты T, удовлетворящие условию where
        /// </summary>
        /// <typeparam name="T">Тип искомого объекта</typeparam>
        /// <param name="where">Условие отбора</param>
        /// <returns></returns>
        public override List<T> Find<T>(Dictionary<string, string> where)
        {

        }

        /// <summary>
        /// Возвращает первый найденный объект T, удовлетворящий условию where
        /// </summary>
        /// <typeparam name="T">Тип искомого объекта</typeparam>
        /// <param name="where">Условие отбора</param>
        /// <returns></returns>
        public override T FindOne<T>(Dictionary<string, string> where)
        {

        }

        /// <summary>
        /// Возвращает версию данных
        /// </summary>
        /// <returns></returns>
        public override string GetVersionData()
        {

        }
    }
}
