﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    public abstract class BaseDBController
    {
        public abstract List<T> Find<T>(Dictionary<string, string> where, bool isOne = false);

        public abstract T FindOne<T>Dictionary<string, string> where);
    }
}
