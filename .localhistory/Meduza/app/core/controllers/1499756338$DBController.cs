﻿using System.Collections.Generic;

namespace MeduzaServer
{
    /// <summary>
    /// Абстракция для контроллеров базы данных
    /// </summary>
    public abstract class DBController
    {
        /// <summary>
        /// Возвращает все объекты T, удовлетворящие условию where
        /// </summary>
        /// <typeparam name="T">Тип искомого объекта</typeparam>
        /// <param name="where">Условие отбора</param>
        /// <returns></returns>
        public abstract List<T> Find<T>(Dictionary<string, string> where);

        /// <summary>
        /// Возвращает первый найденный объект T, удовлетворящий условию where
        /// </summary>
        /// <typeparam name="T">Тип искомого объекта</typeparam>
        /// <param name="where">Условие отбора</param>
        /// <returns></returns>
        public abstract T FindOne<T>(Dictionary<string, string> where);

        /// <summary>
        /// Возвращает версию данных
        /// </summary>
        /// <returns></returns>
        public abstract string GetVersionData();

        /// <summary>
        /// Добавление лога
        /// </summary>
        public abstract void AddLog();
    }
}
