﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    public abstract class BaseDBController
    {
        public abstract List<object> Find(string tableName, Dictionary<string, string> where, bool isOne = false);
    }
}
