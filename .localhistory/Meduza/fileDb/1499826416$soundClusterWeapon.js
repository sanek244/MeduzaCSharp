'use strict';
const SoundClusterWeapon = require('../app/core/models/base/soundCluster/SoundClusterWeapon');
let id = 0;

module.exports = [
    new SoundClusterWeapon({
        id: id++,
        name: 'MP-38',
        shotId: 0,
        emptyShotId: 1,
        rechargeId: -1, //TODO
    })
];
