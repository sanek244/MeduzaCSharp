'use strict';
const Map = require('../../app/core/models/Map');
const Building = require('../../app/core/models/Building');
const Size = require('../../app/core/models/base/Size');
const Point = require('../../app/core/models/base/Point');

module.exports = [
    new Building({name: 'block', 'rectangle.location': new Point({x: 300, y: 300})}),
    new Building({name: 'block', 'rectangle.location': new Point({x: 0, y: 0})}),
];