'use strict';
const Sound = require('../app/core/models/Sound');
const BaseSound = require('../app/core/models/base/BaseSound');
let id = 0;

module.exports = [
    new BaseSound({
        id: id++,
        name: 'mp5_1_shot',
        label: 'MP5',
        path: '',
        extension: 'wav',
        radius: 1500,
        volume: 1,
    }),
    new BaseSound({
        id: id++,
        name: 'mp5_1_empty',
        label: 'MP5 empty',
        path: '',
        extension: 'wav',
        radius: 500,
        volume: 1,
    })
];