'use strict';
const ViewsClusterBulletContacts = require('../app/core/models/base/viewsCluster/ViewsClusterBulletContacts');
const Animation = require('../app/core/models/base/Animation');
const TypeMaterial = require('../app/core/enums/TypeMaterial');
let id = 0;

module.exports = [
    new ViewsClusterBulletContacts({
        id: id++,
        name: '9x19',
        [TypeMaterial.CONCRETE + 'Id']: 1,
        [TypeMaterial.BIO + 'Id']: 2,
    }),
];
