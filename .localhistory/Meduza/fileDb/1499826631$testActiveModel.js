[
    new ActiveModelTest({
        id: id++,
        name: 'test1',
        label: 'Тест1',
        testField: 2352,
    }),
    new ActiveModelTest({
        id: id++,
        name: 'test2',
        label: 'Тест2',
        testField: 2352,
        testFieldObjectId: 2
    }),
    new ActiveModelTest({
        id: id++,
        name: 'test3',
        label: 'Тест3',
        testField: 5488,
    }),
    new ActiveModelTest({
        id: id++,
        name: 'test4',
        label: 'Тест4',
        testField: 34,
    }),
    new ActiveModelTest({
        id: id++,
        name: 'test5',
        label: 'Тест5',
        testField: 141,
    }),

]