'use strict';

const ViewsClusterWeapon = require('../app/core/models/base/viewsCluster/ViewsClusterWeapon');
const Img = require('../app/core/models/base/Img');
const Animation = require('../app/core/models/base/Animation');
let id = 0;

module.exports = [
    new ViewsClusterWeapon({
        id: id++,
        name: 'MP-38',
        zIndex: 8,
        states: {
            base: new Img({
                name: 'MP-38_top',
                path: 'weapon',
                fileExtension: 'png',
                zIndex: 8,
            }),
        },
        shotAnimationId: 1,
    })
];
