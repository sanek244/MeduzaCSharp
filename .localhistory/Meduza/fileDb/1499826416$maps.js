'use strict';

const Map = require('../app/core/models/Map');
const Size = require('../app/core/models/base/Size');
const StartPosition = require('../app/core/models/StartPosition');
const TypeRace = require('../app/core/enums/TypeRace');
const Point = require('../app/core/models/base/Point');
const Rectangle = require('../app/core/models/base/Rectangle');
let id = 0;

module.exports = [
    new Map({
        id: id++,
        name: 'testingPolygon',
        size: new Size(10000, 5000),
        maxPlayers: 2,
        description: 'Тестовый полигон разработки',
        startPoints: [
            new StartPosition({name: 'startHuman1', rectangle: new Rectangle(new Point(100, 100), new Size(50, 50)), race: TypeRace.HUMAN}),
            new StartPosition({name: 'startHuman2', rectangle: new Rectangle(new Point(200, 100), new Size(50, 50)), race: TypeRace.HUMAN}),
        ]
    }),

];