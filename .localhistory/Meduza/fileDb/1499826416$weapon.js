'use strict';
const Weapon = require('../app/core/models/Weapon');
const ViewsClusterWeapon = require('../app/core/models/base/viewsCluster/ViewsClusterWeapon');
const Point = require('../app/core/models/base/Point');
const TypeScatter = require('../app/core/enums/TypeScatter');
const TypeHolding = require('../app/core/enums/TypeHolding');
const TypeRecharge = require('../app/core/enums/TypeRecharge');
let id = 0;

module.exports = [
    new Weapon({
        id: id++,
        name: 'MP-38',
        rapidityFire: 200,
        damage: 15,
        shotSpeed: 150,
        holderVolume: 30,
        reloadingTime: 2000,
        scatterMax: 10,
        scatterType: TypeScatter.INCREASING,
        bulletId: 0,
        bulletCreatePoint: new Point({x: 33, y: 0}),
        rechargeType: TypeRecharge.AUTOMATON,
        holdingType: TypeHolding.AUTOMATON,
        soundClusterWeaponId: 0,
        viewType: ViewsClusterWeapon.tableName,
        viewId: 0,
    }),
];