'use strict';

const ActiveModelTest2 = require('../tests/testCore/testModels/ActiveModelTest2');

let id = 0;

module.exports = [
    new ActiveModelTest2({
        id: id++,
        name: 'test1',
        label: 'Тест1',
        textTest: 's fsd gsd g',
    }),
    new ActiveModelTest2({
        id: id++,
        name: 'test2',
        label: 'Тест2',
        textTest: 'sdgs dggsd g',
    }),
    new ActiveModelTest2({
        id: id++,
        name: 'test3',
        label: 'Тест3',
        textTest: 'Рыбная рыбалка на пароге рыбного дождя',
    }),
    new ActiveModelTest2({
        id: id++,
        name: 'test4',
        label: 'Тест4',
        textTest: 'gjgfjsd gsd g',
    }),
    new ActiveModelTest2({
        id: id++,
        name: 'test5',
        label: 'Тест5',
        textTest: 'fgkfkfr t 4gkfg 43t43 t',
    }),

];