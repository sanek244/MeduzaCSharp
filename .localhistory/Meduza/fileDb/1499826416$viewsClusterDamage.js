'use strict';
const ViewsClusterDamage = require('../app/core/models/base/viewsCluster/ViewsClusterDamage');
const Img = require('../app/core/models/base/Img');
const Animation = require('../app/core/models/base/Animation');
const Size = require('../app/core/models/base/Size');
let id = 0;

module.exports = [
    new ViewsClusterDamage({
        id: id++,
        name: 'block1',
        zIndex: 5,
        states: {
            base: new Img({
                name: 'block1',
                path: 'blocks',
                fileExtension: 'jpg',
                size: new Size(31, 31)
            }),
        },
        damages: []
    })
];
