'use strict';
const ViewsClusterPeople = require('../app/core/models/base/viewsCluster/ViewsClusterPeople');
const Img = require('../app/core/models/base/Img');
const Animation = require('../app/core/models/base/Animation');
const Size = require('../app/core/models/base/Size');
let id = 0;

module.exports = [
    new ViewsClusterPeople({
        id: id++,
        name: 'warrior',
        zIndex: 7,
        //preview: null, //TODO
        states: {
            base: new Img({
                label: 'warrior',
                name: 'warrior_1',
                path: 'gamers/warrior',
                fileExtension: 'png',
                size: new Size(43, 40)
            }),
        },
        recoilWeapon: {
            automaton: new Animation({
                name: 'warrior_scatter_',
                path: 'gamers/warrior',
                frameCount: 5,
                fileExtension: 'png',
                size: new Size(49, 40)
            })
        }
    }),
];
