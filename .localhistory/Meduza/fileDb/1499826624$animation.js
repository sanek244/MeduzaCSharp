[
    new Animate({
        id: id++,
        name: 'shot_1',
        path: 'shot/shot_1',
        frameChangeTime: 75,
        fileExtension: 'png',
        isRandomFirst: true,
        zIndex: 5,
        size: new Size(17, 18)
    }),
    new Animate({
        id: id++,
        name: 'lightContact_concrete',
        path: 'bullet\\bullet_contacts\\light_concrete',
        frameChangeTime: 35,
        fileExtension: 'png',
        zIndex: 5,
        size: new Size(25, 25)
    }),
    new Animate({
        id: id++,
        name: 'lightContact_bio',
        path: 'bullet\\bullet_contacts\\light_bio',
        frameChangeTime: 35,
        fileExtension: 'png',
        zIndex: 5,
        size: new Size(25, 25)
    }),
]