'use strict';
const SoundClusterBulletContacts = require('../app/core/models/base/soundCluster/SoundClusterBulletContacts');
const BaseSound = require('../app/core/models/base/BaseSound');
const TypeMaterial = require('../app/core/enums/TypeMaterial');
let id = 0;

module.exports = [
    new SoundClusterBulletContacts({
        id: id++,
        name: '9x19',
        [TypeMaterial.CONCRETE + 'Id']: -1, //TODO
        [TypeMaterial.BIO + 'Id']: -1,
    }),
];
