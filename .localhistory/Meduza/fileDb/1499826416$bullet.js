'use strict';
const Bullet = require('../app/core/models/Bullet');
const Img = require('../app/core/models/base/Img');
let id = 0;

module.exports = [
    new Bullet({
        id: id++,
        name: '9x19',
        label: 'стандартная пуля для пистолетов',
        distance: 1500,
        damage: 1,
        viewType: Img.tableName,
        viewId: 0,
        viewsClusterContactAnimationsId: 0
    }),
];