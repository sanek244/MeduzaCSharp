'use strict';
const Img = require('../app/core/models/base/Img');
const Size = require('../app/core/models/base/Size');
let id = 0;

module.exports = [
    new Img({
        id: id++,
        name: '9x19_1',
        path: 'bullet',
        fileExtension: 'png',
        size: new Size(8, 3)
    }),
];