'use strict';
const ViewsCluster = require('../app/core/models/base/viewsCluster/ViewsCluster');
const Img = require('../app/core/models/base/Img');
const Animation = require('../app/core/models/base/Animation');
const Size = require('../app/core/models/base/Size');
let id = 0;

module.exports = [
    new ViewsCluster({
        id: id++,
        name: 'warrior',
        zIndex: 7,
        states: {
            base: new Img({
                name: 'warrior_bottom_1',
                path: 'gamers/warrior',
                fileExtension: 'png',
                size: new Size(74, 43)
            }),
            run: new Animation({
                name: 'warrior_bottom_',
                path: 'gamers/warrior',
                frameCount: 9,
                fileExtension: 'png',
                size: new Size(74, 43)
            }),
        },
    }),
];
