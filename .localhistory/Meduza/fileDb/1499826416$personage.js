'use strict';
const Personage = require('../app/core/models/Personage');
const TypeRace = require('../app/core/enums/TypeRace');
const Point = require('../app/core/models/base/Point');
const ViewsClusterPeople = require('../app/core/models/base/viewsCluster/ViewsClusterPeople');
let id = 0;

module.exports = [
    new Personage({
        id: id++,
        name: 'warrior',
        race: TypeRace.HUMAN,
        viewType: ViewsClusterPeople.tableName,
        viewId: 0,
        itemsClusterId: 0
    }),
];