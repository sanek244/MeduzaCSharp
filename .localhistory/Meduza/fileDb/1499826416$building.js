'use strict';
const Building = require('../app/core/models/Building');
const TypeMaterial = require('../app/core/enums/TypeMaterial');
const ViewsClusterDamage = require('../app/core/models/base/viewsCluster/ViewsClusterDamage');
let id = 0;

module.exports = [
    new Building({
        id: id++,
        name: 'block1',
        viewType: ViewsClusterDamage.tableName,
        viewId: 0,
        armor: 3,
        health: 100,
        material: TypeMaterial.CONCRETE,
    }),
];