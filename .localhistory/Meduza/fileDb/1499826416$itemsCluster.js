'use strict';
const ItemsCluster = require('../app/core/models/base/ItemsCluster');
const SimpleGameObject = require('../app/core/models/SimpleGameObject');
const ViewsCluster = require('../app/core/models/base/viewsCluster/ViewsCluster');
let id = 0;

module.exports = [
    new ItemsCluster({
        id: id++,
        name: 'warrior',
        bottom: new SimpleGameObject({
            viewType: ViewsCluster.tableName,
            viewId: 0,
        })
    })
];
