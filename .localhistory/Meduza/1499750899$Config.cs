﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeduzaServer
{
    public static class Config
    {
        public Dictionary<string, string> Components = new Dictionary<string, string>() {
        db:{
            class: '../controllers/FileController',
        },
        test: {
            class: '../../../tests/testCore/testModels/testApplication/testController',
            testFieldApp1: 356,
            testFieldApp2: 'wrsdfsf'
        }
    },
    params:{
        testParam: 'sdsaa',
        directProject: __dirname,
        mode: 'development', //development || production
        mysqlDb: {
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'game_meduza'
        },
    }
    }
}
