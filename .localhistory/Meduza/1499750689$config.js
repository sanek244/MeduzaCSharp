"use strict";

// Прокачка строки - функция format: 'Покупай машину {0} за {1} рублей'.format('mustang', 3100200);
if (!String.prototype.format) {
    String.prototype.format = function() {
        const args = arguments;
        return this.replace(/{(\d+)}/g, (match, number) => {
            if(args[number] === null){
                return 'null';
            }
            if(args[number] && args[number].className){
                return args[number].className();
            }
            if(typeof args[number] === 'undefined'){
                return match;
            }
            if(typeof args[number] === 'object'){
                if(args[number].toObject){
                    return args[number].toObject();
                }
                return JSON.stringify(args[number]);
            }
            return args[number];
        });
    };
}

Array.prototype.max = function(){
    return Math.max.apply(Math, this);
};
Array.prototype.maxF = function(functionMap){
    return Math.max.apply(Math, this.map(functionMap));
};

Array.prototype.min = function(){
    return Math.min.apply(Math, this);
};
Array.prototype.minF = function(functionMap){
    return Math.min.apply(Math, this.map(functionMap));
};

module.exports = {
    components:{
        db:{
            class: '../controllers/FileController',
        },
        test: {
            class: '../../../tests/testCore/testModels/testApplication/testController',
            testFieldApp1: 356,
            testFieldApp2: 'wrsdfsf'
        }
    },
    params:{
        testParam: 'sdsaa',
        directProject: __dirname,
        mode: 'development', //development || production
        mysqlDb: {
            host: 'localhost',
            user: 'root',
            password: '',
            database: 'game_meduza'
        },
    }
};