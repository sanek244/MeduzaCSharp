using Xunit;
using MeduzaTest;
using System.Linq;
using System.Collections.Generic;

namespace Tests
{
    public class TestSpaceCollection
    {
        public SpaceCollection spaceCollection = new SpaceCollection(new Size(1000, 2000));
        public SpaceCollection spaceCollectionBig = new SpaceCollection(new Size(1000, 2000), 100);

        public Gamer gamer = new Gamer() {
            rectangle = new Rectangle(new Point(100, 200), new Size(25, 30))
        };

        public Building building = new Building() {
            name = "building1",
            rectangle = new Rectangle(new Point(200, 250), new Size(500, 700)),
            health = 100
        };
        public Building building2 = new Building(){
            name = "building2",
            rectangle = new Rectangle(new Point(200, 250), new Size(500, 100)),
            health = 100
        };
        public Building building3 = new Building(){
            name = "building3",
            rectangle = new Rectangle(new Point(210, 250), new Size(200, 200)),
            health = 150
        };
        public Building building4 = new Building(){
            name = "building4",
            rectangle = new Rectangle(new Point(210, 250), new Size(200, 400)),
            health = 120
        };
        public Building building5 = new Building(){
            name = "building5",
            rectangle = new Rectangle(new Point(220, 250), new Size(100, 100)),
            health = 150
        };
        public Building building6 = new Building(){
            name = "building6",
            rectangle = new Rectangle(new Point(190, 350), new Size(50, 350)),
            health = 150
        };
        public Sound soundCenter = new Sound(){
            rectangle = new Rectangle(new Point(500, 500)),
            sound = new BaseSound(){radius = 400}
        };
        public Sound soundLeft = new Sound(){
            rectangle = new Rectangle(new Point(250, 500)),
            sound = new BaseSound(){radius = 400}
        };
        public Bullet bullet = new Bullet(){
            rectangle = new Rectangle(new Point(100, 100), new Size(5, 5)),
        };

        [Fact]
        public void getNumberSquare0_outside()
        {
            Assert.Equal(spaceCollection.GetNumberSquare(1601, 67), spaceCollection.GetNumberSquare(1000, 67));
        }
        [Fact]
        public void getNumberSquare0_outside2()
        {
            Assert.Equal(spaceCollection.GetNumberSquare(-65756, 67), spaceCollection.GetNumberSquare(0, 67));
        }
        [Fact]
        public void getNumberSquare0_outside3()
        {
            Assert.Equal(spaceCollection.GetNumberSquare(456, -456), spaceCollection.GetNumberSquare(456, 0));
        }
        [Fact]
        public void getNumberSquare0_outside4()
        {
            Assert.Equal(spaceCollection.GetNumberSquare(456, 3000), spaceCollection.GetNumberSquare(456, 1000));
        }

        [Fact]
        public void getNumberSquare0()
        {
            Assert.Equal(spaceCollection.GetNumberSquare(0, 0), 0);
        }
        [Fact]
        public void getNumberSquare1()
        {
            Assert.Equal(spaceCollection.GetNumberSquare(200, 0), 1);
        }
        [Fact]
        public void getNumberSquare2()
        {
            Assert.Equal(spaceCollection.GetNumberSquare(474, 0), 2);
        }
        [Fact]
        public void getNumberSquare3()
        {
            Assert.Equal(spaceCollection.GetNumberSquare(64, 300), 1000 / 200);
        }
        [Fact]
        public void getNumberSquare4()
        {
            Assert.Equal(spaceCollection.GetNumberSquare(364, 300), 1000 / 200 + 1);
        }



        [Fact]
        public void getPositionSquare0()
        {
            Point a = spaceCollection.GetPositionSquare(11);
            Assert.Equal(a.x, 200);
            Assert.Equal(a.y, 400);
        }
        [Fact]
        public void getPositionSquare1()
        {
            Point a = spaceCollection.GetPositionSquare(18);
            Assert.Equal(a.x, 600);
            Assert.Equal(a.y, 600);
        }
        [Fact]
        public void getPositionSquare2()
        {
            Point a = spaceCollection.GetPositionSquare(20);
            Assert.Equal(a.x, 0);
            Assert.Equal(a.y, 800);
        }
        [Fact]
        public void getPositionSquare3()
        {
            Point a = spaceCollection.GetPositionSquare(14);
            Assert.Equal(a.x, 800);
            Assert.Equal(a.y, 400);
        }
        [Fact]
        public void getPositionSquare_center0()
        {
            Point a = spaceCollection.GetPositionSquare(11, true);
            Assert.Equal(a.x, 300);
            Assert.Equal(a.y, 500);
        }
        [Fact]
        public void getPositionSquare_center1()
        {
            Point a = spaceCollection.GetPositionSquare(18, true);
            Assert.Equal(a.x, 700);
            Assert.Equal(a.y, 700);
        }
        [Fact]
        public void getPositionSquare_center2()
        {
            Point a = spaceCollection.GetPositionSquare(20, true);
            Assert.Equal(a.x, 100);
            Assert.Equal(a.y, 900);
        }
        [Fact]
        public void getPositionSquare_center3()
        {
            Point a = spaceCollection.GetPositionSquare(14, true);
            Assert.Equal(a.x, 900);
            Assert.Equal(a.y, 500);
        }
        [Fact]
        public void getModelSquares_1square()
        {
            var res = spaceCollection.GetModelSquares(gamer).ToList();
            Assert.Equal(res.Count, 1);
            Assert.Equal(res[0], 5);
        }
        [Fact]
        public void getModelSquares_2square()
        {
            var res = spaceCollection.GetModelSquares(building2).ToList();
            Assert.Equal(res.Count, 2);
            Assert.Equal(res[0], 6);
            Assert.Equal(res[1], 8);
        }
        [Fact]
        public void getModelSquares_4square()
        {
            var res = spaceCollection.GetModelSquares(building3).ToList();
            Assert.Equal(res.Count, 4);
            Assert.Equal(res[0], 6);
            Assert.Equal(res[1], 7);
            Assert.Equal(res[2], 12);
            Assert.Equal(res[3], 11);
        }
        [Fact]
        public void getModelSquares_6square()
        {
            var res = spaceCollection.GetModelSquares(building4).ToList();
            Assert.Equal(res.Count, 6);
            Assert.Equal(res[0], 6);
            Assert.Equal(res[1], 7);
            Assert.Equal(res[2], 11);
            Assert.Equal(res[3], 12);
            Assert.Equal(res[4], 16);
            Assert.Equal(res[5], 17);
        }
        [Fact]
        public void getModelSquares_12square()
        {
            var res = spaceCollection.GetModelSquares(building).ToList();
            Assert.Equal(res.Count, 12);
            Assert.Equal(res[0], 6);
            Assert.Equal(res[1], 7);
            Assert.Equal(res[2], 8);
            Assert.Equal(res[3], 11);
            Assert.Equal(res[4], 12);
            Assert.Equal(res[5], 13);
            Assert.Equal(res[6], 16);
            Assert.Equal(res[7], 17);
            Assert.Equal(res[8], 18);
            Assert.Equal(res[9], 21);
            Assert.Equal(res[10], 22);
            Assert.Equal(res[11], 23);
        }
        [Fact]
        public void getModelSquares_sound()
        {
            /*var res = spaceCollection.GetModelSquares(soundCenter).ToList();
            Assert.Equal(res.Count, 13);
            Assert.Equal(res.IndexOf(2) != -1, true);
            Assert.Equal(res.IndexOf(6) != -1, true);
            Assert.Equal(res.IndexOf(7) != -1, true);
            Assert.Equal(res.IndexOf(8) != -1, true);
            Assert.Equal(res.IndexOf(10) != -1, true);
            Assert.Equal(res.IndexOf(11) != -1, true);
            Assert.Equal(res.IndexOf(12) != -1, true);
            Assert.Equal(res.IndexOf(13) != -1, true);
            Assert.Equal(res.IndexOf(14) != -1, true);
            Assert.Equal(res.IndexOf(16) != -1, true);
            Assert.Equal(res.IndexOf(17) != -1, true);
            Assert.Equal(res.IndexOf(18) != -1, true);
            Assert.Equal(res.IndexOf(22) != -1, true);*/
        }
        
    
        [Fact]
        public void GetObjectSquares_1square()
        { 
            var res = spaceCollection.GetObjectSquares(new Point(100, 200), new Size(25, 30)).ToList();
            Assert.Equal(res.Count, 1);
            Assert.Equal(res[0], 5);
        }
        [Fact]
        public void GetObjectSquares_2squares()
        {
            var res = spaceCollection.GetObjectSquares(new Point(200, 250), new Size(500, 100)).ToList();
            Assert.Equal(res.Count, 2);
            Assert.Equal(res[0], 6);
            Assert.Equal(res[1], 8);
        }
        [Fact]
        public void GetObjectSquares_4squares()
        {
            var res = spaceCollection.GetObjectSquares(new Point(210, 250), new Size(200, 200)).ToList();
            Assert.Equal(res.Count, 4);
            Assert.Equal(res[0], 6);
            Assert.Equal(res[1], 7);
            Assert.Equal(res[2], 12);
            Assert.Equal(res[3], 11);
        }
        [Fact]
        public void GetObjectSquares_6squares()
        {
            var res = spaceCollection.GetObjectSquares(new Point(210, 250), new Size(200, 400)).ToList();
            Assert.Equal(res.Count, 6);
            Assert.Equal(res[0], 6);
            Assert.Equal(res[1], 7);
            Assert.Equal(res[2], 11);
            Assert.Equal(res[3], 12);
            Assert.Equal(res[4], 16);
            Assert.Equal(res[5], 17);
        }
        [Fact]
        public void GetObjectSquares_12squares()
        {
            var res = spaceCollection.GetObjectSquares(new Point(200, 250), new Size(500, 700)).ToList();
            Assert.Equal(res.Count, 12);
            Assert.Equal(res[0], 6);
            Assert.Equal(res[1], 7);
            Assert.Equal(res[2], 8);
            Assert.Equal(res[3], 11);
            Assert.Equal(res[4], 12);
            Assert.Equal(res[5], 13);
            Assert.Equal(res[6], 16);
            Assert.Equal(res[7], 17);
            Assert.Equal(res[8], 18);
            Assert.Equal(res[9], 21);
            Assert.Equal(res[10], 22);
            Assert.Equal(res[11], 23);
        }

        [Fact]
        public void getModelSquaresSound()
        {
            var res = spaceCollectionBig.GetModelSquaresSound(soundLeft);
            var resAssert = new HashSet<int>(){11, 12, 13, 20, 21, 22, 23, 24, 25, 30, 31, 32, 33, 34, 35, 40, 41, 42, 43, 44, 45,
                50, 51, 52, 53, 54, 55, 60, 61, 62, 63, 64, 65, 70, 71, 72, 73, 74, 75, 81, 82, 83 };

            Assert.Equal(res.Count, resAssert.Count);
            foreach(var x in resAssert) {
                Assert.Equal(res.IndexOf(x) != -1, true);
            }
        }

        [Fact]
        public void AddModel_ok()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(gamer);

            Assert.Equal(spaceCollection.modelCoords[5].Count, 1);
            Assert.Equal(spaceCollection.modelCoords[5][0] is Gamer, true);

            Assert.Equal(spaceCollection.modelSquares[gamer.objectId].Count, 1);
            Assert.Equal(spaceCollection.modelSquares[gamer.objectId].ToList()[0], 5);

            Assert.Equal(spaceCollection.models[gamer.objectId] is Gamer, true);

            Assert.Equal(spaceCollection.modelChangedCoords[5].Count, 1);
            Assert.Equal(spaceCollection.modelChangedCoords[5][0] is Gamer, true);
        }
        [Fact]
        public void AddModel_ok_big()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building);
            var squares = new List<int>() { 6, 7, 8, 11, 12, 13, 16, 17, 18, 21, 22, 23 };
            foreach(var square in squares) {
            Assert.Equal(spaceCollection.modelCoords.ContainsKey(square), true);
            Assert.Equal(spaceCollection.modelChangedCoords.ContainsKey(square), true);
        }
            Assert.Equal(spaceCollection.modelCoords[squares[0]][0] is Building, true);

            Assert.Equal(spaceCollection.modelSquares[building.objectId].Count, squares.Count);
            Assert.Equal(spaceCollection.models[building.objectId] is Building, true);

            Assert.Equal(spaceCollection.modelChangedCoords[squares[0]][0] is Building, true);
        }

        [Fact]
        public void addModelInChanged_ok_big()
        {
            var squares = new HashSet<int>() { 6, 7, 8, 11, 12, 13, 16, 17, 18, 21, 22, 23 };
            spaceCollection.modelChangedCoords.Clear();
            spaceCollection.modelSquares.Clear();
            spaceCollection.modelSquares.Add(building.objectId, squares);
            spaceCollection.AddModelInChanged(building);

            foreach(var square in squares) {
            Assert.Equal(spaceCollection.modelChangedCoords.ContainsKey(square), true);
        }
            Assert.Equal(spaceCollection.modelChangedCoords[6][0] is Building, true);
        }

        [Fact]
        public void ClearChanges_ok()
        {
            spaceCollection.ClearChanges();
            Assert.Equal(spaceCollection.modelChangedCoords.Count, 0);
        }


        [Fact]
        public void updateModel_Equal()
        {
            spaceCollection.modelCoords.Clear();
            spaceCollection.modelSquares.Clear();
            spaceCollection.modelChangedCoords.Clear();
            spaceCollection.AddModel(building3);
            Assert.Equal(spaceCollection.UpdateModel(building3), false);
        }
        [Fact]
        public void updateModel_ok()
        {
            spaceCollection.modelCoords.Clear();
            spaceCollection.modelSquares.Clear();
            building3.rectangle.location = new Point(210, 250);
            spaceCollection.AddModel(building3);
            spaceCollection.modelChangedCoords.Clear();
            building3.rectangle.location = new Point(390, 190);
            spaceCollection.UpdateModel(building3);
            var squaresAssert = new HashSet<int>() { 1, 2, 7, 6 }; //����: [ 6, 7, 12, 11 ]
            var squaresChangesAssert = new HashSet<int>() { 1, 2, 12, 11 };

            Assert.Equal(spaceCollection.modelCoords.Count, squaresAssert.Count);
            foreach(var square in squaresAssert) {
                Assert.Equal(spaceCollection.modelCoords.ContainsKey(square), true);
                Assert.Equal(spaceCollection.modelChangedCoords.ContainsKey(square), true);
        }
            Assert.Equal(spaceCollection.modelCoords[1][0] is Building, true);
            Assert.Equal(spaceCollection.modelSquares[building3.objectId].Count, squaresAssert.Count);
            Assert.Equal(spaceCollection.modelChangedCoords.Count, squaresChangesAssert.Count);
            Assert.Equal(spaceCollection.modelChangedCoords[1][0] is Building, true);
        }

        [Fact]
        public void deleteModel_ok()
        {
            spaceCollection.modelCoords.Clear();
            spaceCollection.models.Clear();
            spaceCollection.modelSquares.Clear();
            building3.rectangle.location = new Point(210, 250);
            spaceCollection.AddModel(building3);
            spaceCollection.modelChangedCoords.Clear();
            spaceCollection.DeleteModel(building3);
            var squaresAssert = new HashSet<int>() { 6, 7, 12, 11  };

            Assert.Equal(spaceCollection.modelCoords.Count, 0);
            Assert.Equal(spaceCollection.modelSquares.ContainsKey(building3.objectId), false);
            Assert.Equal(spaceCollection.models.ContainsKey(building3.objectId), false);
            Assert.Equal(spaceCollection.modelChangedCoords.Count, squaresAssert.Count);
            foreach(var square in squaresAssert) {
                Assert.Equal(spaceCollection.modelChangedCoords.ContainsKey(square), true);
        }
            Assert.Equal(spaceCollection.modelChangedCoords[6][0] is Building, true);
        }

        [Fact]
        public void getModel_object_id()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3);
            Assert.Equal(spaceCollection.GetModel(building3.objectId) is Building, true);
        }
        [Fact]
        public void getModel_null()
        {
            spaceCollection.ClearAll();
            Assert.Equal(spaceCollection.GetModel(building.objectId), null);
        }
        [Fact]
        public void getModel_null_after_remove()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3);
            spaceCollection.DeleteModel(building3);
            Assert.Equal(spaceCollection.GetModel(building3.objectId), null);
        }
    
        [Fact]
        public void GetModels_0()
        {
            spaceCollection.ClearAll();
            Assert.Equal(spaceCollection.GetModels().Count, 0);
        }
        [Fact]
        public void GetModels_all()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3);
            spaceCollection.AddModel(building2);
            spaceCollection.AddModel(building);
            spaceCollection.AddModel(building4);
            spaceCollection.AddModel(gamer);
            spaceCollection.AddModel(soundCenter);
            spaceCollection.AddModel(soundLeft);
            Assert.Equal(spaceCollection.GetModels().Count, 7);
        }


        [Fact]
        public void find_Gamer()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3);
            spaceCollection.AddModel(building2);
            spaceCollection.AddModel(building);
            spaceCollection.AddModel(building4);
            spaceCollection.AddModel(gamer);
            spaceCollection.AddModel(soundCenter);
            spaceCollection.AddModel(soundLeft);
            var res = spaceCollection.Find(gamer.ClassName());
            Assert.Equal(res.Count, 1);
            Assert.Equal(res[0] is Gamer, true);
            Assert.Equal(res[0].objectId, gamer.objectId);
        }

        [Fact]
        public void find_Buildings()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3);
            spaceCollection.AddModel(building2);
            spaceCollection.AddModel(building);
            spaceCollection.AddModel(building4);
            spaceCollection.AddModel(gamer);
            spaceCollection.AddModel(soundCenter);
            spaceCollection.AddModel(soundLeft);
            var res = spaceCollection.Find(building.ClassName());
            Assert.Equal(res.Count, 4);
            Assert.Equal(res[0] is Building, true);
        }
        


        [Fact]
        public void getModelsInArea_area_1()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3);
            spaceCollection.AddModel(building2);
            spaceCollection.AddModel(building);
            spaceCollection.AddModel(building4);
            spaceCollection.AddModel(gamer);
            spaceCollection.AddModel(soundCenter);
            spaceCollection.AddModel(soundLeft);
            var res = spaceCollection.GetModelsInArea(new Rectangle(new Point(700, 900), new Size(200, 400)), true);
            Assert.Equal(res.Count, 2);
            Assert.Equal(res[0].objectId == building.objectId || res[0].objectId == soundCenter.objectId, true);
            Assert.Equal(res[1].objectId == building.objectId || res[1].objectId == soundCenter.objectId, true);
        }

        [Fact]
        public void getModelsInArea_area_2()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building6);
            var res = spaceCollection.GetModelsInArea(new Rectangle(new Point(150, 450), new Size(230, 205)), false);
            Assert.Equal(res.Count, 1);
        }

        [Fact]
        public void getModelsInArea_area_3()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(gamer);
            var res = spaceCollection.GetModelsInArea(new Rectangle(new Point(100, 300), new Size(30, 30)), true);
            Assert.Equal(res.Count, 1);
        }

        [Fact]
        public void getModelsInArea_exactly_1()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(gamer);
            var res = spaceCollection.GetModelsInArea(new Rectangle(new Point(100, 300), new Size(30, 30)), true, true);
            Assert.Equal(res.Count, 0);
        }

        [Fact]
        public void getModelsInArea_exactly_2()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building6);
            var res = spaceCollection.GetModelsInArea(new Rectangle(new Point(150, 450), new Size(230, 205)), false, true);
            Assert.Equal(res.Count, 1);
        }

        [Fact]
        public void getModelsInArea_exactly_3()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3);
            spaceCollection.AddModel(building2);
            spaceCollection.AddModel(building);
            spaceCollection.AddModel(building4);
            spaceCollection.AddModel(gamer);
            var res = spaceCollection.GetModelsInArea(new Rectangle(new Point(200, 300), new Size(10, 20)), true, true);
            Assert.Equal(res.Count, 2);
            Assert.Equal(res[0].objectId == building.objectId || res[0].objectId == building2.objectId, true);
            Assert.Equal(res[1].objectId == building.objectId || res[1].objectId == building2.objectId, true);
        }

        [Fact]
        public void getModelsInArea_onlyChanged()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3);
            spaceCollection.AddModel(building2);
            spaceCollection.AddModel(building);
            spaceCollection.AddModel(building4);
            spaceCollection.AddModel(gamer);
            spaceCollection.AddModel(soundCenter);
            spaceCollection.AddModel(soundLeft);
            spaceCollection.modelChangedCoords.Clear();
            spaceCollection.DeleteModel(building2);
            spaceCollection.DeleteModel(building3);
            var res = spaceCollection.GetModelsInArea(new Rectangle(new Point(300, 300), new Size(200, 200)), true, false, true);
            Assert.Equal(res.Count, 2);
            Assert.Equal(res[0].objectId == building3.objectId || res[0].objectId == building2.objectId, true);
            Assert.Equal(res[1].objectId == building3.objectId || res[1].objectId == building2.objectId, true);
        }


        [Fact]
        public void GetMoveArea_ok()
        {
            var res = SpaceCollection.GetMoveArea(gamer, new Point(gamer.rectangle.location.x + 500, gamer.rectangle.location.y + 300));
            Assert.Equal(res.location.x, gamer.rectangle.location.x);
            Assert.Equal(res.location.y, gamer.rectangle.location.y);
            Assert.Equal(res.size.width, gamer.rectangle.size.width + 500);
            Assert.Equal(res.size.height, gamer.rectangle.size.height + 300);
        }

        [Fact]
        public void GetMoveArea_ok_2_back()
        {
            var res = SpaceCollection.GetMoveArea(gamer, new Point(gamer.rectangle.location.x - 500, gamer.rectangle.location.y - 300));
            Assert.Equal(res.location.x, gamer.rectangle.location.x - 500);
            Assert.Equal(res.location.y, gamer.rectangle.location.y - 300);
            Assert.Equal(res.size.width, gamer.rectangle.size.width + 500);
            Assert.Equal(res.size.height, gamer.rectangle.size.height + 300);
        }

    

        [Fact]
        public void ClearAll_ok()
        {
            spaceCollection.ClearAll();
            Assert.Equal(spaceCollection.modelCoords.Count, 0);
            Assert.Equal(spaceCollection.modelSquares.Count, 0);
            Assert.Equal(spaceCollection.models.Count, 0);
            Assert.Equal(spaceCollection.modelChangedCoords.Count, 0);
        }

    

        [Fact]
        public void getModelContacts_one_location()
        {
            var res = spaceCollection.GetModelContacts(gamer, gamer.rectangle.location);
            Assert.Equal(res.contacts.Count, 0);
            Assert.Equal(res.lastlocationModel.x, gamer.rectangle.location.x);
            Assert.Equal(res.lastlocationModel.y, gamer.rectangle.location.y);
        }

        [Fact]
        public void GetModelContacts_object_right_direct_in_empty_space()
        {
            spaceCollection.ClearAll();
            var newLocation = new Point(
                gamer.rectangle.location.x + 500,
                gamer.rectangle.location.y + 300);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 0);
            Assert.Equal(res.lastlocationModel.x, newLocation.x);
            Assert.Equal(res.lastlocationModel.y, newLocation.y);
        }

        [Fact]
        public void GetModelContacts_object_right_direct_in_empty_space_2()
        {
            spaceCollection.ClearAll();
            var newLocation = new Point(
                gamer.rectangle.location.x + 1,
                gamer.rectangle.location.y + 1);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 0);
            Assert.Equal(res.lastlocationModel.x, newLocation.x);
            Assert.Equal(res.lastlocationModel.y, newLocation.y);
        }

        [Fact]
        public void GetModelContacts_object_right_direct_0_degrees_with_1_object()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3); //Point(210, 250), Size(200, 200)
            gamer.rectangle.location.x = 100;
            gamer.rectangle.location.y = 300;
            var newLocation = new Point(
                gamer.rectangle.location.x + 500,
                gamer.rectangle.location.y);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 1);
            Assert.Equal(res.lastlocationModel.x, 210 - gamer.rectangle.size.width);
            Assert.Equal(res.lastlocationModel.y, gamer.rectangle.location.y);
        }

        [Fact]
        public void GetModelContacts_object_left_direct_180_degrees_with_1_object()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3); //Point(210, 250), Size(200, 200)
            gamer.rectangle.location.x = 500;
            gamer.rectangle.location.y = 300;
            var newLocation = new Point(100, 300);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 1);
            Assert.Equal(res.lastlocationModel.x, 410);
            Assert.Equal(res.lastlocationModel.y, 300);
        }

        [Fact]
        public void GetModelContacts_object_top_direct_270_degrees_with_1_object()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3); //Point(210, 250), Size(200, 200)
            gamer.rectangle.location.x = 300;
            gamer.rectangle.location.y = 700;
            var newLocation = new Point(300, 0);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 1);
            Assert.Equal(res.lastlocationModel.x, 300);
            Assert.Equal(res.lastlocationModel.y, 450);
        }

        [Fact]
        public void GetModelContacts_object_bottom_direct_90_degrees_with_1_object()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3); //Point(210, 250), Size(200, 200)
            gamer.rectangle.location.x = 300;
            gamer.rectangle.location.y = 0;
            var newLocation = new Point(300, 700);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 1);
            Assert.Equal(res.lastlocationModel.x, 300);
            Assert.Equal(res.lastlocationModel.y, 250 - gamer.rectangle.size.height);
        }

        [Fact]
        public void GetModelContacts_object_right_direct_0_degrees_with_3_object()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3); //Point(210, 250), Size(200, 200)
            spaceCollection.AddModel(building5); //Point(220, 250), Size(100, 100)
            spaceCollection.AddModel(building6); //Point(190, 350), Size(50, 350)
            gamer.rectangle.size.height = 400;
            gamer.rectangle.location.x = 100;
            gamer.rectangle.location.y = 200;
            var newLocation = new Point(700, gamer.rectangle.location.y);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 1);
            Assert.Equal(res.contacts[0].model.objectId, building6.objectId);
            Assert.Equal(res.lastlocationModel.x, 190 - gamer.rectangle.size.width);
            Assert.Equal(res.lastlocationModel.y, gamer.rectangle.location.y);
        }

        [Fact]
        public void GetModelContacts_object_left_direct_180_degrees_with_3_object()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3); //Point(210, 250), Size(200, 200)
            spaceCollection.AddModel(building5); //Point(220, 250), Size(100, 100)
            spaceCollection.AddModel(building6); //Point(190, 350), Size(50, 350)
            gamer.rectangle.size.height = 400;
            gamer.rectangle.location.x = 700;
            gamer.rectangle.location.y = 200;
            var newLocation = new Point(0, gamer.rectangle.location.y);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 1);
            Assert.Equal(res.contacts[0].model.objectId, building3.objectId);
            Assert.Equal(res.lastlocationModel.x, 210 + building3.rectangle.size.width);
            Assert.Equal(res.lastlocationModel.y, gamer.rectangle.location.y);
        }

        [Fact]
        public void GetModelContacts_object_bottom_direct_90_degrees_with_3_object_2_contact()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3); //Point(210, 250), Size(200, 200)
            spaceCollection.AddModel(building5); //Point(220, 250), Size(100, 100)
            spaceCollection.AddModel(building6); //Point(190, 350), Size(50, 350)
            gamer.rectangle.size.width = 100;
            gamer.rectangle.location.x = 250;
            gamer.rectangle.location.y = 0;
            var newLocation = new Point(gamer.rectangle.location.x, 800);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 2);
            Assert.Equal(res.contacts[0].model.objectId, building3.objectId);
            Assert.Equal(res.contacts[1].model.objectId, building5.objectId);
            Assert.Equal(res.lastlocationModel.x, gamer.rectangle.location.x);
            Assert.Equal(res.lastlocationModel.y, 250 - gamer.rectangle.size.height);
        }

        [Fact]
        public void GetModelContacts_object_top_direct_270_degrees_with_3_object()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3); //Point(210, 250), Size(200, 200)
            spaceCollection.AddModel(building5); //Point(220, 250), Size(100, 100)
            spaceCollection.AddModel(building6); //Point(190, 350), Size(50, 350)
            gamer.rectangle.size.width = 300;
            gamer.rectangle.location.x = 300;
            gamer.rectangle.location.y = 900;
            var newLocation = new Point(gamer.rectangle.location.x, 100);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 1);
            Assert.Equal(res.contacts[0].model.objectId, building3.objectId);
            Assert.Equal(res.lastlocationModel.x, gamer.rectangle.location.x);
            Assert.Equal(res.lastlocationModel.y, 450);
        }


        [Fact]
        public void GetModelContacts_object_diagonal_right_top_with_3_object()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3); //Point(210, 250), Size(200, 200)
            spaceCollection.AddModel(building5); //Point(220, 250), Size(100, 100)
            spaceCollection.AddModel(building6); //Point(190, 350), Size(50, 350)
            gamer.rectangle.size.width = 30;
            gamer.rectangle.size.height = 30;
            gamer.rectangle.location.x = 50;
            gamer.rectangle.location.y = 550;
            var newLocation = new Point(250, 275);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 1);
            Assert.Equal(res.contacts[0].model.objectId, building6.objectId);
            var x = building6.rectangle.location.x - gamer.rectangle.size.width;
            Assert.Equal(res.lastlocationModel.x < x && res.lastlocationModel.x > x - 1, true);
            Assert.Equal(res.lastlocationModel.y > 350 && res.lastlocationModel.y < 400, true);
        }

        [Fact]
        public void GetModelContacts_object_diagonal_right_bottom_with_3_object()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3); //Point(210, 250), Size(200, 200)
            spaceCollection.AddModel(building5); //Point(220, 250), Size(100, 100)
            spaceCollection.AddModel(building6); //Point(190, 350), Size(50, 350)
            gamer.rectangle.size.width = 30;
            gamer.rectangle.size.height = 30;
            gamer.rectangle.location.x = 25;
            gamer.rectangle.location.y = 100;
            var newLocation = new Point(300, 375);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 1);
            Assert.Equal(res.contacts[0].model.objectId, building3.objectId);
            var x = building3.rectangle.location.x - gamer.rectangle.size.width;
            Assert.Equal(res.lastlocationModel.x < x && res.lastlocationModel.x > x - 1, true);
            Assert.Equal(res.lastlocationModel.y > 250 && res.lastlocationModel.y < 300, true);
        }

        [Fact]
        public void GetModelContacts_object_diagonal_left_bottom_with_3_object()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3); //Point(210, 250), Size(200, 200)
            spaceCollection.AddModel(building5); //Point(220, 250), Size(100, 100)
            spaceCollection.AddModel(building6); //Point(190, 350), Size(50, 350)
            gamer.rectangle.size.width = 30;
            gamer.rectangle.size.height = 30;
            gamer.rectangle.location.x = 300;
            gamer.rectangle.location.y = 150;
            var newLocation = new Point(250, 275);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 2);
            Assert.Equal(res.contacts[0].model.objectId == building3.objectId || res.contacts[0].model.objectId == building5.objectId, true);
            Assert.Equal(res.contacts[1].model.objectId == building3.objectId || res.contacts[1].model.objectId == building5.objectId, true);
            var y = building3.rectangle.location.y - gamer.rectangle.size.height;
            Assert.Equal(res.lastlocationModel.x > 200 && res.lastlocationModel.x < 300, true);
            Assert.Equal(res.lastlocationModel.y < y && res.lastlocationModel.y > y - 1, true);
        }

        [Fact]
        public void GetModelContacts_object_diagonal_left_top_with_3_object()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3); //Point(210, 250), Size(200, 200)
            spaceCollection.AddModel(building5); //Point(220, 250), Size(100, 100)
            spaceCollection.AddModel(building6); //Point(190, 350), Size(50, 350)
            gamer.rectangle.size.width = 30;
            gamer.rectangle.size.height = 30;
            gamer.rectangle.location.x = 350;
            gamer.rectangle.location.y = 625;
            var newLocation = new Point(150, 450);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 1);
            Assert.Equal(res.contacts[0].model.objectId, building6.objectId);
            var x = building6.rectangle.location.x + building6.rectangle.size.width;
            Assert.Equal(res.lastlocationModel.x > x && res.lastlocationModel.x < x + 1, true);
            Assert.Equal(res.lastlocationModel.y > 450 && res.lastlocationModel.y < 650, true);
        }

        [Fact]
        public void GetModelContacts_real_dx_dy()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3); //Point(210, 250), Size(200, 200)
            spaceCollection.AddModel(building5); //Point(220, 250), Size(100, 100)
            spaceCollection.AddModel(building6); //Point(190, 350), Size(50, 350)
            gamer.rectangle.size.width = 30;
            gamer.rectangle.size.height = 30;
            gamer.rectangle.location.x = 240.1f;
            gamer.rectangle.location.y = 450.1f;
            var newLocation = new Point(239.8f, 449.9f);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 2);
            Assert.Equal(res.contacts[0].model.objectId, building3.objectId);
            Assert.Equal(res.contacts[1].model.objectId, building6.objectId);
            var x = building6.rectangle.location.x + building6.rectangle.size.width;
            Assert.Equal(res.lastlocationModel.x > x && res.lastlocationModel.x < x + 1, true);
            Assert.Equal(res.lastlocationModel.y > 450 && res.lastlocationModel.y < 451, true);
        }

        [Fact]
        public void GetModelContacts_object_diagonal_left_top_with_3_object_not_contact()
        {
            spaceCollection.ClearAll();
            spaceCollection.AddModel(building3); //Point(210, 250), Size(200, 200)
            spaceCollection.AddModel(building5); //Point(220, 250), Size(100, 100)
            spaceCollection.AddModel(building6); //Point(190, 350), Size(50, 350)
            gamer.rectangle.size.width = 30;
            gamer.rectangle.size.height = 30;
            gamer.rectangle.location.x = 450;
            gamer.rectangle.location.y = 625;
            var newLocation = new Point(250, 460);

            var res = spaceCollection.GetModelContacts(gamer, newLocation);
            Assert.Equal(res.contacts.Count, 0);
            Assert.Equal(res.lastlocationModel.x, newLocation.x);
            Assert.Equal(res.lastlocationModel.y, newLocation.y);
        }
    }

}
