﻿using System;
using System.Text.RegularExpressions;

namespace MeduzaTest
{
    /// <summary>
    /// Вспомогательный класс
    /// </summary>
    public class Helper
    {
        Random rnd = new Random();

        /// <summary>
        /// Расстояние между точками
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <returns></returns>
        static public double Distance(Point point1, Point point2)
        {
            return Math.Sqrt(Math.Pow(point1.x - point2.x, 2) + Math.Pow(point1.y - point2.y, 2));
        }

        /// <summary>
        /// Возвращает угол поворота между относительной прямой паралельной оси X и проведённой через точку А, и точкой Б
        /// Угол считается относительно положительной оси Х по часовой стрелке
        /// </summary>
        /// <param name="pointCenter">Центральная точка</param>
        /// <param name="pointB">Точка Б</param>
        /// <returns>Угол между точкой Б и параллельной к очи X прямой, проходящей через точку А</returns>
        static public float GetRotateAngle(Point pointCenter, Point pointB)
        {
            float x = pointCenter.x - pointB.x;
            float y = pointCenter.y - pointB.y;

            if (x == 0 && y == 0) {
                return 0;
            }

            float angle = (float)((Math.Atan(y / x) + (x >= 0 ? Math.PI : 0)) * 180 / Math.PI);

            return angle >= 0 ? angle : 360 + angle;
        }

        /// <summary>
        /// Возвращает сгенерированный 'уникальный' идентификатор
        /// </summary>
        /// <returns></returns>
        static public string GenerateUid()
        {
            MatchEvaluator myEvaluator = new MatchEvaluator((c) => {
                var r = (int)(rnd.NextDouble() * 16) | 0;
                var v = c.Value == "x" ? r : (r & 0x3 | 0x8);

                return Convert.ToString(v, 16);
            });

            return Regex.Replace("xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx", "[xy]", myEvaluator);
        }
    }
}
