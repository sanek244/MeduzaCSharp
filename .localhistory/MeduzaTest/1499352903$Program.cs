﻿using System;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;

namespace MeduzaTest
{
    class Program
    {
        static void Main(string[] args)
        {

            Point a = new Point(25);
            Console.WriteLine((float)a.Get("x"));
            //Console.WriteLine(a.Get("x"));
            Console.ReadKey();
        }

        static void log(params object[] objs)
        {
            Console.WriteLine();
            foreach(var obj in objs) {
                Console.Write(obj + " ");
            }
        }
    }
}