﻿using System;

namespace MeduzaTest
{
    class TestSpaceCollection
    {
        static public void native()
        {
            Console.WriteLine("100 objects x 1000 getModelContacts + updateModel");

            var spaceCollection = new SpaceCollection(new Size(10000, 20000));

            var timeBegin2 = DateTime.Now;
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    spaceCollection.AddModel(new SimpleGameObject(){
                        rectangle = new Rectangle(new Point(20 * i, 30 * j), new Size(5, 5)),
                    });
                }
            }

            var timeBegin = DateTime.Now;
            for(int i = 0; i< 100; i++) {
                spaceCollection.ClearChanges();
                var models = spaceCollection.GetModels();

                foreach(var model in models) {
                    var newLocation = new Point(
                        model.rectangle.location.x + (float)Helper.rnd.NextDouble(),
                        model.rectangle.location.x + (float)Helper.rnd.NextDouble());

                    spaceCollection.GetModelContacts(model, newLocation);
                    spaceCollection.UpdateModel(model);
                }
            }

            Console.WriteLine("\nload data: " + (timeBegin - timeBegin2));
            Console.WriteLine("time complete: " + (DateTime.Now - timeBegin));
        }
    }
}
