﻿using System;
using System.Reflection;
using System.Linq;
using System.Collections.Generic;

namespace Meduza
{
    class Program
    {
        static void Main(string[] args)
        {
            TestSpaceCollection.native();

            Console.ReadKey();
        }

        static void log(params object[] objs)
        {
            Console.WriteLine();
            foreach(var obj in objs) {
                Console.Write(obj + " ");
            }
        }
    }
}