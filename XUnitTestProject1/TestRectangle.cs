using Xunit;
using Meduza;

namespace Tests
{
    public class TestRectangle
    {
        public Rectangle ractangle = new Rectangle() {
            location = new Point(234, 8546),
            size = new Size(355, 957)
        };

        [Fact]
        public void IsRightAngle1()
        {
            Rectangle a = new Rectangle() { rotateAngle = 0};
            Assert.Equal(a.IsRightAngle(), true);
        }

        [Fact]
        public void IsRightAngle2()
        {
            Rectangle a = new Rectangle() { rotateAngle = 0.23f };
            Assert.Equal(a.IsRightAngle(), false);
        }

        [Fact]
        public void IsRightAngle3()
        {
            Rectangle a = new Rectangle() { rotateAngle = 90 };
            Assert.Equal(a.IsRightAngle(), true);
        }

        [Fact]
        public void IsRightAngle4()
        {
            Rectangle a = new Rectangle() { rotateAngle = 1 };
            Assert.Equal(a.IsRightAngle(), false);
        }

        [Fact]
        public void IsRightAngle5()
        {
            Rectangle a = new Rectangle() { rotateAngle = 180 };
            Assert.Equal(a.IsRightAngle(), true);
        }

        [Fact]
        public void IsRightAngle6()
        {
            Rectangle a = new Rectangle() { rotateAngle = 53 };
            Assert.Equal(a.IsRightAngle(), false);
        }

        [Fact]
        public void IsRightAngle7()
        {
            Rectangle a = new Rectangle() { rotateAngle = 270 };
            Assert.Equal(a.IsRightAngle(), true);
        }

        [Fact]
        public void IsRightAngle8()
        {
            Rectangle a = new Rectangle() { rotateAngle = 123 };
            Assert.Equal(a.IsRightAngle(), false);
        }

        [Fact]
        public void IsRightAngle9()
        {
            Rectangle a = new Rectangle() { rotateAngle = 360 };
            Assert.Equal(a.IsRightAngle(), true);
        }

        [Fact]
        public void IsRightAngle10()
        {
            Rectangle a = new Rectangle() { rotateAngle = 280 };
            Assert.Equal(a.IsRightAngle(), false);
        }



        [Fact]
        public void isContainsPoint0()
        {
            Assert.Equal(ractangle.IsContainsPoint(new Point(300, 8600)), true);
        }

        [Fact]
        public void isContainsPoint1()
        {
            Assert.Equal(ractangle.IsContainsPoint(new Point(1000, 10000)), false);
        }

        [Fact]
        public void isContainsPoint2()
        {
            Assert.Equal(ractangle.IsContainsPoint(new Point(100, 100)), false);
        }


        [Fact]
        public void isContainsModel0()
        {
            var model = new SimpleGameObject() { rectangle = new Rectangle(new Point(300, 9000), new Size(10, 10))};
            Assert.Equal(ractangle.IsContainsModel(model), true);
        }

        [Fact]
        public void isContainsModel1()
        {
            var model = new SimpleGameObject() { rectangle = new Rectangle(new Point(200, 9000), new Size(10, 10)) };
            Assert.Equal(ractangle.IsContainsModel(model), false);
        }

        [Fact]
        public void isContainsModel2()
        {
            var model = new SimpleGameObject() { rectangle = new Rectangle(new Point(300, 8546 + 957 - 5), new Size(10, 10)) };
            Assert.Equal(ractangle.IsContainsModel(model), false);
        }

        [Fact]
        public void isContainsModel3()
        {
            var model = new SimpleGameObject() { rectangle = new Rectangle(new Point(300, 8546 + 957 - 10), new Size(10, 10)) };
            Assert.Equal(ractangle.IsContainsModel(model), false);
        }

        [Fact]
        public void isContainsModel4()
        {
            var model = new SimpleGameObject() { rectangle = new Rectangle(new Point(300, 8546 + 957 - 11), new Size(10, 10)) };
            Assert.Equal(ractangle.IsContainsModel(model), true);
        }

        [Fact]
        public void IsIntersectionOrContainsModel0()
        {
            var model = new SimpleGameObject() { rectangle = new Rectangle(new Point(300, 9000), new Size(10, 10)) };
            Assert.Equal(ractangle.IsIntersectionOrContainsModel(model), true);
        }

        [Fact]
        public void IsIntersectionOrContainsModel1()
        {
            var model = new SimpleGameObject() { rectangle = new Rectangle(new Point(300, 8546 + 957 - 5), new Size(10, 10)) };
            Assert.Equal(ractangle.IsIntersectionOrContainsModel(model), true);
        }

        [Fact]
        public void IsIntersectionOrContainsModel2()
        {
            var model = new SimpleGameObject() { rectangle = new Rectangle(new Point(300, 8546 + 957 - 10), new Size(10, 10)) };
            Assert.Equal(ractangle.IsIntersectionOrContainsModel(model), true);
        }

        [Fact]
        public void IsIntersectionOrContainsModel3()
        {
            var model = new SimpleGameObject() { rectangle = new Rectangle(new Point(300, 8546 + 957 + 11), new Size(10, 10)) };
            Assert.Equal(ractangle.IsIntersectionOrContainsModel(model), false);
        }

    }
}
