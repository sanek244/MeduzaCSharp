﻿class Client {
    constructor() {
        this.gamerObjectId = Client.getCookie("hashCod");
        this.sounds = [];
        this.viewClasters = [];
        this.timeDraw = new Date().getTime(); //время прошедшее между прорисовками
        this.scatterCursor = 0;
        this.cursor = { x: 0, y: 0 };
        this.objects = {};
        this.view = {}; //визуальная часть [zIndex] = [id objects] = object

        const canvasTag = document.getElementById('canvas');
        canvasTag.height = window.innerHeight;
        canvasTag.width = window.innerWidth;
        this.canvas = canvasTag.getContext('2d');

        const socket = io();
        //TODO: загрузка начальной картинки/фона

        //отправка на сервер хэш кода из кук
        socket.emit("hash", this.gamerObjectId);

        //отправка на сервер версии данных, если есть
        let versionData = 0;
        if (localStorage && localStorage.getItem('versionData')) {
            versionData = localStorage.getItem('versionData');
        }
        socket.emit("getMediaData", versionData);


        this.setEvents();
    }

    /**
     * Устанавливает все обытия взаимодействия клиента с сервером
     */
    setEvents() {
        const socket = io();

        //пилинг с сервера
        socket.on('ping', msg => {
            socket.emit("ping", true); //ответ серверу
        });

        //для новых клиентов их хэш-код(id) с сервера(безопасность п 0.1)
        socket.on('hash', msg => {
            document.cookie = "hash=" + msg;
            this.gamerObjectId = msg;
        });

        //загрузка всех данных в начале игры
        socket.on('getMediaData', msg => {
            if (msg) {
                this.sounds = msg.sounds;
                this.viewClasters = Client.createImages(msg.viewClasters);
                localStorage.setItem('mediaDataSound', this.sounds);
                localStorage.setItem('mediaDataImg', this.viewClasters);
            }
            this.loadMediaData();
        });

        //потеря соединения с игроком, msg - его nickname
        socket.on('disconnect', msg => {
            console.log("от нас отсоединился: " + msg);
        });

        //ошибки с сервера(безопасность)
        socket.on('errors', msg => {
            console.log(msg);
            alert(msg);
        });

        //картинка с сервера
        socket.on('view', msg => {
            this.applyObjects(msg.objects);
            this.startSounds(msg.sound);
            this.drawAll();
        });

        //для одновременных нажатий нескольких клавиш
        window.onkeydown = window.onkeyup = (e) => {
            e = e || event;
            //отправка на сервер тип нажатия и код клавиши
            socket.emit("keyControl", e.type + ":" + e.which);
        };

        //клик мыши
        document.onmouseup = document.onmousedown = (e) => {
            socket.emit("click", e.clientX + ":" + e.clientY + ":" + e.type + ":" + e.button); //отправка на сервер координат мыши
        };

        //перемещение мыши
        $(window).mousemove((e) => {
            this.cursor = {
                x: e.pageX,
                y: e.pageY
            };
            Client.cursorBuild(e.pageX, e.pageY, this.scatterCursor); //сборка курсора :D
            socket.emit("mousemove", e.clientX + ":" + e.clientY);
            Client.clearSelection();
        });

        //запрет перемещения картинки курсора
        $("img").mousedown((e) => {
            if (window.event.stopPropagation)
                window.event.stopPropagation();
            window.event.cancelBubble = true;
            e.cancelBubble = true;
            e.preventDefault();
        });

        //document.oncontextmenu=function(e){return false};

        $(window).resize(() => { //изменение размера окна
            const canvasTag = document.getElementById('canvas');
            canvasTag.height = window.innerHeight;
            canvasTag.width = window.innerWidth;
            this.canvas = canvasTag.getContext('2d');

            socket.emit("resize");
        });
    }

    /**
     * Загрузка медиафайлов с сервера
     */
    loadMediaData() {
        if (localStorage && !this.sounds.length) {
            this.sounds = localStorage.getItem('mediaDataSound');
            this.viewClasters = localStorage.getItem('mediaDataImg');
        }
        //TODO: картинки отобразить на canvas, звуки воспроизвести?
        //this.canvas.drawImage(this.viewClasters[img.nameImg][i], -10000, -10000); //рисуем, что бы браузер загрузил картинку
        //TODO: найти способ загрузить файлы
    }

    /**
     * Запуск звуковых файлов
     * @param sounds
     */
    startSounds(sounds) {
        sounds.map(sound => {
            // Создаём новый элемент Audio
            const audio = new Audio();
            audio.src = "sound//" +
                this.sounds[sound.id]['path'] + '/' +
                this.sounds[sound.id]['name'] + '.' +
                this.sounds[sound.id]['extension'];
            audio.autoplay = true;
            audio.volume = sound.volume;
        });
    }

    /**
     * Принятие новых изменений
     */
    applyObjects(objects) {
        if (objects) {
            objects.map(object => {
                if (object['isDelete'] === true) {
                    this.deleteViewObject(object['objectId']);
                    return; //exit from map
                }

                //пересборка курсора
                if (object['objectId'] === this.gamerObjectId) {
                    this.scatterCursor = object['recoil'];
                    Client.cursorBuild(this.cursor.x, this.cursor.y, this.scatterCursor);
                }

                const cluster = this.viewClasters[object['viewsClusterId']];
                const zIndex = cluster['zIndex'];

                //защита
                if (!(object['state'] in cluster.states)) {
                    object['state'] = 'base';
                }

                if (!this.view[zIndex]) {
                    this.view[zIndex] = {};
                }
                this.view[zIndex][object['objectId']] = object;
                this.objects[object['objectId']] = object;
            });
        }
    }


    /**
     * Прорисовка всех объектов
     * @param objects
     */
    drawAll() {
        this.timeDraw = new Date().getTime() - this.timeDraw; //время прошедшее между прорисовками
        this.canvas.clearRect(0, 0, window.innerWidth, window.innerHeight);
        this.drawNet(); //сетка

        //прорисовка всего
        _.forIn(this.view, (data, zIndex) => {
            _.forIn(data, (object, objectId) => {
                const cluster = this.viewClasters[object['viewsClusterId']];
                const img = cluster.states[object['state']];

                //Если объект за пределами границ экрана
                if (object['x'] + img['width'] < 0 || object['y'] + img['height'] < 0 ||
                    object['x'] > window.innerWidth || object['y'] > window.innerHeight) {
                    this.deleteViewObject(objectId);
                    return;
                }

                this.drawObject(data, img); //отрисовка объекта

                this.canvas.rotate(0);
            });
        });

        this.timeDraw = new Date().getTime();
    }

    /**
     * Прорисовка объекта
     * @param data
     * @param img
     */
    drawObject(data, img) {
        /*
         GObjects[i].timeLife = data[6];
         GObjects[i].timeLife -= this.timeDraw; //время жизни объекта
         this.canvas.globalAlpha = GObjects[i].timeLife / data[6];
         draw(i, data, this.timeDraw); //отрисовка объекта
         this.canvas.globalAlpha = 1;
         break;*/

        if (data['rotateAngle'] !== 0) {
            //Поворот объекта относительно верхнего левого угла
            this.canvas.translate(data['x'], data['y']);
            this.canvas.rotate(data['rotateAngle'] * Math.PI / 180);
        }

        if (img['type'] === 'Animation') {
            if (!GObjects[id].img.time) //первый раз
                GObjects[id].img.time = GObjects[id].img.timeChangeFrame;

            GObjects[id].img.time -= this.timeDraw; //время смены кадра
            if (GObjects[id].img.time < 0) {
                GObjects[id].img.currentFrame = GObjects[id].img.currentFrame < GObjects[id].img.nframes ? GObjects[id].img.currentFrame + 1 : 1;
                GObjects[id].img.time = GObjects[id].img.timeChangeFrame;
                if (!GObjects[id].img.lifeObject) //картинка не живущая с физическим объектом
                {
                    data[5]--; //количество кадров, которые надо показать
                    if (data[5] <= 0)
                        data[0] = "t"; //данный объект надо плавно скрыть
                }
            }
        }

        if (data['rotateAngle'] !== 0) {
            this.canvas.drawImage(img, -img['width'] / 2, -img['height'] / 2);
            this.canvas.rotate(-data['rotateAngle'] * Math.PI / 180);
            this.canvas.translate(-data['x'] || 0, -data['y'] || 0);
        }
        else {
            this.canvas.drawImage(img, data['x'] - img['width'] / 2, data['y'] - img['height'] / 2);
        }
    }

    /**
     * Сетка на карте (develop mode)
     */
    drawNet() {
        //this.canvas.fillStyle = "rgba(255,0,0, 0.5)";
        for (let i = 0; i < 5; i++) //горизонт
        {
            this.canvas.beginPath();
            this.canvas.moveTo(0, i * 200);
            this.canvas.lineTo(1400, i * 200);
            this.canvas.stroke();
        }

        for (let i = 0; i < 10; i++) //вертикаль
        {
            this.canvas.beginPath();
            this.canvas.moveTo(i * 200, 0);
            this.canvas.lineTo(i * 200, 700);
            this.canvas.stroke();
        }
    }

    /**
     * Удаление объекта с его представлением
     * @param id
     */
    deleteViewObject(id) {
        if (this.objects[id]) {
            const cluster = this.viewClasters[object['viewsClusterId']];
            delete view[cluster.zIndex][id];
            if (Object.keys(view[cluster.zIndex]).length === 0) {
                delete view[cluster.zIndex];
            }
            delete this.objects[id];
        }
    }

    /**
     * Создание изображения по данным
     * @param img
     * @param i
     * @returns {*}
     */
    static createImage(img, i = '') {
        const imgObject = new Image();
        imgObject.data = img;
        imgObject.data['name'] += i;
        imgObject.src = 'img\\' + img['path'] + '\\' + img['name'] + i + "." + img['fileExtension'];
        return imgObject;
    }

    /**
     * Создание объектов Img в кластерах с данными по изображениям
     * @param {Array} viewClusters
     */
    static createImages(viewClusters) {
        return viewClusters.map(viewCluster => {
            if (viewCluster['preview']) {
                viewCluster['preview'] = Client.createImage(viewCluster['preview']);
            }

            viewCluster['damages'] = viewCluster['damages'].map(v => Client.createImage(img));

            _.forIn(viewCluster['states'], (img, state) => {
                viewCluster['states'][state] = Client.createImage(img);
            });

            _.forIn(viewCluster['holdingWeapon'], (img, state) => {
                viewCluster['holdingWeapon'][state] = Client.createImage(img);
            });

            _.forIn(viewCluster['recoilWeapon'], (img, state) => {
                viewCluster['recoilWeapon'][state] = Client.createImage(img);
            });
        });
    }


    /**
     * Возвращает cookie с именем name, если есть, если нет, то пустоту
     * @param {string} name - имя cookie записи
     * @returns {string}
     */
    static getCookie(name) {
        const matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : "";
    }

    static clearSelection() {
        if (window.getSelection)
            window.getSelection().removeAllRanges();
        else  // старый IE
            document.selection.empty();
    }

    /**
     * Сборка игрового курсора
     * @param x
     * @param y
     * @param scatter
     */
    static cursorBuild(x, y, scatter) {
        $("#cursor_center").css({ top: y - 3, left: x - 3 });
        $("#cursor_part1").css({ top: y - 12 - scatter, left: x - 12 - scatter });
        $("#cursor_part2").css({ top: y - 12 - scatter, left: x + 1 + scatter });
        $("#cursor_part3").css({ top: y + 1 + scatter, left: x + 1 + scatter });
        $("#cursor_part4").css({ top: y + 1 + scatter, left: x - 12 - scatter });
    }

}


window.onload = function(){ //страница загружена, можно выполнять js код
	new Client();
};